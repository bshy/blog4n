doctype 5
html ->
	head ->
		meta charset: 'utf-8'
		title "#{@config.app.name or 'Untitled'} | #{@config.app.description}"
		meta(name: 'description', content: @config.app.description) if @config.app.description?
		link rel: 'stylesheet', href: '#{@config.app.domain}\public\stylesheets\bootstrap.css}'
		link rel: 'stylesheet', href: '#{@config.app.domain}\public\stylesheets\bootstrap-responsive.css}'
		link rel: 'stylesheet', href: '#{@config.app.domain}\public\stylesheets\docs.css}'

		script src: '#{@config.app.domain}\public\javascripts\jquery-1.8.0.js}'
		script src: '#{@config.app.domain}\public\javascripts\bootstrap.js}'
	body ->
		div '.navbar navbar-fixed-top' -> 
		footer ->
		# CoffeeScript comments. Not visible in the output document.
		comment 'HTML comments.'
		p 'Bye!'