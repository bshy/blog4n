doctype 5
html lang: 'zh', ->
	head ->
		meta charset: 'utf-8'
		meta keywords: ''
		meta description: ''

		ie 'IE', ->
			script src="http://html5shim.googlecode.com/svn/trunk/html5.js"

		title "博客管理中心 - #{@config.app.name}"
		link rel: 'stylesheet/less',type:'text/css', href: '/assets/bootstrap/swatchmaker.less'
		#link rel: 'stylesheet', href: '/assets/bootstrap/bootstrap.css'
		link rel: 'stylesheet', href: '/stylesheets/admin.css'

		link rel: 'stylesheet', href: '/assets/jquery.jgrowl/jquery.jgrowl.css'
		script src: '/assets/less/less-1.3.0.min.js'
		script src: '/assets/jquery-1.8.0.js'

		#script src: 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.js'
		script src: '/assets/jquery.jgrowl/jquery.jgrowl.js'
		script src: '/assets/bootstrap/bootstrap.js'

	body ->
		#header 
		comment #{config.app.name}
		section ->
			div '.navbar navbar-fixed-top', ->
				div '.navbar-inner', ->
					div '.container', ->
						a '.brand' ,href:'/', ->
							text "#{config.app.name}"
						div '.nav-collapse',id:'main-menu', ->
							ul '.nav',id:'main-menu-left', ->
                if @menus?
                  for menu in @menus
                      if menu.children?
                        li '.dropdown', ->
                          a '.dropdown-toggle', 'data-toggle':'dropdown', href:'#', ->
                            text menu.displayText
                            b '.caret', ->
                          ul '.dropdown-menu', ->
                            for __children in menu.children
                              li ->
                                a href:__children.url, ->
                                  text __children.displayText
                              #li '.divider', ->
                      else
                        li ->
                          a onclick:'', href:menu.url, ->
                            text menu.displayText
                  li '.dropdown', ->
                    a '.dropdown-toggle', 'data-toggle':'dropdown', href:'#', ->
                      text 'Download'
                      b '.caret', ->
                    ul '.dropdown-menu', ->
                      li ->
                        a target:'_blank', href:'bootstrap.min.css', ->
                          text 'bootstrap.min.css'
                      li ->
                        a target:'_blank', href:'bootstrap.css', ->
                          text 'bootstrap.css'
                      li '.divider', ->
                      li ->
                        a target:'_blank', href:'variables.less', ->
                          text 'variables.less'
                      li ->
                        a target:'_blank', href:'bootswatch.less', ->
                          text 'bootswatch.less'
							ul '.nav pull-right', id:'main-menu-right',->
								li ->
									a rel:'tooltip', target:'_blank', href:'/', onclick:'', 'data-original-title':'返回网站首页.', ->
										text '返回网站'
										i '.icon-share-alt icon-white', ->
								li ->
									a  href:'#', onclick:'',->
										text '注销'
										i '.icon-share-alt icon-white', ->
		div '.container', ->
			div '.container-fluid main-container', ->
				div 'row-fluid', ->
					div 'span12', ->
						main_container_render()
		coffeescript ->
			$('a[rel=tooltip]').tooltip 
				'placement': 'bottom'