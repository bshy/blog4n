#link rel:"stylesheet", href:"/assets/CodeMirror-2.33/lib/codemirror.css"
#script src:"/assets/CodeMirror-2.33/lib/codemirror.js"
#script src:"/assets/CodeMirror-2.33/lib/util/runmode.js"

link rel:'stylesheet', href:'/assets/google-code-prettify/src/prettify.css'
script src:'/assets/google-code-prettify/src/prettify.js'

link rel: 'stylesheet', href: '/assets/pagedown/markdown.css'
script src:'/assets/pagedown/Markdown.Converter.js'
script src:'/assets/showdown/showdown.js'
script src:'/assets/pagedown/Markdown.Editor.js'

form ".form-horizontal well",action:"/admin/post/", ->
  fieldset ->
	  legend ->
      text '#{if @post_item? then "保存博文" else "发表博文"}',
    div ".control-group", ->
      label ".control-label", for: "title", -> '标题'
      div class:"controls", ->
        input "#title.input-xlarge",name:"p.title",type:"text",style:"width:655px;" ,value:"#{ if @post_item? then @post_item.title else ''}" , ->
    div ".control-group", ->
      label ".control-label", for: "title", -> '短链接'
      div class:"controls", ->
        input "#title.input-xlarge",name:"p.short_url",type:"text" ,style:"width:655px;",value:"#{ if @post_item? then (if @post_item.short? then @post_item.short else '') else ''}", ->
    div ".control-group", ->
      label ".control-label", for:"textarea", -> '内容'
      div ".controls", ->
        div '.tabbable', ->
          ul '.nav nav-pills', ->
            li '.active', ->
              a href:'#markdown', 'data-toggle':'pill', -> 'markdown'
            li ->
              a href:'#preview', 'data-toggle':'pill', -> '预览'
          div '.tab-content', ->
            div '.active tab-pane', id:'markdown', ->
              div id:'wmd-button-bar', ->
              div '.input', ->
                textarea '.span8', id:'wmd-input', name:'t_content', rows:'20',value:"#{if @post_item? then @post_item.content else ''}", ->
            div '.tab-pane',id:'preview', ->
              div '.wmd-preview topic-wmd-preview', id:'wmd-preview', ->
    div ".control-group", ->
      label ".control-label", for: "title", ->
        strong -> '原创?'
      div class:"controls", ->
        div ".container-fluid", ->
          div ".row-fluid", ->
            div ".span1", ->
              input "#post-type-origin",name : "p.post_type",type:"radio" ,value:"origin",checked:"true", ->
            div ".span1.center",style:"", ->
              label for:"post-type-origin", -> "原创"
            div ".span1", ->
              if @post_item? && @post_item.post_type=='nonorigin'
                input "#post-type-nonorigin",name : "p.post_type",type:"radio" ,value:"nonorigin","checked":true, ->
              else
                input "#post-type-nonorigin",name : "p.post_type",type:"radio" ,value:"nonorigin", ->
            div ".span1", ->
              label for:"post-type-nonorigin", -> "转载"
    div ".control-group", ->
      label ".control-label", for: "title", ->
        strong -> '隐私设置?'
      div class:"controls", ->
        div ".container-fluid", ->
          div ".row-fluid", ->
            div ".span1", ->
              input "#post-privacy-all",name : "p.post_privacy",type:"radio" ,value:"all",checked:"true", ->
            div ".span3.center",style:"", ->
              label for:"post-privacy-all", -> "全部可见"
            div ".span1", ->
              if @post_item? && @post_item.post_privacy=='self'
                input "#post-privacy-self",name : "p.post_privacy",type:"radio" ,value:"self",checked:"true", ->
              else
                input "#post-privacy-self",name : "p.post_privacy",type:"radio" ,value:"self", ->
            div ".span3", ->
              label for:"post-privacy-self", -> "仅我可见"
    div ".control-group", ->
      label ".control-label", for: "title", ->
        strong -> "评论设置?"
      div class:"controls", ->
        div ".container-fluid", ->
          div ".row-fluid", ->
            div ".span1", ->
              input "#post-can-comment.input-xlarge",name : "p.deny_comment",type:"radio" ,value:"allow",checked:"true", ->
            div ".span3.center",style:"", -> "允许"
            div ".span1", ->
              if @post_item? && @post_item.post_type=='deny'
                input "#post-deny-comment.input-xlarge",name : "p.deny_comment",type:"radio" ,value:"deny","checked":true, ->
              else
                input "#post-deny-comment.input-xlarge",name : "p.deny_comment",type:"radio" ,value:"deny", ->
            div ".span3", ->
              label for:"post_type1", -> "禁止"
    div ".control-group", ->
      div class:"controls", ->
        a ".btn.btn-info",href:"#",style:"width:80px;margin-top:25px;", ->
          i ".icon-exclamation-sign.icon-white" ,->
          text '发布'
coffeescript ->
	$(document).ready(
		-> 
			(->
				converter = new Showdown.converter()
				editor = new Markdown.Editor(converter)
				editor.run()
				editor.hooks.chain('onPreviewRefresh', ->
					#$('#wmd-preview > pre > code').addClass('prettypring');
					prettyPrint()
					#doHighlight()
				)
			)()

			$('#submit_btn').click(
				->
					values=[];
					$('.tag_selectable').each(
						->
							if($(this).hasClass('tag_select')) 
								values.push($(this).attr('tag_id'))
					);
					$('#topic_tags').val(values)
					$('#create_topic_form').submit()
			)
	)