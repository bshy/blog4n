table ".table table-bordered table-striped",style:'margin-top:20px;', ->
	thead ->
		tr ->
			th ->
				text '标题'
			th ->
				text '作者'
			th ->
				text '发布时间'
			th ->
				text '内容'
			th ->
				text '点击量'
			th ->
				text '状态'
			th ->
				div ".btn-group", ->
					a ".btn",href:"#", -> "操作"
					a ".btn dropdown-toggle",'data-toggle':"dropdown",href:"#", ->
						span ".caret", ->
					ul ".dropdown-menu", ->
						li ->
							a href:"/admin/post/0/edit", -> '发布'
						li ".divider", ->
						li ->
							a href:"#", -> 'Separated link'
    tbody ->
		tr ->
			if @post_items?
				for item in @post_items
					td ->
						a href:"/admin/post/#{item._id}/edit", ->
							text item.title
					td ->
						a ".btn.btn-large",href:"#", -> 'Default'
					td ->
						a ".btn.btn-small",href:"#", -> 'Default'
					td ->
						a ".btn.disabled",href:"#", -> 'Default'
					td ->
						a ".btn",href:"#", ->
							i ".icon-cog", ->
							text 'Default'
					td ->
					td ->
						div ".btn-group", ->
							a ".btn",href:"#", -> '修改'
							a ".btn dropdown-toggle",'data-toggle':"dropdown",href:"#", ->
								span ".caret", ->
							ul ".dropdown-menu", ->
								li ->
									a href:"#", -> 'Action'
								li ->
									a href:"#", -> 'Another action'
								li ->
									a href:"#", -> 'Something else here'
								li ".divider", ->
								li ->
									a href:"#", -> 'Separated link'
