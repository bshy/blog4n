doctype 5
html lang:"zh", ->
	head ->
		meta charset: 'utf-8'
		meta keywords: ''
		meta description: ''

		ie 'IE', ->
			script src="http://html5shim.googlecode.com/svn/trunk/html5.js"
		title "#{ if @title? then @title ' - ' else ''}#{@config.app.name}"
		link rel: 'stylesheet/less',type:'text/css', href: '/assets/bootstrap/swatchmaker.less'

		#link rel:"stylesheet" href:'/stylesheets/bootstrap.css'
		#link rel:"stylesheet" href:'/stylesheets/bootstrap-responsive.css'

		link rel:"stylesheet", href:'/stylesheets/common.css'
		link rel:"stylesheet", href:'/assets/jquery.isotope/jquery.isotope.css'
		link rel:"stylesheet", href:'/assets/jquery.jgrowl/jquery.jgrowl.css'
		#link rel:"stylesheet" href:'/stylesheets/docs.css'>

		script src:"/assets/less/less-1.3.0.min.js"
		script src:"https://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.js"
		script src:"/assets/jquery.isotope/jquery.isotope.js"
		script src:"/assets/jquery.scrollpagination/jquery.scrollpagination.js"
		script src:"/assets/jquery.jgrowl/jquery.jgrowl.js"
		script src:"/assets/bootstrap/bootstrap.js"
	body ->
		section ->
			div '.navbar navbar-fixed-top', ->
				div '.navbar-inner', ->
					div '.container', ->
						a '.brand' ,href:'/#', ->
							text "#{config.app.name}"
						div '.nav-collapse',id:'main-menu', ->
						ul '.nav',id:'main-menu-left', ->
							if @menus?
								for menu in @menus
									if menu.type == 0
										li ->
											a onclick:'', href:menu.url, ->
												text menu.displayText
						div '.nav-collapse', id:'main-menu', ->
							ul '.nav', id:'main-menu-left', ->
			div ".container", ->
				div ".container-fluid", ->
					div ".row-fluid", ->
						div ".span12 top-banner", ->
					div ".row-fluid", ->
						#content
						div ".span12 content", style:"background-color:", ->
							div "#waterfall-container", ->
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , ->
										h2 -> "相关内容"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "2222"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "3333"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "4444"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "555"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "666"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "777"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "888"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "999"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "000"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "111"
								div ".waterfall-wrap", style:"background-color:;", ->
									div ".waterfall-item" , -> "111222"
		div "#waterfall-msg.top-right", style:"", ->
	coffeescript ->
		$(->
			__id = 1
			# 已加载页面
			$['__page'] = 
				'next_page' : 1
			scroll_height=0  #//初始化滚动条总长
			__top=0  #初始化滚动条的当前位置
			height = $(window).height()

			#$('.sidebar').css('height',height-43).css('right',rigthOffset).css('top',43);
			#$('.content').css('min-height',height-20);
			$container = $('#waterfall-container');
			$container.isotope(
				#layoutMode : 'masonry',
				itemSelector : '.waterfall-wrap',
				#filter: 'waterfall-filter',
				masonry : 
					columnWidth : 230
				
				#columnWidth : function(containerWidth){
				#	console.log(containerWidth / 3);
				#	return containerWidth / 2;
				#}
				#filter: '.item'
			, ($items) ->
				#var id = this.attr('id'),
				#len = $items.length;
				#console.log( 'Isotope has filtered for ' + len + ' items in #' + id );
				$items.each (index,item)->
					#console.log item
			)

			$container.scrollPagination(
				'contentPage':'/c/collect',
				'contentData':->
						'p' : $['__page']['next_page']
				,
				'delay' : 500,
				'scrollTarget':$(window),
				'heightOffset':40,
				'ajax_method':'get',
				'beforeLoad':->
					#$('.waterfall-next').fadeIn();
				,
				'afterLoad':(elementsLoaded)->
					#$('.waterfall-next').fadeOut();
				,
				'dataType':'json',
				'loaded': (data) ->
					# 已加载页面将 页号存入 全局变量__loaded_page中,再次滚动到触发滚动条函数时不做加载.
					$['__page'][$['__page']['next_page']] = true;
					# 计算出下次加载的页面的页号
					$['__page']['next_page'] = $['__page']['next_page'] + 1;
					#console.log($['__page']);
					$.each(data["posts"],(index , item)->
						#console.log($('#post-' + item.id));
						if $('#post-' + item.id)[0] # 已加载元素 不做添加
							return
						$newElement = $(['<div class="waterfall-wrap"><div class="waterfall-item">',"<h2>",item.title,"</h2>",item.content,'</div></div>'].join(''));
						#console.log($newElement);
						$container.append( $newElement ).isotope( 'appended', $newElement );
					)
			)
		)