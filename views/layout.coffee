doctype 5
html lang: 'zh', ->
  head ->
    meta charset: 'utf-8'
    meta charset: 'utf-8'
    comment 'Le HTML5 shim, for IE6-8 support of HTML elements'
    comment '[if lt IE 9]>\r\n\t\t\t<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>\r\n\t\t<![endif]'
    title "#{@title or 'Untitled'} | mynode"
    meta(name: 'description', content: @config.app.description) if @config.app.description?
    link rel: 'stylesheet', href: '/stylesheets/bootstrap.css'
    link rel: 'stylesheet', href: '/stylesheets/bootstrap-responsive.css'
    link rel: 'stylesheet', href: '/stylesheets/docs.css'

    script src: '/javascripts/jquery-1.8.0.js'
    script src: '/javascripts/bootstrap.js'
  body ->
    @body
    comment 'Navbar \r\n\t\t=================================================='
    div '.navbar.navbar-fixed-top', ->
      div '.navbar-inner', ->
        div '.container', ->
          div '#main-menu.nav-collapse', ->
            ul '#main-menu-left.nav', ->
              li '#preview-menu.dropdown',
              li ->
                a onclick: 'pageTracker._link(this.href); return false;', href: 'http://news.bootswatch.com', 'News'
              li ->
              li '.dropdown', ->
                ul '#swatch-menu.dropdown-menu', ->
                  li ->
                    a href: '/default', 'Default'
                  li '.divider'
                  li ->
                    a href: '/amelia', 'Amelia'
                  li ->
                    a href: '/cerulean', 'Cerulean'
                  li ->
                    a href: '/cyborg', 'Cyborg'
                  li ->
                    a href: '/journal', 'Journal'
                  li ->
                    a href: '/readable', 'Readable'
                  li ->
                    a href: '/simplex', 'Simplex'
                  li ->
                    a href: '/slate', 'Slate'
                  li ->
                    a href: '/spacelab', 'Spacelab'
                  li ->
                    a href: '/spruce', 'Spruce'
                  li ->
                    a href: '/superhero', 'Superhero'
                  li ->
                    a href: '/united', 'United'
            ul '#main-menu-right.nav.pull-right', ->
              li ->
                a rel: 'tooltip', target: '_blank', href: 'http://builtwithbootstrap.com/', title: 'Showcase of Bootstrap sites &amp; apps', onclick: '_gaq.push([\\\'_trackEvent\\\', \\\'click\\\', \\\'outbound\\\', \\\'builtwithbootstrap\\\']);', ->
                  text 'Built With Bootstrap'
                  i '.icon-share-alt.icon-white'
              li ->
                a rel: 'tooltip', target: '_blank', href: 'https://wrapbootstrap.com/?ref=bsw', title: 'Marketplace for premium Bootstrap templates', onclick: '_gaq.push([\\\'_trackEvent\\\', \\\'click\\\', \\\'outbound\\\', \\\'wrapbootstrap\\\']);', ->
                  text 'WrapBootstrap'
                  i '.icon-share-alt.icon-white'
    div '.container', ->
      comment 'Masthead\r\n=================================================='
      header '#overview.jumbotron.subhead', ->
        h1 'Simplex'
        p '.lead', 'A preview of changes in this swatch.'
        div '.subnav', ->
          ul '.nav.nav-pills', ->
            li ->
              a href: '#typography', 'Typography'
            li ->
              a href: '#navbar', 'Navbar'
            li ->
              a href: '#buttons', 'Buttons'
            li ->
              a href: '#forms', 'Forms'
            li ->
              a href: '#miscellaneous', 'Miscellaneous'
      comment 'Typography\r\n=================================================='
      section '#typography', ->
        div '.page-header', ->
          h1 'Typography'
        comment 'Headings & Paragraph Copy'
        div '.row', ->
          div '.span4', ->
            div '.well', ->
              h1 'h1. Heading 1'
              h2 'h2. Heading 2'
              h3 'h3. Heading 3'
              h4 'h4. Heading 4'
              h5 'h5. Heading 5'
              h6 'h6. Heading 6'
          div '.span4', ->
            h3 'Example body text'
            p 'Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nasceturridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit.'
            p 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui.'
          div '.span4', ->
            h3 'Example addresses'
            address ->
              strong 'Twitter, Inc.'
              br()
              text '795 Folsom Ave, Suite 600'
              br()
              text 'San Francisco, CA 94107'
              br()
              abbr title: 'Phone', 'P:'
              text '(123) 456-7890'
            address ->
              strong 'Full Name'
              br()
              a href: 'mailto:#', 'first.last@gmail.com'
      section '#navbar', ->
        div '.page-header', ->
          h1 'Navbar'
        div '.navbar', ->
          div '.navbar-inner', ->
          comment '/navbar-inner'
        comment '/navbar'
    script type: 'text/javascript', '!function(){\r\n\t\t\tvar $win = $(window)\r\n\t\t\t\t, $nav = $(\'.subnav\')\r\n\t\t\t\t, navHeight =$(\'.navbar\').first().height()\r\n\t\t\t\t, navTop = $(\'.subnav\').length && $(\'.subnav\').offset().top - navHeight\r\n\t\t\t\t, isFixed= 0\r\n\r\n\t\t\tprocessScroll()\r\n\r\n\t\t\t$win.on(\'scroll\', processScroll)\r\n\r\n\t\t\tfunction processScroll() {\r\n\t\t\t\tvar i, scrollTop = $win.scrollTop()\r\n\t\t\t\tif (scrollTop >= navTop && !isFixed) {\r\n\t\t\t\t\tisFixed = 1\r\n\t\t\t\t\t$nav.addClass(\'subnav-fixed\')\r\n\t\t\t\t} else if (scrollTop <= navTop && isFixed) {\r\n\t\t\t\t\tisFixed = 0\r\n\t\t\t\t\t$nav.removeClass(\'subnav-fixed\')\r\n\t\t\t\t}\r\n\t\t\t}\r\n\t\t\t$(\'a[rel=tooltip]\').tooltip({\r\n\t\t\t\t\'placement\': \'bottom\'\r\n\t\t\t});\r\n\t\t}(window.jQuery)'