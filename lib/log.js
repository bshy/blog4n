var colors = require('colors');
module.exports = require('tracer').colorConsole({
	format : [
		"{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
		{
			error : ["{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})\nCall Stack:{{stacklist}}",colors.red,colors.bold]// error format
		} 
	],
	dateformat : "HH:MM:ss.L",
	preprocess :  function(data){
		if(data.title==='error'){
			var callstack = '',len=data.stack.length;
			for(var i=0; i<len; i+=1){
				callstack += '\n'+data.stack[i];
			}
			data.stacklist = callstack;
		}
		data.file = data.file.replace(__dirname,'');
		data.title = data.title.toUpperCase();
	},
	filters : {
		log : colors.blank,
		trace : colors.magenta,
		debug : colors.blue,
		info : colors.green,
		warn : colors.yellow,
		error : [ colors.red, colors.bold ]
	}
});