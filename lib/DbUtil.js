/*module.exports = function(model){
	return require('../models/' + model);
}
*/
module.exports = {
	getScheme : function(SCHEME){
		var Mongolian = require("mongolian"),
			config = require("../web.config"),
			mongoserver = new Mongolian(config.db.url),
			db = mongoserver.db(config.db.dbname);

		mongoserver.log = require('../lib/log');
		return db.collection(SCHEME);
	},
	ObjectId : require('mongolian').ObjectId,
	getIdentity : function(){
		throw new Error();
	}
}