var config = require('../../web.config'),
	dbUtil = require('../../lib/DbUtil'),
	logger = require('../../lib/log'), 
	async = require('async');

var _getMenu = function(callback){
	dbUtil.getScheme('menus').find({type:0}).sort({order:1}).toArray(function(err,data){
		callback(err,data);
	});
}

/**
 * 查看content集合
 */
function admin_collect(req, res,next){
	
	var task1 = function(callback){
		dbUtil.getScheme('posts').find().toArray(function(err,data){
			//console.log(data);
			require('../../lib/consolidate').coffeekup(
				config.app.view.dir + '/admin/post/management.coffee',
				{
					post_items : data,
					config:config
				},
				function(err,template){
					callback(err,template);
				}
			)
		});
	}

	async.series([_getMenu,task1],function(err,results){
		if(err){
			throw err;
		}
		res.render('admin/index', {
			menus : results[0],
			title: 'Express' ,
			locals:{
				main_container_render : function(){
					//console.log(results[1]);
					return results[1];
				}
			}
		});
	});

};

/**
 * 修改content
 * var content = {
 		_id : ObjectId //id
		title : string //标题
		short : string //短链接
		content : string //内容

 }
 */
function edit(req,res,next){
	var task = [];
	task.push(_getMenu); // 0
    console.log(req.params.id);
    console.log(!!req.params.id);
	if(req.params.id!='0'){ // edit
		task.push(function(callback){ // index 1
            console.log(new dbUtil.ObjectId(req.params.id));
            dbUtil.getScheme('posts').findOne({_id:new dbUtil.ObjectId(req.params.id)},function(err,data){
                console.log(data,1111);
                require('../../lib/consolidate').coffeekup(
                    config.app.view.dir + '/admin/post/edit.coffee',
                    {
                        post_item : data,
                        config:config
                    },
                    function(err,template){
                        callback(err,template);
                    }
                )
            })
        });
	}else{//add
        task.push(function(callback){
            require('../../lib/consolidate').coffeekup(
                config.app.view.dir + '/admin/post/edit.coffee',
                {
                    post_item : null,
                    config:config
                },
                function(err,template){
                    callback(err,template);
                }
            )
        });
	}

	async.series(task,function(err,results){
		if(err){
			throw err;
		}
		res.render('admin/index', {
			menus : results[0],
			locals:{
				main_container_render : function(){
					//console.log(results[1]);
                    console.log(results[1]);
					return results[1];
				}
			}
		});
	});
}

function save(req,res){
    var task = [];
    task.push(_getMenu);
    task.push(function(prevResult,callback){
        try{
            var obj = body.p;
            dbUtil.getScheme('posts').update({
                    _id:new dbUtil.ObjectId(obj._id)
                },{
                    $set:obj
                },
                true  //当找不到时新增
            );
            callback(null,obj);
        }catch(e){
            callback(e,null);
        }
    });

    task.push(function(prevResult,callback){
        console.log(obj);
        dbUtil.getScheme('posts').findOne({_id:new dbUtil.ObjectId(prevResult._id)},function(err,data){
            console.log(data,1111);
            require('../../lib/consolidate').coffeekup(
                config.app.view.dir + '/admin/post/edit.coffee',
                {
                    post_item : data,
                    config:config
                },
                function(err,template){
                    callback(err,template);
                }
            )
        })
    });

    async.waterfall(task,function(err,results){
        if(err){
            throw err;
        }
        console.log(results);
        res.render('admin/index', {
            menus : results[0],
            locals:{
                main_container_render : function(){
                    //console.log(results[1]);
                    console.log(results[1]);
                    return results[1];
                }
            }
        });
    });
}

exports.mappings = [
	{action:'get',url:'/admin/post/collect',method:admin_collect},
	{action:'all',url:'/admin/post/:id/edit',method:edit}
];