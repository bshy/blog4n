/**
 * Created with JetBrains WebStorm.
 * User: Administrator
 * Date: 12-9-20
 * Time: 下午3:45
 * To change this template use File | Settings | File Templates.
 */


/**
 * 查看posts集合
 */
function collect(req, res,next){

    res.type('application/json');
    var p = req.query.p;
    var result = {
        "success" : false
    }
    if(!p){
        result.msg = "无效的分页参数";
        return res.json(500,result);
    }
    var task1 = function(callback){

        var start = config.site.post_page_limit * ( p - 1);
        dbUtil.getScheme('posts').find().sort({_id:1}).skip(start).limit(config.site.post_page_limit).toArray(function(err,data){
            callback(err,data);
        });
    }

    async.series([task1],function(err,results){
        if(err){
            throw err;
        }
        var result = {
            "success" : true,
            "posts" : results[0],
            "current_page" : p
        }
        result = JSON.stringify(result,function(key,value){
            //console.log(key,this[key] instanceof dbUtil.ObjectId);
            return this[key] instanceof dbUtil.ObjectId
                ? this[key].toString()
                : value;
        });
        res.send(result);
    });

};
exports.mappings = [
    {action:'get',url:'/post/collect',method:collect}
]