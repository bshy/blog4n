/**
 * config
 */

var path = require('path');
var fs = require('fs');
var url = require('url');

config = exports;


config.app={};
config.site= {};
config.db={};
config.mail= {};

config.app.debug= true;
config.app.domain= 'localhost';
config.app.port= 3000;
config.app.name= 'tide‘s blog';
config.app.description= 'My Node CRM';
config.app.version= '0.0.1';

var urlinfo = url.parse(config.app.domain);
config.app.hostname = urlinfo.hostname || config.app.domain;

config.app.view= {};
config.app.view.dir= path.join(__dirname, 'views');
config.app.view.engine= 'html';
//config.app.view.register_suffix= '.coffee';
config.app.view.register_func= require('./lib/consolidate').underscore;
config.app.actions_dir= path.join(__dirname, 'actions');

// site settings
config.site.post_page_limit = 10;
config.site.headers= [
'<meta name="author" content="tide" />',
'<meta name="description" content="Based on The blog4n">',
'<meta name="viewport" content="width=device-width,initial-scale=1">'
];
config.site.logo= ''; // default is `name`
config.site.static_host= ''; // 静态文件存储域名
config.site.enable_search_preview= false; // 开启google search preview

config.site.upload_dir= path.join(__dirname, 'public', 'data');
config.site.session_secret='blog4n';

// 话题列表显示的话题数量
config.site.list_topic_count= 20;

config.db.url='localhost:27017';
config.db.dbname='mynode';
config.db.server_options= {auto_reconnect: true, native_parser: true};
config.db.options = {logger:require('./lib/log')};

config.app.nav = {};
config.app.nav.admin = [
	{
		url : '1',text : '博文管理',target:'',permission : 'PostManage',
		children : [
			{url : '11',text : '发布博文',target : '',permission : 'PostReleased'},
			{url : '12',text : '草稿箱',target : '',permission : 'PostManage'},
			{url : '13',text : '回收站',target : '',permission : 'PostManage'}
		]
	},
	{
		url : '2',text : '分类管理',target:'',permission : 'CategoryManage',
		children : [
			{url : '21',text : '新增分类',target : '',permission : 'CategoryCreate'},
			{url : '22',text : '标签管理',target : '',permission : 'TagManage'}
		]
	},
	{url : '3',text : '资源管理',target:'',permission : 'ResourceManage'},
	{url : '4',text : '网站统计',target:'',permission : 'SiteStat'}
]

