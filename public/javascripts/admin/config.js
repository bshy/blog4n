/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: config.js
 * filesize: 8488 Bytes
 * last modified: Fri, 17 May 2013 15:07:57 +0200
 *
 */
var config = {
	tooltip: {
		gravity: "nw",
		fade: !1,
		opacity: 1,
		offset: 0
	}
};
$(document).ready(function() {
	$.fn.wl_Alert && ($.fn.wl_Alert.defaults = {
		speed: 500,
		sticky: !1,
		onBeforeClose: function() {},
		onClose: function() {}
	});
	$.fn.wl_Autocomplete && ($.fn.wl_Autocomplete.defaults = {});
	$.fn.wl_Breadcrump && ($.fn.wl_Breadcrump.defaults = {
		start: 0,
		numbers: !1,
		allownextonly: !1,
		disabled: !1,
		connect: null,
		onChange: function() {}
	});
	$.fn.wl_Calendar && ($.fn.wl_Calendar.defaults = {});
	$.fn.wl_Chart && ($.fn.wl_Chart.defaults = {
		width: null,
		height: 300,
		hideTable: !0,
		tableBefore: !1,
		data: {},
		stack: !1,
		type: "lines",
		points: null,
		shadowSize: 2,
		fill: null,
		fillColor: null,
		lineWidth: null,
		legend: !0,
		legendPosition: "ne",
		tooltip: !0,
		tooltipGravity: "n",
		tooltipPattern: function(b, a, c, d) {
			return "value is " + b + " from " + a + " at " + c + " (" + d + ")"
		},
		orientation: "horizontal",
		colors: "#b2e7b2 #f0b7b7 #b5f0f0 #e8e8b3 #efb7ef #bbb6f0".split(" "),
		flot: {},
		onClick: function() {}
	});
	$.fn.wl_Color && ($.fn.wl_Color.defaults = {
		mousewheel: !0,
		onChange: function() {}
	});
	$.fn.wl_Date && ($.fn.wl_Date.defaults = {
		value: null,
		mousewheel: !0,
		dayNames: "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
		dayNamesMin: "Su Mo Tu We Th Fr Sa".split(" "),
		dayNamesShort: "Sun Mon Tue Wed Thu Fri Sat".split(" "),
		firstDay: 0,
		nextText: "next",
		prevText: "prev",
		currentText: "Today",
		showWeek: !0,
		dateFormat: "mm/dd/yy"
	});
	$.confirm && ($.confirm.defaults = {
		text: {
			header: "Please confirm",
			ok: "Yes",
			cancel: "No"
		}
	});
	$.prompt && ($.prompt.defaults = {
		text: {
			header: "Please prompt",
			ok: "OK",
			cancel: "Cancel"
		}
	});
	$.alert && ($.alert.defaults = {
		nativ: !1,
		resizable: !1,
		modal: !0,
		text: {
			header: "Notification",
			ok: "OK"
		}
	});
	$.fn.wl_Editor && ($.fn.wl_Editor.defaults = {
		css: "css/light/editor.css",
		buttons: "bold|italic|underline|strikeThrough|justifyLeft|justifyCenter|justifyRight|justifyFull|highlight|colorpicker|indent|outdent|subscript|superscript|undo|redo|insertOrderedList|insertUnorderedList|insertHorizontalRule|createLink|insertImage|h1|h2|h3|h4|h5|h6|paragraph|rtl|ltr|cut|copy|paste|increaseFontSize|decreaseFontSize|html|code|removeFormat|insertTable",
		initialContent: ""
	});
	$.fn.wl_File && ($.fn.wl_File.defaults = {
		url: "upload.php",
		autoUpload: !0,
		paramName: "files",
		multiple: !1,
		allowedExtensions: "jpg jpeg gif png doc zip docx txt pdf".split(" "),
		maxNumberOfFiles: 0,
		maxFileSize: 0,
		minFileSize: 0,
		sequentialUploads: !1,
		dragAndDrop: !0,
		formData: {},
		text: {
			ready: "ready",
			cancel: "cancel",
			remove: "remove",
			uploading: "uploading...",
			done: "done",
			start: "start upload",
			add_files: "add files",
			cancel_all: "cancel upload",
			remove_all: "remove all"
		},
		onAdd: function() {},
		onDelete: function() {},
		onCancel: function() {},
		onSend: function() {},
		onDone: function() {},
		onFinish: function() {},
		onFail: function() {},
		onAlways: function() {},
		onProgress: function() {},
		onProgressAll: function() {},
		onStart: function() {},
		onStop: function() {},
		onChange: function() {},
		onDrop: function() {},
		onDragOver: function() {},
		onFileError: function(b, a) {
			$.msg("file is not allowed: " + a.name, {
				header: b.msg + " (" + b.code + ")"
			})
		}
	});
	$.fn.wl_Fileexplorer && ($.fn.wl_Fileexplorer.defaults = {
		toolbar: ["back reload open select quicklook info rename copy cut paste rm mkdir mkfile upload duplicate edit archive extract resize icons list help".split(" ")]
	});
	$.fn.wl_Form && ($.fn.wl_Form.defaults = {
		submitButton: "button.submit",
		resetButton: "button.reset",
		method: "post",
		action: null,
		ajax: !0,
		serialize: !1,
		parseQuery: !0,
		dataType: "text",
		status: !0,
		sent: !1,
		confirmSend: !0,
		text: {
			required: "This field is required",
			valid: "This field is invalid",
			password: "This password is to short",
			passwordmatch: "This password doesn't match",
			fileinqueue: "There is at least one file in the queue",
			incomplete: "Please fill out the form correctly!",
			send: "send form...",
			sendagain: "send again?",
			success: "form sent!",
			error: "error while sending!",
			parseerror: "Can't unserialize query string:\n %e"
		},
		tooltip: {
			gravity: "nw"
		},
		onRequireError: function() {},
		onValidError: function() {},
		onPasswordError: function() {},
		onFileError: function() {},
		onBeforePrepare: function() {},
		onBeforeSubmit: function() {},
		onReset: function() {},
		onComplete: function() {},
		onError: function() {},
		onSuccess: function() {}
	});
	$.fn.wl_Gallery && ($.fn.wl_Gallery.defaults = {
		group: "wl_gallery",
		editBtn: !0,
		deleteBtn: !0,
		fancybox: {},
		onEdit: function() {},
		onDelete: function() {},
		onMove: function() {}
	});
	$.fn.wl_Multiselect && ($.fn.wl_Multiselect.defaults = {
		height: 200,
		items: [],
		selected: [],
		showUsed: !1,
		searchfield: !0,
		onAdd: function() {},
		onRemove: function() {},
		onSelect: function() {},
		onUnselect: function() {},
		onSort: function() {}
	});
	$.fn.wl_Number && ($.fn.wl_Number.defaults = {
		step: 1,
		decimals: 0,
		start: 0,
		min: null,
		max: null,
		mousewheel: !0,
		onChange: function() {},
		onError: function() {}
	});
	$.fn.wl_Password && ($.fn.wl_Password.defaults = {
		confirm: !0,
		showStrength: !0,
		words: "too short;bad;medium;good;very good;excellent".split(";"),
		minLength: 3,
		text: {
			confirm: "please confirm",
			nomatch: "password doesn't match"
		}
	});
	$.fn.wl_Slider && ($.fn.wl_Slider.defaults = {
		min: 0,
		max: 100,
		step: 1,
		animate: !1,
		disabled: !1,
		orientation: "horizontal",
		range: !1,
		mousewheel: !0,
		connect: null,
		tooltip: !1,
		tooltipGravity: ["s", "w"],
		tooltipPattern: "%n",
		onSlide: function() {},
		onChange: function() {}
	});
	$.fn.wl_Time && ($.fn.wl_Time.defaults = {
		step: 5,
		timeformat: 24,
		roundtime: !0,
		time: null,
		value: null,
		mousewheel: !0,
		onDateChange: function() {},
		onHourChange: function() {},
		onChange: function() {}
	});
	$.fn.wl_Valid && ($.fn.wl_Valid.defaults = {
		errorClass: "error",
		instant: !0,
		regex: /.*/,
		minLength: 0,
		onChange: function() {},
		onError: function() {}
	});
	$.fn.wl_Mail && ($.fn.wl_Mail.defaults = {
		regex: /^([\w-]+(?:\.[\w-]+)*)\@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$|(\[?(\d{1,3}\.){3}\d{1,3}\]?)$/i,
		onChange: function(b, a) {
			b.val(a.toLowerCase())
		}
	});
	$.fn.wl_URL && ($.fn.wl_URL.defaults = {
		regex: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w]))*\.+(([\w#!:.?+=&%@!\-\/]))?/,
		instant: !1,
		onChange: function(b, a) {
			"" != a && !/^(ftp|http|https):\/\//.test(a) && b.val("http://" + a).trigger("change.wl_Valid")
		}
	});
	$.fn.wl_Widget && ($.fn.wl_Widget.defaults = {
		collapsed: !1,
		load: null,
		reload: !1,
		removeContent: !0,
		collapsible: !0,
		sortable: !0,
		text: {
			loading: "loading...",
			reload: "reload",
			collapse: "collapse widget",
			expand: "expand widget"
		},
		onDrag: function() {},
		onDrop: function() {},
		onExpand: function() {},
		onCollapse: function() {}
	});
});