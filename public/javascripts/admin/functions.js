/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: functions.js
 * filesize: 3267 Bytes
 * last modified: Fri, 17 May 2013 15:07:57 +0200
 *
 */
$(document).ready(function(){$.parseData=function(a,b){/^\[(.*)\]$/.test(a)&&(a=a.substr(1,a.length-2).split(","));b&&(!$.isArray(a)&&null!=a)&&(a=Array(a));return a};$.preload=function(){for(var a=[],b=arguments.length;b--;){var c=document.createElement("img");c.src=arguments[b];a.push(c)}};$.fn.fadeInSlide=function(a,b){$.isFunction(a)&&(b=a);a||(a=200);b||(b=function(){});this.each(function(){$(this).fadeTo(a/2,1).slideDown(a/2,function(){b()})});return this};$.fn.fadeOutSlide=function(a,b){$.isFunction(a)&&
(b=a);a||(a=200);b||(b=function(){});this.each(function(){var c=$(this);c.fadeTo(a/2,0).slideUp(a/2,function(){c.remove();b()})});return this};$.fn.textFadeOut=function(a,b,c){if(!a)return!1;$.isFunction(b)&&(c=b);b||(b=2E3);c||(c=function(){});this.each(function(){var d=$(this);d.stop().text(a).show().delay(b).fadeOut(1E3,function(){d.text("").show();c()})});return this};$.leadingZero=function(a){a=parseInt(a,10);isNaN(a)||(10>a?a="0"+a:a);return a}});
