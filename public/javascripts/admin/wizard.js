/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: wizard.js
 * filesize: 680 Bytes
 * last modified: Fri, 17 May 2013 15:07:57 +0200
 *
 */
$(document).ready(function(){var a=$("body"),b=$("#content");$.browser.msie||a.fadeTo(0,0).delay(500).fadeTo(1E3,1);$("input").not("input[type=submit]").uniform();b.find(".breadcrumb").wl_Breadcrumb({allownextonly:!0,onChange:function(){}});$("#loginbtn").click(function(){location.href="login.html"})});
