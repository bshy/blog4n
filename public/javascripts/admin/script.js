/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: script.js
 * filesize: 23327 Bytes
 * last modified: Fri, 17 May 2013 15:07:57 +0200
 *
 */
$(document).ready(function() {
	
	var basePath = location.protocol + "//" + location.host + (location.port&&(":" + location.port));
	var content = $("#content"),
		workspace = $('#workspace'),
		panelTemplate = '<div id="workspace" class="widgets ui-sortable">\
			<div class="widget sortable" data-load="<%=url%>" data-reload="1" data-remove-content="false">\
				<h3 class="handle">\
					AJAX Widget\
					<a class="collapse" title="collapse widget"></a>\
					<a class="reload" title="reload"></a>\
				</h3>\
				<div style="height: auto;">\
					<h3>This is AJAX Content</h3>\
					<p>last load: Mon, 24 Jun 2013 19:08:14 +0200</p>\
				</div>\
			</div>\
		</div>';
	//$("#message_header").click(function() {
	//	$.msg("This is with a custom Header", {
	//		header: "Custom Header"
	//	})
	//});
	
	// 配置框
	$("#wl_config").click(function() {
		var pageoptions = $("#pageoptions");
		200 > pageoptions.height() ? (pageoptions.animate({
			height: 200
		}), $(this).addClass("active")) : (pageoptions.animate({
			height: 20
		}), $(this).removeClass("active"));
		return ! 1
	});
	
	//子菜单
	var j = $("ul#headernav");
	j.bind("click", function() {
		var a = j.find("ul").eq(0);
		a.is(":hidden") ? a.addClass("shown") : a.removeClass("shown")
	});


	j.find("ul > li").bind("click", function(target) {
		target.stopPropagation();
		workspace.empty();
		console.log($(this).attr('data-load-url'));
		console.log(basePath);
		var str = _.template(panelTemplate,{url : basePath + $(this).attr('data-load-url')});
		workspace.html(str);
		content.wl_Widget();
	});


	j.find("ul > li > a > div").bind("click", function(target) {
		target.stopPropagation();
		target = $(this).parent().parent().children("ul");
		if (target.length){
			if(target.is(":hidden")){
				target.addClass("shown");
			}else{
				target.removeClass("shown");
			}
		}
	});

	// 搜索框
	var f = $("#searchform"),
	e = $("#search");
	e.bind({
		"focus.wl": function() {
			e.select().parent().animate({
				width: "150px"
			},
			100)
		},
		"blur.wl": function() {
			e.parent().animate({
				width: "90px"
			},
			100);
			g.fadeOut()
		}
	});
	e.attr("placeholder", "Live Search");
	var g = $("#searchboxresult"),
	l,
	h,
	k;
	g.length || (g = $('<div id="searchboxresult"></div>').insertAfter("#searchbox"));
	e.bind({
		"keyup.wl": function() {
			if (h == e.val()) return ! 1;
			h = e.val();
			clearTimeout(l);
			if (3 > h.length) return g.fadeOut(),
			e.removeClass("load"),
			!1;
			l = setTimeout(function() {
				e.addClass("load");
				$.post("search.php", {
					term: h
				},
				function(a) {
					e.removeClass("load");
					if (3 > h.length) return g.fadeOut(),
					!1;
					var b = a.length,
					c = "";
					if (b) for (var d = 0; d < b; d++) k = "",
					105 < a[d].text.length && (k = 'title="' + a[d].text + '"', a[d].text = $.trim(a[d].text.substr(0, 100)) + "&hellip;"),
					c += '<li><a href="' + a[d].href + '" ' + k + ">",
					a[d].img && (c += '<img src="' + a[d].img + '" width="50">'),
					c += "" + a[d].text + "</a></li>";
					else c += '<li><a class="noresult">Nothing found for<br>"' + h + '"</a></li>';
					g.html(c).fadeIn()
				},
				"json")
			},
			800)
		}
	});
	f.bind("submit.wl",function() {
		e.val()
	});

//置顶按钮
	var c = $("<a/>", {
		id: "mod_scroll_top_btn",
		title: "scroll to top"
	}).html("scroll to top").appendTo("body").bind("click touchstart",function() {
		$("html, body").animate({
			scrollTop: 0
		})
	}).hide();
	$(window).bind("scroll.mod",function() {
		var a = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
		0 < a ? c.fadeIn() : c.fadeOut();
		void 0 !== window.orientation && c.offset({
			top: $(window).height() + a
		})
	}).trigger("scroll.mod");
	$(document).bind("touchstart.mod",function() {
		c.hide()
	})











	return;
	b.find("input[type=number].integer").wl_Number();
	b.find("input[type=number].decimal").wl_Number({
		decimals: 2,
		step: 0.5
	});
	b.find("input.date, div.date").wl_Date();
	b.find("input.time").wl_Time();
	b.find("input.autocomplete").wl_Autocomplete({
		source: "ActionScript AppleScript Asp BASIC C C++ Clojure COBOL ColdFusion Erlang Fortran Groovy Haskell Java JavaScript Lisp Perl PHP Python Ruby Scala Scheme".split(" ")
	});
	b.find("textarea[data-autogrow]").elastic();
	b.find("textarea.html").wl_Editor();
	b.find("input[data-regex]").wl_Valid();
	b.find("input[type=email]").wl_Mail();
	b.find("input[type=url]").wl_URL();
	b.find("input[type=file]").wl_File();
	b.find("input[type=password]").wl_Password();
	b.find("input.color").wl_Color();
	b.find("div.slider").wl_Slider();
	b.find("select[multiple]").wl_Multiselect();
	b.find("div.alert").wl_Alert();
	b.find("ul.breadcrumb").wl_Breadcrumb();
	b.find("table.datatable").dataTable({
		sPaginationType: "full_numbers"
	});
	$("select, input[type=file]").not("select[multiple]").uniform();
	$("input:checkbox, input:radio").checkbox();
	b.find("table.chart").wl_Chart({
		onClick: function(a, b, c, d) {
			$.msg("value is " + a + " from " + b + " at " + c + " (" + d + ")", {
				header: "Custom Callback"
			})
		}
	});
	b.find("div.fileexplorer").wl_Fileexplorer();
	console.log(b.find("div.calendar"));
	/*
	b.find("div.calendar").wl_Calendar({
		eventSources: [{
			url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic"
		},
		{
			events: [{
				title: "Fixed Event",
				start: "2012-02-01"
			},
			{
				title: "long fixed Event",
				start: "2012-02-06",
				end: "2012-02-14"
			}],
			color: "#f0a8a8",
			textColor: "#ffffff"
		},
		{
			events: [{
				title: "Editable",
				start: "2012-02-09 12:30:00"
			}],
			editable: !0,
			color: "#a2e8a2",
			textColor: "#ffffff"
		}]
	});
	*/
	b.find("input[title]").tipsy({
		gravity: function() {
			return $(this).data("tooltip-gravity") || config.tooltip.gravity
		},
		fade: config.tooltip.fade,
		opacity: config.tooltip.opacity,
		color: config.tooltip.color,
		offset: config.tooltip.offset
	});
	b.find("div.accordion").accordion({
		collapsible: !0,
		autoHeight: !1
	});
	b.find("div.tab").tabs({
		fx: {
			opacity: "toggle",
			duration: "fast"
		}
	});
	
	
	
	f = $("#nav");
	f.delegate("li", "click.wl",
	function(a) {
		var b = $(this),
		c = b.parent(),
		d = c.find("a");
		c.find("ul").slideUp("fast");
		d.removeClass("active");
		b.find("ul:hidden").slideDown("fast");
		b.find("a").eq(0).addClass("active");
		a.stopPropagation()
	});
	
	b.find("div.slider#slider_callback").wl_Slider({
		onSlide: function(a) {
			$("#slider_callback_bar").width(a + "%").text(a)
		}
	});
	b.find("div.slider#slider_mousewheel").wl_Slider({
		onSlide: function() {
			var a = $("#slider_mousewheel"),
			b = a.find("a");
			_h1 = b.eq(0).offset();
			_h2 = b.eq(1).offset();
			value = _h1.left + (_h2.left - _h1.left) / 2 - a.offset().left + 5;
			$("#slider_mousewheel_left").width(value);
			$("#slider_mousewheel_right").width(a.width() - value)
		}
	});
	$("#slider_mousewheel_left, #slider_mousewheel_right").bind("mousewheel",
	function(a) {
		a.preventDefault();
		$.alert("Use the Slider above!\nThis is just for visualisation")
	});
	b.find(".clearLocalStorage").bind("click",
	function() { (new $.wl_Store).flush() && $.msg("LocalStorage as been cleared!");
		return ! 1
	});
	b.find("#save_store").bind("click",
	function() {
		$.prompt("Storing some values?", "This text is for the storage",
		function(a) { (new $.wl_Store("doc-store")).save("testvalue", a) ? $.msg("Your data has been saved!. You can reload the page now!", {
				live: 1E4
			}) : $.msg("Sorry, but a problem while storing your data occurs! Maybe your browser isn't supported!", {
				live: 1E4
			})
		})
	});
	b.find("#restore_store").bind("click",
	function() {
		var a = (new $.wl_Store("doc-store")).get("testvalue");
		a ? $.alert("your value is:\n" + a) : $.alert("No value is set!")
	});
	$("#formfiller").click(function() {
		var a = this;
		$.confirm("To fill a form with your data you have to add a query string to the location.\nhttp://domain.tld/path?key=value&key2=value2",
		function() {
			window.location.href = a.href
		});
		return ! 1
	});
	$("#formsubmitswitcher").click(function() {
		var a = $(this);
		"send form natively" == a.text() ? (b.find("form").wl_Form("set", "ajax", !1), $.msg("The form will now use the browser native submit method"), a.text("send form with ajax")) : (b.find("form").wl_Form("set", "ajax", !0), $.msg("The form will now be sent with an ajax request"), a.text("send form natively"));
		return ! 1
	});
	b.find("form").wl_Form({
		onSuccess: function(a, b) {
			window.console && (console.log(b), console.log(a));
			$.msg("Custom Callback on success\nDevelopers! Check your Console!")
		},
		onError: function(a, b) {
			$.msg("Callback on Error\nError Status: " + a + "\nError Msg: " + b)
		}
	});
	b.find("ul.gallery").wl_Gallery({
		onEdit: function(a, b, c) {
			b && $.confirm("For demonstration I use pixlr to edit images.\nDo you like to continue?",
			function() {
				window.open("http://pixlr.com/editor/?referrer=whitelabel&image=" + escape(b) + "&title=" + escape(c) + "")
			})
		},
		onDelete: function(a, b) {
			b && $.confirm("Do you really like to delete this?",
			function() {
				a.fadeOut()
			})
		},
		onMove: function() {}
	});
	$("#message").click(function() {
		$.msg("This is a simple Message")
	});
	$("#message_sticky").click(function() {
		$.msg("This Message will stay until you click the cross", {
			sticky: !0
		})
	});
	
	$("#message_delay").click(function() {
		$.msg("This stays exactly 10 seconds", {
			live: 1E4
		})
	});
	$("#message_methods").click(function() {
		var a = $.msg("This message can be accessed via public methods", {
			sticky: !0
		});
		setTimeout(function() {
			a && a.setHeader("Set a Header")
		},
		3E3);
		setTimeout(function() {
			a && a.setBody("Set a custom Body")
		},
		5E3);
		setTimeout(function() {
			a && a.setBody("..and close it with an optional callback function")
		},
		8E3);
		setTimeout(function() {
			a && a.close(function() {
				$.alert("This is the Callback function")
			})
		},
		12E3)
	});
	$("#dialog").click(function() {
		$.alert("This is a simple Message")
	});
	$("#dialog_confirm").click(function() {
		$.confirm("Do you really like to confirm this?",
		function() {
			$.msg("confirmed!")
		},
		function() {
			$.msg("You clicked cancel!")
		})
	});
	$("#dialog_prompt").click(function() {
		$.prompt("What do you really like?", "Pizza",
		function(a) {
			$.msg("So, you like '" + a + "'?")
		},
		function() {
			$.msg("You clicked cancel!")
		})
	});
	$("#dialog_switch").click(function() {
		$.alert.defaults.nativ ? $(this).text("switch to nativ dialogs") : $(this).text("switch to jQuery Dialogs");
		$.alert.defaults.nativ = !$.alert.defaults.nativ
	});
	$("#dialog_methods").click(function() {
		var a = $.alert("This message can be accessed via public methods");
		setTimeout(function() {
			a && a.setHeader("Set a Header")
		},
		3E3);
		setTimeout(function() {
			a && a.setBody("Set a custom Body")
		},
		5E3);
		setTimeout(function() {
			a && a.setBody("..and close it with an optional callback function")
		},
		8E3);
		setTimeout(function() {
			a && a.close(function() {
				$.msg("This is the Callback function")
			})
		},
		12E3)
	});
	$("#enablebreadcrumb").click(function() {
		$("ul.breadcrumb").eq(4).wl_Breadcrumb("enable");
		$.msg("enabled!")
	});
	$("#disablebreadcrumb").click(function() {
		$("ul.breadcrumb").eq(4).wl_Breadcrumb("disable");
		$.msg("disabled!")
	});
	$("ul.breadcrumb").eq(5).wl_Breadcrumb({
		onChange: function(a, b) {
			$.msg(a.text() + " with id " + b)
		}
	});
	var m = location.pathname.replace(/\/([^.]+)\//g, ""),
	f = f.find('a[href="' + m + '"]');
	f.parent().parent().is("#nav") || f.parent().parent().parent().find("a").eq(0).addClass("active").next().show();
	f.addClass("active")
});