define(function(require, exports ,module){
	require('./_combined_plugins');
	require('./functions');
	require('./config');
	var basePath = location.protocol + "//" + location.host + "/",
		panelTemplate = '<div class="widget sortable" data-load="<%=url%>" data-remove-content="false">\
				<h3 class="handle">\
					<%=text%>\
					<a class="reload" title="reload"></a>\
				</h3>\
				<div style="height: 587px;">\
					<h3>This is AJAX Content</h3>\
					<p>last load: Mon, 24 Jun 2013 19:08:14 +0200</p>\
				</div>\
			</div>';
	//$("#message_header").click(function() {
	//	$.msg("This is with a custom Header", {
	//		header: "Custom Header"
	//	})
	//});
	
	// 配置框
	$("#wl_config").click(function() {
		var pageoptions = $("#pageoptions");
		200 > pageoptions.height() ? (pageoptions.animate({
			height: 200
		}), $(this).addClass("active")) : (pageoptions.animate({
			height: 20
		}), $(this).removeClass("active"));
		return ! 1
	});
	
	//子菜单
	var headernav = $("ul#headernav");
	headernav.bind("click", function() {
		var a = headernav.find("ul").eq(0);
		a.is(":hidden") ? a.addClass("shown") : a.removeClass("shown")
	});


	var content = $("#content"),
		workspace = $('#workspace');

	headernav.find("ul > li").bind("click", function(target) {
		target.stopPropagation();
		
		$("#workspace").data('wl_Widget',null);
		workspace.empty();
		var me = $(this);
		var str = _.template(panelTemplate,{url : basePath + me.attr('data-load-url'),text : me.attr('data-text')});
		if(me.data('leaf')){
			me.parent().removeClass("shown");
		}
		workspace.html(str);
		$("#workspace").wl_Widget();
		var ul = me.find('a + ul');
		if(ul){
			if(!ul.is(":hidden")){
				ul.removeClass("shown");
			}
		}
	});

	headernav.find("li > ul").bind('mouseleave',function(target){
		var me = $(this);
		me.is(":hidden") ? '' : me.removeClass("shown")
	});

	headernav.find("ul > li > a > div").bind("click", function(target) {
		target.stopPropagation();
		target = $(this).parent().parent().children("ul");
		if (target.length){
			if(target.is(":hidden")){
				target.addClass("shown");
			}else{
				target.removeClass("shown");
			}
		}
	});

	// 搜索框
	var f = $("#searchform"),
	e = $("#search");
	e.bind({
		"focus.wl": function() {
			e.select().parent().animate({
				width: "150px"
			},
			100)
		},
		"blur.wl": function() {
			e.parent().animate({
				width: "90px"
			},
			100);
			g.fadeOut()
		}
	});
	e.attr("placeholder", "Live Search");
	var g = $("#searchboxresult"),
	l,
	h,
	k;
	g.length || (g = $('<div id="searchboxresult"></div>').insertAfter("#searchbox"));
	e.bind({
		"keyup.wl": function() {
			if (h == e.val()) return ! 1;
			h = e.val();
			clearTimeout(l);
			if (3 > h.length) return g.fadeOut(),
			e.removeClass("load"),
			!1;
			l = setTimeout(function() {
				e.addClass("load");
				$.post("search.php", {
					term: h
				},
				function(a) {
					e.removeClass("load");
					if (3 > h.length) return g.fadeOut(),
					!1;
					var b = a.length,
					c = "";
					if (b) for (var d = 0; d < b; d++) k = "",
					105 < a[d].text.length && (k = 'title="' + a[d].text + '"', a[d].text = $.trim(a[d].text.substr(0, 100)) + "&hellip;"),
					c += '<li><a href="' + a[d].href + '" ' + k + ">",
					a[d].img && (c += '<img src="' + a[d].img + '" width="50">'),
					c += "" + a[d].text + "</a></li>";
					else c += '<li><a class="noresult">Nothing found for<br>"' + h + '"</a></li>';
					g.html(c).fadeIn()
				},
				"json")
			},
			800)
		}
	});
	f.bind("submit.wl",function() {
		e.val()
	});

//置顶按钮
	var c = $("<a/>", {
		id: "mod_scroll_top_btn",
		title: "scroll to top"
	}).html("scroll to top").appendTo("body").bind("click touchstart",function() {
		$("html, body").animate({
			scrollTop: 0
		})
	}).hide();
	$(window).bind("scroll.mod",function() {
		var a = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
		0 < a ? c.fadeIn() : c.fadeOut();
		void 0 !== window.orientation && c.offset({
			top: $(window).height() + a
		})
	}).trigger("scroll.mod");
	$(document).bind("touchstart.mod",function() {
		c.hide()
	})
})