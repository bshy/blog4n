/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: _combined_plugins.js
 * filesize: 838502 Bytes
 * last modified: Tue, 21 May 2013 14:19:18 +0200
 *
 */
(function(a) {
	function e(b) {
		b.stopPropagation();
		var c = b.originalEvent.changedTouches[0];
		return a.extend(b, {
			type: n[b.type],
			which: 1,
			pageX: c.pageX,
			pageY: c.pageY,
			screenX: c.screenX,
			screenY: c.screenY,
			clientX: c.clientX,
			clientY: c.clientY
		})
	}
	a.support.touch = "object" === typeof Touch;
	if (a.support.touch) {
		var b = a.ui.mouse.prototype,
		c = b._mouseInit,
		g = b._mouseDown,
		d = b._mouseUp,
		n = {
			touchstart: "mousedown",
			touchmove: "mousemove",
			touchend: "mouseup"
		};
		b._mouseInit = function() {
			var a = this;
			a.element.bind("touchstart." + a.widgetName,
			function(b) {
				return a._mouseDown(e(b))
			});
			c.call(a)
		};
		b._mouseDown = function(c) {
			var d = this,
			t = g.call(d, c);
			if (d.options.handle && !a(c.target).is(d.options.handle)) b._mouseUp(c);
			else return d._touchMoveDelegate = function(a) {
				return d._mouseMove(e(a))
			},
			d._touchEndDelegate = function(a) {
				return d._mouseUp(e(a))
			},
			a(document).bind("touchmove." + d.widgetName, d._touchMoveDelegate).bind("touchend." + d.widgetName, d._touchEndDelegate),
			t
		};
		b._mouseUp = function(b) {
			a(document).unbind("touchmove." + this.widgetName, this._touchMoveDelegate).unbind("touchend." + this.widgetName, this._touchEndDelegate);
			return d.call(this, b)
		}
	}
})(jQuery); (function(a) {
	function e(b) {
		var c = b || window.event,
		e = [].slice.call(arguments, 1),
		j = 0,
		k = 0,
		t = 0;
		b = a.event.fix(c);
		b.type = "mousewheel";
		c.wheelDelta && (j = c.wheelDelta / 120);
		c.detail && (j = -c.detail / 3);
		t = j;
		void 0 !== c.axis && c.axis === c.HORIZONTAL_AXIS && (t = 0, k = -1 * j);
		void 0 !== c.wheelDeltaY && (t = c.wheelDeltaY / 120);
		void 0 !== c.wheelDeltaX && (k = -1 * c.wheelDeltaX / 120);
		e.unshift(b, j, k, t);
		return (a.event.dispatch || a.event.handle).apply(this, e)
	}
	var b = ["DOMMouseScroll", "mousewheel"];
	if (a.event.fixHooks) for (var c = b.length; c;) a.event.fixHooks[b[--c]] = a.event.mouseHooks;
	a.event.special.mousewheel = {
		setup: function() {
			if (this.addEventListener) for (var a = b.length; a;) this.addEventListener(b[--a], e, !1);
			else this.onmousewheel = e
		},
		teardown: function() {
			if (this.removeEventListener) for (var a = b.length; a;) this.removeEventListener(b[--a], e, !1);
			else this.onmousewheel = null
		}
	};
	a.fn.extend({
		mousewheel: function(a) {
			return a ? this.bind("mousewheel", a) : this.trigger("mousewheel")
		},
		unmousewheel: function(a) {
			return this.unbind("mousewheel", a)
		}
	})
})(jQuery); (function(a) {
	function e(a) {
		if (a.attr("title") || "string" != typeof a.attr("original-title")) a.attr("original-title", a.attr("title") || "").removeAttr("title")
	}
	function b(b, g) {
		this.$element = a(b);
		this.options = g;
		this.enabled = !0;
		e(this.$element)
	}
	b.prototype = {
		show: function() {
			var b, g, d = this.getTitle();
			if (d && this.enabled) {
				var e = this.tip();
				e.find(".tipsy-inner")[this.options.html ? "html": "text"](d);
				e[0].className = "tipsy";
				e.remove().css({
					top: 0,
					left: 0,
					visibility: "hidden",
					display: "block"
				}).appendTo(this.options.appendTo);
				d = a.extend({},
				this.$element.offset(), {
					width: this.$element[0].offsetWidth,
					height: this.$element[0].offsetHeight
				});
				"body" != this.options.appendTo && (d.top = d.left = 0);
				var j = e[0].offsetWidth,
				k = e[0].offsetHeight,
				t = "function" == typeof this.options.gravity ? this.options.gravity.call(this.$element[0]) : this.options.gravity,
				A;
				switch (t.charAt(0)) {
				case "n":
					A = {
						top: d.top + d.height + this.options.offset,
						left: d.left + d.width / 2 - j / 2
					};
					b = 15;
					g = -j / 2;
					break;
				case "s":
					A = {
						top: d.top - k - this.options.offset,
						left: d.left + d.width / 2 - j / 2
					};
					b = -k - 15;
					g = -j / 2;
					break;
				case "e":
					A = {
						top: d.top + d.height / 2 - k / 2,
						left: d.left - j - this.options.offset
					};
					b = -k / 2;
					g = -12 - j;
					break;
				case "w":
					A = {
						top: d.top + d.height / 2 - k / 2,
						left: d.left + d.width + this.options.offset
					},
					b = -k / 2,
					g = 12
				}
				2 == t.length && ("w" == t.charAt(1) ? (A.left = d.left - 5, g += j / 2 - 15) : (g -= j / 2 - 15, A.left = d.left + d.width - j + 5));
				this.options.followMouse && a(document).bind("mousemove.tipsy",
				function(a) {
					e.css({
						left: a.pageX + g,
						top: a.pageY + b
					})
				});
				e.css(A).addClass("tipsy-" + t);
				this.options.fade ? e.stop().css({
					opacity: 0,
					display: "block",
					visibility: "visible"
				}).animate({
					opacity: this.options.opacity
				}) : e.css({
					visibility: "visible",
					opacity: this.options.opacity
				})
			}
		},
		hide: function() {
			this.options.followMouse && a(document).unbind("mousemove.tipsy");
			this.options.fade ? this.tip().stop().fadeOut(function() {
				a(this).remove()
			}) : this.tip().remove()
		},
		getTitle: function() {
			var a, b = this.$element,
			d = this.options;
			e(b);
			"string" == typeof d.title ? a = b.attr("title" == d.title ? "original-title": d.title) : "function" == typeof d.title && (a = d.title.call(b[0]));
			a || (a = d.fallback);
			return (a = ("" + a).replace(/(^\s*|\s*$)/, "")) || d.fallback
		},
		setTitel: function(a) {
			this.options.fallback = a
		},
		tip: function() {
			this.$tip || (this.$tip = a('<div class="tipsy"></div>').html('<div class="tipsy-arrow"></div><div class="tipsy-outer"><div class="tipsy-inner"/></div></div>'));
			return this.$tip
		},
		validate: function() {
			this.$element[0].parentNode || (this.hide(), this.options = this.$element = null)
		},
		enable: function() {
			this.enabled = !0
		},
		disable: function() {
			this.enabled = !1
		},
		update: function() {
			this.tip().find(".tipsy-inner")[this.options.html ? "html": "text"](this.options.fallback)
		},
		toggleEnabled: function() {
			this.enabled = !this.enabled
		}
	};
	a.fn.tipsy = function(c, g, d) {
		function e(g) {
			var d = a.data(g, "tipsy");
			d || (d = new b(g, a.fn.tipsy.elementOptions(g, c)), a.data(g, "tipsy", d));
			return d
		}
		function j() {
			var a = e(this);
			a.hoverState = "in";
			0 == c.delayIn ? a.show() : setTimeout(function() {
				"in" == a.hoverState && a.show()
			},
			c.delayIn)
		}
		function k() {
			var a = e(this);
			a.hoverState = "out";
			0 == c.delayOut ? a.hide() : setTimeout(function() {
				"out" == a.hoverState && a.hide()
			},
			c.delayOut)
		}
		if (!0 === c) return this.data("tipsy");
		if ("string" == typeof c) return this.data("tipsy")[c](g, d);
		c = a.extend({},
		a.fn.tipsy.defaults, c);
		c.live || this.each(function() {
			e(this)
		});
		"manual" != c.trigger && (g = c.live ? "live": "bind", d = "hover" == c.trigger ? "mouseleave": "blur", this[g](("hover" == c.trigger ? "mouseenter": "focus") + ".tipsy", j)[g](d + ".tipsy", k));
		return this
	};
	a.fn.tipsy.defaults = {
		delayIn: 0,
		delayOut: 0,
		fade: !1,
		fallback: "",
		gravity: "n",
		html: !1,
		live: !1,
		offset: 0,
		opacity: 0.8,
		followMouse: !1,
		appendTo: "body",
		title: "title",
		trigger: "hover"
	};
	a.fn.tipsy.elementOptions = function(b, g) {
		return a.metadata ? a.extend({},
		g, a(b).metadata()) : g
	};
	a.fn.tipsy.autoNS = function() {
		return a(this).offset().top > a(document).scrollTop() + a(window).height() / 2 ? "s": "n"
	};
	a.fn.tipsy.autoWE = function() {
		return a(this).offset().left > a(document).scrollLeft() + a(window).width() / 2 ? "e": "w"
	}
})(jQuery); (function(a) {
	a.uniform = {
		options: {
			selectClass: "selector",
			radioClass: "radio",
			checkboxClass: "checker",
			fileClass: "uploader",
			filenameClass: "filename",
			fileBtnClass: "action",
			fileDefaultText: "No file selected",
			fileBtnText: "Choose File",
			checkedClass: "checked",
			focusClass: "focus",
			disabledClass: "disabled",
			buttonClass: "button",
			activeClass: "active",
			hoverClass: "hover",
			useID: !0,
			idPrefix: "uniform",
			resetSelector: !1,
			autoHide: !0
		},
		elements: []
	};
	a.support.selectOpacity = a.browser.msie && 7 > a.browser.version ? !1 : !0;
	a.fn.uniform = function(e) {
		function b(b) {
			b = a(b).get();
			1 < b.length ? a.each(b,
			function(b, c) {
				a.uniform.elements.push(c)
			}) : a.uniform.elements.push(b)
		}
		e = a.extend(a.uniform.options, e);
		var c = this; ! 1 != e.resetSelector && a(e.resetSelector).mouseup(function() {
			setTimeout(function() {
				a.uniform.update(c)
			},
			10)
		});
		a.uniform.restore = function(b) {
			void 0 == b && (b = a(a.uniform.elements));
			a(b).each(function() {
				a(this).is(":checkbox") ? a(this).unwrap().unwrap() : a(this).is("select") ? (a(this).siblings("span").remove(), a(this).unwrap()) : a(this).is(":radio") ? a(this).unwrap().unwrap() : a(this).is(":file") ? (a(this).siblings("span").remove(), a(this).unwrap()) : a(this).is("button, :submit, :reset, a, input[type='button']") && a(this).unwrap().unwrap();
				a(this).unbind(".uniform");
				a(this).css("opacity", "1");
				var c = a.inArray(a(b), a.uniform.elements);
				a.uniform.elements.splice(c, 1)
			})
		};
		a.uniform.noSelect = function(b) {
			function c() {
				return ! 1
			}
			a(b).each(function() {
				this.onselectstart = this.ondragstart = c;
				a(this).mousedown(c).css({
					MozUserSelect: "none"
				})
			})
		};
		a.uniform.update = function(b) {
			void 0 == b && (b = a(a.uniform.elements));
			b = a(b);
			b.each(function() {
				var c = a(this);
				if (c.is("select")) {
					var n = c.siblings("span"),
					j = c.parent("div");
					j.removeClass(e.hoverClass + " " + e.focusClass + " " + e.activeClass);
					n.html(c.find(":selected").html());
					c.is(":disabled") ? j.addClass(e.disabledClass) : j.removeClass(e.disabledClass)
				} else if (c.is(":checkbox")) n = c.closest("span"),
				j = c.closest("div"),
				j.removeClass(e.hoverClass + " " + e.focusClass + " " + e.activeClass),
				n.removeClass(e.checkedClass),
				c.is(":checked") && n.addClass(e.checkedClass),
				c.is(":disabled") ? j.addClass(e.disabledClass) : j.removeClass(e.disabledClass);
				else if (c.is(":radio")) n = c.closest("span"),
				j = c.closest("div"),
				j.removeClass(e.hoverClass + " " + e.focusClass + " " + e.activeClass),
				n.removeClass(e.checkedClass),
				c.is(":checked") && n.addClass(e.checkedClass),
				c.is(":disabled") ? j.addClass(e.disabledClass) : j.removeClass(e.disabledClass);
				else if (c.is(":file")) j = c.parent("div"),
				n = c.siblings(e.filenameClass),
				btnTag = c.siblings(e.fileBtnClass),
				j.removeClass(e.hoverClass + " " + e.focusClass + " " + e.activeClass),
				n.text(c.val()),
				c.is(":disabled") ? j.addClass(e.disabledClass) : j.removeClass(e.disabledClass);
				else if (c.is(":submit") || c.is(":reset") || c.is("button") || c.is("a") || b.is("input[type=button]")) j = c.closest("div"),
				j.removeClass(e.hoverClass + " " + e.focusClass + " " + e.activeClass),
				c.is(":disabled") ? j.addClass(e.disabledClass) : j.removeClass(e.disabledClass)
			})
		};
		return this.each(function() {
			if (a.support.selectOpacity) {
				var c = a(this);
				if (c.is("select")) {
					if (!0 != c.attr("multiple") && (void 0 == c.attr("size") || 1 >= c.attr("size"))) {
						var d = a(c),
						n = a("<div />"),
						j = a("<span />");
						"none" == !d.css("display") && e.autoHide && n.hide();
						n.addClass(e.selectClass);
						e.useID && "" != c.attr("id") && n.attr("id", e.idPrefix + "-" + c.attr("id"));
						d = c.find(":selected:first");
						0 == d.length && (d = c.find("option:first"));
						j.html(d.html());
						c.css("opacity", 0);
						c.wrap(n);
						c.before(j);
						n = c.parent("div");
						j = c.siblings("span");
						c.bind({
							"change.uniform": function() {
								j.text(c.find(":selected").html());
								n.removeClass(e.activeClass)
							},
							"focus.uniform": function() {
								n.addClass(e.focusClass)
							},
							"blur.uniform": function() {
								n.removeClass(e.focusClass);
								n.removeClass(e.activeClass)
							},
							"mousedown.uniform touchbegin.uniform": function() {
								n.addClass(e.activeClass)
							},
							"mouseup.uniform touchend.uniform": function() {
								n.removeClass(e.activeClass)
							},
							"click.uniform touchend.uniform": function() {
								n.removeClass(e.activeClass)
							},
							"mouseenter.uniform": function() {
								n.addClass(e.hoverClass)
							},
							"mouseleave.uniform": function() {
								n.removeClass(e.hoverClass);
								n.removeClass(e.activeClass)
							},
							"keyup.uniform": function() {
								j.text(c.find(":selected").html())
							}
						});
						a(c).attr("disabled") && n.addClass(e.disabledClass);
						a.uniform.noSelect(j);
						b(c)
					}
				} else if (c.is(":checkbox")) {
					var d = a(c),
					k = a("<div />"),
					t = a("<span />");
					"none" == !d.css("display") && e.autoHide && k.hide();
					k.addClass(e.checkboxClass);
					e.useID && "" != c.attr("id") && k.attr("id", e.idPrefix + "-" + c.attr("id"));
					a(c).wrap(k);
					a(c).wrap(t);
					t = c.parent();
					k = t.parent();
					a(c).css("opacity", 0).bind({
						"focus.uniform": function() {
							k.addClass(e.focusClass)
						},
						"blur.uniform": function() {
							k.removeClass(e.focusClass)
						},
						"click.uniform touchend.uniform": function() {
							a(c).attr("checked") ? t.addClass(e.checkedClass) : t.removeClass(e.checkedClass)
						},
						"mousedown.uniform touchbegin.uniform": function() {
							k.addClass(e.activeClass)
						},
						"mouseup.uniform touchend.uniform": function() {
							k.removeClass(e.activeClass)
						},
						"mouseenter.uniform": function() {
							k.addClass(e.hoverClass)
						},
						"mouseleave.uniform": function() {
							k.removeClass(e.hoverClass);
							k.removeClass(e.activeClass)
						}
					});
					a(c).attr("checked") && t.addClass(e.checkedClass);
					a(c).attr("disabled") && k.addClass(e.disabledClass);
					b(c)
				} else if (c.is(":radio")) {
					var d = a(c),
					A = a("<div />"),
					B = a("<span />");
					"none" == !d.css("display") && e.autoHide && A.hide();
					A.addClass(e.radioClass);
					e.useID && "" != c.attr("id") && A.attr("id", e.idPrefix + "-" + c.attr("id"));
					a(c).wrap(A);
					a(c).wrap(B);
					B = c.parent();
					A = B.parent();
					a(c).css("opacity", 0).bind({
						"focus.uniform": function() {
							A.addClass(e.focusClass)
						},
						"blur.uniform": function() {
							A.removeClass(e.focusClass)
						},
						"click.uniform touchend.uniform": function() {
							if (a(c).attr("checked")) {
								var b = e.radioClass.split(" ")[0];
								a("." + b + " span." + e.checkedClass + ":has([name='" + a(c).attr("name") + "'])").removeClass(e.checkedClass);
								B.addClass(e.checkedClass)
							} else B.removeClass(e.checkedClass)
						},
						"mousedown.uniform touchend.uniform": function() {
							a(c).is(":disabled") || A.addClass(e.activeClass)
						},
						"mouseup.uniform touchbegin.uniform": function() {
							A.removeClass(e.activeClass)
						},
						"mouseenter.uniform touchend.uniform": function() {
							A.addClass(e.hoverClass)
						},
						"mouseleave.uniform": function() {
							A.removeClass(e.hoverClass);
							A.removeClass(e.activeClass)
						}
					});
					a(c).attr("checked") && B.addClass(e.checkedClass);
					a(c).attr("disabled") && A.addClass(e.disabledClass);
					b(c)
				} else if (c.is(":file")) {
					var r = a(c),
					p = a("<div />"),
					z = a("<span>" + e.fileDefaultText + "</span>"),
					d = a("<span>" + e.fileBtnText + "</span>");
					"none" == !r.css("display") && e.autoHide && p.hide();
					p.addClass(e.fileClass);
					z.addClass(e.filenameClass);
					d.addClass(e.fileBtnClass);
					e.useID && "" != r.attr("id") && p.attr("id", e.idPrefix + "-" + r.attr("id"));
					r.wrap(p);
					r.after(d);
					r.after(z);
					p = r.closest("div");
					z = r.siblings("." + e.filenameClass);
					d = r.siblings("." + e.fileBtnClass);
					if (!r.attr("size")) {
						var q = p.width();
						r.attr("size", q / 10)
					}
					var F = function() {
						var a = r.val();
						"" === a ? a = e.fileDefaultText: (a = a.split(/[\/\\]+/), a = a[a.length - 1]);
						z.text(a)
					};
					F();
					r.css("opacity", 0).bind({
						"focus.uniform": function() {
							p.addClass(e.focusClass)
						},
						"blur.uniform": function() {
							p.removeClass(e.focusClass)
						},
						"mousedown.uniform": function() {
							a(c).is(":disabled") || p.addClass(e.activeClass)
						},
						"mouseup.uniform": function() {
							p.removeClass(e.activeClass)
						},
						"mouseenter.uniform": function() {
							p.addClass(e.hoverClass)
						},
						"mouseleave.uniform": function() {
							p.removeClass(e.hoverClass);
							p.removeClass(e.activeClass)
						}
					});
					a.browser.msie ? r.bind("click.uniform.ie7",
					function() {
						setTimeout(F, 0)
					}) : r.bind("change.uniform", F);
					r.attr("disabled") && p.addClass(e.disabledClass);
					a.uniform.noSelect(z);
					a.uniform.noSelect(d);
					b(c)
				} else if (c.is(":text, :password, input[type='email']")) $el = a(c),
				$el.addClass($el.attr("type")),
				b(c);
				else if (c.is("textarea")) a(c).addClass("uniform"),
				b(c);
				else if (c.is("a") || c.is(":submit") || c.is(":reset") || c.is("button") || c.is("input[type=button]")) {
					var d = a(c),
					D = a("<div>"),
					q = a("<span>");
					D.addClass(e.buttonClass);
					e.useID && "" != d.attr("id") && D.attr("id", e.idPrefix + "-" + d.attr("id"));
					var E;
					if (d.is("a") || d.is("button")) E = d.text();
					else if (d.is(":submit") || d.is(":reset") || d.is("input[type=button]")) E = d.attr("value");
					E = "" == E ? d.is(":reset") ? "Reset": "Submit": E;
					q.html(E);
					d.css("opacity", 0);
					d.wrap(D);
					d.wrap(q);
					D = d.closest("div");
					q = d.closest("span");
					d.is(":disabled") && D.addClass(e.disabledClass);
					D.bind({
						"mouseenter.uniform": function() {
							D.addClass(e.hoverClass)
						},
						"mouseleave.uniform": function() {
							D.removeClass(e.hoverClass);
							D.removeClass(e.activeClass)
						},
						"mousedown.uniform touchbegin.uniform": function() {
							D.addClass(e.activeClass)
						},
						"mouseup.uniform touchend.uniform": function() {
							D.removeClass(e.activeClass)
						},
						"click.uniform touchend.uniform": function(b) {
							if (a(b.target).is("span") || a(b.target).is("div")) c[0].dispatchEvent ? (b = document.createEvent("MouseEvents"), b.initEvent("click", !0, !0), c[0].dispatchEvent(b)) : c[0].click()
						}
					});
					c.bind({
						"focus.uniform": function() {
							D.addClass(e.focusClass)
						},
						"blur.uniform": function() {
							D.removeClass(e.focusClass)
						}
					});
					a.uniform.noSelect(D);
					b(c)
				}
			}
		})
	}
})(jQuery); (function(a) {
	a.fn.extend({
		elastic: function() {
			var e = "paddingTop paddingRight paddingBottom paddingLeft fontSize lineHeight fontFamily width fontWeight".split(" ");
			return this.each(function() {
				function b(a, b) {
					curratedHeight = Math.floor(parseInt(a, 10));
					g.height() != curratedHeight && g.css({
						height: curratedHeight + "px",
						overflow: b
					})
				}
				function c() {
					var a = g.val().replace(/&/g, "&amp;").replace(/  /g, "&nbsp;").replace(/<|>/g, "&gt;").replace(/\n/g, "<br />"),
					c = d.html().replace(/<br>/ig, "<br />");
					a + "&nbsp;" != c && (d.html(a + "&nbsp;"), 3 < Math.abs(d.height() + n - g.height()) && (a = d.height() + n, a >= k ? b(k, "auto") : a <= j ? b(j, "hidden") : b(a, "hidden")))
				}
				if ("textarea" != this.type) return ! 1;
				var g = a(this),
				d = a("<div />").css({
					position: "absolute",
					display: "none",
					"word-wrap": "break-word"
				}),
				n = parseInt(g.css("line-height"), 10) || parseInt(g.css("font-size"), "10"),
				j = parseInt(g.css("height"), 10) || 3 * n,
				k = parseInt(g.css("max-height"), 10) || Number.MAX_VALUE,
				t = 0;
				0 > k && (k = Number.MAX_VALUE);
				d.appendTo(g.parent());
				for (t = e.length; t--;) d.css(e[t].toString(), g.css(e[t].toString()));
				g.css({
					overflow: "hidden"
				});
				g.bind("keyup change cut paste",
				function() {
					c()
				});
				g.bind("blur",
				function() {
					d.height() < k && (d.height() > j ? g.height(d.height()) : g.height(j))
				});
				g.live("input paste",
				function() {
					setTimeout(c, 250)
				});
				c()
			})
		}
	})
})(jQuery); (function(a) {
	a.fn.miniColors = function(e, b) {
		var c = function(a) {
			g(a);
			a.attr("disabled", !0);
			a.data("trigger").css("opacity", 0.5)
		},
		g = function(b) {
			b || (b = ".miniColors");
			a(b).each(function() {
				var b = a(this).data("selector");
				a(this).removeData("selector");
				a(b).fadeOut(100,
				function() {
					a(this).remove()
				})
			});
			a(document).unbind("mousedown.miniColors");
			a(document).unbind("mousemove.miniColors")
		},
		d = function(b, c) {
			var d = b.data("colorPicker");
			d.hide();
			var g = {
				x: c.clientX - b.data("selector").find(".miniColors-colors").offset().left + a(document).scrollLeft() - 5,
				y: c.clientY - b.data("selector").find(".miniColors-colors").offset().top + a(document).scrollTop() - 5
			}; - 5 >= g.x && (g.x = -5);
			144 <= g.x && (g.x = 144); - 5 >= g.y && (g.y = -5);
			144 <= g.y && (g.y = 144);
			b.data("colorPosition", g);
			d.css("left", g.x).css("top", g.y).show();
			d = Math.round(0.67 * (g.x + 5));
			0 > d && (d = 0);
			100 < d && (d = 100);
			g = 100 - Math.round(0.67 * (g.y + 5));
			0 > g && (g = 0);
			100 < g && (g = 100);
			var e = b.data("hsb");
			e.s = d;
			e.b = g;
			j(b, e, !0)
		},
		n = function(b, c) {
			var d = b.data("huePicker");
			d.hide();
			var g = {
				y: c.clientY - b.data("selector").find(".miniColors-colors").offset().top + a(document).scrollTop() - 1
			}; - 1 >= g.y && (g.y = -1);
			149 <= g.y && (g.y = 149);
			b.data("huePosition", g);
			d.css("top", g.y).show();
			d = Math.round(2.4 * (150 - g.y - 1));
			0 > d && (d = 0);
			360 < d && (d = 360);
			g = b.data("hsb");
			g.h = d;
			j(b, g, !0)
		},
		j = function(a, b, c) {
			a.data("hsb", b);
			var d = r(B(b));
			c && a.val("#" + d);
			a.data("trigger").css("backgroundColor", "#" + d);
			a.data("selector") && a.data("selector").find(".miniColors-colors").css("backgroundColor", "#" + r(B({
				h: b.h,
				s: 100,
				b: 100
			})));
			a.data("change") && a.data("change").call(a, "#" + d, B(b))
		},
		k = function(a) {
			var b = Math.ceil(a.s / 0.67);
			0 > b && (b = 0);
			150 < b && (b = 150);
			a = 150 - Math.ceil(a.b / 0.67);
			0 > a && (a = 0);
			150 < a && (a = 150);
			return {
				x: b - 5,
				y: a - 5
			}
		},
		t = function(a) {
			a = 150 - a.h / 2.4;
			0 > a && (h = 0);
			150 < a && (h = 150);
			return {
				y: a - 1
			}
		},
		A = function(a) {
			a = a.replace(/[^A-Fa-f0-9]/, "");
			3 == a.length && (a = a[0] + a[0] + a[1] + a[1] + a[2] + a[2]);
			return 6 === a.length ? a: null
		},
		B = function(a) {
			var b, c, d;
			b = Math.round(a.h);
			var g = Math.round(255 * a.s / 100);
			a = Math.round(255 * a.b / 100);
			if (0 == g) b = c = d = a;
			else {
				var g = (255 - g) * a / 255,
				e = (a - g) * (b % 60) / 60;
				360 == b && (b = 0);
				60 > b ? (b = a, d = g, c = g + e) : 120 > b ? (c = a, d = g, b = a - e) : 180 > b ? (c = a, b = g, d = g + e) : 240 > b ? (d = a, b = g, c = a - e) : 300 > b ? (d = a, c = g, b = g + e) : 360 > b ? (b = a, c = g, d = a - e) : d = c = b = 0
			}
			return {
				r: Math.round(b),
				g: Math.round(c),
				b: Math.round(d)
			}
		},
		r = function(b) {
			var c = [b.r.toString(16), b.g.toString(16), b.b.toString(16)];
			a.each(c,
			function(a, b) {
				1 == b.length && (c[a] = "0" + b)
			});
			return c.join("")
		},
		p = function(a) {
			var b = a,
			b = parseInt( - 1 < b.indexOf("#") ? b.substring(1) : b, 16);
			a = b >> 16;
			var c = (b & 65280) >> 8,
			b = b & 255,
			d = {
				h: 0,
				s: 0,
				b: 0
			},
			g = Math.min(a, c, b),
			e = Math.max(a, c, b),
			g = e - g;
			d.b = e;
			d.s = 0 != e ? 255 * g / e: 0;
			d.h = 0 != d.s ? a == e ? (c - b) / g: c == e ? 2 + (b - a) / g: 4 + (a - c) / g: -1;
			d.h *= 60;
			0 > d.h && (d.h += 360);
			d.s *= 100 / 255;
			d.b *= 100 / 255;
			0 === d.s && (d.h = 360);
			return d
		};
		switch (e) {
		case "readonly":
			return a(this).each(function() {
				a(this).attr("readonly", b)
			}),
			a(this);
		case "disabled":
			return a(this).each(function() {
				if (b) c(a(this));
				else {
					var d = a(this);
					d.attr("disabled", !1);
					d.data("trigger").css("opacity", 1)
				}
			}),
			a(this);
		case "value":
			return a(this).each(function() {
				"string" !== typeof b && (b = r(B(b)));
				a(this).val(b).trigger("keyup")
			}),
			a(this);
		case "destroy":
			return a(this).each(function() {
				var b = a(this);
				g();
				b = a(b);
				b.data("trigger").remove();
				b.removeAttr("autocomplete");
				b.removeData("trigger");
				b.removeData("selector");
				b.removeData("hsb");
				b.removeData("huePicker");
				b.removeData("colorPicker");
				b.removeData("mousebutton");
				b.removeData("moving");
				b.unbind("click.miniColors");
				b.unbind("focus.miniColors");
				b.unbind("blur.miniColors");
				b.unbind("keyup.miniColors");
				b.unbind("keydown.miniColors");
				b.unbind("paste.miniColors");
				a(document).unbind("mousedown.miniColors");
				a(document).unbind("mousemove.miniColors")
			}),
			a(this);
		default:
			return e || (e = {}),
			a(this).each(function() {
				if ("input" === a(this)[0].tagName.toLowerCase() && !a(this).data("trigger")) {
					var b = a(this),
					q = e,
					F = A(b.val());
					F || (F = "FFFFFF");
					var D = p(F),
					F = a('<a class="miniColors-trigger" style="background-color: #' + F + '" href="#"></a>');
					F.insertAfter(b);
					b.addClass("miniColors").attr("maxlength", 7).attr("autocomplete", "off");
					b.data("trigger", F);
					b.data("hsb", D);
					q.change && b.data("change", q.change);
					q.readonly && b.attr("readonly", !0);
					q.disabled && c(b);
					F.bind("click.miniColors",
					function(a) {
						a.preventDefault();
						b.trigger("focus")
					});
					b.bind("focus.miniColors",
					function() {
						if (!b.attr("disabled")) {
							g();
							var c = a('<div class="miniColors-selector"></div>');
							c.append('<div class="miniColors-colors" style="background-color: #FFF;"><div class="miniColors-colorPicker"></div></div>');
							c.append('<div class="miniColors-hues"><div class="miniColors-huePicker"></div></div>');
							c.css({
								top: b.is(":visible") ? b.offset().top + b.outerHeight() : b.data("trigger").offset().top + b.data("trigger").outerHeight(),
								left: b.is(":visible") ? b.offset().left: b.data("trigger").offset().left,
								display: "none"
							}).addClass(b.attr("class"));
							var e = b.data("hsb");
							c.find(".miniColors-colors").css("backgroundColor", "#" + r(B({
								h: e.h,
								s: 100,
								b: 100
							})));
							var j = b.data("colorPosition");
							j || (j = k(e));
							c.find(".miniColors-colorPicker").css("top", j.y + "px").css("left", j.x + "px"); (j = b.data("huePosition")) || (j = t(e));
							c.find(".miniColors-huePicker").css("top", j.y + "px");
							b.data("selector", c);
							b.data("huePicker", c.find(".miniColors-huePicker"));
							b.data("colorPicker", c.find(".miniColors-colorPicker"));
							b.data("mousebutton", 0);
							a("BODY").append(c);
							c.fadeIn(100);
							c.bind("selectstart",
							function() {
								return ! 1
							});
							a(document).bind("mousedown.miniColors",
							function(c) {
								b.data("mousebutton", 1);
								a(c.target).parents().andSelf().hasClass("miniColors-colors") && (c.preventDefault(), b.data("moving", "colors"), d(b, c));
								a(c.target).parents().andSelf().hasClass("miniColors-hues") && (c.preventDefault(), b.data("moving", "hues"), n(b, c));
								a(c.target).parents().andSelf().hasClass("miniColors-selector") ? c.preventDefault() : a(c.target).parents().andSelf().hasClass("miniColors") || g(b)
							});
							a(document).bind("mouseup.miniColors",
							function() {
								b.data("mousebutton", 0);
								b.removeData("moving")
							});
							a(document).bind("mousemove.miniColors",
							function(a) {
								1 === b.data("mousebutton") && ("colors" === b.data("moving") && d(b, a), "hues" === b.data("moving") && n(b, a))
							})
						}
					});
					b.bind("blur.miniColors",
					function() {
						var a = A(b.val());
						b.val(a ? "#" + a: "")
					});
					b.bind("keydown.miniColors",
					function(a) {
						9 === a.keyCode && g(b)
					});
					b.bind("keyup.miniColors",
					function() {
						var c = b.val().replace(/[^A-F0-9#]/ig, "");
						b.val(c);
						if (c = A(b.val())) {
							var c = p(c),
							d = b.data("hsb");
							c.h === d.h && c.s === d.s && c.b === d.b || (d = k(c), a(b.data("colorPicker")).css("top", d.y + "px").css("left", d.x + "px"), d = t(c), a(b.data("huePicker")).css("top", d.y + "px"), j(b, c, !1));
							c = !0
						} else c = !1;
						c || b.data("trigger").css("backgroundColor", "#FFF")
					});
					b.bind("paste.miniColors",
					function() {
						setTimeout(function() {
							b.trigger("keyup")
						},
						5)
					})
				}
			}),
			a(this)
		}
	}
})(jQuery); (function(a) {
	var e = 0;
	a.ajaxTransport("iframe",
	function(b) {
		if ("POST" === b.type || "GET" === b.type) {
			var c, g;
			return {
				send: function(d, n) {
					c = a('<form style="display:none;"></form>');
					g = a('<iframe src="javascript:false;" name="iframe-transport-' + (e += 1) + '"></iframe>').bind("load",
					function() {
						var d;
						g.unbind("load").bind("load",
						function() {
							var b;
							try {
								b = g.contents()
							} catch(d) {
								b = a()
							}
							n(200, "success", {
								iframe: b
							});
							a('<iframe src="javascript:false;"></iframe>').appendTo(c);
							c.remove()
						});
						c.prop("target", g.prop("name")).prop("action", b.url).prop("method", b.type);
						b.formData && a.each(b.formData,
						function(b, d) {
							a('<input type="hidden"/>').prop("name", d.name).val(d.value).appendTo(c)
						});
						b.fileInput && (b.fileInput.length && "POST" === b.type) && (d = b.fileInput.clone(), b.fileInput.after(function(a) {
							return d[a]
						}), b.paramName && b.fileInput.each(function() {
							a(this).prop("name", b.paramName)
						}), c.append(b.fileInput).prop("enctype", "multipart/form-data").prop("encoding", "multipart/form-data"));
						c.submit();
						d && d.length && b.fileInput.each(function(b, c) {
							var g = a(d[b]);
							a(c).prop("name", g.prop("name"));
							g.replaceWith(c)
						})
					});
					c.append(g).appendTo("body")
				},
				abort: function() {
					g && g.unbind("load").prop("src", "javascript".concat(":false;"));
					c && c.remove()
				}
			}
		}
	});
	a.ajaxSetup({
		converters: {
			"iframe text": function(a) {
				return a.text()
			},
			"iframe json": function(b) {
				return a.parseJSON(b.text())
			},
			"iframe html": function(a) {
				return a.find("body").html()
			},
			"iframe script": function(b) {
				return a.globalEval(b.text())
			}
		}
	})
})(jQuery); (function(a) {
	a.widget("blueimp.fileupload", {
		options: {
			namespace: void 0,
			dropZone: a(document),
			fileInput: void 0,
			replaceFileInput: !0,
			paramName: void 0,
			singleFileUploads: !0,
			sequentialUploads: !1,
			forceIframeTransport: !1,
			multipart: !0,
			maxChunkSize: void 0,
			uploadedBytes: void 0,
			recalculateProgress: !0,
			formData: function(a) {
				return a.serializeArray()
			},
			add: function(a, b) {
				b.submit()
			},
			processData: !1,
			contentType: !1,
			cache: !1
		},
		_refreshOptionsList: ["namespace", "dropZone", "fileInput"],
		_isXHRUpload: function(a) {
			return ! a.forceIframeTransport && "undefined" !== typeof XMLHttpRequestUpload && "undefined" !== typeof File && (!a.multipart || "undefined" !== typeof FormData)
		},
		_getFormData: function(e) {
			var b;
			return "function" === typeof e.formData ? e.formData(e.form) : a.isArray(e.formData) ? e.formData: e.formData ? (b = [], a.each(e.formData,
			function(a, g) {
				b.push({
					name: a,
					value: g
				})
			}), b) : []
		},
		_getTotal: function(e) {
			var b = 0;
			a.each(e,
			function(a, g) {
				b += g.size || 1
			});
			return b
		},
		_onProgress: function(a, b) {
			if (a.lengthComputable) {
				var c = b.total || this._getTotal(b.files),
				g = parseInt(a.loaded / a.total * (b.chunkSize || c), 10) + (b.uploadedBytes || 0);
				this._loaded += g - (b.loaded || b.uploadedBytes || 0);
				b.lengthComputable = !0;
				b.loaded = g;
				b.total = c;
				this._trigger("progress", a, b);
				this._trigger("progressall", a, {
					lengthComputable: !0,
					loaded: this._loaded,
					total: this._total
				})
			}
		},
		_initProgressListener: function(e) {
			var b = this,
			c = e.xhr ? e.xhr() : a.ajaxSettings.xhr();
			c.upload && c.upload.addEventListener && (c.upload.addEventListener("progress",
			function(a) {
				b._onProgress(a, e)
			},
			!1), e.xhr = function() {
				return c
			})
		},
		_initXHRData: function(e) {
			var b, c = e.files[0];
			if (!e.multipart || e.blob) e.headers = a.extend(e.headers, {
				"X-File-Name": c.name,
				"X-File-Type": c.type,
				"X-File-Size": c.size
			}),
			e.blob ? e.multipart || (e.contentType = "application/octet-stream", e.data = e.blob) : (e.contentType = c.type, e.data = c);
			e.multipart && "undefined" !== typeof FormData && (e.formData instanceof FormData ? b = e.formData: (b = new FormData, a.each(this._getFormData(e),
			function(a, c) {
				b.append(c.name, c.value)
			})), e.blob ? b.append(e.paramName, e.blob) : a.each(e.files,
			function(a, c) {
				c instanceof Blob && b.append(e.paramName, c)
			}), e.data = b);
			e.blob = null
		},
		_initIframeSettings: function(a) {
			a.dataType = "iframe " + (a.dataType || "");
			a.formData = this._getFormData(a)
		},
		_initDataSettings: function(a) {
			this._isXHRUpload(a) ? this._chunkedUpload(a, !0) || (a.data || this._initXHRData(a), this._initProgressListener(a)) : this._initIframeSettings(a)
		},
		_initFormSettings: function(e) {
			if (!e.form || !e.form.length) e.form = a(e.fileInput.prop("form"));
			e.paramName || (e.paramName = e.fileInput.prop("name") || "files[]");
			e.url || (e.url = e.form.prop("action") || location.href);
			e.type = (e.type || e.form.prop("method") || "").toUpperCase();
			"POST" !== e.type && "PUT" !== e.type && (e.type = "POST")
		},
		_getAJAXSettings: function(e) {
			e = a.extend({},
			this.options, e);
			this._initFormSettings(e);
			this._initDataSettings(e);
			return e
		},
		_enhancePromise: function(a) {
			a.success = a.done;
			a.error = a.fail;
			a.complete = a.always;
			return a
		},
		_getXHRPromise: function(e, b, c) {
			var g = a.Deferred(),
			d = g.promise();
			b = b || this.options.context || d; ! 0 === e ? g.resolveWith(b, c) : !1 === e && g.rejectWith(b, c);
			d.abort = g.promise;
			return this._enhancePromise(d)
		},
		_chunkedUpload: function(e, b) {
			var c = this,
			g = e.files[0],
			d = g.size,
			n = e.uploadedBytes = e.uploadedBytes || 0,
			j = e.maxChunkSize || d,
			k = g.webkitSlice || g.mozSlice || g.slice,
			t,
			A;
			if (!this._isXHRUpload(e) || (!k || !(n || j < d)) || e.data) return ! 1;
			if (b) return ! 0;
			if (n >= d) return g.error = "uploadedBytes",
			this._getXHRPromise(!1);
			d = Math.ceil((d - n) / j);
			t = function(b) {
				return ! b ? c._getXHRPromise(!0) : t(b -= 1).pipe(function() {
					var d = a.extend({},
					e);
					d.blob = k.call(g, n + b * j, n + (b + 1) * j);
					d.chunkSize = d.blob.size;
					c._initXHRData(d);
					c._initProgressListener(d);
					return A = (a.ajax(d) || c._getXHRPromise(!1, d.context)).done(function() {
						d.loaded || c._onProgress(a.Event("progress", {
							lengthComputable: !0,
							loaded: d.chunkSize,
							total: d.chunkSize
						}), d);
						e.uploadedBytes = d.uploadedBytes += d.chunkSize
					})
				})
			};
			d = t(d);
			d.abort = function() {
				return A.abort()
			};
			return this._enhancePromise(d)
		},
		_beforeSend: function(a, b) {
			0 === this._active && this._trigger("start");
			this._active += 1;
			this._loaded += b.uploadedBytes || 0;
			this._total += this._getTotal(b.files)
		},
		_onDone: function(e, b, c, g) {
			this._isXHRUpload(g) || this._onProgress(a.Event("progress", {
				lengthComputable: !0,
				loaded: 1,
				total: 1
			}), g);
			g.result = e;
			g.textStatus = b;
			g.jqXHR = c;
			this._trigger("done", null, g)
		},
		_onFail: function(a, b, c, g) {
			g.jqXHR = a;
			g.textStatus = b;
			g.errorThrown = c;
			this._trigger("fail", null, g);
			g.recalculateProgress && (this._loaded -= g.loaded || g.uploadedBytes || 0, this._total -= g.total || this._getTotal(g.files))
		},
		_onAlways: function(a, b, c, g, d) {
			this._active -= 1;
			d.result = a;
			d.textStatus = b;
			d.jqXHR = c;
			d.errorThrown = g;
			this._trigger("always", null, d);
			0 === this._active && (this._trigger("stop"), this._loaded = this._total = 0)
		},
		_onSend: function(e, b) {
			var c = this,
			g, d, n = c._getAJAXSettings(b),
			j = function(b, d) {
				return g = g || (!1 !== b && !1 !== c._trigger("send", e, n) && (c._chunkedUpload(n) || a.ajax(n)) || c._getXHRPromise(!1, n.context, d)).done(function(a, b, d) {
					c._onDone(a, b, d, n)
				}).fail(function(a, b, d) {
					c._onFail(a, b, d, n)
				}).always(function(a, b, d) { ! d || "string" === typeof d ? c._onAlways(void 0, b, a, d, n) : c._onAlways(a, b, d, void 0, n)
				})
			};
			this._beforeSend(e, n);
			return this.options.sequentialUploads ? (d = this._sequence = this._sequence.pipe(j, j), d.abort = function() {
				return ! g ? j(!1, [void 0, "abort", "abort"]) : g.abort()
			},
			this._enhancePromise(d)) : j()
		},
		_onAdd: function(e, b) {
			var c = this,
			g = !0,
			d = a.extend({},
			this.options, b);
			if (d.singleFileUploads && this._isXHRUpload(d)) return a.each(b.files,
			function(d, j) {
				var k = a.extend({},
				b, {
					files: [j]
				});
				k.submit = function() {
					return c._onSend(e, k)
				};
				return g = c._trigger("add", e, k)
			}),
			g;
			if (b.files.length) return b = a.extend({},
			b),
			b.submit = function() {
				return c._onSend(e, b)
			},
			this._trigger("add", e, b)
		},
		_normalizeFile: function(a, b) {
			void 0 === b.name && void 0 === b.size && (b.name = b.fileName, b.size = b.fileSize)
		},
		_replaceFileInput: function(e) {
			var b = e.clone(!0);
			a("<form></form>").append(b)[0].reset();
			e.after(b).detach();
			this.options.fileInput = this.options.fileInput.map(function(a, g) {
				return g === e[0] ? b[0] : g
			})
		},
		_onChange: function(e) {
			var b = e.data.fileupload,
			c = {
				files: a.each(a.makeArray(e.target.files), b._normalizeFile),
				fileInput: a(e.target),
				form: a(e.target.form)
			};
			c.files.length || (c.files = [{
				name: e.target.value.replace(/^.*\\/, "")
			}]);
			c.form.length ? c.fileInput.data("blueimp.fileupload.form", c.form) : c.form = c.fileInput.data("blueimp.fileupload.form");
			b.options.replaceFileInput && b._replaceFileInput(c.fileInput);
			if (!1 === b._trigger("change", e, c) || !1 === b._onAdd(e, c)) return ! 1
		},
		_onDrop: function(e) {
			var b = e.data.fileupload,
			c = e.dataTransfer = e.originalEvent.dataTransfer,
			c = {
				files: a.each(a.makeArray(c && c.files), b._normalizeFile)
			};
			if (!1 === b._trigger("drop", e, c) || !1 === b._onAdd(e, c)) return ! 1;
			e.preventDefault()
		},
		_onDragOver: function(a) {
			var b = a.data.fileupload,
			c = a.dataTransfer = a.originalEvent.dataTransfer;
			if (!1 === b._trigger("dragover", a)) return ! 1;
			c && (c.dropEffect = c.effectAllowed = "copy");
			a.preventDefault()
		},
		_initEventHandlers: function() {
			var a = this.options.namespace || this.name;
			this.options.dropZone.bind("dragover." + a, {
				fileupload: this
			},
			this._onDragOver).bind("drop." + a, {
				fileupload: this
			},
			this._onDrop);
			this.options.fileInput.bind("change." + a, {
				fileupload: this
			},
			this._onChange)
		},
		_destroyEventHandlers: function() {
			var a = this.options.namespace || this.name;
			this.options.dropZone.unbind("dragover." + a, this._onDragOver).unbind("drop." + a, this._onDrop);
			this.options.fileInput.unbind("change." + a, this._onChange)
		},
		_beforeSetOption: function() {},
		_afterSetOption: function() {
			var e = this.options;
			e.fileInput || (e.fileInput = a());
			e.dropZone || (e.dropZone = a());
			this._initEventHandlers()
		},
		_setOption: function(e, b) {
			var c = -1 !== a.inArray(e, this._refreshOptionsList);
			c && this._beforeSetOption(e, b);
			a.Widget.prototype._setOption.call(this, e, b);
			c && this._afterSetOption(e, b)
		},
		_create: function() {
			var e = this.options;
			void 0 === e.fileInput ? e.fileInput = this.element.is("input:file") ? this.element: this.element.find("input:file") : e.fileInput || (e.fileInput = a());
			e.dropZone || (e.dropZone = a());
			this._sequence = this._getXHRPromise(!0);
			this._active = this._loaded = this._total = 0;
			this._initEventHandlers()
		},
		destroy: function() {},
		enable: function() {
			a.Widget.prototype.enable.call(this);
			this._initEventHandlers()
		},
		disable: function() {
			this._destroyEventHandlers();
			a.Widget.prototype.disable.call(this)
		},
		add: function(e) {
			e && !this.options.disabled && (e.files = a.each(a.makeArray(e.files), this._normalizeFile), this._onAdd(null, e))
		},
		send: function(e) {
			return e && !this.options.disabled && (e.files = a.each(a.makeArray(e.files), this._normalizeFile), e.files.length) ? this._onSend(null, e) : this._getXHRPromise(!1, e && e.context)
		}
	})
})(jQuery); (function(a) {
	var e, b, c, g, d, n, j, k, t, A, B = 0,
	r = {},
	p = [],
	z = 0,
	q = {},
	F = [],
	D = null,
	E = new Image,
	V = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i,
	ha = /[^\.]\.(swf)\s*$/i,
	W,
	sa = 1,
	S = 0,
	U = "",
	ba,
	ia,
	ga = !1,
	Y = a.extend(a("<div/>")[0], {
		prop: 0
	}),
	xa = a.browser.msie && 7 > a.browser.version && !window.XMLHttpRequest,
	ua = function() {
		b.hide();
		E.onerror = E.onload = null;
		D && D.abort();
		e.empty()
	},
	ta = function() { ! 1 === r.onError(p, B, r) ? (b.hide(), ga = !1) : (r.titleShow = !1, r.width = "auto", r.height = "auto", e.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>'), N())
	},
	va = function() {
		var c = p[B],
		d,
		g,
		t,
		j,
		A,
		k;
		ua();
		r = a.extend({},
		a.fn.fancybox.defaults, "undefined" == typeof a(c).data("fancybox") ? r: a(c).data("fancybox"));
		k = r.onStart(p, B, r);
		if (!1 === k) ga = !1;
		else {
			"object" == typeof k && (r = a.extend(r, k));
			t = r.title || (c.nodeName ? a(c).attr("title") : c.title) || "";
			c.nodeName && !r.orig && (r.orig = a(c).children("img:first").length ? a(c).children("img:first") : a(c));
			"" === t && (r.orig && r.titleFromAlt) && (t = r.orig.attr("alt"));
			d = r.href || (c.nodeName ? a(c).attr("href") : c.href) || null;
			if (/^(?:javascript)/i.test(d) || "#" == d) d = null;
			r.type ? (g = r.type, d || (d = r.content)) : r.content ? g = "html": d && (g = d.match(V) ? "image": d.match(ha) ? "swf": a(c).hasClass("iframe") ? "iframe": 0 === d.indexOf("#") ? "inline": "ajax");
			if (g) switch ("inline" == g && (c = d.substr(d.indexOf("#")), g = 0 < a(c).length ? "inline": "ajax"), r.type = g, r.href = d, r.title = t, r.autoDimensions && ("html" == r.type || "inline" == r.type || "ajax" == r.type ? (r.width = "auto", r.height = "auto") : r.autoDimensions = !1), r.modal && (r.overlayShow = !0, r.hideOnOverlayClick = !1, r.hideOnContentClick = !1, r.enableEscapeButton = !1, r.showCloseButton = !1), r.padding = parseInt(r.padding, 10), r.margin = parseInt(r.margin, 10), e.css("padding", r.padding + r.margin), a(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change",
			function() {
				a(this).replaceWith(n.children())
			}), g) {
			case "html":
				e.html(r.content);
				N();
				break;
			case "inline":
				if (!0 === a(c).parent().is("#fancybox-content")) {
					ga = !1;
					break
				}
				a('<div class="fancybox-inline-tmp" />').hide().insertBefore(a(c)).bind("fancybox-cleanup",
				function() {
					a(this).replaceWith(n.children())
				}).bind("fancybox-cancel",
				function() {
					a(this).replaceWith(e.children())
				});
				a(c).appendTo(e);
				N();
				break;
			case "image":
				ga = !1;
				a.fancybox.showActivity();
				E = new Image;
				E.onerror = function() {
					ta()
				};
				E.onload = function() {
					ga = !0;
					E.onerror = E.onload = null;
					r.width = E.width;
					r.height = E.height;
					a("<img />").attr({
						id: "fancybox-img",
						src: E.src,
						alt: r.title
					}).appendTo(e);
					na()
				};
				E.src = d;
				break;
			case "swf":
				r.scrolling = "no";
				j = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + r.width + '" height="' + r.height + '"><param name="movie" value="' + d + '"></param>';
				A = "";
				a.each(r.swf,
				function(a, b) {
					j += '<param name="' + a + '" value="' + b + '"></param>';
					A += " " + a + '="' + b + '"'
				});
				j += '<embed src="' + d + '" type="application/x-shockwave-flash" width="' + r.width + '" height="' + r.height + '"' + A + "></embed></object>";
				e.html(j);
				N();
				break;
			case "ajax":
				ga = !1;
				a.fancybox.showActivity();
				r.ajax.win = r.ajax.success;
				D = a.ajax(a.extend({},
				r.ajax, {
					url: d,
					data: r.ajax.data || {},
					error: function(a) {
						0 < a.status && ta()
					},
					success: function(a, c, g) {
						if (200 == ("object" == typeof g ? g: D).status) {
							if ("function" == typeof r.ajax.win) {
								k = r.ajax.win(d, a, c, g);
								if (!1 === k) {
									b.hide();
									return
								}
								if ("string" == typeof k || "object" == typeof k) a = k
							}
							e.html(a);
							N()
						}
					}
				}));
				break;
			case "iframe":
				na()
			} else ta()
		}
	},
	N = function() {
		var b = r.width,
		c = r.height,
		b = -1 < b.toString().indexOf("%") ? parseInt((a(window).width() - 2 * r.margin) * parseFloat(b) / 100, 10) + "px": "auto" == b ? "auto": b + "px",
		c = -1 < c.toString().indexOf("%") ? parseInt((a(window).height() - 2 * r.margin) * parseFloat(c) / 100, 10) + "px": "auto" == c ? "auto": c + "px";
		e.wrapInner('<div style="width:' + b + ";height:" + c + ";overflow: " + ("auto" == r.scrolling ? "auto": "yes" == r.scrolling ? "scroll": "hidden") + ';position:relative;"></div>');
		r.width = e.width();
		r.height = e.height();
		na()
	},
	na = function() {
		var D, L;
		b.hide();
		if (g.is(":visible") && !1 === q.onCleanup(F, z, q)) a.event.trigger("fancybox-cancel"),
		ga = !1;
		else {
			ga = !0;
			a(n.add(c)).unbind();
			a(window).unbind("resize.fb scroll.fb");
			a(document).unbind("keydown.fb");
			g.is(":visible") && "outside" !== q.titlePosition && g.css("height", g.height());
			F = p;
			z = B;
			q = r;
			q.overlayShow ? (c.css({
				"background-color": q.overlayColor,
				opacity: q.overlayOpacity,
				cursor: q.hideOnOverlayClick ? "pointer": "auto",
				height: a(document).height()
			}), c.is(":visible") || (xa && a("select:not(#fancybox-tmp select)").filter(function() {
				return "hidden" !== this.style.visibility
			}).css({
				visibility: "hidden"
			}).one("fancybox-cleanup",
			function() {
				this.style.visibility = "inherit"
			}), c.show())) : c.hide();
			D = G();
			var C = {},
			R = q.autoScale,
			E = 2 * q.padding;
			C.width = -1 < q.width.toString().indexOf("%") ? parseInt(D[0] * parseFloat(q.width) / 100, 10) : q.width + E;
			C.height = -1 < q.height.toString().indexOf("%") ? parseInt(D[1] * parseFloat(q.height) / 100, 10) : q.height + E;
			if (R && (C.width > D[0] || C.height > D[1]))"image" == r.type || "swf" == r.type ? (R = q.width / q.height, C.width > D[0] && (C.width = D[0], C.height = parseInt((C.width - E) / R + E, 10)), C.height > D[1] && (C.height = D[1], C.width = parseInt((C.height - E) * R + E, 10))) : (C.width = Math.min(C.width, D[0]), C.height = Math.min(C.height, D[1]));
			C.top = parseInt(Math.max(D[3] - 20, D[3] + 0.5 * (D[1] - C.height - 40)), 10);
			C.left = parseInt(Math.max(D[2] - 20, D[2] + 0.5 * (D[0] - C.width - 40)), 10);
			ia = C;
			U = q.title || "";
			S = 0;
			k.empty().removeAttr("style").removeClass();
			if (!1 !== q.titleShow && (U = D = a.isFunction(q.titleFormat) ? q.titleFormat(U, F, z, q) : U && U.length ? "float" == q.titlePosition ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + U + '</td><td id="fancybox-title-float-right"></td></tr></table>': '<div id="fancybox-title-' + q.titlePosition + '">' + U + "</div>": !1) && "" !== U) switch (k.addClass("fancybox-title-" + q.titlePosition).html(U).appendTo("body").show(), q.titlePosition) {
			case "inside":
				k.css({
					width:
					ia.width - 2 * q.padding,
					marginLeft: q.padding,
					marginRight: q.padding
				});
				S = k.outerHeight(!0);
				k.appendTo(d);
				ia.height += S;
				break;
			case "over":
				k.css({
					marginLeft:
					q.padding,
					width: ia.width - 2 * q.padding,
					bottom: q.padding
				}).appendTo(d);
				break;
			case "float":
				k.css("left", -1 * parseInt((k.width() - ia.width - 40) / 2, 10)).appendTo(g);
				break;
			default:
				k.css({
					width:
					ia.width - 2 * q.padding,
					paddingLeft: q.padding,
					paddingRight: q.padding
				}).appendTo(g)
			}
			k.hide();
			g.is(":visible") ? (a(j.add(t).add(A)).hide(), D = g.position(), ba = {
				top: D.top,
				left: D.left,
				width: g.width(),
				height: g.height()
			},
			L = ba.width == ia.width && ba.height == ia.height, n.fadeTo(q.changeFade, 0.3,
			function() {
				var b = function() {
					n.html(e.contents()).fadeTo(q.changeFade, 1, Ba)
				};
				a.event.trigger("fancybox-change");
				n.empty().removeAttr("filter").css({
					"border-width": q.padding,
					width: ia.width - 2 * q.padding,
					height: r.autoDimensions ? "auto": ia.height - S - 2 * q.padding
				});
				L ? b() : (Y.prop = 0, a(Y).animate({
					prop: 1
				},
				{
					duration: q.changeSpeed,
					easing: q.easingChange,
					step: qa,
					complete: b
				}))
			})) : (g.removeAttr("style"), n.css("border-width", q.padding), "elastic" == q.transitionIn ? (ba = w(), n.html(e.contents()), g.show(), q.opacity && (ia.opacity = 0), Y.prop = 0, a(Y).animate({
				prop: 1
			},
			{
				duration: q.speedIn,
				easing: q.easingIn,
				step: qa,
				complete: Ba
			})) : ("inside" == q.titlePosition && 0 < S && k.show(), n.css({
				width: ia.width - 2 * q.padding,
				height: r.autoDimensions ? "auto": ia.height - S - 2 * q.padding
			}).html(e.contents()), g.css(ia).fadeIn("none" == q.transitionIn ? 0 : q.speedIn, Ba)))
		}
	},
	Ba = function() {
		a.support.opacity || (n.get(0).style.removeAttribute("filter"), g.get(0).style.removeAttribute("filter"));
		r.autoDimensions && n.css("height", "auto");
		g.css("height", "auto");
		U && U.length && k.show();
		q.showCloseButton && j.show(); (q.enableEscapeButton || q.enableKeyboardNav) && a(document).bind("keydown.fb",
		function(b) {
			if (27 == b.keyCode && q.enableEscapeButton) b.preventDefault(),
			a.fancybox.close();
			else if ((37 == b.keyCode || 39 == b.keyCode) && q.enableKeyboardNav && "INPUT" !== b.target.tagName && "TEXTAREA" !== b.target.tagName && "SELECT" !== b.target.tagName) b.preventDefault(),
			a.fancybox[37 == b.keyCode ? "prev": "next"]()
		});
		q.showNavArrows ? ((q.cyclic && 1 < F.length || 0 !== z) && t.show(), (q.cyclic && 1 < F.length || z != F.length - 1) && A.show()) : (t.hide(), A.hide());
		q.hideOnContentClick && n.bind("click", a.fancybox.close);
		q.hideOnOverlayClick && c.bind("click", a.fancybox.close);
		a(window).bind("resize.fb", a.fancybox.resize);
		q.centerOnScroll && a(window).bind("scroll.fb", a.fancybox.center);
		"iframe" == q.type && a('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (a.browser.msie ? 'allowtransparency="true""': "") + ' scrolling="' + r.scrolling + '" src="' + q.href + '"></iframe>').appendTo(n);
		g.show();
		ga = !1;
		a.fancybox.center();
		q.onComplete(F, z, q);
		var b, d;
		F.length - 1 > z && (b = F[z + 1].href, "undefined" !== typeof b && b.match(V) && (d = new Image, d.src = b));
		0 < z && (b = F[z - 1].href, "undefined" !== typeof b && b.match(V) && (d = new Image, d.src = b))
	},
	qa = function(a) {
		var b = {
			width: parseInt(ba.width + (ia.width - ba.width) * a, 10),
			height: parseInt(ba.height + (ia.height - ba.height) * a, 10),
			top: parseInt(ba.top + (ia.top - ba.top) * a, 10),
			left: parseInt(ba.left + (ia.left - ba.left) * a, 10)
		};
		"undefined" !== typeof ia.opacity && (b.opacity = 0.5 > a ? 0.5 : a);
		g.css(b);
		n.css({
			width: b.width - 2 * q.padding,
			height: b.height - S * a - 2 * q.padding
		})
	},
	G = function() {
		return [a(window).width() - 2 * q.margin, a(window).height() - 2 * q.margin, a(document).scrollLeft() + q.margin, a(document).scrollTop() + q.margin]
	},
	w = function() {
		var b = r.orig ? a(r.orig) : !1,
		c = {};
		b && b.length ? (c = b.offset(), c.top += parseInt(b.css("paddingTop"), 10) || 0, c.left += parseInt(b.css("paddingLeft"), 10) || 0, c.top += parseInt(b.css("border-top-width"), 10) || 0, c.left += parseInt(b.css("border-left-width"), 10) || 0, c.width = b.width(), c.height = b.height(), c = {
			width: c.width + 2 * q.padding,
			height: c.height + 2 * q.padding,
			top: c.top - q.padding - 20,
			left: c.left - q.padding - 20
		}) : (b = G(), c = {
			width: 2 * q.padding,
			height: 2 * q.padding,
			top: parseInt(b[3] + 0.5 * b[1], 10),
			left: parseInt(b[2] + 0.5 * b[0], 10)
		});
		return c
	},
	R = function() {
		b.is(":visible") ? (a("div", b).css("top", -40 * sa + "px"), sa = (sa + 1) % 12) : clearInterval(W)
	};
	a.fn.fancybox = function(b) {
		if (!a(this).length) return this;
		a(this).data("fancybox", a.extend({},
		b, a.metadata ? a(this).metadata() : {})).unbind("click.fb").bind("click.fb",
		function(b) {
			b.preventDefault();
			ga || (ga = !0, a(this).blur(), p = [], B = 0, b = a(this).attr("rel") || "", !b || "" == b || "nofollow" === b ? p.push(this) : (p = a("a[rel=" + b + "], area[rel=" + b + "]"), B = p.index(this)), va())
		});
		return this
	};
	a.fancybox = function(b, c) {
		var d;
		if (!ga) {
			ga = !0;
			d = "undefined" !== typeof c ? c: {};
			p = [];
			B = parseInt(d.index, 10) || 0;
			if (a.isArray(b)) {
				for (var g = 0,
				e = b.length; g < e; g++)"object" == typeof b[g] ? a(b[g]).data("fancybox", a.extend({},
				d, b[g])) : b[g] = a({}).data("fancybox", a.extend({
					content: b[g]
				},
				d));
				p = jQuery.merge(p, b)
			} else "object" == typeof b ? a(b).data("fancybox", a.extend({},
			d, b)) : b = a({}).data("fancybox", a.extend({
				content: b
			},
			d)),
			p.push(b);
			if (B > p.length || 0 > B) B = 0;
			va()
		}
	};
	a.fancybox.showActivity = function() {
		clearInterval(W);
		b.show();
		W = setInterval(R, 66)
	};
	a.fancybox.hideActivity = function() {
		b.hide()
	};
	a.fancybox.next = function() {
		return a.fancybox.pos(z + 1)
	};
	a.fancybox.prev = function() {
		return a.fancybox.pos(z - 1)
	};
	a.fancybox.pos = function(a) {
		ga || (a = parseInt(a), p = F, -1 < a && a < F.length ? (B = a, va()) : q.cyclic && 1 < F.length && (B = a >= F.length ? 0 : F.length - 1, va()))
	};
	a.fancybox.cancel = function() {
		ga || (ga = !0, a.event.trigger("fancybox-cancel"), ua(), r.onCancel(p, B, r), ga = !1)
	};
	a.fancybox.close = function() {
		function b() {
			c.fadeOut("fast");
			k.empty().hide();
			g.hide();
			a.event.trigger("fancybox-cleanup");
			n.empty();
			q.onClosed(F, z, q);
			F = r = [];
			z = B = 0;
			q = r = {};
			ga = !1
		}
		if (!ga && !g.is(":hidden")) if (ga = !0, q && !1 === q.onCleanup(F, z, q)) ga = !1;
		else if (ua(), a(j.add(t).add(A)).hide(), a(n.add(c)).unbind(), a(window).unbind("resize.fb scroll.fb"), a(document).unbind("keydown.fb"), n.find("iframe").attr("src", xa && /^https/i.test(window.location.href || "") ? "javascript:void(false)": "about:blank"), "inside" !== q.titlePosition && k.empty(), g.stop(), "elastic" == q.transitionOut) {
			ba = w();
			var d = g.position();
			ia = {
				top: d.top,
				left: d.left,
				width: g.width(),
				height: g.height()
			};
			q.opacity && (ia.opacity = 1);
			k.empty().hide();
			Y.prop = 1;
			a(Y).animate({
				prop: 0
			},
			{
				duration: q.speedOut,
				easing: q.easingOut,
				step: qa,
				complete: b
			})
		} else g.fadeOut("none" == q.transitionOut ? 0 : q.speedOut, b)
	};
	a.fancybox.resize = function() {
		c.is(":visible") && c.css("height", a(document).height());
		a.fancybox.center(!0)
	};
	a.fancybox.center = function(a) {
		var b, c;
		ga || (c = !0 === a ? 1 : 0, b = G(), !c && (g.width() > b[0] || g.height() > b[1]) || g.stop().animate({
			top: parseInt(Math.max(b[3] - 20, b[3] + 0.5 * (b[1] - n.height() - 40) - q.padding)),
			left: parseInt(Math.max(b[2] - 20, b[2] + 0.5 * (b[0] - n.width() - 40) - q.padding))
		},
		"number" == typeof a ? a: 200))
	};
	a.fancybox.init = function() {
		a("#fancybox-wrap").length || (a("body").append(e = a('<div id="fancybox-tmp"></div>'), b = a('<div id="fancybox-loading"><div></div></div>'), c = a('<div id="fancybox-overlay"></div>'), g = a('<div id="fancybox-wrap"></div>')), d = a('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(g), d.append(n = a('<div id="fancybox-content"></div>'), j = a('<a id="fancybox-close"></a>'), k = a('<div id="fancybox-title"></div>'), t = a('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), A = a('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')), j.click(a.fancybox.close), b.click(a.fancybox.cancel), t.click(function(b) {
			b.preventDefault();
			a.fancybox.prev()
		}), A.click(function(b) {
			b.preventDefault();
			a.fancybox.next()
		}), a.fn.mousewheel && g.bind("mousewheel.fb",
		function(b, c) {
			if (ga) b.preventDefault();
			else if (0 == a(b.target).get(0).clientHeight || a(b.target).get(0).scrollHeight === a(b.target).get(0).clientHeight) b.preventDefault(),
			a.fancybox[0 < c ? "prev": "next"]()
		}), a.support.opacity || g.addClass("fancybox-ie"), xa && (b.addClass("fancybox-ie6"), g.addClass("fancybox-ie6"), a('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)": "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(d)))
	};
	a.fn.fancybox.defaults = {
		padding: 10,
		margin: 40,
		opacity: !1,
		modal: !1,
		cyclic: !1,
		scrolling: "auto",
		width: 560,
		height: 340,
		autoScale: !0,
		autoDimensions: !0,
		centerOnScroll: !1,
		ajax: {},
		swf: {
			wmode: "transparent"
		},
		hideOnOverlayClick: !0,
		hideOnContentClick: !1,
		overlayShow: !0,
		overlayOpacity: 0.7,
		overlayColor: "#777",
		titleShow: !0,
		titlePosition: "float",
		titleFormat: null,
		titleFromAlt: !1,
		transitionIn: "fade",
		transitionOut: "fade",
		speedIn: 300,
		speedOut: 300,
		changeSpeed: 300,
		changeFade: "fast",
		easingIn: "swing",
		easingOut: "swing",
		showCloseButton: !0,
		showNavArrows: !0,
		enableEscapeButton: !0,
		enableKeyboardNav: !0,
		onStart: function() {},
		onCancel: function() {},
		onComplete: function() {},
		onCleanup: function() {},
		onClosed: function() {},
		onError: function() {}
	};
	a(document).ready(function() {
		a.fancybox.init()
	})
})(jQuery); (function(a) {
	function e() {
		if (d.jStorage) try {
			g = t(String(d.jStorage))
		} catch(a) {
			d.jStorage = "{}"
		} else d.jStorage = "{}";
		j = d.jStorage ? String(d.jStorage).length: 0
	}
	function b() {
		try {
			d.jStorage = k(g),
			n && (n.setAttribute("jStorage", d.jStorage), n.save("jStorage")),
			j = d.jStorage ? String(d.jStorage).length: 0
		} catch(a) {}
	}
	function c(a) {
		if (!a || "string" != typeof a && "number" != typeof a) throw new TypeError("Key name must be string or numeric");
		return ! 0
	}
	if (!a || !a.toJSON && (!Object.toJSON && !window.JSON) && !a.browser.msie) throw Error("jQuery, MooTools or Prototype needs to be loaded before jStorage!");
	var g = {},
	d = {
		jStorage: "{}"
	},
	n = null,
	j = 0,
	k = a.toJSON || Object.toJSON || window.JSON && (JSON.encode || JSON.stringify),
	t = a.evalJSON || window.JSON && (JSON.decode || JSON.parse) ||
	function(a) {
		return String(a).evalJSON()
	},
	A = !1;
	_XMLService = {
		isXML: function(a) {
			return (a = (a ? a.ownerDocument || a: 0).documentElement) ? "HTML" !== a.nodeName: !1
		},
		encode: function(a) {
			if (!this.isXML(a)) return ! 1;
			try {
				return (new XMLSerializer).serializeToString(a)
			} catch(b) {
				try {
					return a.xml
				} catch(c) {}
			}
			return ! 1
		},
		decode: function(a) {
			var b = "DOMParser" in window && (new DOMParser).parseFromString || window.ActiveXObject &&
			function(a) {
				var b = new ActiveXObject("Microsoft.XMLDOM");
				b.async = "false";
				b.loadXML(a);
				return b
			};
			if (!b) return ! 1;
			a = b.call("DOMParser" in window && new DOMParser || window, a, "text/xml");
			return this.isXML(a) ? a: !1
		}
	};
	a.jStorage = {
		version: "0.1.5.2",
		set: function(a, d) {
			c(a);
			_XMLService.isXML(d) && (d = {
				_is_xml: !0,
				xml: _XMLService.encode(d)
			});
			g[a] = d;
			b();
			return d
		},
		get: function(a, b) {
			c(a);
			return a in g ? g[a] && "object" == typeof g[a] && g[a]._is_xml && g[a]._is_xml ? _XMLService.decode(g[a].xml) : g[a] : "undefined" == typeof b ? null: b
		},
		deleteKey: function(a) {
			c(a);
			return a in g ? (delete g[a], b(), !0) : !1
		},
		flush: function() {
			g = {};
			b();
			return ! 0
		},
		storageObj: function() {
			function a() {}
			a.prototype = g;
			return new a
		},
		index: function() {
			var a = [],
			b;
			for (b in g) g.hasOwnProperty(b) && a.push(b);
			return a
		},
		storageSize: function() {
			return j
		},
		currentBackend: function() {
			return A
		},
		storageAvailable: function() {
			return !! A
		},
		reInit: function() {
			var a;
			if (n && n.addBehavior) {
				a = document.createElement("link");
				n.parentNode.replaceChild(a, n);
				n = a;
				n.style.behavior = "url(#default#userData)";
				document.getElementsByTagName("head")[0].appendChild(n);
				n.load("jStorage");
				a = "{}";
				try {
					a = n.getAttribute("jStorage")
				} catch(b) {}
				d.jStorage = a;
				A = "userDataBehavior"
			}
			e()
		}
	};
	a: {
		if ("localStorage" in window) try {
			window.localStorage && (d = window.localStorage, A = "localStorage")
		} catch(B) {} else if ("globalStorage" in window) try {
			window.globalStorage && (d = window.globalStorage[window.location.hostname], A = "globalStorage")
		} catch(r) {} else if (n = document.createElement("link"), n.addBehavior) {
			n.style.behavior = "url(#default#userData)";
			document.getElementsByTagName("head")[0].appendChild(n);
			n.load("jStorage");
			a = "{}";
			try {
				a = n.getAttribute("jStorage")
			} catch(p) {}
			d.jStorage = a;
			A = "userDataBehavior"
		} else {
			n = null;
			break a
		}
		e()
	}
})(window.jQuery || window.$); (function(a) {
	a.fn.checkbox = function(e) {
		var b = {
			cls: "jquery-checkbox"
		},
		b = a.extend(b, e || {});
		return this.each(function() {
			var c = this,
			g = c.checked,
			d = c.disabled,
			e = a(c);
			c.stateInterval && clearInterval(c.stateInterval);
			c.stateInterval = setInterval(function() {
				c.disabled != d && e.trigger((d = !!c.disabled) ? "disable": "enable");
				c.checked != g && e.trigger((g = !!c.checked) ? "check": "uncheck")
			},
			10);
			var j = a(c).is(":radio") ? "radio": "checkbox";
			c.wrapper && c.wrapper.remove();
			c.wrapper = a('<span class="' + b.cls + " " + j + '"><span><span class="checkboxplaceholder"></span></span></span>');
			c.wrapperInner = c.wrapper.children("span:eq(0)");
			c.wrapper.bind({
				click: function() {
					e.trigger("click");
					return ! 1
				},
				mouseover: function() {
					c.wrapperInner.addClass("hover")
				},
				mouseout: function() {
					c.wrapperInner.removeClass("hover")
				},
				mousedown: function() {
					c.wrapperInner.addClass("pressed")
				},
				mouseup: function() {
					c.wrapperInner.removeClass("pressed")
				}
			});
			e.css({
				position: "absolute",
				zIndex: -1,
				visibility: "hidden"
			}).after(c.wrapper);
			j = !1;
			j = e.closest("label"),
			j.length || (j = !1);
			j && j.bind({
				click: function() {
					e.trigger("click");
					return ! 1
				},
				mouseover: function() {
					c.wrapper.trigger("mouseover")
				},
				mouseout: function() {
					c.wrapper.trigger("mouseout")
				},
				mousedown: function() {
					c.wrapper.addClass("pressed")
				},
				mouseup: function() {
					c.wrapper.removeClass("pressed")
				}
			});
			e.bind("disable",
			function() {
				c.wrapperInner.addClass("disabled")
			}).bind("enable",
			function() {
				c.wrapperInner.removeClass("disabled")
			});
			e.bind("check",
			function() {
				c.wrapper.addClass("checked")
			}).bind("uncheck",
			function() {
				c.wrapper.removeClass("checked")
			});
			window.getSelection && c.wrapper.css("MozUserSelect", "none");
			c.checked && c.wrapper.addClass("checked");
			c.disabled && c.wrapperInner.addClass("disabled")
		})
	}
})(jQuery);
$(document).ready(function() {
	var a = jQuery,
	e = function() {
		this.controls = {
			bold: {
				groupIndex: 0,
				visible: !1,
				tags: ["b", "strong"],
				css: {
					fontWeight: "bold"
				},
				tooltip: "Bold",
				hotkey: {
					ctrl: 1,
					key: 66
				}
			},
			copy: {
				groupIndex: 8,
				visible: !1,
				tooltip: "Copy"
			},
			colorpicker: {
				visible: !1,
				groupIndex: 1,
				tooltip: "Colorpicker",
				exec: function() {
					a.wysiwyg.controls.colorpicker.init(this)
				}
			},
			createLink: {
				groupIndex: 6,
				visible: !1,
				exec: function() {
					var c = this;
					a.wysiwyg.controls && a.wysiwyg.controls.link ? a.wysiwyg.controls.link.init(this) : a.wysiwyg.autoload ? a.wysiwyg.autoload.control("wysiwyg.link.js",
					function() {
						c.controls.createLink.exec.apply(c)
					}) : b.error("$.wysiwyg.controls.link not defined. You need to include wysiwyg.link.js file")
				},
				tags: ["a"],
				tooltip: "Create link"
			},
			cut: {
				groupIndex: 8,
				visible: !1,
				tooltip: "Cut"
			},
			decreaseFontSize: {
				groupIndex: 9,
				visible: !1,
				tags: ["small"],
				tooltip: "Decrease font size",
				exec: function() {
					this.decreaseFontSize()
				}
			},
			h1: {
				groupIndex: 7,
				visible: !1,
				className: "h1",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h1>": "h1",
				tags: ["h1"],
				tooltip: "Header 1"
			},
			h2: {
				groupIndex: 7,
				visible: !1,
				className: "h2",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h2>": "h2",
				tags: ["h2"],
				tooltip: "Header 2"
			},
			h3: {
				groupIndex: 7,
				visible: !1,
				className: "h3",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h3>": "h3",
				tags: ["h3"],
				tooltip: "Header 3"
			},
			h4: {
				groupIndex: 7,
				visible: !1,
				className: "h4",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h4>": "h4",
				tags: ["h4"],
				tooltip: "Header 4"
			},
			h5: {
				groupIndex: 7,
				visible: !1,
				className: "h5",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h5>": "h5",
				tags: ["h5"],
				tooltip: "Header 5"
			},
			h6: {
				groupIndex: 7,
				visible: !1,
				className: "h6",
				command: a.browser.msie || a.browser.safari || a.browser.opera ? "FormatBlock": "heading",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<h6>": "h6",
				tags: ["h6"],
				tooltip: "Header 6"
			},
			highlight: {
				tooltip: "Highlight",
				className: "highlight",
				groupIndex: 1,
				visible: !1,
				css: {
					backgroundColor: "rgb(255, 255, 102)"
				},
				exec: function() {
					var b, c;
					b = a.browser.msie || a.browser.safari ? "backcolor": "hilitecolor";
					if (a.browser.msie) c = this.getInternalRange().parentElement();
					else {
						c = this.getInternalSelection();
						for (c = c.extentNode || c.focusNode; void 0 === c.style;) if (c = c.parentNode, c.tagName && "body" === c.tagName.toLowerCase()) return
					}
					this.editorDoc.execCommand(b, !1, "rgb(255, 255, 102)" === c.style.backgroundColor || "#ffff66" === c.style.backgroundColor ? "#ffffff": "#ffff66")
				}
			},
			html: {
				groupIndex: 10,
				visible: !1,
				exec: function() {
					var b;
					this.options.resizeOptions && a.fn.resizable && (b = this.element.height());
					this.viewHTML ? (this.setContent(this.original.value), a(this.original).hide(), this.editor.show(), this.options.resizeOptions && a.fn.resizable && (b === this.element.height() && this.element.height(b + this.editor.height()), this.element.resizable(a.extend(!0, {
						alsoResize: this.editor
					},
					this.options.resizeOptions))), this.ui.toolbar.find("li").each(function() {
						var b = a(this);
						b.hasClass("html") ? b.removeClass("active") : b.removeClass("disabled")
					})) : (this.saveContent(), a(this.original).css({
						width: this.element.outerWidth() - 6,
						height: this.element.height() - this.ui.toolbar.height() - 6,
						resize: "none"
					}).show(), this.editor.hide(), this.options.resizeOptions && a.fn.resizable && (b === this.element.height() && this.element.height(this.ui.toolbar.height()), this.element.resizable("destroy")), this.ui.toolbar.find("li").each(function() {
						var b = a(this);
						b.hasClass("html") ? b.addClass("active") : !1 === b.hasClass("fullscreen") && b.removeClass("active").addClass("disabled")
					}));
					this.viewHTML = !this.viewHTML
				},
				tooltip: "View source code"
			},
			increaseFontSize: {
				groupIndex: 9,
				visible: !1,
				tags: ["big"],
				tooltip: "Increase font size",
				exec: function() {
					this.increaseFontSize()
				}
			},
			insertImage: {
				groupIndex: 6,
				visible: !1,
				exec: function() {
					var c = this;
					a.wysiwyg.controls && a.wysiwyg.controls.image ? a.wysiwyg.controls.image.init(this) : a.wysiwyg.autoload ? a.wysiwyg.autoload.control("wysiwyg.image.js",
					function() {
						c.controls.insertImage.exec.apply(c)
					}) : b.error("$.wysiwyg.controls.image not defined. You need to include wysiwyg.image.js file")
				},
				tags: ["img"],
				tooltip: "Insert image"
			},
			insertOrderedList: {
				groupIndex: 5,
				visible: !1,
				tags: ["ol"],
				tooltip: "Insert Ordered List"
			},
			insertTable: {
				groupIndex: 6,
				visible: !1,
				exec: function() {
					var c = this;
					a.wysiwyg.controls && a.wysiwyg.controls.table ? a.wysiwyg.controls.table(this) : a.wysiwyg.autoload ? a.wysiwyg.autoload.control("wysiwyg.table.js",
					function() {
						c.controls.insertTable.exec.apply(c)
					}) : b.error("$.wysiwyg.controls.table not defined. You need to include wysiwyg.table.js file")
				},
				tags: ["table"],
				tooltip: "Insert table"
			},
			insertUnorderedList: {
				groupIndex: 5,
				visible: !1,
				tags: ["ul"],
				tooltip: "Insert Unordered List"
			},
			italic: {
				groupIndex: 0,
				visible: !1,
				tags: ["i", "em"],
				css: {
					fontStyle: "italic"
				},
				tooltip: "Italic",
				hotkey: {
					ctrl: 1,
					key: 73
				}
			},
			justifyLeft: {
				visible: !1,
				groupIndex: 1,
				css: {
					textAlign: "left"
				},
				tooltip: "Justify Left"
			},
			justifyCenter: {
				groupIndex: 1,
				visible: !1,
				tags: ["center"],
				css: {
					textAlign: "center"
				},
				tooltip: "Justify Center"
			},
			justifyRight: {
				groupIndex: 1,
				visible: !1,
				css: {
					textAlign: "right"
				},
				tooltip: "Justify Right"
			},
			justifyFull: {
				groupIndex: 1,
				visible: !1,
				css: {
					textAlign: "justify"
				},
				tooltip: "Justify Full"
			},
			ltr: {
				groupIndex: 9,
				visible: !1,
				exec: function() {
					var b = this.dom.getElement("p");
					if (!b) return ! 1;
					a(b).attr("dir", "ltr");
					return ! 0
				},
				tooltip: "Left to Right"
			},
			rtl: {
				groupIndex: 9,
				visible: !1,
				exec: function() {
					var b = this.dom.getElement("p");
					if (!b) return ! 1;
					a(b).attr("dir", "rtl");
					return ! 0
				},
				tooltip: "Right to Left"
			},
			indent: {
				groupIndex: 2,
				visible: !1,
				tooltip: "Indent"
			},
			outdent: {
				groupIndex: 2,
				visible: !1,
				tooltip: "Outdent"
			},
			insertHorizontalRule: {
				groupIndex: 5,
				visible: !1,
				tags: ["hr"],
				tooltip: "Insert Horizontal Rule"
			},
			paragraph: {
				groupIndex: 7,
				visible: !1,
				className: "paragraph",
				command: "FormatBlock",
				arguments: a.browser.msie || a.browser.safari || a.browser.opera ? "<p>": "p",
				tags: ["p"],
				tooltip: "Paragraph"
			},
			paste: {
				groupIndex: 8,
				visible: !1,
				tooltip: "Paste"
			},
			undo: {
				groupIndex: 4,
				visible: !1,
				tooltip: "Undo"
			},
			redo: {
				groupIndex: 4,
				visible: !1,
				tooltip: "Redo"
			},
			removeFormat: {
				groupIndex: 10,
				visible: !1,
				exec: function() {
					this.removeFormat()
				},
				tooltip: "Remove formatting"
			},
			underline: {
				groupIndex: 0,
				visible: !1,
				tags: ["u"],
				css: {
					textDecoration: "underline"
				},
				tooltip: "Underline",
				hotkey: {
					ctrl: 1,
					key: 85
				}
			},
			strikeThrough: {
				groupIndex: 0,
				visible: !1,
				tags: ["s", "strike"],
				css: {
					textDecoration: "line-through"
				},
				tooltip: "Strike-through"
			},
			subscript: {
				groupIndex: 3,
				visible: !1,
				tags: ["sub"],
				tooltip: "Subscript"
			},
			superscript: {
				groupIndex: 3,
				visible: !1,
				tags: ["sup"],
				tooltip: "Superscript"
			},
			code: {
				visible: !1,
				groupIndex: 6,
				tooltip: "Code snippet",
				exec: function() {
					var b = this.getInternalRange(),
					c = a(b.commonAncestorContainer),
					b = b.commonAncestorContainer.nodeName.toLowerCase();
					c.parent("code").length ? c.unwrap() : "body" !== b && c.wrap("<code/>")
				}
			},
			cssWrap: {
				visible: !1,
				groupIndex: 6,
				tooltip: "CSS Wrapper",
				exec: function() {
					a.wysiwyg.controls.cssWrap.init(this)
				}
			}
		};
		this.defaults = {
			html: '<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>INITIAL_CONTENT</body></html>',
			debug: !1,
			controls: {},
			css: {},
			events: {},
			autoGrow: !1,
			autoSave: !0,
			brIE: !0,
			formHeight: 270,
			formWidth: 440,
			iFrameClass: null,
			initialContent: "<p>Initial content</p>",
			maxHeight: 1E4,
			maxLength: 0,
			messages: {
				nonSelection: "Select the text you wish to link"
			},
			toolbarHtml: '<ul role="menu" class="toolbar"></ul>',
			removeHeadings: !1,
			replaceDivWithP: !1,
			resizeOptions: !1,
			rmUnusedControls: !1,
			rmUnwantedBr: !0,
			tableFiller: null,
			initialMinHeight: null,
			controlImage: {
				forceRelativeUrls: !1
			},
			controlLink: {
				forceRelativeUrls: !1
			},
			plugins: {
				autoload: !1,
				i18n: !1,
				rmFormat: {
					rmMsWordMarkup: !1
				}
			}
		};
		this.availableControlProperties = "arguments callback className command css custom exec groupIndex hotkey icon tags tooltip visible".split(" ");
		this.element = this.editorDoc = this.editor = null;
		this.options = {};
		this.savedRange = this.original = null;
		this.timers = [];
		this.validKeyCodes = [8, 9, 13, 16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46];
		this.isDestroyed = !1;
		this.dom = {
			ie: {
				parent: null
			},
			w3c: {
				parent: null
			}
		};
		this.dom.parent = this;
		this.dom.ie.parent = this.dom;
		this.dom.w3c.parent = this.dom;
		this.ui = {};
		this.ui.self = this;
		this.ui.toolbar = null;
		this.ui.initialHeight = null;
		this.dom.getAncestor = function(a, b) {
			for (b = b.toLowerCase(); a && "body" !== a.tagName.toLowerCase();) {
				if (b === a.tagName.toLowerCase()) return a;
				a = a.parentNode
			}
			return null
		};
		this.dom.getElement = function(a) {
			return window.getSelection ? this.w3c.getElement(a) : this.ie.getElement(a)
		};
		this.dom.ie.getElement = function(a) {
			var b = this.parent,
			c = b.parent.getInternalSelection(),
			d = c.createRange();
			if ("Control" === c.type) if (1 === d.length) c = d.item(0);
			else return null;
			else c = d.parentElement();
			return b.getAncestor(c, a)
		};
		this.dom.w3c.getElement = function(a) {
			var b = this.parent,
			c = b.parent.getInternalRange(),
			d;
			if (!c) return null;
			d = c.commonAncestorContainer;
			3 === d.nodeType && (d = d.parentNode);
			d === c.startContainer && (d = d.childNodes[c.startOffset]);
			return b.getAncestor(d, a)
		};
		this.ui.addHoverClass = function() {
			a(this).addClass("wysiwyg-button-hover")
		};
		this.ui.appendControls = function() {
			var b = this,
			c = this.self.parseControls(),
			d = !0,
			g = [],
			e = {},
			j,
			n = function(a, c) {
				c.groupIndex && j !== c.groupIndex && (j = c.groupIndex, d = !1);
				c.visible && (d || (b.appendItemSeparator(), d = !0), c.custom ? b.appendItemCustom(a, c) : b.appendItem(a, c))
			};
			a.each(c,
			function(a, b) {
				var c = "empty";
				void 0 !== b.groupIndex && (c = "" === b.groupIndex ? "empty": b.groupIndex);
				void 0 === e[c] && (g.push(c), e[c] = {});
				e[c][a] = b
			});
			g.sort(function(a, b) {
				if ("number" === typeof a && typeof a === typeof b) return a - b;
				a = a.toString();
				b = b.toString();
				return a > b ? 1 : a === b ? 0 : -1
			});
			0 < g.length && (j = g[0]);
			for (c = 0; c < g.length; c += 1) a.each(e[g[c]], n)
		};
		this.ui.appendItem = function(b, c) {
			var d = this.self,
			g = c.className || c.command || b || "empty",
			e = c.tooltip || c.command || b || "";
			return a('<li role="menuitem" unselectable="on">' + g + "</li>").addClass(g).attr("title", e).hover(this.addHoverClass, this.removeHoverClass).click(function() {
				if (a(this).hasClass("disabled")) return ! 1;
				d.triggerControl.apply(d, [b, c]);
				this.blur();
				d.ui.returnRange();
				d.ui.focus();
				return ! 0
			}).appendTo(d.ui.toolbar)
		};
		this.ui.appendItemCustom = function(b, c) {
			var d = this.self,
			g = c.tooltip || c.command || b || "";
			c.callback && a(window).bind("trigger-" + b + ".wysiwyg", c.callback);
			return a('<li role="menuitem" unselectable="on" style="background: url(\'' + c.icon + "') no-repeat;\"></li>").addClass("custom-command-" + b).addClass("wysiwyg-custom-command").addClass(b).attr("title", g).hover(this.addHoverClass, this.removeHoverClass).click(function() {
				if (a(this).hasClass("disabled")) return ! 1;
				d.triggerControl.apply(d, [b, c]);
				this.blur();
				d.ui.returnRange();
				d.ui.focus();
				d.triggerControlCallback(b);
				return ! 0
			}).appendTo(d.ui.toolbar)
		};
		this.ui.appendItemSeparator = function() {
			var b = this.self;
			return a('<li role="separator" class="separator"></li>').appendTo(b.ui.toolbar)
		};
		this.autoSaveFunction = function() {
			this.saveContent()
		};
		this.ui.checkTargets = function(b) {
			var c = this.self;
			a.each(c.options.controls,
			function(d, g) {
				var e = g.className || g.command || d || "empty",
				j, n, k, D = function(a, b) {
					"function" === typeof b ? b(k.css(a).toString().toLowerCase(), c) && c.ui.toolbar.find("." + e).addClass("active") : k.css(a).toString().toLowerCase() === b && c.ui.toolbar.find("." + e).addClass("active")
				};
				"fullscreen" !== e && c.ui.toolbar.find("." + e).removeClass("active");
				if (g.tags || g.options && g.options.tags) {
					j = g.tags || g.options && g.options.tags;
					for (n = b; n && 1 === n.nodeType;) - 1 !== a.inArray(n.tagName.toLowerCase(), j) && c.ui.toolbar.find("." + e).addClass("active"),
					n = n.parentNode
				}
				if (g.css || g.options && g.options.css) {
					j = g.css || g.options && g.options.css;
					for (k = a(b); k && 1 === k[0].nodeType;) a.each(j, D),
					k = k.parent()
				}
			})
		};
		this.ui.designMode = function() {
			var a = this.self,
			b;
			b = function(c) {
				if ("on" === a.editorDoc.designMode) a.timers.designMode && window.clearTimeout(a.timers.designMode),
				a.innerDocument() !== a.editorDoc && a.ui.initFrame();
				else {
					try {
						a.editorDoc.designMode = "on"
					} catch(d) {}
					c -= 1;
					0 < c && (a.timers.designMode = window.setTimeout(function() {
						b(c)
					},
					100))
				}
			};
			b(3)
		};
		this.destroy = function() {
			this.isDestroyed = !0;
			var b, c = this.element.closest("form");
			for (b = 0; b < this.timers.length; b += 1) window.clearTimeout(this.timers[b]);
			c.unbind(".wysiwyg");
			this.element.remove();
			a.removeData(this.original, "wysiwyg");
			a(this.original).show();
			return this
		};
		this.getRangeText = function() {
			var a = this.getInternalRange();
			a.toString ? a = a.toString() : a.text && (a = a.text);
			return a
		};
		this.execute = function(a, b) {
			"undefined" === typeof b && (b = null);
			this.editorDoc.execCommand(a, !1, b)
		};
		this.extendOptions = function(b) {
			var c = {};
			"object" === typeof b.controls && (c = b.controls, delete b.controls);
			b = a.extend(!0, {},
			this.defaults, b);
			b.controls = a.extend(!0, {},
			c, this.controls, c);
			b.rmUnusedControls && a.each(b.controls,
			function(a) {
				c[a] || delete b.controls[a]
			});
			return b
		};
		this.ui.focus = function() {
			var a = this.self;
			a.editor.get(0).contentWindow.focus();
			return a
		};
		this.ui.returnRange = function() {
			var a = this.self,
			c;
			if (null !== a.savedRange) {
				if (window.getSelection) {
					c = window.getSelection();
					0 < c.rangeCount && c.removeAllRanges();
					try {
						c.addRange(a.savedRange)
					} catch(d) {
						b.error(d)
					}
				} else window.document.createRange ? window.getSelection().addRange(a.savedRange) : window.document.selection && a.savedRange.select();
				a.savedRange = null
			}
		};
		this.increaseFontSize = function() {
			if (a.browser.mozilla || a.browser.opera) this.editorDoc.execCommand("increaseFontSize", !1, null);
			else if (a.browser.safari) {
				var c = this.editorDoc.createElement("big");
				this.getInternalRange().surroundContents(c)
			} else b.error("Internet Explorer?")
		};
		this.decreaseFontSize = function() {
			if (a.browser.mozilla || a.browser.opera) this.editorDoc.execCommand("decreaseFontSize", !1, null);
			else if (a.browser.safari) {
				var c = this.editorDoc.createElement("small");
				this.getInternalRange().surroundContents(c)
			} else b.error("Internet Explorer?")
		};
		this.getContent = function() {
			this.viewHTML && this.setContent(this.original.value);
			return this.events.filter("getContent", this.editorDoc.body.innerHTML)
		};
		this.events = {
			_events: {},
			bind: function(a, b) {
				"object" !== typeof this._events.eventName && (this._events[a] = []);
				this._events[a].push(b)
			},
			trigger: function(b, c) {
				if ("object" === typeof this._events.eventName) {
					var d = this.editor;
					a.each(this._events[b],
					function(a, b) {
						"function" === typeof b && b.apply(d, c)
					})
				}
			},
			filter: function(b, c) {
				if ("object" === typeof this._events[b]) {
					var d = this.editor,
					g = Array.prototype.slice.call(arguments, 1);
					a.each(this._events[b],
					function(a, b) {
						"function" === typeof b && (c = b.apply(d, g))
					})
				}
				return c
			}
		};
		this.getElementByAttributeValue = function(b, c, d) {
			var g, e = this.editorDoc.getElementsByTagName(b);
			for (b = 0; b < e.length; b += 1) if (g = e[b].getAttribute(c), a.browser.msie && (g = g.substr(g.length - d.length)), g === d) return e[b];
			return ! 1
		};
		this.getInternalRange = function() {
			var a = this.getInternalSelection();
			return ! a ? null: a.rangeCount && 0 < a.rangeCount ? a.getRangeAt(0) : a.createRange ? a.createRange() : null
		};
		this.getInternalSelection = function() {
			if (this.editor.get(0).contentWindow) {
				if (this.editor.get(0).contentWindow.getSelection) return this.editor.get(0).contentWindow.getSelection();
				if (this.editor.get(0).contentWindow.selection) return this.editor.get(0).contentWindow.selection
			}
			return this.editorDoc.getSelection ? this.editorDoc.getSelection() : this.editorDoc.selection ? this.editorDoc.selection: null
		};
		this.getRange = function() {
			var a = this.getSelection();
			if (!a) return null;
			if (a.rangeCount && 0 < a.rangeCount) a.getRangeAt(0);
			else if (a.createRange) return a.createRange();
			return null
		};
		this.getSelection = function() {
			return window.getSelection ? window.getSelection() : window.document.selection
		};
		this.ui.grow = function() {
			var b = this.self,
			c = a(b.editorDoc.body),
			d = a.browser.msie ? c[0].scrollHeight: c.height() + 2 + 20,
			d = Math.max(d, b.ui.initialHeight),
			d = Math.min(d, b.options.maxHeight);
			b.editor.attr("scrolling", d < b.options.maxHeight ? "no": "auto");
			c.css("overflow", d < b.options.maxHeight ? "hidden": "");
			b.editor.get(0).height = d;
			return b
		};
		this.init = function(b, c) {
			var d = this,
			g = a(b).closest("form"),
			e = b.width || b.clientWidth || 0,
			j = b.height || b.clientHeight || 0;
			this.options = this.extendOptions(c);
			this.original = b;
			this.ui.toolbar = a(this.options.toolbarHtml);
			a.browser.msie && 8 > parseInt(a.browser.version, 10) && (this.options.autoGrow = !1);
			0 === e && b.cols && (e = 8 * b.cols + 21);
			0 === j && b.rows && (j = 16 * b.rows + 16);
			this.editor = a("https:" === window.location.protocol ? '<iframe src="javascript:false;"></iframe>': "<iframe></iframe>").attr("frameborder", "0");
			this.options.iFrameClass ? this.editor.addClass(this.options.iFrameClass) : (this.editor.css({
				minHeight: (j - 6).toString() + "px",
				width: 50 < e ? (e - 8).toString() + "px": ""
			}), a.browser.msie && 7 > parseInt(a.browser.version, 10) && this.editor.css("height", j.toString() + "px"));
			this.editor.attr("tabindex", a(b).attr("tabindex"));
			this.element = a("<div/>").addClass("wysiwyg");
			this.options.iFrameClass || this.element.css({
				width: 0 < e ? e.toString() + "px": "100%"
			});
			a(b).hide().before(this.element);
			this.viewHTML = !1;
			this.initialContent = a(b).val();
			this.ui.initFrame();
			this.options.resizeOptions && a.fn.resizable && this.element.resizable(a.extend(!0, {
				alsoResize: this.editor
			},
			this.options.resizeOptions));
			this.options.autoSave && g.bind("submit.wysiwyg",
			function() {
				d.autoSaveFunction()
			});
			g.bind("reset.wysiwyg",
			function() {
				d.resetFunction()
			})
		};
		this.ui.initFrame = function() {
			var b = this.self,
			c;
			b.ui.appendControls();
			b.element.append(b.ui.toolbar).append(a("<div>\x3c!-- --\x3e</div>").css({
				clear: "both"
			})).append(b.editor);
			b.editorDoc = b.innerDocument();
			if (b.isDestroyed) return null;
			b.ui.designMode();
			b.editorDoc.open();
			b.editorDoc.write(b.options.html.replace(/INITIAL_CONTENT/,
			function() {
				return b.wrapInitialContent()
			}));
			b.editorDoc.close();
			a.wysiwyg.plugin.bind(b);
			a(b.editorDoc).trigger("initFrame.wysiwyg");
			a(b.editorDoc).bind("click.wysiwyg",
			function(a) {
				b.ui.checkTargets(a.target ? a.target: a.srcElement)
			});
			a(b.original).focus(function() {
				a(this).filter(":visible") || b.ui.focus()
			});
			a(b.editorDoc).keydown(function(a) {
				var c;
				return 8 === a.keyCode && (c = /^<([\w]+)[^>]*>(<br\/?>)?<\/\1>$/, c.test(b.getContent())) ? (a.stopPropagation(), !1) : !0
			});
			a.browser.msie ? b.options.brIE && a(b.editorDoc).keydown(function(a) {
				return 13 === a.keyCode ? (a = b.getRange(), a.pasteHTML("<br/>"), a.collapse(!1), a.select(), !1) : !0
			}) : a(b.editorDoc).keydown(function(a) {
				var c;
				if (a.ctrlKey || a.metaKey) for (c in b.controls) if (b.controls[c].hotkey && b.controls[c].hotkey.ctrl && a.keyCode === b.controls[c].hotkey.key) return b.triggerControl.apply(b, [c, b.controls[c]]),
				!1;
				return ! 0
			});
			b.options.plugins.rmFormat.rmMsWordMarkup && a(b.editorDoc).bind("keyup.wysiwyg",
			function(c) {
				if (c.ctrlKey || c.metaKey) 86 === c.keyCode && a.wysiwyg.rmFormat && ("object" === typeof b.options.plugins.rmFormat.rmMsWordMarkup ? a.wysiwyg.rmFormat.run(b, {
					rules: {
						msWordMarkup: b.options.plugins.rmFormat.rmMsWordMarkup
					}
				}) : a.wysiwyg.rmFormat.run(b, {
					rules: {
						msWordMarkup: {
							enabled: !0
						}
					}
				}))
			});
			b.options.autoSave && a(b.editorDoc).keydown(function() {
				b.autoSaveFunction()
			}).keyup(function() {
				b.autoSaveFunction()
			}).mousedown(function() {
				b.autoSaveFunction()
			}).bind(a.support.noCloneEvent ? "input.wysiwyg": "paste.wysiwyg",
			function() {
				b.autoSaveFunction()
			});
			b.options.autoGrow && (b.ui.initialHeight = null !== b.options.initialMinHeight ? b.options.initialMinHeight: a(b.editorDoc).height(), a(b.editorDoc.body).css("border", "1px solid white"), c = function() {
				b.ui.grow()
			},
			a(b.editorDoc).keyup(c), a(b.editorDoc).bind("editorRefresh.wysiwyg", c), b.ui.grow());
			b.options.css && (String === b.options.css.constructor ? a.browser.msie ? (c = b.editorDoc.createStyleSheet(b.options.css), a(c).attr({
				media: "all"
			})) : (c = a("<link/>").attr({
				href: b.options.css,
				media: "all",
				rel: "stylesheet",
				type: "text/css"
			}), a(b.editorDoc).find("head").append(c)) : b.timers.initFrame_Css = window.setTimeout(function() {
				a(b.editorDoc.body).css(b.options.css)
			},
			0));
			0 === b.initialContent.length && ("function" === typeof b.options.initialContent ? b.setContent(b.options.initialContent()) : b.setContent(b.options.initialContent));
			0 < b.options.maxLength && a(b.editorDoc).keydown(function(c) {
				a(b.editorDoc).text().length >= b.options.maxLength && -1 === a.inArray(c.which, b.validKeyCodes) && c.preventDefault()
			});
			a.each(b.options.events,
			function(c, d) {
				a(b.editorDoc).bind(c + ".wysiwyg",
				function(a) {
					d.apply(b.editorDoc, [a, b])
				})
			});
			a.browser.msie ? a(b.editorDoc).bind("beforedeactivate.wysiwyg",
			function() {
				b.savedRange = b.getInternalRange()
			}) : a(b.editorDoc).bind("blur.wysiwyg",
			function() {
				b.savedRange = b.getInternalRange()
			});
			a(b.editorDoc.body).addClass("wysiwyg");
			b.options.events && b.options.events.save && (c = b.options.events.save, a(b.editorDoc).bind("keyup.wysiwyg", c), a(b.editorDoc).bind("change.wysiwyg", c), a.support.noCloneEvent ? a(b.editorDoc).bind("input.wysiwyg", c) : (a(b.editorDoc).bind("paste.wysiwyg", c), a(b.editorDoc).bind("cut.wysiwyg", c)));
			if (b.options.xhtml5 && b.options.unicode) {
				var d = {
					ne: 8800,
					le: 8804,
					para: 182,
					xi: 958,
					darr: 8595,
					nu: 957,
					oacute: 243,
					Uacute: 218,
					omega: 969,
					prime: 8242,
					pound: 163,
					igrave: 236,
					thorn: 254,
					forall: 8704,
					emsp: 8195,
					lowast: 8727,
					brvbar: 166,
					alefsym: 8501,
					nbsp: 160,
					delta: 948,
					clubs: 9827,
					lArr: 8656,
					Omega: 937,
					Auml: 196,
					cedil: 184,
					and: 8743,
					plusmn: 177,
					ge: 8805,
					raquo: 187,
					uml: 168,
					equiv: 8801,
					laquo: 171,
					rdquo: 8221,
					Epsilon: 917,
					divide: 247,
					fnof: 402,
					chi: 967,
					Dagger: 8225,
					iacute: 237,
					rceil: 8969,
					sigma: 963,
					Oslash: 216,
					acute: 180,
					frac34: 190,
					lrm: 8206,
					upsih: 978,
					Scaron: 352,
					part: 8706,
					exist: 8707,
					nabla: 8711,
					image: 8465,
					prop: 8733,
					zwj: 8205,
					omicron: 959,
					aacute: 225,
					Yuml: 376,
					Yacute: 221,
					weierp: 8472,
					rsquo: 8217,
					otimes: 8855,
					kappa: 954,
					thetasym: 977,
					harr: 8596,
					Ouml: 214,
					Iota: 921,
					ograve: 242,
					sdot: 8901,
					copy: 169,
					oplus: 8853,
					acirc: 226,
					sup: 8835,
					zeta: 950,
					Iacute: 205,
					Oacute: 211,
					crarr: 8629,
					Nu: 925,
					bdquo: 8222,
					lsquo: 8216,
					apos: 39,
					Beta: 914,
					eacute: 233,
					egrave: 232,
					lceil: 8968,
					Kappa: 922,
					piv: 982,
					Ccedil: 199,
					ldquo: 8220,
					Xi: 926,
					cent: 162,
					uarr: 8593,
					hellip: 8230,
					Aacute: 193,
					ensp: 8194,
					sect: 167,
					Ugrave: 217,
					aelig: 230,
					ordf: 170,
					curren: 164,
					sbquo: 8218,
					macr: 175,
					Phi: 934,
					Eta: 919,
					rho: 961,
					Omicron: 927,
					sup2: 178,
					euro: 8364,
					aring: 229,
					Theta: 920,
					mdash: 8212,
					uuml: 252,
					otilde: 245,
					eta: 951,
					uacute: 250,
					rArr: 8658,
					nsub: 8836,
					agrave: 224,
					notin: 8713,
					ndash: 8211,
					Psi: 936,
					Ocirc: 212,
					sube: 8838,
					szlig: 223,
					micro: 181,
					not: 172,
					sup1: 185,
					middot: 183,
					iota: 953,
					ecirc: 234,
					lsaquo: 8249,
					thinsp: 8201,
					sum: 8721,
					ntilde: 241,
					scaron: 353,
					cap: 8745,
					atilde: 227,
					lang: 10216,
					__replacement: 65533,
					isin: 8712,
					gamma: 947,
					Euml: 203,
					ang: 8736,
					upsilon: 965,
					Ntilde: 209,
					hearts: 9829,
					Alpha: 913,
					Tau: 932,
					spades: 9824,
					dagger: 8224,
					THORN: 222,
					"int": 8747,
					lambda: 955,
					Eacute: 201,
					Uuml: 220,
					infin: 8734,
					rlm: 8207,
					Aring: 197,
					ugrave: 249,
					Egrave: 200,
					Acirc: 194,
					rsaquo: 8250,
					ETH: 208,
					oslash: 248,
					alpha: 945,
					Ograve: 210,
					Prime: 8243,
					mu: 956,
					ni: 8715,
					real: 8476,
					bull: 8226,
					beta: 946,
					icirc: 238,
					eth: 240,
					prod: 8719,
					larr: 8592,
					ordm: 186,
					perp: 8869,
					Gamma: 915,
					reg: 174,
					ucirc: 251,
					Pi: 928,
					psi: 968,
					tilde: 732,
					asymp: 8776,
					zwnj: 8204,
					Agrave: 192,
					deg: 176,
					AElig: 198,
					times: 215,
					Delta: 916,
					sim: 8764,
					Otilde: 213,
					Mu: 924,
					uArr: 8657,
					circ: 710,
					theta: 952,
					Rho: 929,
					sup3: 179,
					diams: 9830,
					tau: 964,
					Chi: 935,
					frac14: 188,
					oelig: 339,
					shy: 173,
					or: 8744,
					dArr: 8659,
					phi: 966,
					iuml: 239,
					Lambda: 923,
					rfloor: 8971,
					iexcl: 161,
					cong: 8773,
					ccedil: 231,
					Icirc: 206,
					frac12: 189,
					loz: 9674,
					rarr: 8594,
					cup: 8746,
					radic: 8730,
					frasl: 8260,
					euml: 235,
					OElig: 338,
					hArr: 8660,
					Atilde: 195,
					Upsilon: 933,
					there4: 8756,
					ouml: 246,
					oline: 8254,
					Ecirc: 202,
					yacute: 253,
					auml: 228,
					permil: 8240,
					sigmaf: 962,
					iquest: 191,
					empty: 8709,
					pi: 960,
					Ucirc: 219,
					supe: 8839,
					Igrave: 204,
					yen: 165,
					rang: 10217,
					trade: 8482,
					lfloor: 8970,
					minus: 8722,
					Zeta: 918,
					sub: 8834,
					epsilon: 949,
					yuml: 255,
					Sigma: 931,
					Iuml: 207,
					ocirc: 244
				};
				b.events.bind("getContent",
				function(a) {
					return a.replace(/&(?:amp;)?(?!amp|lt|gt|quot)([a-z][a-z0-9]*);/gi,
					function(a, b) {
						d[b] || (b = b.toLowerCase(), d[b] || (b = "__replacement"));
						return String.fromCharCode(d[b])
					})
				})
			}
		};
		this.innerDocument = function() {
			var a = this.editor.get(0);
			if ("iframe" === a.nodeName.toLowerCase()) {
				if (a.contentDocument) return a.contentDocument;
				if (a.contentWindow) return a.contentWindow.document;
				if (this.isDestroyed) return null;
				b.error("Unexpected error in innerDocument")
			}
			return a
		};
		this.insertHtml = function(b) {
			var c;
			if (!b || 0 === b.length) return this;
			a.browser.msie ? (this.ui.focus(), this.editorDoc.execCommand("insertImage", !1, "#jwysiwyg#"), (c = this.getElementByAttributeValue("img", "src", "#jwysiwyg#")) && a(c).replaceWith(b)) : a.browser.mozilla ? 1 === a(b).length ? (c = this.getInternalRange(), c.deleteContents(), c.insertNode(a(b).get(0))) : this.editorDoc.execCommand("insertHTML", !1, b) : this.editorDoc.execCommand("insertHTML", !1, b) || (this.editor.focus(), this.editorDoc.execCommand("insertHTML", !1, b));
			this.saveContent();
			return this
		};
		this.parseControls = function() {
			var b = this;
			a.each(this.options.controls,
			function(c, d) {
				a.each(d,
				function(d) {
					if ( - 1 === a.inArray(d, b.availableControlProperties)) throw c + '["' + d + '"]: property "' + d + '" not exists in Wysiwyg.availableControlProperties';
				})
			});
			return this.options.parseControls ? this.options.parseControls.call(this) : this.options.controls
		};
		this.removeFormat = function() {
			a.browser.msie && this.ui.focus();
			this.options.removeHeadings && this.editorDoc.execCommand("formatBlock", !1, "<p>");
			this.editorDoc.execCommand("removeFormat", !1, null);
			this.editorDoc.execCommand("unlink", !1, null);
			a.wysiwyg.rmFormat && a.wysiwyg.rmFormat.enabled && ("object" === typeof this.options.plugins.rmFormat.rmMsWordMarkup ? a.wysiwyg.rmFormat.run(this, {
				rules: {
					msWordMarkup: this.options.plugins.rmFormat.rmMsWordMarkup
				}
			}) : a.wysiwyg.rmFormat.run(this, {
				rules: {
					msWordMarkup: {
						enabled: !0
					}
				}
			}));
			return this
		};
		this.ui.removeHoverClass = function() {
			a(this).removeClass("wysiwyg-button-hover")
		};
		this.resetFunction = function() {
			this.setContent(this.initialContent)
		};
		this.saveContent = function() {
			if (!this.viewHTML) {
				if (this.original) {
					var b;
					b = this.getContent();
					this.options.rmUnwantedBr && (b = b.replace(/<br\/?>$/, ""));
					this.options.replaceDivWithP && (b = a("<div/>").addClass("temp").append(b), b.children("div").each(function() {
						var b = a(this),
						c = b.find("p"),
						d;
						if (0 === c.length) {
							c = a("<p></p>");
							if (0 < this.attributes.length) for (d = 0; d < this.attributes.length; d += 1) c.attr(this.attributes[d].name, b.attr(this.attributes[d].name));
							c.append(b.html());
							b.replaceWith(c)
						}
					}), b = b.html());
					a(this.original).val(b);
					this.options.events && this.options.events.save && this.options.events.save.call(this)
				}
				return this
			}
		};
		this.setContent = function(a) {
			this.editorDoc.body.innerHTML = a;
			this.saveContent();
			return this
		};
		this.triggerControl = function(a, c) {
			var d = c.command || a,
			g = c.arguments || [];
			if (c.exec) c.exec.apply(this);
			else {
				this.ui.focus();
				this.ui.withoutCss();
				try {
					this.editorDoc.execCommand(d, !1, g)
				} catch(e) {
					b.error(e)
				}
			}
			this.options.autoSave && this.autoSaveFunction()
		};
		this.triggerControlCallback = function(b) {
			a(window).trigger("trigger-" + b + ".wysiwyg", [this])
		};
		this.ui.withoutCss = function() {
			var b = this.self;
			if (a.browser.mozilla) try {
				b.editorDoc.execCommand("styleWithCSS", !1, !1)
			} catch(c) {
				try {
					b.editorDoc.execCommand("useCSS", !1, !0)
				} catch(d) {}
			}
			return b
		};
		this.wrapInitialContent = function() {
			var a = this.initialContent;
			return ! a.match(/<\/?p>/gi) ? "<p>" + a + "</p>": a
		}
	},
	b = window.console ? window.console: {
		log: a.noop,
		error: function(b) {
			a.error(b)
		}
	};
	a.wysiwyg = {
		messages: {
			noObject: "Something goes wrong, check object"
		},
		addControl: function(b, c, d) {
			return b.each(function() {
				var b = a(this).data("wysiwyg"),
				g = {};
				if (!b) return this;
				g[c] = a.extend(!0, {
					visible: !0,
					custom: !0
				},
				d);
				a.extend(!0, b.options.controls, g);
				g = a(b.options.toolbarHtml);
				b.ui.toolbar.replaceWith(g);
				b.ui.toolbar = g;
				b.ui.appendControls()
			})
		},
		clear: function(b) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.setContent("")
			})
		},
		console: b,
		destroy: function(b) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.destroy()
			})
		},
		document: function(b) {
			b = b.data("wysiwyg");
			return ! b ? void 0 : a(b.editorDoc)
		},
		getContent: function(a) {
			a = a.data("wysiwyg");
			return ! a ? void 0 : a.getContent()
		},
		init: function(b, c) {
			return b.each(function() {
				var b = a.extend(!0, {},
				c),
				d;
				"textarea" !== this.nodeName.toLowerCase() || a(this).data("wysiwyg") || (d = new e, d.init(this, b), a.data(this, "wysiwyg", d), a(d.editorDoc).trigger("afterInit.wysiwyg"))
			})
		},
		insertHtml: function(b, c) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.insertHtml(c)
			})
		},
		plugin: {
			listeners: {},
			bind: function(b) {
				var c = this;
				a.each(this.listeners,
				function(d, g) {
					var e, j;
					for (e = 0; e < g.length; e += 1) j = c.parseName(g[e]),
					a(b.editorDoc).bind(d + ".wysiwyg", {
						plugin: j
					},
					function(c) {
						a.wysiwyg[c.data.plugin.name][c.data.plugin.method].apply(a.wysiwyg[c.data.plugin.name], [b])
					})
				})
			},
			exists: function(b) {
				if ("string" !== typeof b) return ! 1;
				b = this.parseName(b);
				return ! a.wysiwyg[b.name] || !a.wysiwyg[b.name][b.method] ? !1 : !0
			},
			listen: function(b, c) {
				var d;
				d = this.parseName(c);
				if (!a.wysiwyg[d.name] || !a.wysiwyg[d.name][d.method]) return ! 1;
				this.listeners[b] || (this.listeners[b] = []);
				this.listeners[b].push(c);
				return ! 0
			},
			parseName: function(a) {
				if ("string" !== typeof a) return ! 1;
				a = a.split(".");
				return 2 > a.length ? !1 : {
					name: a[0],
					method: a[1]
				}
			},
			register: function(c) {
				c.name || b.error("Plugin name missing");
				a.each(a.wysiwyg,
				function(a) {
					a === c.name && b.error("Plugin with name '" + c.name + "' was already registered")
				});
				a.wysiwyg[c.name] = c;
				return ! 0
			}
		},
		removeFormat: function(b) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.removeFormat()
			})
		},
		save: function(b) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.saveContent()
			})
		},
		selectAll: function(a) {
			var b = a.data("wysiwyg");
			if (!b) return this;
			a = b.editorDoc.body;
			window.getSelection ? (b = b.getInternalSelection(), b.selectAllChildren(a)) : (b = a.createTextRange(), b.moveToElementText(a), b.select())
		},
		setContent: function(b, c) {
			return b.each(function() {
				var b = a(this).data("wysiwyg");
				if (!b) return this;
				b.setContent(c)
			})
		},
		triggerControl: function(c, d) {
			return c.each(function() {
				var c = a(this).data("wysiwyg");
				if (!c) return this;
				c.controls[d] || b.error("Control '" + d + "' not exists");
				c.triggerControl.apply(c, [d, c.controls[d]])
			})
		},
		support: {
			prop: "prop" in a.fn && "removeProp" in a.fn
		},
		utils: {
			extraSafeEntities: [["<", ">", "'", '"', " "], [32]],
			encodeEntities: function(b) {
				var c = this,
				d, g = [];
				0 === this.extraSafeEntities[1].length && a.each(this.extraSafeEntities[0],
				function(a, b) {
					c.extraSafeEntities[1].push(b.charCodeAt(0))
				});
				d = b.split("");
				a.each(d,
				function(b) {
					var e = d[b].charCodeAt(0);
					a.inArray(e, c.extraSafeEntities[1]) && (65 > e || 127 < e || 90 < e && 97 > e) ? g.push("&#" + e + ";") : g.push(d[b])
				});
				return g.join("")
			}
		}
	};
	a.fn.wysiwyg = function(c) {
		var d = arguments,
		g;
		if ("undefined" !== typeof a.wysiwyg[c]) return d = Array.prototype.concat.call([d[0]], [this], Array.prototype.slice.call(d, 1)),
		a.wysiwyg[c].apply(a.wysiwyg, Array.prototype.slice.call(d, 1));
		if ("object" === typeof c || !c) return Array.prototype.unshift.call(d, this),
		a.wysiwyg.init.apply(a.wysiwyg, d);
		if (a.wysiwyg.plugin.exists(c)) return g = a.wysiwyg.plugin.parseName(c),
		d = Array.prototype.concat.call([d[0]], [this], Array.prototype.slice.call(d, 1)),
		a.wysiwyg[g.name][g.method].apply(a.wysiwyg[g.name], Array.prototype.slice.call(d, 1));
		b.error("Method '" + c + "' does not exist on jQuery.wysiwyg.\nTry to include some extra controls or plugins")
	};
	a.fn.getWysiwyg = function() {
		return a.data(this, "wysiwyg")
	};
	var c = jQuery;
	if (void 0 === c.wysiwyg) throw "wysiwyg.image.js depends on $.wysiwyg";
	c.wysiwyg.controls || (c.wysiwyg.controls = {});
	c.wysiwyg.controls.image = {
		init: function(a) {
			var b = this,
			d, g, e, j, n, k = {
				alt: "",
				self: a.dom.getElement("img"),
				src: "http://",
				title: ""
			};
			e = {
				legend: "Insert Image",
				preview: "Preview",
				url: "URL",
				title: "Title",
				description: "Description",
				width: "Width",
				height: "Height",
				original: "Original W x H",
				"float": "Float",
				floatNone: "None",
				floatLeft: "Left",
				floatRight: "Right",
				submit: "Insert Image",
				loading: "loading",
				reset: "Cancel"
			};
			d = '<form class="wysiwyg" title="{legend}"><img src="" alt="{preview}" width="100%"><br>{url}: <input type="text" name="src" value=""><br>{title}: <input type="text" name="imgtitle" value=""><br>{description}: <input type="text" name="description" value=""><br>{width} x {height}: <input type="text" name="width" value="" class="width integer"> x <input type="text" name="height" value="" class="height integer"><br>{float}: <select name="float"><option value="">{floatNone}</option><option value="left">{floatLeft}</option><option value="right">{floatRight}</option></select></label><hr><button class="button" id="wysiwyg_submit">{submit}</button> <button class="button" id="wysiwyg_reset">{reset}</button></form>';
			for (j in e) c.wysiwyg.i18n && (n = c.wysiwyg.i18n.t(e[j], "dialogs.image"), n === e[j] && (n = c.wysiwyg.i18n.t(e[j], "dialogs")), e[j] = n),
			d = d.replace("{" + j + "}", e[j]);
			k.self && (k.src = k.self.src ? k.self.src: "", k.alt = k.self.alt ? k.self.alt: "", k.title = k.self.title ? k.self.title: "", k.width = k.self.width ? k.self.width: "", k.height = k.self.height ? k.self.height: "", k.asp = k.width / k.width);
			d = c(d);
			d = b.makeForm(d, k);
			g = d.appendTo("body");
			g.dialog({
				modal: !0,
				resizable: !1,
				open: function() {
					c("#wysiwyg_submit", g).click(function() {
						b.processInsert(g.container, a, k);
						c(g).dialog("close");
						return ! 1
					});
					c("#wysiwyg_reset", g).click(function() {
						c(g).dialog("close");
						return ! 1
					});
					c("fieldset", g).click(function(a) {
						a.stopPropagation()
					});
					c("select, input[type=text]", g).uniform();
					c(".width", g).wl_Number({
						step: 10,
						onChange: function(a) {
							c(".height", g).val(Math.ceil(a / (k.asp || 1)))
						}
					});
					c(".height", g).wl_Number({
						step: 10,
						onChange: function(a) {
							c(".width", g).val(Math.floor(a * (k.asp || 1)))
						}
					});
					c('input[name="src"]', g).wl_URL()
				},
				close: function() {
					g.dialog("destroy");
					g.remove()
				}
			});
			c(a.editorDoc).trigger("editorRefresh.wysiwyg")
		},
		processInsert: function(a, b, d) {
			var g = c('input[name="src"]', a).val(),
			e = c('input[name="imgtitle"]', a).val(),
			j = c('input[name="description"]', a).val(),
			n = c('input[name="width"]', a).val(),
			k = c('input[name="height"]', a).val();
			a = c('select[name="float"]', a).val();
			var D = [],
			E;
			b.options.controlImage.forceRelativeUrls && (E = window.location.protocol + "//" + window.location.hostname, 0 === g.indexOf(E) && (g = g.substr(E.length)));
			d.self ? (c(d.self).attr("src", g).attr("title", e).attr("alt", j).css("float", a), n.toString().match(/^[0-9]+(px|%)?$/) ? c(d.self).css("width", n) : c(d.self).css("width", ""), k.toString().match(/^[0-9]+(px|%)?$/) ? c(d.self).css("height", k) : c(d.self).css("height", ""), b.saveContent()) : ((d = n.toString().match(/^[0-9]+(px|%)?$/)) && (d[1] ? D.push("width: " + n + ";") : D.push("width: " + n + "px;")), (d = k.toString().match(/^[0-9]+(px|%)?$/)) && (d[1] ? D.push("height: " + k + ";") : D.push("height: " + k + "px;")), 0 < a.length && D.push("float: " + a + ";"), 0 < D.length && (D = ' style="' + D.join(" ") + '"'), b.insertHtml("<img src='" + g + "' title='" + e + "' alt='" + j + "'" + D + "/>"))
		},
		makeForm: function(a, b) {
			a.find("input[name=src]").val(b.src);
			a.find("input[name=imgtitle]").val(b.title);
			a.find("input[name=description]").val(b.alt);
			a.find('input[name="width"]').val(b.width);
			a.find('input[name="height"]').val(b.height);
			a.find("img").attr("src", b.src);
			b.asp = b.width / b.height;
			a.find("input[name=src]").bind("change",
			function() {
				var d = new Image,
				g = c("#wysiwyg_submit", a).find("span").text();
				a.find("img").removeAttr("src");
				c("#wysiwyg_submit", a).prop("disabled", !0).find("span").text("wait...");
				d.onload = function() {
					a.find("img").attr("src", d.src);
					b.asp = d.width / d.height;
					a.find('input[name="width"]').val(d.width);
					a.find('input[name="height"]').val(d.height);
					c("#wysiwyg_submit", a).find("span").text(g);
					c("#wysiwyg_submit", a).prop("disabled", !1)
				};
				d.src = this.value
			});
			return a
		}
	};
	c.wysiwyg.insertImage = function(a, b, d) {
		return a.each(function() {
			var a = c(this).data("wysiwyg"),
			g,
			e;
			if (!a || !b || 0 === b.length) return this;
			c.browser.msie && a.ui.focus();
			if (d) {
				if (a.editorDoc.execCommand("insertImage", !1, "#jwysiwyg#"), g = a.getElementByAttributeValue("img", "src", "#jwysiwyg#")) for (e in g.src = b, d) d.hasOwnProperty(e) && g.setAttribute(e, d[e])
			} else a.editorDoc.execCommand("insertImage", !1, b);
			a.saveContent();
			c(a.editorDoc).trigger("editorRefresh.wysiwyg");
			return this
		})
	};
	var g = jQuery;
	if (void 0 === g.wysiwyg) throw "wysiwyg.table.js depends on $.wysiwyg";
	g.wysiwyg.controls || (g.wysiwyg.controls = {});
	var d = function(a, b, c) {
		if (!isNaN(b) && !isNaN(a) && !(null === b || null === a)) {
			var d, g = ['<table border="1" style="width: 100%;"><tbody>'];
			a = parseInt(a, 10);
			b = parseInt(b, 10);
			null === c && (c = "&nbsp;");
			for (c = "<td>" + c + "</td>"; 0 < b; b -= 1) {
				g.push("<tr>");
				for (d = a; 0 < d; d -= 1) g.push(c);
				g.push("</tr>")
			}
			g.push("</tbody></table>");
			return this.insertHtml(g.join(""))
		}
	};
	g.wysiwyg.controls.table = function(a) {
		var b, c, e, j = "Insert table",
		n = "Count of columns",
		k = "Count of rows",
		F = "Insert table",
		D = "Cancel";
		g.wysiwyg.i18n && (j = g.wysiwyg.i18n.t(j, "dialogs.table"), n = g.wysiwyg.i18n.t(n, "dialogs.table"), k = g.wysiwyg.i18n.t(k, "dialogs.table"), F = g.wysiwyg.i18n.t(F, "dialogs.table"), D = g.wysiwyg.i18n.t(D, "dialogs"));
		a.insertTable || (a.insertTable = d);
		b = g('<form class="wysiwyg" title="' + j + '">' + n + ': <input type="text" name="colCount" value="3" class="integer" ><br>' + k + ': <input type="text" name="rowCount" value="3" class="integer" ><hr><button class="button" id="wysiwyg_submit">' + F + '</button> <button class="button" id="wysiwyg_reset">' + D + "</button></form>").appendTo("body");
		b.dialog({
			modal: !0,
			resizable: !1,
			open: function() {
				g("#wysiwyg_submit", b).click(function(d) {
					d.preventDefault();
					e = g('input[name="rowCount"]', b).val();
					c = g('input[name="colCount"]', b).val();
					a.insertTable(c, e, a.defaults.tableFiller);
					g(b).dialog("close")
				});
				g("#wysiwyg_reset", b).click(function(a) {
					a.preventDefault();
					g(b).dialog("close")
				});
				g("select, input[type=text]", b).uniform();
				g(".integer", b).wl_Number()
			},
			close: function() {
				b.dialog("destroy");
				b.remove()
			}
		});
		g(a.editorDoc).trigger("editorRefresh.wysiwyg")
	};
	g.wysiwyg.insertTable = function(a, b, c, e) {
		return a.each(function() {
			var a = g(this).data("wysiwyg");
			a.insertTable || (a.insertTable = d);
			if (!a) return this;
			a.insertTable(b, c, e);
			g(a.editorDoc).trigger("editorRefresh.wysiwyg");
			return this
		})
	};
	var n = jQuery;
	if (void 0 === n.wysiwyg) throw "wysiwyg.link.js depends on $.wysiwyg";
	n.wysiwyg.controls || (n.wysiwyg.controls = {});
	n.wysiwyg.controls.link = {
		init: function(a) {
			var b, c, d, g, e, j, k, D, E;
			c = "Insert Link";
			d = "Link URL";
			g = "Link Title";
			e = "Link Target";
			D = "Insert Link";
			E = "Cancel";
			n.wysiwyg.i18n && (c = n.wysiwyg.i18n.t(c, "dialogs.link"), d = n.wysiwyg.i18n.t(d, "dialogs.link"), g = n.wysiwyg.i18n.t(g, "dialogs.link"), e = n.wysiwyg.i18n.t(e, "dialogs.link"), D = n.wysiwyg.i18n.t(D, "dialogs.link"), E = n.wysiwyg.i18n.t(E, "dialogs"));
			e = '<form class="wysiwyg" title="' + c + '">' + d + ': <input type="text" name="linkhref" value="">' + g + ': <input type="text" name="linktitle" value="">' + e + ': <input type="text" name="linktarget" value=""><hr><button class="button" id="wysiwyg_submit">' + D + '</button> <button class="button" id="wysiwyg_reset">' + E + "</button></form>";
			b = a.dom.getElement("a");
			c = "http://";
			g = d = "";
			b && (c = b.href ? b.href: c, d = b.title ? b.title: "", g = b.target ? b.target: "");
			e = n(e);
			e.find("input[name=linkhref]").val(c);
			e.find("input[name=linktitle]").val(d);
			e.find("input[name=linktarget]").val(g);
			j = n.browser.msie ? e.appendTo(a.editorDoc.body) : e.appendTo("body");
			j.dialog({
				modal: !0,
				resizable: !1,
				open: function() {
					n("#wysiwyg_submit", j).click(function(c) {
						c.preventDefault();
						c = n('input[name="linkhref"]', j).val();
						var d = n('input[name="linktitle"]', j).val(),
						g = n('input[name="linktarget"]', j).val(),
						e;
						a.options.controlLink.forceRelativeUrls && (e = window.location.protocol + "//" + window.location.hostname, 0 === c.indexOf(e) && (c = c.substr(e.length)));
						b ? "string" === typeof c && (0 < c.length ? n(b).attr("href", c).attr("title", d).attr("target", g) : n(b).replaceWith(b.innerHTML)) : (n.browser.msie && a.ui.returnRange(), k = a.getRangeText(), img = a.dom.getElement("img"), k && 0 < k.length || img ? (n.browser.msie && a.ui.focus(), "string" === typeof c && (0 < c.length ? a.editorDoc.execCommand("createLink", !1, c) : a.editorDoc.execCommand("unlink", !1, null)), b = a.dom.getElement("a"), n(b).attr("href", c).attr("title", d), n(b).attr("target", g)) : a.options.messages.nonSelection && n.dialog(a.options.messages.nonSelection));
						a.saveContent();
						n(j).dialog("close")
					});
					n("#wysiwyg_reset", j).click(function(a) {
						a.preventDefault();
						n(j).dialog("close")
					});
					n("select, input", j).uniform();
					n('input[name="linkhref"]', j).wl_URL()
				},
				close: function() {
					j.dialog("destroy");
					j.remove()
				}
			});
			n(a.editorDoc).trigger("editorRefresh.wysiwyg")
		}
	};
	n.wysiwyg.createLink = function(a, b) {
		return a.each(function() {
			var a = n(this).data("wysiwyg"),
			c;
			if (!a || !b || 0 === b.length) return this; (c = a.getRangeText()) && 0 < c.length ? (n.browser.msie && a.ui.focus(), a.editorDoc.execCommand("unlink", !1, null), a.editorDoc.execCommand("createLink", !1, b)) : a.options.messages.nonSelection && window.alert(a.options.messages.nonSelection)
		})
	};
	var j = jQuery;
	if (void 0 === j.wysiwyg) throw "wysiwyg.cssWrap.js depends on $.wysiwyg";
	j.wysiwyg.controls || (j.wysiwyg.controls = {});
	j.wysiwyg.controls.cssWrap = {
		init: function(a) {
			var b, c, d, g = {
				legend: "Wrap Element",
				wrapperType: "Wrapper Type",
				ID: "ID",
				"class": "Class",
				wrap: "Wrap",
				unwrap: "Unwrap",
				cancel: "Cancel"
			};
			b = '<form class="wysiwyg" title="{legend}"><fieldset>{wrapperType}: <select name="type"><option value="span">Span</option><option value="div">Div</option></select><br>{ID}: <input name="id" type="text"><br>{class}: <input name="class" type="text" ><hr><button class="cssWrap-unwrap" style="display:none;">{unwrap}</button> <button class="cssWrap-submit">{wrap}</button> <button class="cssWrap-cancel">{cancel}</button></fieldset></form>';
			for (c in g) j.wysiwyg.i18n && (d = j.wysiwyg.i18n.t(g[c]), d === g[c] && (d = j.wysiwyg.i18n.t(g[c], "dialogs")), g[c] = d),
			b = b.replace("{" + c + "}", g[c]);
			j(".wysiwyg-dialog-wrapper").length || (j(b).appendTo("body"), j("form.wysiwyg").dialog({
				modal: !0,
				resizable: !1,
				open: function() {
					$this = j(this);
					var b = a.getInternalRange(),
					c;
					if (b) j.browser.msie && a.ui.focus(),
					c = j(b.commonAncestorContainer);
					else return alert("You must select some elements before you can wrap them."),
					$this.dialog("close"),
					0;
					var d = b.commonAncestorContainer.nodeName.toLowerCase();
					c.parent(".wysiwygCssWrapper").length && (alert(c.parent(".wysiwygCssWrapper").get(0).nodeName.toLowerCase()), $this.find("select[name=type]").val(c.parent(".wysiwygCssWrapper").get(0).nodeName.toLowerCase()), $this.find("select[name=type]").attr("disabled", "disabled"), $this.find("input[name=id]").val(c.parent(".wysiwygCssWrapper").attr("id")), $this.find("input[name=class]").val(c.parent(".wysiwygCssWrapper").attr("class").replace("wysiwygCssWrapper ", "")), j("form.wysiwyg").find(".cssWrap-unwrap").show(), j("form.wysiwyg").find(".cssWrap-unwrap").click(function(a) {
						a.preventDefault();
						"body" !== d && c.unwrap();
						$this.dialog("close");
						return 1
					}));
					j("form.wysiwyg").find(".cssWrap-submit").click(function(a) {
						a.preventDefault();
						a = j("form.wysiwyg").find("select[name=type]").val();
						var b = j("form.wysiwyg").find("input[name=id]").val(),
						g = j("form.wysiwyg").find("input[name=class]").val();
						"body" !== d && (c.parent(".wysiwygCssWrapper").length ? (c.parent(".wysiwygCssWrapper").attr("id", g), c.parent(".wysiwygCssWrapper").attr("class", g)) : c.wrap("<" + a + ' id="' + b + '" class="wysiwygCssWrapper ' + g + '"/>'));
						$this.dialog("close")
					});
					j("form.wysiwyg").find(".cssWrap-cancel").click(function(a) {
						a.preventDefault();
						$this.dialog("close");
						return 1
					});
					j("form.wysiwyg").find("select, input[type=text]").uniform()
				},
				close: function() {
					j(this).dialog("destroy");
					j(this).remove()
				}
			}), a.saveContent());
			j(a.editorDoc).trigger("editorRefresh.wysiwyg");
			return 1
		}
	};
	var k = jQuery;
	if (void 0 === k.wysiwyg) throw "wysiwyg.colorpicker.js depends on $.wysiwyg";
	k.wysiwyg.controls || (k.wysiwyg.controls = {});
	k.wysiwyg.controls.colorpicker = {
		modalOpen: !1,
		init: function(a) {
			if (!0 === k.wysiwyg.controls.colorpicker.modalOpen) return ! 1;
			k.wysiwyg.controls.colorpicker.modalOpen = !0;
			var b, c, d, g, e;
			d = {
				legend: "Colorpicker",
				color: "Color",
				submit: "Apply",
				cancel: "Cancel"
			};
			c = '<form class="wysiwyg" title="{legend}">{color}: <input type="text" class="color" id="wysiwyg_colorpicker" name="wysiwyg_colorpicker" value=""><hr><button id="wysiwyg_colorpicker-submit">{submit}</button> <button id="wysiwyg_colorpicker-cancel">{cancel}</button></form>';
			for (g in d) k.wysiwyg.i18n && (e = k.wysiwyg.i18n.t(d[g], "dialogs.colorpicker"), e === d[g] && (e = k.wysiwyg.i18n.t(d[g], "dialogs")), d[g] = e),
			c = c.replace("{" + g + "}", d[g]);
			b = k(c).appendTo("body");
			b.dialog({
				modal: !0,
				resizable: !1,
				open: function() {
					k.browser.msie && a.ui.returnRange();
					a.getRangeText();
					var c = a.getContent(),
					d = "",
					g = /#([a-fA-F0-9]{3,6})/;
					c.match(g) ? (g.exec(c), d = RegExp.$1) : (g = /rgb\((\d+), (\d+), (\d+)\)/, c.match(g) && (g.exec(c), c = RegExp.$2, d = RegExp.$3, d = parseInt(RegExp.$1).toString(16) + parseInt(c).toString(16) + parseInt(d).toString(16)));
					k("#wysiwyg_colorpicker").val("#" + d).wl_Color();
					k("#wysiwyg_colorpicker-submit").click(function(c) {
						c.preventDefault();
						c = k("#wysiwyg_colorpicker").val();
						k.browser.msie && (a.ui.returnRange(), a.ui.focus());
						c && a.editorDoc.execCommand("ForeColor", !1, c);
						a.saveContent();
						k(b).dialog("close");
						return ! 1
					});
					k("#wysiwyg_colorpicker-cancel").click(function(c) {
						c.preventDefault();
						k.browser.msie && a.ui.returnRange();
						k(b).dialog("close");
						return ! 1
					})
				},
				close: function() {
					k.wysiwyg.controls.colorpicker.modalOpen = !1;
					b.dialog("destroy");
					b.remove()
				}
			})
		}
	}
});
$(document).ready(function() {
	var a = jQuery,
	e = void 0,
	b = function(b, k, r) {
		function t() {
			setTimeout(function() { ! C.start && 0 !== a("body")[0].offsetWidth && w()
			},
			0)
		}
		function p(b) {
			if (!C || b != C.name) {
				L++;
				H();
				var c = C,
				u;
				c ? ((c.beforeHide || va)(), ta(D, D.height()), c.element.hide()) : ta(D, 1);
				D.css("overflow", "hidden"); (C = R[b]) ? C.element.show() : C = R[b] = new T[b](u = Z = a("<div class='fc-view fc-view-" + b + "' style='position:absolute'/>").appendTo(D), K);
				c && Ra.deactivateButton(c.name);
				Ra.activateButton(b);
				w();
				D.css("overflow", "");
				c && ta(D, 1);
				u || (C.afterShow || va)();
				L--
			}
		}
		function w(a) {
			if (0 !== fa.offsetWidth) {
				L++;
				H();
				X === e && z();
				var c = !1; ! C.start || a || ca < C.start || ca >= C.end ? (C.render(ca, a || 0), pa(!0), c = !0) : C.sizeDirty ? (C.clearEvents(), pa(), c = !0) : C.eventsDirty && (C.clearEvents(), c = !0);
				C.sizeDirty = !1;
				C.eventsDirty = !1;
				a = c; ! k.lazyFetching || v(C.visStart, C.visEnd) ? u() : a && I();
				F = b.outerWidth();
				Ra.updateTitle(C.title);
				a = new Date;
				a >= C.start && a < C.end ? Ra.disableButton("today") : Ra.enableButton("today");
				L--;
				C.trigger("viewDisplay", fa)
			}
		}
		function G() {
			A();
			0 !== fa.offsetWidth && (z(), pa(), H(), C.clearEvents(), C.renderEvents(E), C.sizeDirty = !1)
		}
		function A() {
			a.each(R,
			function(a, b) {
				b.sizeDirty = !0
			})
		}
		function z() {
			X = k.contentHeight ? k.contentHeight: k.height ? k.height - (ra ? ra.height() : 0) - xa(D) : Math.round(D.width() / Math.max(k.aspectRatio, 0.5))
		}
		function pa(a) {
			L++;
			C.setHeight(X, a);
			Z && (Z.css("position", "relative"), Z = null);
			C.setWidth(D.width(), a);
			L--
		}
		function da() {
			if (!L) if (C.start) {
				var a = ++la;
				setTimeout(function() {
					if (a == la && (!L && 0 !== fa.offsetWidth) && F != (F = b.outerWidth())) L++,
					G(),
					C.trigger("windowResize", fa),
					L--
				},
				200)
			} else t()
		}
		function u() {
			ea(C.visStart, C.visEnd)
		}
		function I(a) {
			P();
			0 !== fa.offsetWidth && (C.clearEvents(), C.renderEvents(E, a), C.eventsDirty = !1)
		}
		function P() {
			a.each(R,
			function(a, b) {
				b.eventsDirty = !0
			})
		}
		function H() {
			C && C.unselect()
		}
		var K = this;
		K.options = k;
		K.render = function(u) {
			D ? (z(), A(), P(), w(u)) : (b.addClass("fc"), k.isRTL && b.addClass("fc-rtl"), k.theme && b.addClass("ui-widget"), D = a("<div class='fc-content' style='position:relative'/>").prependTo(b), Ra = new c(K, k), (ra = Ra.render()) && b.prepend(ra), p(k.defaultView), a(window).resize(da), 0 !== a("body")[0].offsetWidth || t())
		};
		K.destroy = function() {
			a(window).unbind("resize", da);
			Ra.destroy();
			D.remove();
			b.removeClass("fc fc-rtl ui-widget")
		};
		K.refetchEvents = u;
		K.reportEvents = function(a) {
			E = a;
			I()
		};
		K.reportEventChange = function(a) {
			I(a)
		};
		K.rerenderEvents = I;
		K.changeView = p;
		K.select = function(a, b, c) {
			C.select(a, b, c === e ? !0 : c)
		};
		K.unselect = H;
		K.prev = function() {
			w( - 1)
		};
		K.next = function() {
			w(1)
		};
		K.prevYear = function() {
			d(ca, -1);
			w()
		};
		K.nextYear = function() {
			d(ca, 1);
			w()
		};
		K.today = function() {
			ca = new Date;
			w()
		};
		K.gotoDate = function(a, b, c) {
			a instanceof Date ? ca = B(a) : q(ca, a, b, c);
			w()
		};
		K.incrementDate = function(a, b, c) {
			a !== e && d(ca, a);
			b !== e && n(ca, b);
			c !== e && j(ca, c);
			w()
		};
		K.formatDate = function(a, b) {
			return V(a, b, k)
		};
		K.formatDates = function(a, b, c) {
			return ha(a, b, c, k)
		};
		K.getDate = function() {
			return B(ca)
		};
		K.getView = function() {
			return C
		};
		K.option = function(a, b) {
			if (b === e) return k[a];
			if ("height" == a || "contentHeight" == a || "aspectRatio" == a) k[a] = b,
			G()
		};
		K.trigger = function(a, b) {
			if (k[a]) return k[a].apply(b || fa, Array.prototype.slice.call(arguments, 2))
		};
		g.call(K, k, r);
		var v = K.isFetchNeeded,
		ea = K.fetchEvents,
		fa = b[0],
		Ra,
		ra,
		D,
		C,
		R = {},
		F,
		X,
		Z,
		la = 0,
		L = 0,
		ca = new Date,
		E = [],
		J;
		q(ca, k.year, k.month, k.date);
		k.droppable && a(document).bind("dragstart",
		function(b, c) {
			var u = b.target,
			d = a(u);
			if (!d.parents(".fc").length) {
				var g = k.dropAccept;
				if (a.isFunction(g) ? g.call(u, d) : d.is(g)) J = u,
				C.dragStart(J, b, c)
			}
		}).bind("dragstop",
		function(a, b) {
			J && (C.dragStop(J, a, b), J = null)
		})
	},
	c = function(b, c) {
		function d(g) {
			var j = a("<td class='fc-header-" + g + "'/>"); (g = c.header[g]) && a.each(g.split(" "),
			function(d) {
				0 < d && j.append("<span class='fc-header-space'/>");
				var g;
				a.each(this.split(","),
				function(d, k) {
					if ("title" == k) j.append("<span class='fc-header-title'><h2>&nbsp;</h2></span>"),
					g && g.addClass(e + "-corner-right"),
					g = null;
					else {
						var u;
						b[k] ? u = b[k] : T[k] && (u = function() {
							n.removeClass(e + "-state-hover");
							b.changeView(k)
						});
						if (u) {
							var I = c.theme ? Ba(c.buttonIcons, k) : null,
							P = Ba(c.buttonText, k),
							n = a("<span class='fc-button fc-button-" + k + " " + e + "-state-default'><span class='fc-button-inner'><span class='fc-button-content'>" + (I ? "<span class='fc-icon-wrap'><span class='ui-icon ui-icon-" + I + "'/></span>": P) + "</span><span class='fc-button-effect'><span></span></span></span></span>");
							n && (n.click(function() {
								n.hasClass(e + "-state-disabled") || u()
							}).mousedown(function() {
								n.not("." + e + "-state-active").not("." + e + "-state-disabled").addClass(e + "-state-down")
							}).mouseup(function() {
								n.removeClass(e + "-state-down")
							}).hover(function() {
								n.not("." + e + "-state-active").not("." + e + "-state-disabled").addClass(e + "-state-hover")
							},
							function() {
								n.removeClass(e + "-state-hover").removeClass(e + "-state-down")
							}).appendTo(j), g || n.addClass(e + "-corner-left"), g = n)
						}
					}
				});
				g && g.addClass(e + "-corner-right")
			});
			return j
		}
		this.render = function() {
			e = c.theme ? "ui": "fc";
			if (c.header) return g = a("<table class='fc-header' style='width:100%'/>").append(a("<tr/>").append(d("left")).append(d("center")).append(d("right")))
		};
		this.destroy = function() {
			g.remove()
		};
		this.updateTitle = function(a) {
			g.find("h2").html(a)
		};
		this.activateButton = function(a) {
			g.find("span.fc-button-" + a).addClass(e + "-state-active")
		};
		this.deactivateButton = function(a) {
			g.find("span.fc-button-" + a).removeClass(e + "-state-active")
		};
		this.disableButton = function(a) {
			g.find("span.fc-button-" + a).addClass(e + "-state-disabled")
		};
		this.enableButton = function(a) {
			g.find("span.fc-button-" + a).removeClass(e + "-state-disabled")
		};
		var g = a([]),
		e
	},
	g = function(b, c) {
		function d(a, b) {
			g(a,
			function(c) {
				if (b == H) {
					if (c) {
						for (var u = 0; u < c.length; u++) c[u].source = a,
						k(c[u]);
						w = w.concat(c)
					}
					K--;
					K || t(w)
				}
			})
		}
		function g(c, u) {
			var d, e = aa.sourceFetchers,
			j;
			for (d = 0; d < e.length; d++) {
				j = e[d](c, I, P, u);
				if (!0 === j) return;
				if ("object" == typeof j) {
					g(j, u);
					return
				}
			}
			if (d = c.events) a.isFunction(d) ? (v++||r("loading", null, !0), d(B(I), B(P),
			function(a) {
				u(a); --v || r("loading", null, !1)
			})) : a.isArray(d) ? u(d) : u();
			else if (c.url) {
				var k = c.success,
				n = c.error,
				H = c.complete;
				d = a.extend({},
				c.data || {});
				e = M(c.startParam, b.startParam);
				j = M(c.endParam, b.endParam);
				e && (d[e] = Math.round( + I / 1E3));
				j && (d[j] = Math.round( + P / 1E3));
				v++||r("loading", null, !0);
				a.ajax(a.extend({},
				ra, c, {
					data: d,
					success: function(b) {
						b = b || [];
						var c = C(k, this, arguments);
						a.isArray(c) && (b = c);
						u(b)
					},
					error: function() {
						C(n, this, arguments);
						u()
					},
					complete: function() {
						C(H, this, arguments); --v || r("loading", null, !1)
					}
				}))
			} else u()
		}
		function j(b) {
			a.isFunction(b) || a.isArray(b) ? b = {
				events: b
			}: "string" == typeof b && (b = {
				url: b
			});
			if ("object" == typeof b) {
				var c = b;
				c.className ? "string" == typeof c.className && (c.className = c.className.split(/\s+/)) : c.className = [];
				for (var d = aa.sourceNormalizers,
				g = 0; g < d.length; g++) d[g](c);
				u.push(b);
				return b
			}
		}
		function k(a) {
			var c = a.source || {},
			u = M(c.ignoreTimezone, b.ignoreTimezone);
			a._id = a._id || (a.id === e ? "_fc" + da++:a.id + "");
			a.date && (a.start || (a.start = a.date), delete a.date);
			a._start = B(a.start = F(a.start, u));
			a.end = F(a.end, u);
			a.end && a.end <= a.start && (a.end = null);
			a._end = a.end ? B(a.end) : null;
			a.allDay === e && (a.allDay = M(c.allDayDefault, b.allDayDefault));
			a.className ? "string" == typeof a.className && (a.className = a.className.split(/\s+/)) : a.className = []
		}
		function n(a) {
			return ("object" == typeof a ? a.events || a.url: "") || a
		}
		this.isFetchNeeded = function(a, b) {
			return ! I || a < I || b > P
		};
		this.fetchEvents = function(a, b) {
			I = a;
			P = b;
			w = [];
			var c = ++H,
			g = u.length;
			K = g;
			for (var e = 0; e < g; e++) d(u[e], c)
		};
		this.addEventSource = function(a) {
			if (a = j(a)) K++,
			d(a, H)
		};
		this.removeEventSource = function(b) {
			u = a.grep(u,
			function(a) {
				return ! (a && b && n(a) == n(b))
			});
			w = a.grep(w,
			function(a) {
				return ! (a.source && b && n(a.source) == n(b))
			});
			t(w)
		};
		this.updateEvent = function(a) {
			var b, c = w.length,
			u, d = q().defaultEventEnd,
			g = a.start - a._start,
			e = a.end ? a.end - (a._end || d(a)) : 0;
			for (b = 0; b < c; b++) u = w[b],
			u._id == a._id && u != a && (u.start = new Date( + u.start + g), u.end = a.end ? u.end ? new Date( + u.end + e) : new Date( + d(u) + e) : null, u.title = a.title, u.url = a.url, u.allDay = a.allDay, u.className = a.className, u.editable = a.editable, u.color = a.color, u.backgroudColor = a.backgroudColor, u.borderColor = a.borderColor, u.textColor = a.textColor, k(u));
			k(a);
			t(w)
		};
		this.renderEvent = function(a, b) {
			k(a);
			a.source || (b && (p.events.push(a), a.source = p), w.push(a));
			t(w)
		};
		this.removeEvents = function(b) {
			if (b) {
				if (!a.isFunction(b)) {
					var c = b + "";
					b = function(a) {
						return a._id == c
					}
				}
				w = a.grep(w, b, !0);
				for (d = 0; d < u.length; d++) a.isArray(u[d].events) && (u[d].events = a.grep(u[d].events, b, !0))
			} else {
				w = [];
				for (var d = 0; d < u.length; d++) a.isArray(u[d].events) && (u[d].events = [])
			}
			t(w)
		};
		this.clientEvents = function(b) {
			return a.isFunction(b) ? a.grep(w, b) : b ? (b += "", a.grep(w,
			function(a) {
				return a._id == b
			})) : w
		};
		this.normalizeEvent = k;
		for (var r = this.trigger,
		q = this.getView,
		t = this.reportEvents,
		p = {
			events: []
		},
		u = [p], I, P, H = 0, K = 0, v = 0, w = [], G = 0; G < c.length; G++) j(c[G])
	},
	d = function(a, b, c) {
		a.setFullYear(a.getFullYear() + b);
		c || A(a);
		return a
	},
	n = function(a, b, c) {
		if ( + a) {
			b = a.getMonth() + b;
			var d = B(a);
			d.setDate(1);
			d.setMonth(b);
			a.setMonth(b);
			for (c || A(a); a.getMonth() != d.getMonth();) a.setDate(a.getDate() + (a < d ? 1 : -1))
		}
		return a
	},
	j = function(a, b, c) {
		if ( + a) {
			b = a.getDate() + b;
			var d = B(a);
			d.setHours(9);
			d.setDate(b);
			a.setDate(b);
			c || A(a);
			k(a, d)
		}
		return a
	},
	k = function(a, b) {
		if ( + a) for (; a.getDate() != b.getDate();) a.setTime( + a + (a < b ? 1 : -1) * oa)
	},
	t = function(a, b) {
		a.setMinutes(a.getMinutes() + b);
		return a
	},
	A = function(a) {
		a.setHours(0);
		a.setMinutes(0);
		a.setSeconds(0);
		a.setMilliseconds(0);
		return a
	},
	B = function(a, b) {
		return b ? A(new Date( + a)) : new Date( + a)
	},
	r = function() {
		var a = 0,
		b;
		do b = new Date(1970, a++, 1);
		while (b.getHours());
		return b
	},
	p = function(a, b, c) {
		for (b = b || 1; ! a.getDay() || c && 1 == a.getDay() || !c && 6 == a.getDay();) j(a, b);
		return a
	},
	z = function(a, b) {
		return Math.round((B(a, !0) - B(b, !0)) / fa)
	},
	q = function(a, b, c, d) {
		b !== e && b != a.getFullYear() && (a.setDate(1), a.setMonth(0), a.setFullYear(b));
		c !== e && c != a.getMonth() && (a.setDate(1), a.setMonth(c));
		d !== e && a.setDate(d)
	},
	F = function(a, b) {
		if ("object" == typeof a) return a;
		if ("number" == typeof a) return new Date(1E3 * a);
		if ("string" == typeof a) {
			if (a.match(/^\d+(\.\d+)?$/)) return new Date(1E3 * parseFloat(a));
			b === e && (b = !0);
			return D(a, b) || (a ? new Date(a) : null)
		}
		return null
	},
	D = function(a, b) {
		var c = a.match(/^([0-9]{4})(-([0-9]{2})(-([0-9]{2})([T ]([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2})(:?([0-9]{2}))?))?)?)?)?$/);
		if (!c) return null;
		var d = new Date(c[1], 0, 1);
		if (b || !c[14]) {
			var g = new Date(c[1], 0, 1, 9, 0);
			c[3] && (d.setMonth(c[3] - 1), g.setMonth(c[3] - 1));
			c[5] && (d.setDate(c[5]), g.setDate(c[5]));
			k(d, g);
			c[7] && d.setHours(c[7]);
			c[8] && d.setMinutes(c[8]);
			c[10] && d.setSeconds(c[10]);
			c[12] && d.setMilliseconds(1E3 * Number("0." + c[12]));
			k(d, g)
		} else d.setUTCFullYear(c[1], c[3] ? c[3] - 1 : 0, c[5] || 1),
		d.setUTCHours(c[7] || 0, c[8] || 0, c[10] || 0, c[12] ? 1E3 * Number("0." + c[12]) : 0),
		g = 60 * Number(c[16]) + (c[18] ? Number(c[18]) : 0),
		g *= "-" == c[15] ? 1 : -1,
		d = new Date( + d + 6E4 * g);
		return d
	},
	E = function(a) {
		if ("number" == typeof a) return 60 * a;
		if ("object" == typeof a) return 60 * a.getHours() + a.getMinutes();
		if (a = a.match(/(\d+)(?::(\d+))?\s*(\w+)?/)) {
			var b = parseInt(a[1], 10);
			a[3] && (b %= 12, "p" == a[3].toLowerCase().charAt(0) && (b += 12));
			return 60 * b + (a[2] ? parseInt(a[2], 10) : 0)
		}
	},
	V = function(a, b, c) {
		return ha(a, null, b, c)
	},
	ha = function(a, b, c, d) {
		d = d || Q;
		var g = a,
		e = b,
		j, k = c.length,
		n, r, q, u = "";
		for (j = 0; j < k; j++) if (n = c.charAt(j), "'" == n) for (r = j + 1; r < k; r++) {
			if ("'" == c.charAt(r)) {
				g && (u = r == j + 1 ? u + "'": u + c.substring(j + 1, r), j = r);
				break
			}
		} else if ("(" == n) for (r = j + 1; r < k; r++) {
			if (")" == c.charAt(r)) {
				j = V(g, c.substring(j + 1, r), d);
				parseInt(j.replace(/\D/, ""), 10) && (u += j);
				j = r;
				break
			}
		} else if ("[" == n) for (r = j + 1; r < k; r++) {
			if ("]" == c.charAt(r)) {
				n = c.substring(j + 1, r);
				j = V(g, n, d);
				j != V(e, n, d) && (u += j);
				j = r;
				break
			}
		} else if ("{" == n) g = b,
		e = a;
		else if ("}" == n) g = a,
		e = b;
		else {
			for (r = k; r > j; r--) if (q = Ea[c.substring(j, r)]) {
				g && (u += q(g, d));
				j = r - 1;
				break
			}
			r == j && g && (u += n)
		}
		return u
	},
	W = function(a) {
		var b;
		a.end ? (b = a.end, a = a.allDay, b = B(b), b = a || b.getHours() || b.getMinutes() ? j(b, 1) : A(b)) : b = j(B(a.start), 1);
		return b
	},
	sa = function(a, b) {
		return 100 * (b.msLength - a.msLength) + (a.event.start - b.event.start)
	},
	S = function(a, b, c, d) {
		var g = [],
		e,
		j = a.length,
		k,
		n,
		r,
		q,
		u;
		for (e = 0; e < j; e++) k = a[e],
		n = k.start,
		r = b[e],
		r > c && n < d && (n < c ? (n = B(c), q = !1) : q = !0, r > d ? (r = B(d), u = !1) : u = !0, g.push({
			event: k,
			start: n,
			end: r,
			isStart: q,
			isEnd: u,
			msLength: r - n
		}));
		return g.sort(sa)
	},
	U = function(a) {
		var b = [],
		c,
		d = a.length,
		g,
		e,
		j,
		n;
		for (c = 0; c < d; c++) {
			g = a[c];
			for (e = 0;;) {
				j = !1;
				if (b[e]) for (n = 0; n < b[e].length; n++) if (b[e][n].end > g.start && b[e][n].start < g.end) {
					j = !0;
					break
				}
				if (j) e++;
				else break
			}
			b[e] ? b[e].push(g) : b[e] = [g]
		}
		return b
	},
	ba = function(b, c, d) {
		b.unbind("mouseover").mouseover(function(b) {
			for (var g = b.target,
			j; g != this;) j = g,
			g = g.parentNode;
			if ((g = j._fci) !== e) j._fci = e,
			j = c[g],
			d(j.event, j.element, j),
			a(b.target).trigger(b);
			b.stopPropagation()
		})
	},
	ia = function(b, c, d) {
		for (var g = 0,
		e; g < b.length; g++) e = a(b[g]),
		e.width(Math.max(0, c - Y(e, d)))
	},
	ga = function(b, c, d) {
		for (var g = 0,
		e; g < b.length; g++) e = a(b[g]),
		e.height(Math.max(0, c - xa(e, d)))
	},
	Y = function(b, c) {
		return (parseFloat(a.curCSS(b[0], "paddingLeft", !0)) || 0) + (parseFloat(a.curCSS(b[0], "paddingRight", !0)) || 0) + ((parseFloat(a.curCSS(b[0], "borderLeftWidth", !0)) || 0) + (parseFloat(a.curCSS(b[0], "borderRightWidth", !0)) || 0)) + (c ? (parseFloat(a.curCSS(b[0], "marginLeft", !0)) || 0) + (parseFloat(a.curCSS(b[0], "marginRight", !0)) || 0) : 0)
	},
	xa = function(b, c) {
		return (parseFloat(a.curCSS(b[0], "paddingTop", !0)) || 0) + (parseFloat(a.curCSS(b[0], "paddingBottom", !0)) || 0) + ((parseFloat(a.curCSS(b[0], "borderTopWidth", !0)) || 0) + (parseFloat(a.curCSS(b[0], "borderBottomWidth", !0)) || 0)) + (c ? ua(b) : 0)
	},
	ua = function(b) {
		return (parseFloat(a.curCSS(b[0], "marginTop", !0)) || 0) + (parseFloat(a.curCSS(b[0], "marginBottom", !0)) || 0)
	},
	ta = function(a, b) {
		b = "number" == typeof b ? b + "px": b;
		a.each(function(a, c) {
			c.style.cssText += ";min-height:" + b + ";_height:" + b
		})
	},
	va = function() {},
	N = function(a, b) {
		return a - b
	},
	na = function(a) {
		return (10 > a ? "0": "") + a
	},
	Ba = function(a, b) {
		if (a[b] !== e) return a[b];
		for (var c = b.split(/(?=[A-Z])/), d = c.length - 1, g; 0 <= d; d--) if (g = a[c[d].toLowerCase()], g !== e) return g;
		return a[""]
	},
	qa = function(a) {
		return a.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#039;").replace(/"/g, "&quot;").replace(/\n/g, "<br />")
	},
	G = function(a) {
		return a.id + "/" + a.className + "/" + a.style.cssText.replace(/(^|;)\s*(top|left|width|height)\s*:[^;]*/ig, "")
	},
	w = function(a) {
		a.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui",
		function() {
			return ! 1
		})
	},
	R = function(a) {
		a.children().removeClass("fc-first fc-last").filter(":first-child").addClass("fc-first").end().filter(":last-child").addClass("fc-last")
	},
	la = function(a, b) {
		a.each(function(a, c) {
			c.className = c.className.replace(/^fc-\w*/, "fc-" + pa[b.getDay()])
		})
	},
	L = function(a, b) {
		var c = a.source || {},
		d = a.color,
		g = c.color,
		e = b("eventColor"),
		j = a.backgroundColor || d || c.backgroundColor || g || b("eventBackgroundColor") || e,
		d = a.borderColor || d || c.borderColor || g || b("eventBorderColor") || e,
		c = a.textColor || c.textColor || b("eventTextColor"),
		g = [];
		j && g.push("background-color:" + j);
		d && g.push("border-color:" + d);
		c && g.push("color:" + c);
		return g.join(";")
	},
	C = function(b, c, d) {
		a.isFunction(b) && (b = [b]);
		if (b) {
			var g, e;
			for (g = 0; g < b.length; g++) e = b[g].apply(c, d) || e;
			return e
		}
	},
	M = function() {
		for (var a = 0; a < arguments.length; a++) if (arguments[a] !== e) return arguments[a]
	},
	ja = function(b, c, d) {
		function g(a) {
			if (!u("selectable")) {
				var b = parseInt(this.className.match(/fc\-day(\d+)/)[1]),
				b = q(b);
				I("dayClick", this, b, !0, a)
			}
		}
		function e(a, b, c) {
			c && J.build();
			c = B(p.visStart);
			for (var u = j(B(c), E), d = 0; d < ca; d++) {
				var I = new Date(Math.max(c, a)),
				P = new Date(Math.min(u, b));
				if (I < P) {
					var k;
					oa ? (k = z(P, c) * ba + wa + 1, I = z(I, c) * ba + wa + 1) : (k = z(I, c), I = z(P, c));
					n(d, k, d, I - 1).click(g).mousedown(v)
				}
				j(c, 7);
				j(u, 7)
			}
		}
		function n(a, c, u, d) {
			a = J.rect(a, c, u, d, b);
			return H(a, b)
		}
		function k(a) {
			return {
				row: Math.floor(z(a, p.visStart) / 7),
				col: t(a.getDay())
			}
		}
		function r(a) {
			return j(B(p.visStart), 7 * a.row + a.col * ba + wa)
		}
		function q(a) {
			return j(B(p.visStart), 7 * Math.floor(a / E) + a % E * ba + wa)
		}
		function t(a) {
			return (a - Math.max(Ma, Ya) + E) % E * ba + wa
		}
		var p = this;
		p.renderBasic = function(c, d, e, j) {
			ca = d;
			E = e; (oa = u("isRTL")) ? (ba = -1, wa = E - 1) : (ba = 1, wa = 0);
			Ma = u("firstDay");
			Ya = u("weekends") ? 0 : 1;
			Fa = u("theme") ? "ui": "fc";
			Ka = u("columnFormat");
			if (d = !da) {
				var I = Fa + "-widget-header",
				n = Fa + "-widget-content",
				k;
				e = "<table class='fc-border-separate' style='width:100%' cellspacing='0'><thead><tr>";
				for (k = 0; k < E; k++) e += "<th class='fc- " + I + "'/>";
				e += "</tr></thead><tbody>";
				for (k = 0; k < c; k++) {
					e += "<tr class='fc-week" + k + "'>";
					for (I = 0; I < E; I++) e += "<td class='fc- " + n + " fc-day" + (k * E + I) + "'><div>" + (j ? "<div class='fc-day-number'/>": "") + "<div class='fc-day-content'><div style='position:relative'>&nbsp;</div></div></div></td>";
					e += "</tr>"
				}
				c = a(e + "</tbody></table>").appendTo(b);
				T = c.find("thead");
				pa = T.find("th");
				da = c.find("tbody");
				fa = da.find("tr");
				ra = da.find("td");
				C = ra.filter(":first-child");
				D = fa.eq(0).find("div.fc-day-content div");
				R(T.add(T.find("tr")));
				R(fa);
				fa.eq(0).addClass("fc-first");
				ra.click(g).mousedown(v);
				F = a("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(b)
			} else P();
			var H = d || 1 == ca,
			K = p.start.getMonth(),
			r = A(new Date),
			w,
			t,
			z;
			H && pa.each(function(b, c) {
				w = a(c);
				t = q(b);
				w.html(G(t, Ka));
				la(w, t)
			});
			ra.each(function(b, c) {
				w = a(c);
				t = q(b);
				t.getMonth() == K ? w.removeClass("fc-other-month") : w.addClass("fc-other-month"); + t == +r ? w.addClass(Fa + "-state-highlight fc-today") : w.removeClass(Fa + "-state-highlight fc-today");
				w.find("div.fc-day-number").text(t.getDate());
				H && la(w, t)
			});
			fa.each(function(b, c) {
				z = a(c);
				b < ca ? (z.show(), b == ca - 1 ? z.addClass("fc-last") : z.removeClass("fc-last")) : z.hide()
			})
		};
		p.setHeight = function(b) {
			Z = b;
			b = Z - T.height();
			var c, d, g;
			"variable" == u("weekMode") ? c = d = Math.floor(b / (1 == ca ? 2 : 6)) : (c = Math.floor(b / ca), d = b - c * (ca - 1));
			C.each(function(b, u) {
				b < ca && (g = a(u), ta(g.find("> div"), (b == ca - 1 ? d: c) - xa(g)))
			})
		};
		p.setWidth = function(a) {
			X = a;
			U.clear();
			L = Math.floor(X / E);
			ia(pa.slice(0, -1), L)
		};
		p.renderDayOverlay = e;
		p.defaultSelectionEnd = function(a) {
			return B(a)
		};
		p.renderSelection = function(a, b) {
			e(a, j(B(b), 1), !0)
		};
		p.clearSelection = function() {
			K()
		};
		p.reportDayClick = function(a, b, c) {
			var u = k(a);
			I("dayClick", ra[u.row * E + u.col], a, b, c)
		};
		p.dragStart = function(a, b) {
			M.start(function(a) {
				K();
				a && n(a.row, a.col, a.row, a.col)
			},
			b)
		};
		p.dragStop = function(a, b, c) {
			var u = M.stop();
			K();
			u && (u = r(u), I("drop", a, u, !0, b, c))
		};
		p.defaultEventEnd = function(a) {
			return B(a.start)
		};
		p.getHoverListener = function() {
			return M
		};
		p.colContentLeft = function(a) {
			return U.left(a)
		};
		p.colContentRight = function(a) {
			return U.right(a)
		};
		p.dayOfWeekCol = t;
		p.dateCell = k;
		p.cellDate = r;
		p.cellIsAllDay = function() {
			return ! 0
		};
		p.allDayRow = function(a) {
			return fa.eq(a)
		};
		p.allDayBounds = function() {
			return {
				left: 0,
				right: X
			}
		};
		p.getRowCnt = function() {
			return ca
		};
		p.getColCnt = function() {
			return E
		};
		p.getColWidth = function() {
			return L
		};
		p.getDaySegmentContainer = function() {
			return F
		};
		Aa.call(p, b, c, d);
		Ca.call(p);
		Ga.call(p);
		za.call(p);
		var u = p.opt,
		I = p.trigger,
		P = p.clearEvents,
		H = p.renderOverlay,
		K = p.clearOverlays,
		v = p.daySelectionMousedown,
		G = c.formatDate,
		T, pa, da, fa, ra, C, D, F, X, Z, L, ca, E, J, M, U, oa, ba, wa, Ma, Ya, Fa, Ka;
		w(b.addClass("fc-grid"));
		J = new ka(function(b, c) {
			var u, d, g;
			pa.each(function(b, e) {
				u = a(e);
				d = u.offset().left;
				b && (g[1] = d);
				g = [d];
				c[b] = g
			});
			g[1] = d + u.outerWidth();
			fa.each(function(c, e) {
				c < ca && (u = a(e), d = u.offset().top, c && (g[1] = d), g = [d], b[c] = g)
			});
			g[1] = d + u.outerHeight()
		});
		M = new Pa(J);
		U = new Qa(function(a) {
			return D.eq(a)
		})
	},
	za = function() {
		function b(u) {
			var d = v(),
			g = t(),
			e = B(c.visStart),
			g = j(B(e), g),
			I = a.map(u, W),
			P,
			k,
			n,
			H,
			K,
			r,
			p = [];
			for (P = 0; P < d; P++) {
				k = U(S(u, I, e, g));
				for (n = 0; n < k.length; n++) {
					H = k[n];
					for (K = 0; K < H.length; K++) r = H[K],
					r.row = P,
					r.level = n,
					p.push(r)
				}
				j(e, 7);
				j(g, 7)
			}
			return p
		}
		var c = this;
		c.renderEvents = function(a, c) {
			n(a);
			G(b(a), c)
		};
		c.compileDaySegs = b;
		c.clearEvents = function() {
			r();
			I().empty()
		};
		c.bindDaySeg = function(a, b, c) {
			if (e(a)) {
				var I = P(),
				n;
				b.draggable({
					zIndex: 9,
					delay: 50,
					opacity: d("dragOpacity"),
					revertDuration: d("dragRevertDuration"),
					start: function(c, u) {
						g("eventDragStart", b, a, c, u);
						w(a, b);
						I.start(function(c, u, g, e) {
							b.draggable("option", "revert", !c || !g && !e);
							K();
							c ? (n = 7 * g + e * (d("isRTL") ? -1 : 1), H(j(B(a.start), n), j(W(a), n))) : n = 0
						},
						c, "drag")
					},
					stop: function(c, d) {
						I.stop();
						K();
						g("eventDragStop", b, a, c, d);
						n ? u(this, a, n, 0, a.allDay, c, d) : (b.css("filter", ""), q(a, b))
					}
				})
			}
			c.isEnd && k(a) && T(a, b, c);
			p(a, b)
		};
		ma.call(c);
		var d = c.opt,
		g = c.trigger,
		e = c.isEventDraggable,
		k = c.isEventResizable,
		n = c.reportEvents,
		r = c.reportEventClear,
		p = c.eventElementHandlers,
		q = c.showEvents,
		w = c.hideEvents,
		u = c.eventDrop,
		I = c.getDaySegmentContainer,
		P = c.getHoverListener,
		H = c.renderDayOverlay,
		K = c.clearOverlays,
		v = c.getRowCnt,
		t = c.getColCnt,
		G = c.renderDaySegs,
		T = c.resizableDayEvent
	},
	J = function(b, c, d) {
		function g(a) {
			if (!ea("selectable")) {
				var b = Math.min(La - 1, Math.floor((a.pageX - Z.offset().left - ta) / W)),
				c = G(b),
				u = this.parentNode.className.match(/fc-slot(\d+)/);
				u ? (u = parseInt(u[1]) * ea("slotMinutes"), c.setHours(Math.floor(u / 60)), c.setMinutes(u % 60 + Za), da("dayClick", ba[b], c, !1, a)) : da("dayClick", ba[b], c, !0, a)
			}
		}
		function n(a, b, c) {
			c && na.build();
			var u = B(v.visStart);
			hb ? (c = z(b, u) * Sa + Ta + 1, a = z(a, u) * Sa + Ta + 1) : (c = z(a, u), a = z(b, u));
			c = Math.max(0, c);
			a = Math.min(La, a);
			c < a && k(0, c, 0, a - 1).click(g).mousedown(X)
		}
		function k(a, b, c, u) {
			a = na.rect(a, b, c, u, Y);
			return ra(a, Y)
		}
		function p(a, b) {
			for (var c = B(v.visStart), d = j(B(c), 1), e = 0; e < La; e++) {
				var I = new Date(Math.max(c, a)),
				n = new Date(Math.min(d, b));
				if (I < n) {
					var P = e * Sa + Ta,
					P = na.rect(0, P, 0, P, Ka),
					I = u(c, I),
					n = u(c, n);
					P.top = I;
					P.height = n - I;
					ra(P, Ka).click(g).mousedown(H)
				}
				j(c, 1);
				j(d, 1)
			}
		}
		function q(a) {
			var b = G(a.col);
			a = a.row;
			ea("allDaySlot") && a--;
			0 <= a && t(b, Za + a * ea("slotMinutes"));
			return b
		}
		function G(a) {
			return j(B(v.visStart), a * Sa + Ta)
		}
		function T(a) {
			return ea("allDaySlot") && !a.row
		}
		function pa(a) {
			return (a - Math.max(sa, Da) + La) % La * Sa + Ta
		}
		function u(a, b) {
			a = B(a, !0);
			if (b < t(B(a), Za)) return 0;
			if (b >= t(B(a), cb)) return Xa.height();
			var c = ea("slotMinutes"),
			u = 60 * b.getHours() + b.getMinutes() - Za,
			d = Math.floor(u / c),
			g = va[d];
			g === e && (g = va[d] = Xa.find("tr:eq(" + d + ") td div")[0].offsetTop);
			return Math.max(0, Math.round(g - 1 + aa * (u % c / c)))
		}
		function I(b, c) {
			var d = ea("selectHelper");
			na.build();
			if (d) {
				var e = z(b, v.visStart) * Sa + Ta;
				if (0 <= e && e < La) {
					var e = na.rect(0, e, 0, e, Ka),
					j = u(b, b),
					I = u(b, c);
					if (I > j) {
						e.top = j;
						e.height = I - j;
						e.left += 2;
						e.width -= 5;
						if (a.isFunction(d)) {
							if (d = d(b, c)) e.position = "absolute",
							e.zIndex = 8,
							Q = a(d).css(e).appendTo(Ka)
						} else e.isStart = !0,
						e.isEnd = !0,
						Q = a(ca({
							title: "",
							start: b,
							end: c,
							className: ["fc-select-helper"],
							editable: !1
						},
						e)),
						Q.css("opacity", ea("dragOpacity"));
						Q && (Q.click(g).mousedown(H), Ka.append(Q), ia(Q, e.width, !0), ga(Q, e.height, !0))
					}
				}
			} else p(b, c)
		}
		function P() {
			C();
			Q && (Q.remove(), Q = null)
		}
		function H(b) {
			if (1 == b.which && ea("selectable")) {
				F(b);
				var c;
				ua.start(function(a, b) {
					P();
					if (a && a.col == b.col && !T(a)) {
						var u = q(b),
						d = q(a);
						c = [u, t(B(u), ea("slotMinutes")), d, t(B(d), ea("slotMinutes"))].sort(N);
						I(c[0], c[3])
					} else c = null
				},
				b);
				a(document).one("mouseup",
				function(a) {
					ua.stop();
					c && ( + c[0] == +c[1] && K(c[0], !1, a), D(c[0], c[3], !1, a))
				})
			}
		}
		function K(a, b, c) {
			da("dayClick", ba[pa(a.getDay())], a, b, c)
		}
		var v = this;
		v.renderAgenda = function(c) {
			La = c;
			qa = ea("theme") ? "ui": "fc";
			Da = ea("weekends") ? 0 : 1;
			sa = ea("firstDay"); (hb = ea("isRTL")) ? (Sa = -1, Ta = La - 1) : (Sa = 1, Ta = 0);
			Za = E(ea("minTime"));
			cb = E(ea("maxTime"));
			ib = ea("columnFormat");
			if (Z) fa();
			else {
				c = qa + "-widget-header";
				var u = qa + "-widget-content",
				d, e, j, I, n, P = 0 == ea("slotMinutes") % 15;
				d = "<table style='width:100%' class='fc-agenda-days fc-border-separate' cellspacing='0'><thead><tr><th class='fc-agenda-axis " + c + "'>&nbsp;</th>";
				for (e = 0; e < La; e++) d += "<th class='fc- fc-col" + e + " " + c + "'/>";
				d += "<th class='fc-agenda-gutter " + c + "'>&nbsp;</th></tr></thead><tbody><tr><th class='fc-agenda-axis " + c + "'>&nbsp;</th>";
				for (e = 0; e < La; e++) d += "<td class='fc- fc-col" + e + " " + u + "'><div><div class='fc-day-content'><div style='position:relative'>&nbsp;</div></div></div></td>";
				Z = a(d + ("<td class='fc-agenda-gutter " + u + "'>&nbsp;</td></tr></tbody></table>")).appendTo(b);
				J = Z.find("thead");
				M = J.find("th").slice(1, -1);
				U = Z.find("tbody");
				ba = U.find("td").slice(0, -1);
				oa = ba.find("div.fc-day-content div");
				wa = ba.eq(0);
				Ma = wa.find("> div");
				R(J.add(J.find("tr")));
				R(U.add(U.find("tr")));
				ja = J.find("th:first");
				S = Z.find(".fc-agenda-gutter");
				Y = a("<div style='position:absolute;z-index:2;left:0;width:100%'/>").appendTo(b);
				ea("allDaySlot") ? (Ha = a("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(Y), d = "<table style='width:100%' class='fc-agenda-allday' cellspacing='0'><tr><th class='" + c + " fc-agenda-axis'>" + ea("allDayText") + "</th><td><div class='fc-day-content'><div style='position:relative'/></div></td><th class='" + c + " fc-agenda-gutter'>&nbsp;</th></tr></table>", Ea = a(d).appendTo(Y), Ya = Ea.find("tr"), Ya.find("td").click(g).mousedown(X), ja = ja.add(Ea.find("th:first")), S = S.add(Ea.find("th.fc-agenda-gutter")), Y.append("<div class='fc-agenda-divider " + c + "'><div class='fc-agenda-divider-inner'/></div>")) : Ha = a([]);
				Fa = a("<div style='position:absolute;width:100%;overflow-x:hidden;overflow-y:auto'/>").appendTo(Y);
				Ka = a("<div style='position:relative;width:100%;overflow:hidden'/>").appendTo(Fa);
				Ua = a("<div style='position:absolute;z-index:8;top:0;left:0'/>").appendTo(Ka);
				d = "<table class='fc-agenda-slots' style='width:100%' cellspacing='0'><tbody>";
				j = r();
				I = t(B(j), cb);
				t(j, Za);
				for (e = ha = 0; j < I; e++) n = j.getMinutes(),
				d += "<tr class='fc-slot" + e + " " + (!n ? "": "fc-minor") + "'><th class='fc-agenda-axis " + c + "'>" + (!P || !n ? L(j, ea("axisFormat")) : "&nbsp;") + "</th><td class='" + u + "'><div style='position:relative'>&nbsp;</div></td></tr>",
				t(j, ea("slotMinutes")),
				ha++;
				d += "</tbody></table>";
				Xa = a(d).appendTo(Ka);
				V = Xa.find("div:first");
				Xa.find("td").click(g).mousedown(H);
				ja = ja.add(Xa.find("th:first"))
			}
			j = A(new Date);
			for (c = 0; c < La; c++) e = G(c),
			u = M.eq(c),
			u.html(L(e, ib)),
			d = ba.eq(c),
			+e == +j ? d.addClass(qa + "-state-highlight fc-today") : d.removeClass(qa + "-state-highlight fc-today"),
			la(u.add(d), e)
		};
		v.setWidth = function(b) {
			ma = b;
			db.clear();
			ta = 0;
			ia(ja.width("").each(function(b, c) {
				ta = Math.max(ta, a(c).outerWidth())
			}), ta);
			b = Fa[0].clientWidth; (za = Fa.width() - b) ? (ia(S, za), S.show().prev().removeClass("fc-last")) : S.hide().prev().addClass("fc-last");
			W = Math.floor((b - ta) / La);
			ia(M.slice(0, -1), W)
		};
		v.setHeight = function(a, b) {
			a === e && (a = gb);
			gb = a;
			va = {};
			var c = U.position().top,
			d = Fa.position().top,
			g = Math.min(a - c, Xa.height() + d + 1);
			Ma.height(g - xa(wa));
			Y.css("top", c);
			Fa.height(g - d - 1);
			aa = V.height() + 1;
			if (b) {
				c = function() {
					Fa.scrollTop(j)
				};
				d = r();
				g = B(d);
				g.setHours(ea("firstHour"));
				var j = u(d, g) + 1;
				c();
				setTimeout(c, 0)
			}
		};
		v.beforeHide = function() {
			Ba = Fa.scrollTop()
		};
		v.afterShow = function() {
			Fa.scrollTop(Ba)
		};
		v.defaultEventEnd = function(a) {
			var b = B(a.start);
			return a.allDay ? b: t(b, ea("defaultEventMinutes"))
		};
		v.timePosition = u;
		v.dayOfWeekCol = pa;
		v.dateCell = function(a) {
			return {
				row: Math.floor(z(a, v.visStart) / 7),
				col: pa(a.getDay())
			}
		};
		v.cellDate = q;
		v.cellIsAllDay = T;
		v.allDayRow = function() {
			return Ya
		};
		v.allDayBounds = function() {
			return {
				left: ta,
				right: ma - za
			}
		};
		v.getHoverListener = function() {
			return ua
		};
		v.colContentLeft = function(a) {
			return db.left(a)
		};
		v.colContentRight = function(a) {
			return db.right(a)
		};
		v.getDaySegmentContainer = function() {
			return Ha
		};
		v.getSlotSegmentContainer = function() {
			return Ua
		};
		v.getMinMinute = function() {
			return Za
		};
		v.getMaxMinute = function() {
			return cb
		};
		v.getBodyContent = function() {
			return Ka
		};
		v.getRowCnt = function() {
			return 1
		};
		v.getColCnt = function() {
			return La
		};
		v.getColWidth = function() {
			return W
		};
		v.getSlotHeight = function() {
			return aa
		};
		v.defaultSelectionEnd = function(a, b) {
			return b ? B(a) : t(B(a), ea("slotMinutes"))
		};
		v.renderDayOverlay = n;
		v.renderSelection = function(a, b, c) {
			c ? ea("allDaySlot") && n(a, j(B(b), 1), !0) : I(a, b)
		};
		v.clearSelection = P;
		v.reportDayClick = K;
		v.dragStart = function(a, b) {
			ua.start(function(a) {
				C();
				if (a) if (T(a)) k(a.row, a.col, a.row, a.col);
				else {
					a = q(a);
					var b = t(B(a), ea("defaultEventMinutes"));
					p(a, b)
				}
			},
			b)
		};
		v.dragStop = function(a, b, c) {
			var u = ua.stop();
			C();
			u && da("drop", a, q(u), T(u), b, c)
		};
		Aa.call(v, b, c, d);
		Ca.call(v);
		Ga.call(v);
		ya.call(v);
		var ea = v.opt,
		da = v.trigger,
		fa = v.clearEvents,
		ra = v.renderOverlay,
		C = v.clearOverlays,
		D = v.reportSelection,
		F = v.unselect,
		X = v.daySelectionMousedown,
		ca = v.slotSegHtml,
		L = c.formatDate,
		Z, J, M, U, ba, oa, wa, Ma, Y, Ha, Ea, Ya, Fa, Ka, Ua, Xa, V, ja, S, Q, ma, gb, ta, W, za, aa, Ba, La, ha, na, ua, db, va = {},
		qa, sa, Da, hb, Sa, Ta, Za, cb, ib;
		w(b.addClass("fc-agenda"));
		na = new ka(function(b, c) {
			var u, d, g;
			M.each(function(b, e) {
				u = a(e);
				d = u.offset().left;
				b && (g[1] = d);
				g = [d];
				c[b] = g
			});
			g[1] = d + u.outerWidth();
			ea("allDaySlot") && (u = Ya, d = u.offset().top, b[0] = [d, d + u.outerHeight()]);
			for (var e = Ka.offset().top, j = Fa.offset().top, I = j + Fa.outerHeight(), n = 0; n < ha; n++) b.push([Math.max(j, Math.min(I, e + aa * n)), Math.max(j, Math.min(I, e + aa * (n + 1)))])
		});
		ua = new Pa(na);
		db = new Qa(function(a) {
			return oa.eq(a)
		})
	},
	ya = function() {
		function b(c) {
			c = U(S(c, a.map(c, W), n.visStart, n.visEnd));
			var u, d = c.length,
			g, e, j, I = [];
			for (u = 0; u < d; u++) {
				g = c[u];
				for (e = 0; e < g.length; e++) j = g[e],
				j.row = 0,
				j.level = u,
				I.push(j)
			}
			return I
		}
		function c(a) {
			return a.end ? B(a.end) : t(B(a.start), k("defaultEventMinutes"))
		}
		function d(a, b) {
			var c = "<",
			u = a.url,
			g = L(a, k),
			e = g ? " style='" + g + "'": "",
			j = ["fc-event", "fc-event-skin", "fc-event-vert"];
			p(a) && j.push("fc-event-draggable");
			b.isStart && j.push("fc-corner-top");
			b.isEnd && j.push("fc-corner-bottom");
			j = j.concat(a.className);
			a.source && (j = j.concat(a.source.className || []));
			c = u ? c + ("a href='" + qa(a.url) + "'") : c + "div";
			c += " class='" + j.join(" ") + "' style='position:absolute;z-index:8;top:" + b.top + "px;left:" + b.left + "px;" + g + "'><div class='fc-event-inner fc-event-skin'" + e + "><div class='fc-event-head fc-event-skin'" + e + "><div class='fc-event-time'>" + qa(N(a.start, a.end, k("timeFormat"))) + "</div></div><div class='fc-event-content'><div class='fc-event-title'>" + qa(a.title) + "</div></div><div class='fc-event-bg'></div></div>";
			b.isEnd && q(a) && (c += "<div class='ui-resizable-handle ui-resizable-s'>=</div>");
			return c + ("</" + (u ? "a": "div") + ">")
		}
		function g(a, b, c) {
			var u = b.find("div.fc-event-time");
			if (p(a)) {
				var d = function(b) {
					var c = t(B(a.start), b),
					d;
					a.end && (d = t(B(a.end), b));
					u.text(N(c, d, k("timeFormat")))
				},
				e = function() {
					P && (u.css("display", ""), b.draggable("option", "grid", [pa, da]), P = !1)
				},
				n,
				P = !1,
				H,
				K,
				T,
				G = k("isRTL") ? -1 : 1,
				A = v(),
				z = D(),
				pa = F(),
				da = R();
				b.draggable({
					zIndex: 9,
					scroll: !1,
					grid: [pa, da],
					axis: 1 == z ? "y": !1,
					opacity: k("dragOpacity"),
					revertDuration: k("dragRevertDuration"),
					start: function(c, d) {
						r("eventDragStart", b, a, c, d);
						E(a, b);
						n = b.position();
						K = T = 0;
						A.start(function(c, d, g, I) {
							b.draggable("option", "revert", !c);
							oa();
							c && (H = I * G, k("allDaySlot") && !c.row ? (P || (P = !0, u.hide(), b.draggable("option", "grid", null)), M(j(B(a.start), H), j(W(a), H))) : e())
						},
						c, "drag")
					},
					drag: function(a, b) {
						K = Math.round((b.position.top - n.top) / da) * k("slotMinutes");
						K != T && (P || d(K), T = K)
					},
					stop: function(c, u) {
						var g = A.stop();
						oa();
						r("eventDragStop", b, a, c, u);
						g && (H || K || P) ? J(this, a, H, P ? 0 : K, P, c, u) : (e(), b.css("filter", ""), b.css(n), d(0), Z(a, b))
					}
				})
			}
			if (c.isEnd && q(a)) {
				var ea, fa, Na = R();
				b.resizable({
					handles: {
						s: "div.ui-resizable-s"
					},
					grid: Na,
					start: function(c, u) {
						ea = fa = 0;
						E(a, b);
						b.css("z-index", 9);
						r("eventResizeStart", this, a, c, u)
					},
					resize: function(c, d) {
						ea = Math.round((Math.max(Na, b.height()) - d.originalSize.height) / Na);
						ea != fa && (u.text(N(a.start, !ea && !a.end ? null: t(w(a), k("slotMinutes") * ea), k("timeFormat"))), fa = ea)
					},
					stop: function(c, u) {
						r("eventResizeStop", this, a, c, u);
						ea ? la(this, a, 0, k("slotMinutes") * ea, c, u) : (b.css("z-index", 8), Z(a, b))
					}
				})
			}
			I(a, b)
		}
		var n = this;
		n.renderEvents = function(u, I) {
			T(u);
			var H, v = u.length,
			p = [],
			q = [];
			for (H = 0; H < v; H++) u[H].allDay ? p.push(u[H]) : q.push(u[H]);
			k("allDaySlot") && (ra(b(p), I), P());
			var v = D(),
			p = z(),
			w = A(),
			C = t(B(n.visStart), p),
			F = a.map(q, c),
			R,
			X,
			Z,
			L,
			E,
			J;
			H = [];
			for (R = 0; R < v; R++) {
				Z = X = U(S(q, F, C, t(B(C), w - p)));
				var la = J = E = L = void 0,
				M = void 0,
				oa = void 0;
				for (L = Z.length - 1; 0 < L; L--) {
					la = Z[L];
					for (E = 0; E < la.length; E++) {
						M = la[E];
						for (J = 0; J < Z[L - 1].length; J++) oa = Z[L - 1][J],
						M.end > oa.start && M.start < oa.end && (oa.forward = Math.max(oa.forward || 0, (M.forward || 0) + 1))
					}
				}
				for (Z = 0; Z < X.length; Z++) {
					L = X[Z];
					for (E = 0; E < L.length; E++) J = L[E],
					J.col = R,
					J.level = Z,
					H.push(J)
				}
				j(C, 1, !0)
			}
			var q = H.length,
			Oa, wa, Ja;
			X = "";
			w = {};
			C = {};
			R = K();
			v = D(); (Z = k("isRTL")) ? (L = -1, la = v - 1) : (L = 1, la = 0);
			for (v = 0; v < q; v++) p = H[v],
			F = p.event,
			E = pa(p.start, p.start),
			J = pa(p.start, p.end),
			Oa = p.col,
			M = p.level,
			oa = p.forward || 0,
			wa = da(Oa * L + la),
			Ja = fa(Oa * L + la) - wa,
			Ja = Math.min(Ja - 6, 0.95 * Ja),
			Oa = M ? Ja / (M + oa + 1) : oa ? 2 * (Ja / (oa + 1) - 6) : Ja,
			M = wa + Ja / (M + oa + 1) * M * L + (Z ? Ja - Oa: 0),
			p.top = E,
			p.left = M,
			p.outerWidth = Oa,
			p.outerHeight = J - E,
			X += d(F, p);
			R[0].innerHTML = X;
			Z = R.children();
			for (v = 0; v < q; v++) p = H[v],
			F = p.event,
			X = a(Z[v]),
			L = r("eventRender", F, F, X),
			!1 === L ? X.remove() : (L && !0 !== L && (X.remove(), X = a(L).css({
				position: "absolute",
				top: p.top,
				left: p.left
			}).appendTo(R)), p.element = X, F._id === I ? g(F, X, p) : X[0]._fci = v, ca(F, X));
			ba(R, H, g);
			for (v = 0; v < q; v++) if (p = H[v], X = p.element) R = w[F = p.key = G(X[0])],
			p.vsides = R === e ? w[F] = xa(X, !0) : R,
			R = C[F],
			p.hsides = R === e ? C[F] = Y(X, !0) : R,
			F = X.find("div.fc-event-content"),
			F.length && (p.contentTop = F[0].offsetTop);
			for (v = 0; v < q; v++) if (p = H[v], X = p.element) X[0].style.width = Math.max(0, p.outerWidth - p.hsides) + "px",
			w = Math.max(0, p.outerHeight - p.vsides),
			X[0].style.height = w + "px",
			F = p.event,
			p.contentTop !== e && 10 > w - p.contentTop && (X.find("div.fc-event-time").text(Ma(F.start, k("timeFormat")) + " - " + F.title), X.find("div.fc-event-title").remove()),
			r("eventAfterRender", F, F, X)
		};
		n.compileDaySegs = b;
		n.clearEvents = function() {
			u();
			H().empty();
			K().empty()
		};
		n.slotSegHtml = d;
		n.bindDaySeg = function(a, b, c) {
			if (p(a)) {
				var u = c.isStart,
				d = function() {
					n || (b.width(g).height("").draggable("option", "grid", null), n = !0)
				},
				g,
				e,
				n = !0,
				P,
				H = k("isRTL") ? -1 : 1,
				K = v(),
				w = F(),
				t = R(),
				T = z();
				b.draggable({
					zIndex: 9,
					opacity: k("dragOpacity", "month"),
					revertDuration: k("dragRevertDuration"),
					start: function(c, I) {
						r("eventDragStart", b, a, c, I);
						E(a, b);
						g = b.width();
						K.start(function(c, g, I, v) {
							oa();
							c ? (e = !1, P = v * H, c.row ? u ? n && (b.width(w - 10), ga(b, t * Math.round((a.end ? (a.end - a.start) / Ua: k("defaultEventMinutes")) / k("slotMinutes"))), b.draggable("option", "grid", [w, 1]), n = !1) : e = !0 : (M(j(B(a.start), P), j(W(a), P)), d()), e = e || n && !P) : (d(), e = !0);
							b.draggable("option", "revert", e)
						},
						c, "drag")
					},
					stop: function(c, u) {
						K.stop();
						oa();
						r("eventDragStop", b, a, c, u);
						if (e) d(),
						b.css("filter", ""),
						Z(a, b);
						else {
							var g = 0;
							n || (g = Math.round((b.offset().top - X().offset().top) / t) * k("slotMinutes") + T - (60 * a.start.getHours() + a.start.getMinutes()));
							J(this, a, P, g, n, c, u)
						}
					}
				})
			}
			c.isEnd && q(a) && C(a, b, c);
			I(a, b)
		};
		ma.call(n);
		var k = n.opt,
		r = n.trigger,
		p = n.isEventDraggable,
		q = n.isEventResizable,
		w = n.eventEnd,
		T = n.reportEvents,
		u = n.reportEventClear,
		I = n.eventElementHandlers,
		P = n.setHeight,
		H = n.getDaySegmentContainer,
		K = n.getSlotSegmentContainer,
		v = n.getHoverListener,
		A = n.getMaxMinute,
		z = n.getMinMinute,
		pa = n.timePosition,
		da = n.colContentLeft,
		fa = n.colContentRight,
		ra = n.renderDaySegs,
		C = n.resizableDayEvent,
		D = n.getColCnt,
		F = n.getColWidth,
		R = n.getSlotHeight,
		X = n.getBodyContent,
		ca = n.reportEventElement,
		Z = n.showEvents,
		E = n.hideEvents,
		J = n.eventDrop,
		la = n.eventResize,
		M = n.renderDayOverlay,
		oa = n.clearOverlays,
		wa = n.calendar,
		Ma = wa.formatDate,
		N = wa.formatDates
	},
	Aa = function(a, b, c) {
		function d(a, b) {
			var u = T[a];
			return "object" == typeof u ? Ba(u, b || c) : u
		}
		function g(a, c) {
			return b.trigger.apply(b, [a, c || w].concat(Array.prototype.slice.call(arguments, 2), [w]))
		}
		function n(a) {
			return M(a.editable, (a.source || {}).editable, d("editable"))
		}
		function k(a) {
			return a.end ? B(a.end) : u(a)
		}
		function p(a, b, c) {
			a = v[a._id];
			var u, d = a.length;
			for (u = 0; u < d; u++) if (!b || a[u][0] != b[0]) a[u][c]()
		}
		function r(a, b, c, u) {
			c = c || 0;
			for (var d, g = a.length,
			n = 0; n < g; n++) d = a[n],
			u !== e && (d.allDay = u),
			t(j(d.start, b, !0), c),
			d.end && (d.end = t(j(d.end, b, !0), c)),
			I(d, T)
		}
		function q(a, b, c) {
			c = c || 0;
			for (var u, d = a.length,
			g = 0; g < d; g++) u = a[g],
			u.end = t(j(k(u), b, !0), c),
			I(u, T)
		}
		var w = this;
		w.element = a;
		w.calendar = b;
		w.name = c;
		w.opt = d;
		w.trigger = g;
		w.isEventDraggable = function(a) {
			return n(a) && !d("disableDragging")
		};
		w.isEventResizable = function(a) {
			return n(a) && !d("disableResizing")
		};
		w.reportEvents = function(a) {
			H = {};
			var b, c = a.length,
			u;
			for (b = 0; b < c; b++) u = a[b],
			H[u._id] ? H[u._id].push(u) : H[u._id] = [u]
		};
		w.eventEnd = k;
		w.reportEventElement = function(a, b) {
			K.push(b);
			v[a._id] ? v[a._id].push(b) : v[a._id] = [b]
		};
		w.reportEventClear = function() {
			K = [];
			v = {}
		};
		w.eventElementHandlers = function(a, b) {
			b.click(function(c) {
				if (!b.hasClass("ui-draggable-dragging") && !b.hasClass("ui-resizable-resizing")) return g("eventClick", this, a, c)
			}).hover(function(b) {
				g("eventMouseover", this, a, b)
			},
			function(b) {
				g("eventMouseout", this, a, b)
			})
		};
		w.showEvents = function(a, b) {
			p(a, b, "show")
		};
		w.hideEvents = function(a, b) {
			p(a, b, "hide")
		};
		w.eventDrop = function(a, b, c, u, d, e, j) {
			var I = b.allDay,
			n = b._id;
			r(H[n], c, u, d);
			g("eventDrop", a, b, c, u, d,
			function() {
				r(H[n], -c, -u, I);
				P(n)
			},
			e, j);
			P(n)
		};
		w.eventResize = function(a, b, c, u, d, e) {
			var j = b._id;
			q(H[j], c, u);
			g("eventResize", a, b, c, u,
			function() {
				q(H[j], -c, -u);
				P(j)
			},
			d, e);
			P(j)
		};
		var u = w.defaultEventEnd,
		I = b.normalizeEvent,
		P = b.reportEventChange,
		H = {},
		K = [],
		v = {},
		T = b.options
	},
	ma = function() {
		function b(a) {
			var c = t("isRTL"),
			d,
			g = a.length,
			e,
			j,
			n,
			k;
			d = fa();
			var P = d.left,
			H = d.right,
			v, K, p, r, q, w = "";
			for (d = 0; d < g; d++) e = a[d],
			j = e.event,
			k = ["fc-event", "fc-event-skin", "fc-event-hori"],
			u(j) && k.push("fc-event-draggable"),
			c ? (e.isStart && k.push("fc-corner-right"), e.isEnd && k.push("fc-corner-left"), v = D(e.end.getDay() - 1), K = D(e.start.getDay()), p = e.isEnd ? C(v) : P, r = e.isStart ? ra(K) : H) : (e.isStart && k.push("fc-corner-left"), e.isEnd && k.push("fc-corner-right"), v = D(e.start.getDay()), K = D(e.end.getDay() - 1), p = e.isStart ? C(v) : P, r = e.isEnd ? ra(K) : H),
			k = k.concat(j.className),
			j.source && (k = k.concat(j.source.className || [])),
			n = j.url,
			q = L(j, t),
			w = n ? w + ("<a href='" + qa(n) + "'") : w + "<div",
			w += " class='" + k.join(" ") + "' style='position:absolute;z-index:8;left:" + p + "px;" + q + "'><div class='fc-event-inner fc-event-skin'" + (q ? " style='" + q + "'": "") + ">",
			!j.allDay && e.isStart && (w += "<span class='fc-event-time'>" + qa(E(j.start, j.end, t("timeFormat"))) + "</span>"),
			w += "<span class='fc-event-title'>" + qa(j.title) + "</span></div>",
			e.isEnd && I(j) && (w += "<div class='ui-resizable-handle ui-resizable-" + (c ? "w": "e") + "'>&nbsp;&nbsp;&nbsp;</div>"),
			w += "</" + (n ? "a": "div") + ">",
			e.left = p,
			e.outerWidth = r - p,
			e.startCol = v,
			e.endCol = K + 1;
			return w
		}
		function c(b, u) {
			var d, g = b.length,
			e, j, I;
			for (d = 0; d < g; d++) e = b[d],
			j = e.event,
			I = a(u[d]),
			j = T("eventRender", j, j, I),
			!1 === j ? I.remove() : (j && !0 !== j && (j = a(j).css({
				position: "absolute",
				left: e.left
			}), I.replaceWith(j), I = j), e.element = I)
		}
		function d(a) {
			var b, c = a.length,
			u, g, j, I, n = {};
			for (b = 0; b < c; b++) if (u = a[b], g = u.element) j = u.key = G(g[0]),
			I = n[j],
			I === e && (I = n[j] = Y(g, !0)),
			u.hsides = I
		}
		function g(a) {
			var b, c = a.length,
			u, d;
			for (b = 0; b < c; b++) if (u = a[b], d = u.element) d[0].style.width = Math.max(0, u.outerWidth - u.hsides) + "px"
		}
		function n(a) {
			var b, c = a.length,
			u, d, g, j, I = {};
			for (b = 0; b < c; b++) if (u = a[b], d = u.element) g = u.key,
			j = I[g],
			j === e && (j = I[g] = ua(d)),
			u.outerHeight = d[0].offsetHeight + j
		}
		function k() {
			var a, b = z(),
			c = [];
			for (a = 0; a < b; a++) c[a] = da(a).find("td:first div.fc-day-content > div");
			return c
		}
		function p(a) {
			var b, c = a.length,
			u = [];
			for (b = 0; b < c; b++) u[b] = a[b][0].offsetTop;
			return u
		}
		function r(a, b) {
			var c, u = a.length,
			d, g;
			for (c = 0; c < u; c++) if (d = a[c], g = d.element) g[0].style.top = b[d.row] + (d.top || 0) + "px",
			d = d.event,
			T("eventAfterRender", d, d, g)
		}
		var q = this;
		q.renderDaySegs = function(a, u) {
			var e = R(),
			j = z(),
			I = pa(),
			P = 0,
			v,
			K,
			q,
			w = a.length,
			t,
			T;
			e[0].innerHTML = b(a);
			c(a, e.children());
			K = a.length;
			for (v = 0; v < K; v++) q = a[v],
			(T = q.element) && H(q.event, T);
			K = a.length;
			var G;
			for (v = 0; v < K; v++) if (q = a[v], T = q.element) G = q.event,
			G._id === u ? Z(G, T, q) : T[0]._fci = v;
			ba(e, a, Z);
			d(a);
			g(a);
			n(a);
			e = k();
			for (v = 0; v < j; v++) {
				K = [];
				for (q = 0; q < I; q++) K[q] = 0;
				for (; P < w && (t = a[P]).row == v;) {
					q = K.slice(t.startCol, t.endCol);
					q = Math.max.apply(Math, q);
					t.top = q;
					q += t.outerHeight;
					for (T = t.startCol; T < t.endCol; T++) K[T] = q;
					P++
				}
				e[v].height(Math.max.apply(Math, K))
			}
			r(a, p(e))
		};
		q.resizableDayEvent = function(u, e, I) {
			var H = t("isRTL"),
			G = H ? "w": "e",
			da = e.find("div.ui-resizable-" + G),
			fa = !1;
			w(e);
			e.mousedown(function(a) {
				a.preventDefault()
			}).click(function(a) {
				fa && (a.preventDefault(), a.stopImmediatePropagation())
			});
			da.mousedown(function(w) {
				function t(b) {
					T("eventResizeStop", this, u, b);
					a("body").css("cursor", "");
					da.stop();
					J();
					E && A(this, u, E, 0, b);
					setTimeout(function() {
						fa = !1
					},
					0)
				}
				if (1 == w.which) {
					fa = !0;
					var da = q.getHoverListener(),
					C = z(),
					ra = pa(),
					D = H ? -1 : 1,
					Z = H ? ra - 1 : 0,
					L = e.css("top"),
					E,
					M,
					oa = a.extend({},
					u),
					U = F(u.start);
					la();
					a("body").css("cursor", G + "-resize").one("mouseup", t);
					T("eventResizeStart", this, u, w);
					da.start(function(e, q) {
						if (e) {
							var w = Math.max(U.row, e.row),
							t = e.col;
							1 == C && (w = 0);
							w == U.row && (t = H ? Math.min(U.col, t) : Math.max(U.col, t));
							E = 7 * w + t * D + Z - (7 * q.row + q.col * D + Z);
							w = j(P(u), E, !0);
							if (E) {
								oa.end = w;
								var t = M,
								T = X([oa]),
								A = I.row,
								z = a("<div/>"),
								da = R(),
								pa = T.length,
								fa;
								z[0].innerHTML = b(T);
								z = z.children();
								da.append(z);
								c(T, z);
								d(T);
								g(T);
								n(T);
								r(T, p(k()));
								z = [];
								for (da = 0; da < pa; da++) if (fa = T[da].element) T[da].row === A && fa.css("top", L),
								z.push(fa[0]);
								M = a(z);
								M.find("*").css("cursor", G + "-resize");
								t && t.remove();
								v(u)
							} else M && (K(u), M.remove(), M = null);
							J();
							ca(u.start, j(B(w), 1))
						}
					},
					w)
				}
			})
		};
		var t = q.opt,
		T = q.trigger,
		u = q.isEventDraggable,
		I = q.isEventResizable,
		P = q.eventEnd,
		H = q.reportEventElement,
		K = q.showEvents,
		v = q.hideEvents,
		A = q.eventResize,
		z = q.getRowCnt,
		pa = q.getColCnt,
		da = q.allDayRow,
		fa = q.allDayBounds,
		C = q.colContentLeft,
		ra = q.colContentRight,
		D = q.dayOfWeekCol,
		F = q.dateCell,
		X = q.compileDaySegs,
		R = q.getDaySegmentContainer,
		Z = q.bindDaySeg,
		E = q.calendar.formatDates,
		ca = q.renderDayOverlay,
		J = q.clearOverlays,
		la = q.clearSelection
	},
	Ga = function() {
		function b(a) {
			q && (q = !1, k(), e("unselect", null, a))
		}
		function c(a, b, u, d) {
			q = !0;
			e("select", null, a, b, u, d)
		}
		var d = this;
		d.select = function(a, d, u) {
			b();
			d || (d = j(a, u));
			n(a, d, u);
			c(a, d, u)
		};
		d.unselect = b;
		d.reportSelection = c;
		d.daySelectionMousedown = function(e) {
			var j = d.cellDate,
			u = d.cellIsAllDay,
			I = d.getHoverListener(),
			P = d.reportDayClick;
			if (1 == e.which && g("selectable")) {
				b(e);
				var H;
				I.start(function(a, b) {
					k();
					a && u(a) ? (H = [j(b), j(a)].sort(N), n(H[0], H[1], !0)) : H = null
				},
				e);
				a(document).one("mouseup",
				function(a) {
					I.stop();
					H && ( + H[0] == +H[1] && P(H[0], !0, a), c(H[0], H[1], !0, a))
				})
			}
		};
		var g = d.opt,
		e = d.trigger,
		j = d.defaultSelectionEnd,
		n = d.renderSelection,
		k = d.clearSelection,
		q = !1;
		g("selectable") && g("unselectAuto") && a(document).mousedown(function(c) {
			var d = g("unselectCancel"); (!d || !a(c.target).parents(d).length) && b(c)
		})
	},
	Ca = function() {
		this.renderOverlay = function(d, g) {
			var e = c.shift();
			e || (e = a("<div class='fc-cell-overlay' style='position:absolute;z-index:3'/>"));
			e[0].parentNode != g[0] && e.appendTo(g);
			b.push(e.css(d).show());
			return e
		};
		this.clearOverlays = function() {
			for (var a; a = b.shift();) c.push(a.hide().unbind())
		};
		var b = [],
		c = []
	},
	ka = function(a) {
		var b, c;
		this.build = function() {
			b = [];
			c = [];
			a(b, c)
		};
		this.cell = function(a, d) {
			var g = b.length,
			e = c.length,
			j, n = -1,
			k = -1;
			for (j = 0; j < g; j++) if (d >= b[j][0] && d < b[j][1]) {
				n = j;
				break
			}
			for (j = 0; j < e; j++) if (a >= c[j][0] && a < c[j][1]) {
				k = j;
				break
			}
			return 0 <= n && 0 <= k ? {
				row: n,
				col: k
			}: null
		};
		this.rect = function(a, d, g, e, j) {
			j = j.offset();
			return {
				top: b[a][0] - j.top,
				left: c[d][0] - j.left,
				width: c[e][1] - c[d][0],
				height: b[g][1] - b[a][0]
			}
		}
	},
	Pa = function(b) {
		function c(a) {
			a = b.cell(a.pageX, a.pageY);
			if (!a != !j || a && (a.row != j.row || a.col != j.col)) a ? (e || (e = a), g(a, e, a.row - e.row, a.col - e.col)) : g(a, e),
			j = a
		}
		var d, g, e, j;
		this.start = function(n, k, q) {
			g = n;
			e = j = null;
			b.build();
			c(k);
			d = q || "mousemove";
			a(document).bind(d, c)
		};
		this.stop = function() {
			a(document).unbind(d, c);
			return j
		}
	},
	Qa = function(a) {
		var b = this,
		c = {},
		d = {},
		g = {};
		b.left = function(b) {
			return d[b] = d[b] === e ? (c[b] = c[b] || a(b)).position().left: d[b]
		};
		b.right = function(d) {
			return g[d] = g[d] === e ? b.left(d) + (c[d] = c[d] || a(d)).width() : g[d]
		};
		b.clear = function() {
			c = {};
			d = {};
			g = {}
		}
	},
	Q = {
		defaultView: "month",
		aspectRatio: 1.35,
		header: {
			left: "title",
			center: "",
			right: "today prev,next"
		},
		weekends: !0,
		allDayDefault: !0,
		ignoreTimezone: !0,
		lazyFetching: !0,
		startParam: "start",
		endParam: "end",
		titleFormat: {
			month: "MMMM yyyy",
			week: "MMM d[ yyyy]{ '&#8212;'[ MMM] d yyyy}",
			day: "dddd, MMM d, yyyy"
		},
		columnFormat: {
			month: "ddd",
			week: "ddd M/d",
			day: "dddd M/d"
		},
		timeFormat: {
			"": "h(:mm)t"
		},
		isRTL: !1,
		firstDay: 0,
		monthNames: "January February March April May June July August September October November December".split(" "),
		monthNamesShort: "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
		dayNames: "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
		dayNamesShort: "Sun Mon Tue Wed Thu Fri Sat".split(" "),
		buttonText: {
			prev: "&nbsp;&#9668;&nbsp;",
			next: "&nbsp;&#9658;&nbsp;",
			prevYear: "&nbsp;&lt;&lt;&nbsp;",
			nextYear: "&nbsp;&gt;&gt;&nbsp;",
			today: "today",
			month: "month",
			week: "week",
			day: "day"
		},
		theme: !1,
		buttonIcons: {
			prev: "circle-triangle-w",
			next: "circle-triangle-e"
		},
		unselectAuto: !0,
		dropAccept: "*"
	},
	Da = {
		header: {
			left: "next,prev today",
			center: "",
			right: "title"
		},
		buttonText: {
			prev: "&nbsp;&#9658;&nbsp;",
			next: "&nbsp;&#9668;&nbsp;",
			prevYear: "&nbsp;&gt;&gt;&nbsp;",
			nextYear: "&nbsp;&lt;&lt;&nbsp;"
		},
		buttonIcons: {
			prev: "circle-triangle-e",
			next: "circle-triangle-w"
		}
	},
	aa = a.fullCalendar = {
		version: "1.5.1"
	},
	T = aa.views = {};
	a.fn.fullCalendar = function(c) {
		if ("string" == typeof c) {
			var d = Array.prototype.slice.call(arguments, 1),
			g;
			this.each(function() {
				var b = a.data(this, "fullCalendar");
				b && a.isFunction(b[c]) && (b = b[c].apply(b, d), g === e && (g = b), "destroy" == c && a.removeData(this, "fullCalendar"))
			});
			return g !== e ? g: this
		}
		var j = c.eventSources || [];
		delete c.eventSources;
		c.events && (j.push(c.events), delete c.events);
		c = a.extend(!0, {},
		Q, c.isRTL || c.isRTL === e && Q.isRTL ? Da: {},
		c);
		this.each(function(d, g) {
			var e = a(g),
			n = new b(e, c, j);
			e.data("fullCalendar", n);
			n.render()
		});
		return this
	};
	aa.sourceNormalizers = [];
	aa.sourceFetchers = [];
	var ra = {
		dataType: "json",
		cache: !1
	},
	da = 1;
	aa.addDays = j;
	aa.cloneDate = B;
	aa.parseDate = F;
	aa.parseISO8601 = D;
	aa.parseTime = E;
	aa.formatDate = V;
	aa.formatDates = ha;
	var pa = "sun mon tue wed thu fri sat".split(" "),
	fa = 864E5,
	oa = 36E5,
	Ua = 6E4,
	Ea = {
		s: function(a) {
			return a.getSeconds()
		},
		ss: function(a) {
			return na(a.getSeconds())
		},
		m: function(a) {
			return a.getMinutes()
		},
		mm: function(a) {
			return na(a.getMinutes())
		},
		h: function(a) {
			return a.getHours() % 12 || 12
		},
		hh: function(a) {
			return na(a.getHours() % 12 || 12)
		},
		H: function(a) {
			return a.getHours()
		},
		HH: function(a) {
			return na(a.getHours())
		},
		d: function(a) {
			return a.getDate()
		},
		dd: function(a) {
			return na(a.getDate())
		},
		ddd: function(a, b) {
			return b.dayNamesShort[a.getDay()]
		},
		dddd: function(a, b) {
			return b.dayNames[a.getDay()]
		},
		M: function(a) {
			return a.getMonth() + 1
		},
		MM: function(a) {
			return na(a.getMonth() + 1)
		},
		MMM: function(a, b) {
			return b.monthNamesShort[a.getMonth()]
		},
		MMMM: function(a, b) {
			return b.monthNames[a.getMonth()]
		},
		yy: function(a) {
			return (a.getFullYear() + "").substring(2)
		},
		yyyy: function(a) {
			return a.getFullYear()
		},
		t: function(a) {
			return 12 > a.getHours() ? "a": "p"
		},
		tt: function(a) {
			return 12 > a.getHours() ? "am": "pm"
		},
		T: function(a) {
			return 12 > a.getHours() ? "A": "P"
		},
		TT: function(a) {
			return 12 > a.getHours() ? "AM": "PM"
		},
		u: function(a) {
			return V(a, "yyyy-MM-dd'T'HH:mm:ss'Z'")
		},
		S: function(a) {
			a = a.getDate();
			return 10 < a && 20 > a ? "th": ["st", "nd", "rd"][a % 10 - 1] || "th"
		}
	};
	aa.applyAll = C;
	T.month = function(a, b) {
		var c = this;
		c.render = function(a, b) {
			b && (n(a, b), a.setDate(1));
			var k = B(a, !0);
			k.setDate(1);
			var q = n(B(k), 1),
			r = B(k),
			u = B(q),
			I = d("firstDay"),
			P = d("weekends") ? 0 : 1;
			P && (p(r), p(u, -1, !0));
			j(r, -((r.getDay() - Math.max(I, P) + 7) % 7));
			j(u, (7 - u.getDay() + Math.max(I, P)) % 7);
			I = Math.round((u - r) / (7 * fa));
			"fixed" == d("weekMode") && (j(u, 7 * (6 - I)), I = 6);
			c.title = e(k, d("titleFormat"));
			c.start = k;
			c.end = q;
			c.visStart = r;
			c.visEnd = u;
			g(6, I, P ? 5 : 7, !0)
		};
		ja.call(c, a, b, "month");
		var d = c.opt,
		g = c.renderBasic,
		e = b.formatDate
	};
	T.basicWeek = function(a, b) {
		var c = this;
		c.render = function(a, b) {
			b && j(a, 7 * b);
			var n = j(B(a), -((a.getDay() - d("firstDay") + 7) % 7)),
			k = j(B(n), 7),
			q = B(n),
			u = B(k),
			I = d("weekends");
			I || (p(q), p(u, -1, !0));
			c.title = e(q, j(B(u), -1), d("titleFormat"));
			c.start = n;
			c.end = k;
			c.visStart = q;
			c.visEnd = u;
			g(1, 1, I ? 7 : 5, !1)
		};
		ja.call(c, a, b, "basicWeek");
		var d = c.opt,
		g = c.renderBasic,
		e = b.formatDates
	};
	T.basicDay = function(a, b) {
		var c = this;
		c.render = function(a, b) {
			b && (j(a, b), d("weekends") || p(a, 0 > b ? -1 : 1));
			c.title = e(a, d("titleFormat"));
			c.start = c.visStart = B(a, !0);
			c.end = c.visEnd = j(B(c.start), 1);
			g(1, 1, 1, !1)
		};
		ja.call(c, a, b, "basicDay");
		var d = c.opt,
		g = c.renderBasic,
		e = b.formatDate
	};
	a.extend(!0, Q, {
		weekMode: "fixed"
	});
	T.agendaWeek = function(a, b) {
		var c = this;
		c.render = function(a, b) {
			b && j(a, 7 * b);
			var n = j(B(a), -((a.getDay() - d("firstDay") + 7) % 7)),
			k = j(B(n), 7),
			q = B(n),
			u = B(k),
			I = d("weekends");
			I || (p(q), p(u, -1, !0));
			c.title = e(q, j(B(u), -1), d("titleFormat"));
			c.start = n;
			c.end = k;
			c.visStart = q;
			c.visEnd = u;
			g(I ? 7 : 5)
		};
		J.call(c, a, b, "agendaWeek");
		var d = c.opt,
		g = c.renderAgenda,
		e = b.formatDates
	};
	T.agendaDay = function(a, b) {
		var c = this;
		c.render = function(a, b) {
			b && (j(a, b), d("weekends") || p(a, 0 > b ? -1 : 1));
			var n = B(a, !0),
			k = j(B(n), 1);
			c.title = e(a, d("titleFormat"));
			c.start = c.visStart = n;
			c.end = c.visEnd = k;
			g(1)
		};
		J.call(c, a, b, "agendaDay");
		var d = c.opt,
		g = c.renderAgenda,
		e = b.formatDate
	};
	a.extend(!0, Q, {
		allDaySlot: !0,
		allDayText: "all-day",
		firstHour: 6,
		slotMinutes: 30,
		defaultEventMinutes: 120,
		axisFormat: "h(:mm)tt",
		timeFormat: {
			agenda: "h:mm{ - h:mm}"
		},
		dragOpacity: {
			agenda: 0.5
		},
		minTime: 0,
		maxTime: 24
	});
	var Ha = jQuery,
	ca = Ha.fullCalendar,
	wa = ca.formatDate,
	X = ca.parseISO8601,
	Ma = ca.addDays,
	Z = ca.applyAll;
	ca.sourceNormalizers.push(function(a) {
		if ("gcal" == a.dataType || void 0 === a.dataType && (a.url || "").match(/^(http|https):\/\/www.google.com\/calendar\/feeds\//)) a.dataType = "gcal",
		void 0 === a.editable && (a.editable = !1)
	});
	ca.sourceFetchers.push(function(a, b, c) {
		if ("gcal" == a.dataType) {
			var d = a.success;
			b = Ha.extend({},
			a.data || {},
			{
				"start-min": wa(b, "u"),
				"start-max": wa(c, "u"),
				singleevents: !0,
				"max-results": 9999
			});
			var g = a.currentTimezone;
			g && (b.ctz = g = g.replace(" ", "_"));
			return Ha.extend({},
			a, {
				url: a.url.replace(/\/basic$/, "/full") + "?alt=json-in-script&callback=?",
				dataType: "jsonp",
				data: b,
				startParam: !1,
				endParam: !1,
				success: function(a) {
					var b = [];
					a.feed.entry && Ha.each(a.feed.entry,
					function(a, c) {
						var d = c.gd$when[0].startTime,
						u = X(d, !0),
						e = X(c.gd$when[0].endTime, !0),
						d = -1 == d.indexOf("T"),
						j;
						Ha.each(c.link,
						function(a, b) {
							"text/html" == b.type && (j = b.href, g && (j += ( - 1 == j.indexOf("?") ? "?": "&") + "ctz=" + g))
						});
						d && Ma(e, -1);
						b.push({
							id: c.gCal$uid.value,
							title: c.title.$t,
							url: j,
							start: u,
							end: e,
							allDay: d,
							location: c.gd$where[0].valueString,
							description: c.content.$t
						})
					});
					var c = [b].concat(Array.prototype.slice.call(arguments, 1)),
					c = Z(d, this, c);
					return Ha.isArray(c) ? c: b
				}
			})
		}
	});
	ca.gcalFeed = function(a, b) {
		return Ha.extend({},
		b, {
			url: a,
			dataType: "gcal"
		})
	}
}); (function(a) {
	a.color = {};
	a.color.make = function(b, c, g, d) {
		var e = {};
		e.r = b || 0;
		e.g = c || 0;
		e.b = g || 0;
		e.a = null != d ? d: 1;
		e.add = function(a, b) {
			for (var c = 0; c < a.length; ++c) e[a.charAt(c)] += b;
			return e.normalize()
		};
		e.scale = function(a, b) {
			for (var c = 0; c < a.length; ++c) e[a.charAt(c)] *= b;
			return e.normalize()
		};
		e.toString = function() {
			return 1 <= e.a ? "rgb(" + [e.r, e.g, e.b].join() + ")": "rgba(" + [e.r, e.g, e.b, e.a].join() + ")"
		};
		e.normalize = function() {
			function a(b, c, d) {
				return c < b ? b: c > d ? d: c
			}
			e.r = a(0, parseInt(e.r), 255);
			e.g = a(0, parseInt(e.g), 255);
			e.b = a(0, parseInt(e.b), 255);
			e.a = a(0, e.a, 1);
			return e
		};
		e.clone = function() {
			return a.color.make(e.r, e.b, e.g, e.a)
		};
		return e.normalize()
	};
	a.color.extract = function(b, c) {
		var g;
		do {
			g = b.css(c).toLowerCase();
			if ("" != g && "transparent" != g) break;
			b = b.parent()
		} while (! a . nodeName ( b . get ( 0 ), "body"));
		"rgba(0, 0, 0, 0)" == g && (g = "transparent");
		return a.color.parse(g)
	};
	a.color.parse = function(b) {
		var c, g = a.color.make;
		if (c = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(b)) return g(parseInt(c[1], 10), parseInt(c[2], 10), parseInt(c[3], 10));
		if (c = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(b)) return g(parseInt(c[1], 10), parseInt(c[2], 10), parseInt(c[3], 10), parseFloat(c[4]));
		if (c = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(b)) return g(2.55 * parseFloat(c[1]), 2.55 * parseFloat(c[2]), 2.55 * parseFloat(c[3]));
		if (c = /rgba\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(b)) return g(2.55 * parseFloat(c[1]), 2.55 * parseFloat(c[2]), 2.55 * parseFloat(c[3]), parseFloat(c[4]));
		if (c = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(b)) return g(parseInt(c[1], 16), parseInt(c[2], 16), parseInt(c[3], 16));
		if (c = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(b)) return g(parseInt(c[1] + c[1], 16), parseInt(c[2] + c[2], 16), parseInt(c[3] + c[3], 16));
		b = a.trim(b).toLowerCase();
		if ("transparent" == b) return g(255, 255, 255, 0);
		c = e[b] || [0, 0, 0];
		return g(c[0], c[1], c[2])
	};
	var e = {
		aqua: [0, 255, 255],
		azure: [240, 255, 255],
		beige: [245, 245, 220],
		black: [0, 0, 0],
		blue: [0, 0, 255],
		brown: [165, 42, 42],
		cyan: [0, 255, 255],
		darkblue: [0, 0, 139],
		darkcyan: [0, 139, 139],
		darkgrey: [169, 169, 169],
		darkgreen: [0, 100, 0],
		darkkhaki: [189, 183, 107],
		darkmagenta: [139, 0, 139],
		darkolivegreen: [85, 107, 47],
		darkorange: [255, 140, 0],
		darkorchid: [153, 50, 204],
		darkred: [139, 0, 0],
		darksalmon: [233, 150, 122],
		darkviolet: [148, 0, 211],
		fuchsia: [255, 0, 255],
		gold: [255, 215, 0],
		green: [0, 128, 0],
		indigo: [75, 0, 130],
		khaki: [240, 230, 140],
		lightblue: [173, 216, 230],
		lightcyan: [224, 255, 255],
		lightgreen: [144, 238, 144],
		lightgrey: [211, 211, 211],
		lightpink: [255, 182, 193],
		lightyellow: [255, 255, 224],
		lime: [0, 255, 0],
		magenta: [255, 0, 255],
		maroon: [128, 0, 0],
		navy: [0, 0, 128],
		olive: [128, 128, 0],
		orange: [255, 165, 0],
		pink: [255, 192, 203],
		purple: [128, 0, 128],
		violet: [128, 0, 128],
		red: [255, 0, 0],
		silver: [192, 192, 192],
		white: [255, 255, 255],
		yellow: [255, 255, 0]
	}
})(jQuery); (function(a) {
	function e(c, g, d, e) {
		function j(a, b) {
			b = [ka].concat(b);
			for (var c = 0; c < a.length; ++c) a[c].apply(this, b)
		}
		function k(b) {
			for (var c = [], d = 0; d < b.length; ++d) {
				var g = a.extend(!0, {},
				w.series);
				null != b[d].data ? (g.data = b[d].data, delete b[d].data, a.extend(!0, g, b[d]), b[d].data = g.data) : g.data = b[d];
				c.push(g)
			}
			G = c;
			c = G.length;
			d = [];
			g = [];
			for (b = 0; b < G.length; ++b) {
				var e = G[b].color;
				null != e && (--c, "number" == typeof e ? g.push(e) : d.push(a.color.parse(G[b].color)))
			}
			for (b = 0; b < g.length; ++b) c = Math.max(c, g[b] + 1);
			d = [];
			for (b = g = 0; d.length < c;) e = w.colors.length == b ? a.color.make(100, 100, 100) : a.color.parse(w.colors[b]),
			e.scale("rgb", 1 + 0.2 * (1 == g % 2 ? -1 : 1) * Math.ceil(g / 2)),
			d.push(e),
			++b,
			b >= w.colors.length && (b = 0, ++g);
			for (b = c = 0; b < G.length; ++b) {
				g = G[b];
				null == g.color ? (g.color = d[c].toString(), ++c) : "number" == typeof g.color && (g.color = d[g.color].toString());
				if (null == g.lines.show) {
					var n, e = !0;
					for (n in g) if (g[n] && g[n].show) {
						e = !1;
						break
					}
					e && (g.lines.show = !0)
				}
				g.xaxis = r(ja, t(g, "x"));
				g.yaxis = r(za, t(g, "y"))
			}
			n = function(a, b, c) {
				b < a.datamin && b != -p && (a.datamin = b);
				c > a.datamax && c != p && (a.datamax = c)
			};
			var k = Number.POSITIVE_INFINITY,
			q = Number.NEGATIVE_INFINITY,
			p = Number.MAX_VALUE,
			z, C, B, D, F;
			a.each(A(),
			function(a, b) {
				b.datamin = k;
				b.datamax = q;
				b.used = !1
			});
			for (b = 0; b < G.length; ++b) c = G[b],
			c.datapoints = {
				points: []
			},
			j(Ca.processRawData, [c, c.data, c.datapoints]);
			for (b = 0; b < G.length; ++b) {
				var c = G[b],
				R = c.data,
				L = c.datapoints.format;
				if (!L) {
					L = [];
					L.push({
						x: !0,
						number: !0,
						required: !0
					});
					L.push({
						y: !0,
						number: !0,
						required: !0
					});
					if (c.bars.show || c.lines.show && c.lines.fill) L.push({
						y: !0,
						number: !0,
						required: !1,
						defaultValue: 0
					}),
					c.bars.horizontal && (delete L[L.length - 1].y, L[L.length - 1].x = !0);
					c.datapoints.format = L
				}
				if (null == c.datapoints.pointsize) {
					c.datapoints.pointsize = L.length;
					C = c.datapoints.pointsize;
					e = c.datapoints.points;
					insertSteps = c.lines.show && c.lines.steps;
					c.xaxis.used = c.yaxis.used = !0;
					for (d = z = 0; d < R.length; ++d, z += C) {
						F = R[d];
						var E = null == F;
						if (!E) for (g = 0; g < C; ++g) {
							B = F[g];
							if (D = L[g]) D.number && null != B && (B = +B, isNaN(B) ? B = null: Infinity == B ? B = p: -Infinity == B && (B = -p)),
							null == B && (D.required && (E = !0), null != D.defaultValue && (B = D.defaultValue));
							e[z + g] = B
						}
						if (E) for (g = 0; g < C; ++g) B = e[z + g],
						null != B && (D = L[g], D.x && n(c.xaxis, B, B), D.y && n(c.yaxis, B, B)),
						e[z + g] = null;
						else if (insertSteps && 0 < z && null != e[z - C] && e[z - C] != e[z] && e[z - C + 1] != e[z + 1]) {
							for (g = 0; g < C; ++g) e[z + C + g] = e[z + g];
							e[z + 1] = e[z - C + 1];
							z += C
						}
					}
				}
			}
			for (b = 0; b < G.length; ++b) c = G[b],
			j(Ca.processDatapoints, [c, c.datapoints]);
			for (b = 0; b < G.length; ++b) {
				c = G[b];
				e = c.datapoints.points;
				C = c.datapoints.pointsize;
				F = z = k;
				E = R = q;
				for (d = 0; d < e.length; d += C) if (null != e[d]) for (g = 0; g < C; ++g) if (B = e[d + g], (D = L[g]) && !(B == p || B == -p)) D.x && (B < z && (z = B), B > R && (R = B)),
				D.y && (B < F && (F = B), B > E && (E = B));
				c.bars.show && (d = "left" == c.bars.align ? 0 : -c.bars.barWidth / 2, c.bars.horizontal ? (F += d, E += d + c.bars.barWidth) : (z += d, R += d + c.bars.barWidth));
				n(c.xaxis, z, R);
				n(c.yaxis, F, E)
			}
			a.each(A(),
			function(a, b) {
				b.datamin == k && (b.datamin = null);
				b.datamax == q && (b.datamax = null)
			})
		}
		function t(a, b) {
			var c = a[b + "axis"];
			"object" == typeof c && (c = c.n);
			"number" != typeof c && (c = 1);
			return c
		}
		function A() {
			return a.grep(ja.concat(za),
			function(a) {
				return a
			})
		}
		function B(a) {
			var b = {},
			c, d;
			for (c = 0; c < ja.length; ++c)(d = ja[c]) && d.used && (b["x" + d.n] = d.c2p(a.left));
			for (c = 0; c < za.length; ++c)(d = za[c]) && d.used && (b["y" + d.n] = d.c2p(a.top));
			void 0 !== b.x1 && (b.x = b.x1);
			void 0 !== b.y1 && (b.y = b.y1);
			return b
		}
		function r(b, c) {
			b[c - 1] || (b[c - 1] = {
				n: c,
				direction: b == ja ? "x": "y",
				options: a.extend(!0, {},
				b == ja ? w.xaxis: w.yaxis)
			});
			return b[c - 1]
		}
		function p(b, d) {
			var g = document.createElement("canvas");
			g.className = d;
			g.width = ya;
			g.height = Aa;
			b || a(g).css({
				position: "absolute",
				left: 0,
				top: 0
			});
			a(g).appendTo(c);
			g.getContext || (g = window.G_vmlCanvasManager.initElement(g));
			g.getContext("2d").save();
			return g
		}
		function z() {
			ya = c.width();
			Aa = c.height();
			if (0 >= ya || 0 >= Aa) throw "Invalid dimensions for plot, width = " + ya + ", height = " + Aa;
		}
		function q(a) {
			a.width != ya && (a.width = ya);
			a.height != Aa && (a.height = Aa);
			a = a.getContext("2d");
			a.restore();
			a.save()
		}
		function F(b) {
			var c = b.labelWidth,
			d = b.labelHeight,
			g = b.options.position,
			e = b.options.tickLength,
			j = w.grid.axisMargin,
			n = w.grid.labelMargin,
			k = "x" == b.direction ? ja: za,
			q = a.grep(k,
			function(a) {
				return a && a.options.position == g && a.reserveSpace
			});
			a.inArray(b, q) == q.length - 1 && (j = 0);
			null == e && (e = "full");
			k = a.grep(k,
			function(a) {
				return a && a.reserveSpace
			});
			k = 0 == a.inArray(b, k); ! k && "full" == e && (e = 5);
			isNaN( + e) || (n += +e);
			"x" == b.direction ? (d += n, "bottom" == g ? (J.bottom += d + j, b.box = {
				top: Aa - J.bottom,
				height: d
			}) : (b.box = {
				top: J.top + j,
				height: d
			},
			J.top += d + j)) : (c += n, "left" == g ? (b.box = {
				left: J.left + j,
				width: c
			},
			J.left += c + j) : (J.right += c + j, b.box = {
				left: ya - J.right,
				width: c
			}));
			b.position = g;
			b.tickLength = e;
			b.box.padding = n;
			b.innermost = k
		}
		function D() {
			var d, g = A();
			a.each(g,
			function(a, b) {
				b.show = b.options.show;
				null == b.show && (b.show = b.used);
				b.reserveSpace = b.show || b.options.reserveSpace;
				var c = b.options,
				d = +(null != c.min ? c.min: b.datamin),
				g = +(null != c.max ? c.max: b.datamax),
				e = g - d;
				if (0 == e) {
					if (e = 0 == g ? 1 : 0.01, null == c.min && (d -= e), null == c.max || null != c.min) g += e
				} else {
					var j = c.autoscaleMargin;
					null != j && (null == c.min && (d -= e * j, 0 > d && (null != b.datamin && 0 <= b.datamin) && (d = 0)), null == c.max && (g += e * j, 0 < g && (null != b.datamax && 0 >= b.datamax) && (g = 0)))
				}
				b.min = d;
				b.max = g
			});
			allocatedAxes = a.grep(g,
			function(a) {
				return a.reserveSpace
			});
			J.left = J.right = J.top = J.bottom = 0;
			if (w.grid.show) {
				a.each(allocatedAxes,
				function(d, g) {
					var e = g.options,
					j;
					j = "number" == typeof e.ticks && 0 < e.ticks ? e.ticks: 0.3 * Math.sqrt("x" == g.direction ? ya: Aa);
					j = (g.max - g.min) / j;
					var n, k, q, p;
					if ("time" == e.mode) {
						var r = {
							second: 1E3,
							minute: 6E4,
							hour: 36E5,
							day: 864E5,
							month: 2592E6,
							year: 525949.2 * 6E4
						};
						p = [[1, "second"], [2, "second"], [5, "second"], [10, "second"], [30, "second"], [1, "minute"], [2, "minute"], [5, "minute"], [10, "minute"], [30, "minute"], [1, "hour"], [2, "hour"], [4, "hour"], [8, "hour"], [12, "hour"], [1, "day"], [2, "day"], [3, "day"], [0.25, "month"], [0.5, "month"], [1, "month"], [2, "month"], [3, "month"], [6, "month"], [1, "year"]];
						n = 0;
						null != e.minTickSize && (n = "number" == typeof e.tickSize ? e.tickSize: e.minTickSize[0] * r[e.minTickSize[1]]);
						for (k = 0; k < p.length - 1 && !(j < (p[k][0] * r[p[k][1]] + p[k + 1][0] * r[p[k + 1][1]]) / 2 && p[k][0] * r[p[k][1]] >= n); ++k);
						n = p[k][0];
						q = p[k][1];
						"year" == q && (k = Math.pow(10, Math.floor(Math.log(j / r.year) / Math.LN10)), p = j / r.year / k, n = (1.5 > p ? 1 : 3 > p ? 2 : 7.5 > p ? 5 : 10) * k);
						g.tickSize = e.tickSize || [n, q];
						k = function(a) {
							var c = [],
							d = a.tickSize[0],
							u = a.tickSize[1],
							g = new Date(a.min),
							e = d * r[u];
							"second" == u && g.setUTCSeconds(b(g.getUTCSeconds(), d));
							"minute" == u && g.setUTCMinutes(b(g.getUTCMinutes(), d));
							"hour" == u && g.setUTCHours(b(g.getUTCHours(), d));
							"month" == u && g.setUTCMonth(b(g.getUTCMonth(), d));
							"year" == u && g.setUTCFullYear(b(g.getUTCFullYear(), d));
							g.setUTCMilliseconds(0);
							e >= r.minute && g.setUTCSeconds(0);
							e >= r.hour && g.setUTCMinutes(0);
							e >= r.day && g.setUTCHours(0);
							e >= 4 * r.day && g.setUTCDate(1);
							e >= r.year && g.setUTCMonth(0);
							var j = 0,
							n = Number.NaN,
							k;
							do
							if (k = n, n = g.getTime(), c.push(n), "month" == u) if (1 > d) {
								g.setUTCDate(1);
								var q = g.getTime();
								g.setUTCMonth(g.getUTCMonth() + 1);
								var p = g.getTime();
								g.setTime(n + j * r.hour + (p - q) * d);
								j = g.getUTCHours();
								g.setUTCHours(0)
							} else g.setUTCMonth(g.getUTCMonth() + d);
							else "year" == u ? g.setUTCFullYear(g.getUTCFullYear() + d) : g.setTime(n + e);
							while (n < a.max && n != k);
							return c
						};
						n = function(b, c) {
							var d = new Date(b);
							if (null != e.timeformat) return a.plot.formatDate(d, e.timeformat, e.monthNames);
							var u = c.tickSize[0] * r[c.tickSize[1]],
							g = c.max - c.min,
							j = e.twelveHourClock ? " %p": "";
							fmt = u < r.minute ? "%h:%M:%S" + j: u < r.day ? g < 2 * r.day ? "%h:%M" + j: "%b %d %h:%M" + j: u < r.month ? "%b %d": u < r.year ? g < r.year ? "%b": "%b %y": "%y";
							return a.plot.formatDate(d, fmt, e.monthNames)
						}
					} else {
						q = e.tickDecimals;
						var w = -Math.floor(Math.log(j) / Math.LN10);
						null != q && w > q && (w = q);
						k = Math.pow(10, -w);
						p = j / k;
						if (1.5 > p) n = 1;
						else if (3 > p) {
							if (n = 2, 2.25 < p && (null == q || w + 1 <= q)) n = 2.5,
							++w
						} else n = 7.5 > p ? 5 : 10;
						n *= k;
						null != e.minTickSize && n < e.minTickSize && (n = e.minTickSize);
						g.tickDecimals = Math.max(0, null != q ? q: w);
						g.tickSize = e.tickSize || n;
						k = function(a) {
							var c = [],
							d = b(a.min, a.tickSize),
							u = 0,
							g = Number.NaN,
							e;
							do e = g,
							g = d + u * a.tickSize,
							c.push(g),
							++u;
							while (g < a.max && g != e);
							return c
						};
						n = function(a, b) {
							return a.toFixed(b.tickDecimals)
						}
					}
					if (null != e.alignTicksWithAxis) {
						var t = ("x" == g.direction ? ja: za)[e.alignTicksWithAxis - 1];
						t && (t.used && t != g) && (k = k(g), 0 < k.length && (null == e.min && (g.min = Math.min(g.min, k[0])), null == e.max && 1 < k.length && (g.max = Math.max(g.max, k[k.length - 1]))), k = function(a) {
							var b = [],
							c,
							d;
							for (d = 0; d < t.ticks.length; ++d) c = (t.ticks[d].v - t.min) / (t.max - t.min),
							c = a.min + c * (a.max - a.min),
							b.push(c);
							return b
						},
						"time" != g.mode && null == e.tickDecimals && (j = Math.max(0, -Math.floor(Math.log(j) / Math.LN10) + 1), p = k(g), 1 < p.length && /\..*0$/.test((p[1] - p[0]).toFixed(j)) || (g.tickDecimals = j)))
					}
					g.tickGenerator = k;
					g.tickFormatter = a.isFunction(e.tickFormatter) ?
					function(a, b) {
						return "" + e.tickFormatter(a, b)
					}: n;
					n = g.options.ticks;
					j = [];
					null == n || "number" == typeof n && 0 < n ? j = g.tickGenerator(g) : n && (j = a.isFunction(n) ? n({
						min: g.min,
						max: g.max
					}) : n);
					g.ticks = [];
					for (n = 0; n < j.length; ++n) p = null,
					q = j[n],
					"object" == typeof q ? (k = +q[0], 1 < q.length && (p = q[1])) : k = +q,
					null == p && (p = g.tickFormatter(k, g)),
					isNaN(k) || g.ticks.push({
						v: k,
						label: p
					});
					j = g.ticks;
					g.options.autoscaleMargin && 0 < j.length && (null == g.options.min && (g.min = Math.min(g.min, j[0].v)), null == g.options.max && 1 < j.length && (g.max = Math.max(g.max, j[j.length - 1].v)));
					n = function(b, d) {
						return a('<div style="position:absolute;top:-10000px;' + d + 'font-size:smaller"><div class="' + g.direction + "Axis " + g.direction + g.n + 'Axis">' + b.join("") + "</div></div>").appendTo(c)
					};
					q = g.options;
					k = g.ticks || [];
					p = [];
					var G;
					j = q.labelWidth;
					q = q.labelHeight;
					if ("x" == g.direction) {
						if (null == j && (j = Math.floor(ya / (0 < k.length ? k.length: 1))), null == q) {
							p = [];
							for (w = 0; w < k.length; ++w)(G = k[w].label) && p.push('<div class="tickLabel" style="float:left;width:' + j + 'px">' + G + "</div>");
							0 < p.length && (p.push('<div style="clear:left"></div>'), n = n(p, "width:10000px;"), q = n.height(), n.remove())
						}
					} else if (null == j || null == q) {
						for (w = 0; w < k.length; ++w)(G = k[w].label) && p.push('<div class="tickLabel">' + G + "</div>");
						0 < p.length && (n = n(p, ""), null == j && (j = n.children().width()), null == q && (q = n.find("div.tickLabel").height()), n.remove())
					}
					null == j && (j = 0);
					null == q && (q = 0);
					g.labelWidth = j;
					g.labelHeight = q
				});
				for (d = allocatedAxes.length - 1; 0 <= d; --d) F(allocatedAxes[d]);
				var e = w.grid.minBorderMargin;
				if (null == e) for (d = e = 0; d < G.length; ++d) e = Math.max(e, G[d].points.radius + G[d].points.lineWidth / 2);
				for (var j in J) J[j] += w.grid.borderWidth,
				J[j] = Math.max(e, J[j])
			}
			ma = ya - J.left - J.right;
			Ga = Aa - J.bottom - J.top;
			a.each(g,
			function(a, b) {
				var c = function(a) {
					return a
				},
				d,
				g,
				e = b.options.transform || c,
				j = b.options.inverseTransform;
				"x" == b.direction ? (d = b.scale = ma / Math.abs(e(b.max) - e(b.min)), g = Math.min(e(b.max), e(b.min))) : (d = b.scale = Ga / Math.abs(e(b.max) - e(b.min)), d = -d, g = Math.max(e(b.max), e(b.min)));
				b.p2c = e == c ?
				function(a) {
					return (a - g) * d
				}: function(a) {
					return (e(a) - g) * d
				};
				b.c2p = j ?
				function(a) {
					return j(g + a / d)
				}: function(a) {
					return g + a / d
				}
			});
			if (w.grid.show) {
				a.each(allocatedAxes,
				function(a, b) {
					"x" == b.direction ? (b.box.left = J.left, b.box.width = ma) : (b.box.top = J.top, b.box.height = Ga)
				});
				c.find(".tickLabels").remove();
				d = ['<div class="tickLabels" style="font-size:smaller">'];
				g = A();
				for (e = 0; e < g.length; ++e) {
					j = g[e];
					var n = j.box;
					if (j.show) {
						d.push('<div class="' + j.direction + "Axis " + j.direction + j.n + 'Axis" style="color:' + j.options.color + '">');
						for (var k = 0; k < j.ticks.length; ++k) {
							var q = j.ticks[k];
							if (q.label && !(q.v < j.min || q.v > j.max)) {
								var p = {},
								r;
								"x" == j.direction ? (r = "center", p.left = Math.round(J.left + j.p2c(q.v) - j.labelWidth / 2), "bottom" == j.position ? p.top = n.top + n.padding: p.bottom = Aa - (n.top + n.height - n.padding)) : (p.top = Math.round(J.top + j.p2c(q.v) - j.labelHeight / 2), "left" == j.position ? (p.right = ya - (n.left + n.width - n.padding), r = "right") : (p.left = n.left + n.padding, r = "left"));
								p.width = j.labelWidth;
								r = ["position:absolute", "text-align:" + r];
								for (var t in p) r.push(t + ":" + p[t] + "px");
								d.push('<div class="tickLabel" style="' + r.join(";") + '">' + q.label + "</div>")
							}
						}
						d.push("</div>")
					}
				}
				d.push("</div>");
				c.append(d.join(""))
			}
			c.find(".legend").remove();
			if (w.legend.show) {
				t = [];
				d = !1;
				g = w.legend.labelFormatter;
				for (n = 0; n < G.length; ++n) if (e = G[n], j = e.label) 0 == n % w.legend.noColumns && (d && t.push("</tr>"), t.push("<tr>"), d = !0),
				g && (j = g(j, e)),
				t.push('<td class="legendColorBox"><div style="border:1px solid ' + w.legend.labelBoxBorderColor + ';padding:1px"><div style="width:4px;height:0;border:5px solid ' + e.color + ';overflow:hidden"></div></div></td><td class="legendLabel">' + j + "</td>");
				d && t.push("</tr>");
				0 != t.length && (d = '<table style="font-size:smaller;color:' + w.grid.color + '">' + t.join("") + "</table>", null != w.legend.container ? a(w.legend.container).html(d) : (t = "", g = w.legend.position, e = w.legend.margin, null == e[0] && (e = [e, e]), "n" == g.charAt(0) ? t += "top:" + (e[1] + J.top) + "px;": "s" == g.charAt(0) && (t += "bottom:" + (e[1] + J.bottom) + "px;"), "e" == g.charAt(1) ? t += "right:" + (e[0] + J.right) + "px;": "w" == g.charAt(1) && (t += "left:" + (e[0] + J.left) + "px;"), d = a('<div class="legend">' + d.replace('style="', 'style="position:absolute;' + t + ";") + "</div>").appendTo(c), 0 != w.legend.backgroundOpacity && (g = w.legend.backgroundColor, null == g && (g = (g = w.grid.backgroundColor) && "string" == typeof g ? a.color.parse(g) : a.color.extract(d, "background-color"), g.a = 1, g = g.toString()), e = d.children(), a('<div style="position:absolute;width:' + e.width() + "px;height:" + e.height() + "px;" + t + "background-color:" + g + ';"> </div>').prependTo(d).css("opacity", w.legend.backgroundOpacity))))
			}
		}
		function E() {
			C.clearRect(0, 0, ya, Aa);
			var a = w.grid;
			a.show && a.backgroundColor && (C.save(), C.translate(J.left, J.top), C.fillStyle = qa(w.grid.backgroundColor, Ga, 0, "rgba(255, 255, 255, 0)"), C.fillRect(0, 0, ma, Ga), C.restore());
			a.show && !a.aboveData && ha();
			for (var b = 0; b < G.length; ++b) {
				j(Ca.drawSeries, [C, G[b]]);
				var c = G[b];
				c.lines.show && W(c);
				c.bars.show && U(c);
				c.points.show && sa(c)
			}
			j(Ca.draw, [C]);
			a.show && a.aboveData && ha()
		}
		function V(a, b) {
			var c, d, g, e, j = A();
			for (i = 0; i < j.length; ++i) if (c = j[i], c.direction == b && (e = b + c.n + "axis", !a[e] && 1 == c.n && (e = b + "axis"), a[e])) {
				d = a[e].from;
				g = a[e].to;
				break
			}
			a[e] || (c = "x" == b ? ja[0] : za[0], d = a[b + "1"], g = a[b + "2"]);
			null != d && (null != g && d > g) && (e = d, d = g, g = e);
			return {
				from: d,
				to: g,
				axis: c
			}
		}
		function ha() {
			var b;
			C.save();
			C.translate(J.left, J.top);
			var c = w.grid.markings;
			if (c) {
				if (a.isFunction(c)) {
					var d = ka.getAxes();
					d.xmin = d.xaxis.min;
					d.xmax = d.xaxis.max;
					d.ymin = d.yaxis.min;
					d.ymax = d.yaxis.max;
					c = c(d)
				}
				for (b = 0; b < c.length; ++b) {
					var d = c[b],
					g = V(d, "x"),
					e = V(d, "y");
					null == g.from && (g.from = g.axis.min);
					null == g.to && (g.to = g.axis.max);
					null == e.from && (e.from = e.axis.min);
					null == e.to && (e.to = e.axis.max);
					g.to < g.axis.min || (g.from > g.axis.max || e.to < e.axis.min || e.from > e.axis.max) || (g.from = Math.max(g.from, g.axis.min), g.to = Math.min(g.to, g.axis.max), e.from = Math.max(e.from, e.axis.min), e.to = Math.min(e.to, e.axis.max), g.from == g.to && e.from == e.to || (g.from = g.axis.p2c(g.from), g.to = g.axis.p2c(g.to), e.from = e.axis.p2c(e.from), e.to = e.axis.p2c(e.to), g.from == g.to || e.from == e.to ? (C.beginPath(), C.strokeStyle = d.color || w.grid.markingsColor, C.lineWidth = d.lineWidth || w.grid.markingsLineWidth, C.moveTo(g.from, e.from), C.lineTo(g.to, e.to), C.stroke()) : (C.fillStyle = d.color || w.grid.markingsColor, C.fillRect(g.from, e.to, g.to - g.from, e.from - e.to))))
				}
			}
			d = A();
			c = w.grid.borderWidth;
			for (g = 0; g < d.length; ++g) {
				e = d[g];
				b = e.box;
				var j = e.tickLength,
				n, k, q, p;
				if (e.show && 0 != e.ticks.length) {
					C.strokeStyle = e.options.tickColor || a.color.parse(e.options.color).scale("a", 0.22).toString();
					C.lineWidth = 1;
					"x" == e.direction ? (n = 0, k = "full" == j ? "top" == e.position ? 0 : Ga: b.top - J.top + ("top" == e.position ? b.height: 0)) : (k = 0, n = "full" == j ? "left" == e.position ? 0 : ma: b.left - J.left + ("left" == e.position ? b.width: 0));
					e.innermost || (C.beginPath(), q = p = 0, "x" == e.direction ? q = ma: p = Ga, 1 == C.lineWidth && (n = Math.floor(n) + 0.5, k = Math.floor(k) + 0.5), C.moveTo(n, k), C.lineTo(n + q, k + p), C.stroke());
					C.beginPath();
					for (b = 0; b < e.ticks.length; ++b) {
						var r = e.ticks[b].v;
						q = p = 0;
						r < e.min || (r > e.max || "full" == j && 0 < c && (r == e.min || r == e.max)) || ("x" == e.direction ? (n = e.p2c(r), p = "full" == j ? -Ga: j, "top" == e.position && (p = -p)) : (k = e.p2c(r), q = "full" == j ? -ma: j, "left" == e.position && (q = -q)), 1 == C.lineWidth && ("x" == e.direction ? n = Math.floor(n) + 0.5 : k = Math.floor(k) + 0.5), C.moveTo(n, k), C.lineTo(n + q, k + p))
					}
					C.stroke()
				}
			}
			c && (C.lineWidth = c, C.strokeStyle = w.grid.borderColor, C.strokeRect( - c / 2, -c / 2, ma + c, Ga + c));
			C.restore()
		}
		function W(a) {
			function b(a, c, d, g, e) {
				var j = a.points;
				a = a.pointsize;
				var u = null,
				n = null;
				C.beginPath();
				for (var k = a; k < j.length; k += a) {
					var H = j[k - a],
					q = j[k - a + 1],
					v = j[k],
					p = j[k + 1];
					if (! (null == H || null == v)) {
						if (q <= p && q < e.min) {
							if (p < e.min) continue;
							H = (e.min - q) / (p - q) * (v - H) + H;
							q = e.min
						} else if (p <= q && p < e.min) {
							if (q < e.min) continue;
							v = (e.min - q) / (p - q) * (v - H) + H;
							p = e.min
						}
						if (q >= p && q > e.max) {
							if (p > e.max) continue;
							H = (e.max - q) / (p - q) * (v - H) + H;
							q = e.max
						} else if (p >= q && p > e.max) {
							if (q > e.max) continue;
							v = (e.max - q) / (p - q) * (v - H) + H;
							p = e.max
						}
						if (H <= v && H < g.min) {
							if (v < g.min) continue;
							q = (g.min - H) / (v - H) * (p - q) + q;
							H = g.min
						} else if (v <= H && v < g.min) {
							if (H < g.min) continue;
							p = (g.min - H) / (v - H) * (p - q) + q;
							v = g.min
						}
						if (H >= v && H > g.max) {
							if (v > g.max) continue;
							q = (g.max - H) / (v - H) * (p - q) + q;
							H = g.max
						} else if (v >= H && v > g.max) {
							if (H > g.max) continue;
							p = (g.max - H) / (v - H) * (p - q) + q;
							v = g.max
						} (H != u || q != n) && C.moveTo(g.p2c(H) + c, e.p2c(q) + d);
						u = v;
						n = p;
						C.lineTo(g.p2c(v) + c, e.p2c(p) + d)
					}
				}
				C.stroke()
			}
			C.save();
			C.translate(J.left, J.top);
			C.lineJoin = "round";
			var c = a.lines.lineWidth,
			d = a.shadowSize;
			if (0 < c && 0 < d) {
				C.lineWidth = d;
				C.strokeStyle = "rgba(0,0,0,0.1)";
				var g = Math.PI / 18;
				b(a.datapoints, Math.sin(g) * (c / 2 + d / 2), Math.cos(g) * (c / 2 + d / 2), a.xaxis, a.yaxis);
				C.lineWidth = d / 2;
				b(a.datapoints, Math.sin(g) * (c / 2 + d / 4), Math.cos(g) * (c / 2 + d / 4), a.xaxis, a.yaxis)
			}
			C.lineWidth = c;
			C.strokeStyle = a.color;
			if (d = ba(a.lines, a.color, 0, Ga)) {
				C.fillStyle = d;
				for (var e = a.datapoints,
				d = a.xaxis,
				g = a.yaxis,
				j = e.points,
				e = e.pointsize,
				n = Math.min(Math.max(0, g.min), g.max), k = 0, q = !1, p = 1, r = 0, w = 0; ! (0 < e && k > j.length + e);) {
					var k = k + e,
					t = j[k - e],
					G = j[k - e + p],
					z = j[k],
					A = j[k + p];
					if (q) {
						if (0 < e && null != t && null == z) {
							w = k;
							e = -e;
							p = 2;
							continue
						}
						if (0 > e && k == r + e) {
							C.fill();
							q = !1;
							e = -e;
							p = 1;
							k = r = w + e;
							continue
						}
					}
					if (! (null == t || null == z)) {
						if (t <= z && t < d.min) {
							if (z < d.min) continue;
							G = (d.min - t) / (z - t) * (A - G) + G;
							t = d.min
						} else if (z <= t && z < d.min) {
							if (t < d.min) continue;
							A = (d.min - t) / (z - t) * (A - G) + G;
							z = d.min
						}
						if (t >= z && t > d.max) {
							if (z > d.max) continue;
							G = (d.max - t) / (z - t) * (A - G) + G;
							t = d.max
						} else if (z >= t && z > d.max) {
							if (t > d.max) continue;
							A = (d.max - t) / (z - t) * (A - G) + G;
							z = d.max
						}
						q || (C.beginPath(), C.moveTo(d.p2c(t), g.p2c(n)), q = !0);
						if (G >= g.max && A >= g.max) C.lineTo(d.p2c(t), g.p2c(g.max)),
						C.lineTo(d.p2c(z), g.p2c(g.max));
						else if (G <= g.min && A <= g.min) C.lineTo(d.p2c(t), g.p2c(g.min)),
						C.lineTo(d.p2c(z), g.p2c(g.min));
						else {
							var B = t,
							D = z;
							G <= A && G < g.min && A >= g.min ? (t = (g.min - G) / (A - G) * (z - t) + t, G = g.min) : A <= G && (A < g.min && G >= g.min) && (z = (g.min - G) / (A - G) * (z - t) + t, A = g.min);
							G >= A && G > g.max && A <= g.max ? (t = (g.max - G) / (A - G) * (z - t) + t, G = g.max) : A >= G && (A > g.max && G <= g.max) && (z = (g.max - G) / (A - G) * (z - t) + t, A = g.max);
							t != B && C.lineTo(d.p2c(B), g.p2c(G));
							C.lineTo(d.p2c(t), g.p2c(G));
							C.lineTo(d.p2c(z), g.p2c(A));
							z != D && (C.lineTo(d.p2c(z), g.p2c(A)), C.lineTo(d.p2c(D), g.p2c(A)))
						}
					}
				}
			}
			0 < c && b(a.datapoints, 0, 0, a.xaxis, a.yaxis);
			C.restore()
		}
		function sa(a) {
			function b(a, c, d, g, e, j, n, k) {
				var q = a.points;
				a = a.pointsize;
				for (var p = 0; p < q.length; p += a) {
					var r = q[p],
					w = q[p + 1];
					null == r || (r < j.min || r > j.max || w < n.min || w > n.max) || (C.beginPath(), r = j.p2c(r), w = n.p2c(w) + g, "circle" == k ? C.arc(r, w, c, 0, e ? Math.PI: 2 * Math.PI, !1) : k(C, r, w, c, e), C.closePath(), d && (C.fillStyle = d, C.fill()), C.stroke())
				}
			}
			C.save();
			C.translate(J.left, J.top);
			var c = a.points.lineWidth,
			d = a.shadowSize,
			g = a.points.radius,
			e = a.points.symbol;
			0 < c && 0 < d && (d /= 2, C.lineWidth = d, C.strokeStyle = "rgba(0,0,0,0.1)", b(a.datapoints, g, null, d + d / 2, !0, a.xaxis, a.yaxis, e), C.strokeStyle = "rgba(0,0,0,0.2)", b(a.datapoints, g, null, d / 2, !0, a.xaxis, a.yaxis, e));
			C.lineWidth = c;
			C.strokeStyle = a.color;
			b(a.datapoints, g, ba(a.points, a.color), 0, !1, a.xaxis, a.yaxis, e);
			C.restore()
		}
		function S(a, b, c, d, g, e, j, n, k, q, p, r) {
			var w, t, G, z;
			p ? (z = t = G = !0, w = !1, p = c, c = b + d, g = b + g, a < p && (b = a, a = p, p = b, w = !0, t = !1)) : (w = t = G = !0, z = !1, p = a + d, a += g, g = c, c = b, c < g && (b = c, c = g, g = b, z = !0, G = !1));
			if (! (a < n.min || p > n.max || c < k.min || g > k.max)) if (p < n.min && (p = n.min, w = !1), a > n.max && (a = n.max, t = !1), g < k.min && (g = k.min, z = !1), c > k.max && (c = k.max, G = !1), p = n.p2c(p), g = k.p2c(g), a = n.p2c(a), c = k.p2c(c), j && (q.beginPath(), q.moveTo(p, g), q.lineTo(p, c), q.lineTo(a, c), q.lineTo(a, g), q.fillStyle = j(g, c), q.fill()), 0 < r && (w || t || G || z)) q.beginPath(),
			q.moveTo(p, g + e),
			w ? q.lineTo(p, c + e) : q.moveTo(p, c + e),
			G ? q.lineTo(a, c + e) : q.moveTo(a, c + e),
			t ? q.lineTo(a, g + e) : q.moveTo(a, g + e),
			z ? q.lineTo(p, g + e) : q.moveTo(p, g + e),
			q.stroke()
		}
		function U(a) {
			C.save();
			C.translate(J.left, J.top);
			C.lineWidth = a.bars.lineWidth;
			C.strokeStyle = a.color;
			for (var b = "left" == a.bars.align ? 0 : -a.bars.barWidth / 2, c = a.datapoints, d = b + a.bars.barWidth, g = a.bars.fill ?
			function(b, c) {
				return ba(a.bars, a.color, b, c)
			}: null, e = a.xaxis, j = a.yaxis, n = c.points, c = c.pointsize, k = 0; k < n.length; k += c) null != n[k] && S(n[k], n[k + 1], n[k + 2], b, d, 0, g, e, j, C, a.bars.horizontal, a.bars.lineWidth);
			C.restore()
		}
		function ba(b, c, d, g) {
			var e = b.fill;
			if (!e) return null;
			if (b.fillColor) return qa(b.fillColor, d, g, c);
			b = a.color.parse(c);
			b.a = "number" == typeof e ? e: 0.4;
			b.normalize();
			return b.toString()
		}
		function ia(a) {
			w.grid.hoverable && xa("plothover", a,
			function(a) {
				return ! 1 != a.hoverable
			})
		}
		function ga(a) {
			w.grid.hoverable && xa("plothover", a,
			function() {
				return ! 1
			})
		}
		function Y(a) {
			xa("plotclick", a,
			function(a) {
				return ! 1 != a.clickable
			})
		}
		function xa(a, b, d) {
			var g = L.offset(),
			e = b.pageX - g.left - J.left,
			j = b.pageY - g.top - J.top,
			n = B({
				left: e,
				top: j
			});
			n.pageX = b.pageX;
			n.pageY = b.pageY;
			b = w.grid.mouseActiveRadius;
			var k = b * b + 1,
			q = null,
			p, r;
			for (p = G.length - 1; 0 <= p; --p) if (d(G[p])) {
				var t = G[p],
				z = t.xaxis,
				A = t.yaxis,
				C = t.datapoints.points,
				D = t.datapoints.pointsize,
				F = z.c2p(e),
				R = A.c2p(j),
				E = b / z.scale,
				la = b / A.scale;
				z.options.inverseTransform && (E = Number.MAX_VALUE);
				A.options.inverseTransform && (la = Number.MAX_VALUE);
				if (t.lines.show || t.points.show) for (r = 0; r < C.length; r += D) {
					var M = C[r],
					U = C[r + 1];
					if (null != M && !(M - F > E || M - F < -E || U - R > la || U - R < -la)) M = Math.abs(z.p2c(M) - e),
					U = Math.abs(A.p2c(U) - j),
					U = M * M + U * U,
					U < k && (k = U, q = [p, r / D])
				}
				if (t.bars.show && !q) {
					z = "left" == t.bars.align ? 0 : -t.bars.barWidth / 2;
					t = z + t.bars.barWidth;
					for (r = 0; r < C.length; r += D) if (M = C[r], U = C[r + 1], A = C[r + 2], null != M && (G[p].bars.horizontal ? F <= Math.max(A, M) && F >= Math.min(A, M) && R >= U + z && R <= U + t: F >= M + z && F <= M + t && R >= Math.min(A, U) && R <= Math.max(A, U))) q = [p, r / D]
				}
			}
			q ? (p = q[0], r = q[1], D = G[p].datapoints.pointsize, d = {
				datapoint: G[p].datapoints.points.slice(r * D, (r + 1) * D),
				dataIndex: r,
				series: G[p],
				seriesIndex: p
			}) : d = null;
			d && (d.pageX = parseInt(d.series.xaxis.p2c(d.datapoint[0]) + g.left + J.left), d.pageY = parseInt(d.series.yaxis.p2c(d.datapoint[1]) + g.top + J.top));
			if (w.grid.autoHighlight) {
				for (g = 0; g < Da.length; ++g) e = Da[g],
				e.auto == a && (!d || !(e.series == d.series && e.point[0] == d.datapoint[0] && e.point[1] == d.datapoint[1])) && N(e.series, e.point);
				d && va(d.series, d.datapoint, a)
			}
			c.trigger(a, [n, d])
		}
		function ua() {
			aa || (aa = setTimeout(ta, 30))
		}
		function ta() {
			aa = null;
			M.save();
			M.clearRect(0, 0, ya, Aa);
			M.translate(J.left, J.top);
			var b, c;
			for (b = 0; b < Da.length; ++b) if (c = Da[b], c.series.bars.show) Ba(c.series, c.point);
			else {
				var d = c.series,
				g = c.point;
				c = g[0];
				var g = g[1],
				e = d.xaxis,
				n = d.yaxis;
				if (! (c < e.min || c > e.max || g < n.min || g > n.max)) {
					var k = d.points.radius + d.points.lineWidth / 2;
					M.lineWidth = k;
					M.strokeStyle = a.color.parse(d.color).scale("a", 0.5).toString();
					k *= 1.5;
					c = e.p2c(c);
					g = n.p2c(g);
					M.beginPath();
					"circle" == d.points.symbol ? M.arc(c, g, k, 0, 2 * Math.PI, !1) : d.points.symbol(M, c, g, k, !1);
					M.closePath();
					M.stroke()
				}
			}
			M.restore();
			j(Ca.drawOverlay, [M])
		}
		function va(a, b, c) {
			"number" == typeof a && (a = G[a]);
			if ("number" == typeof b) {
				var d = a.datapoints.pointsize;
				b = a.datapoints.points.slice(d * b, d * (b + 1))
			}
			d = na(a, b); - 1 == d ? (Da.push({
				series: a,
				point: b,
				auto: c
			}), ua()) : c || (Da[d].auto = !1)
		}
		function N(a, b) {
			null == a && null == b && (Da = [], ua());
			"number" == typeof a && (a = G[a]);
			"number" == typeof b && (b = a.data[b]);
			var c = na(a, b); - 1 != c && (Da.splice(c, 1), ua())
		}
		function na(a, b) {
			for (var c = 0; c < Da.length; ++c) {
				var d = Da[c];
				if (d.series == a && d.point[0] == b[0] && d.point[1] == b[1]) return c
			}
			return - 1
		}
		function Ba(b, c) {
			M.lineWidth = b.bars.lineWidth;
			M.strokeStyle = a.color.parse(b.color).scale("a", 0.5).toString();
			var d = a.color.parse(b.color).scale("a", 0.5).toString(),
			g = "left" == b.bars.align ? 0 : -b.bars.barWidth / 2;
			S(c[0], c[1], c[2] || 0, g, g + b.bars.barWidth, 0,
			function() {
				return d
			},
			b.xaxis, b.yaxis, M, b.bars.horizontal, b.bars.lineWidth)
		}
		function qa(b, c, d, g) {
			if ("string" == typeof b) return b;
			c = C.createLinearGradient(0, d, 0, c);
			d = 0;
			for (var e = b.colors.length; d < e; ++d) {
				var j = b.colors[d];
				if ("string" != typeof j) {
					var n = a.color.parse(g);
					null != j.brightness && (n = n.scale("rgb", j.brightness));
					null != j.opacity && (n.a *= j.opacity);
					j = n.toString()
				}
				c.addColorStop(d / (e - 1), j)
			}
			return c
		}
		var G = [],
		w = {
			colors: ["#edc240", "#afd8f8", "#cb4b4b", "#4da74d", "#9440ed"],
			legend: {
				show: !0,
				noColumns: 1,
				labelFormatter: null,
				labelBoxBorderColor: "#ccc",
				container: null,
				position: "ne",
				margin: 5,
				backgroundColor: null,
				backgroundOpacity: 0.85
			},
			xaxis: {
				show: null,
				position: "bottom",
				mode: null,
				color: null,
				tickColor: null,
				transform: null,
				inverseTransform: null,
				min: null,
				max: null,
				autoscaleMargin: null,
				ticks: null,
				tickFormatter: null,
				labelWidth: null,
				labelHeight: null,
				reserveSpace: null,
				tickLength: null,
				alignTicksWithAxis: null,
				tickDecimals: null,
				tickSize: null,
				minTickSize: null,
				monthNames: null,
				timeformat: null,
				twelveHourClock: !1
			},
			yaxis: {
				autoscaleMargin: 0.02,
				position: "left"
			},
			xaxes: [],
			yaxes: [],
			series: {
				points: {
					show: !1,
					radius: 3,
					lineWidth: 2,
					fill: !0,
					fillColor: "#ffffff",
					symbol: "circle"
				},
				lines: {
					lineWidth: 2,
					fill: !1,
					fillColor: null,
					steps: !1
				},
				bars: {
					show: !1,
					lineWidth: 2,
					barWidth: 1,
					fill: !0,
					fillColor: null,
					align: "left",
					horizontal: !1
				},
				shadowSize: 3
			},
			grid: {
				show: !0,
				aboveData: !1,
				color: "#545454",
				backgroundColor: null,
				borderColor: null,
				tickColor: null,
				labelMargin: 5,
				axisMargin: 8,
				borderWidth: 2,
				minBorderMargin: null,
				markings: null,
				markingsColor: "#f4f4f4",
				markingsLineWidth: 2,
				clickable: !1,
				hoverable: !1,
				autoHighlight: !0,
				mouseActiveRadius: 10
			},
			hooks: {}
		},
		R = null,
		la = null,
		L = null,
		C = null,
		M = null,
		ja = [],
		za = [],
		J = {
			left: 0,
			right: 0,
			top: 0,
			bottom: 0
		},
		ya = 0,
		Aa = 0,
		ma = 0,
		Ga = 0,
		Ca = {
			processOptions: [],
			processRawData: [],
			processDatapoints: [],
			drawSeries: [],
			draw: [],
			bindEvents: [],
			drawOverlay: [],
			shutdown: []
		},
		ka = this;
		ka.setData = k;
		ka.setupGrid = D;
		ka.draw = E;
		ka.getPlaceholder = function() {
			return c
		};
		ka.getCanvas = function() {
			return R
		};
		ka.getPlotOffset = function() {
			return J
		};
		ka.width = function() {
			return ma
		};
		ka.height = function() {
			return Ga
		};
		ka.offset = function() {
			var a = L.offset();
			a.left += J.left;
			a.top += J.top;
			return a
		};
		ka.getData = function() {
			return G
		};
		ka.getAxes = function() {
			var b = {};
			a.each(ja.concat(za),
			function(a, c) {
				c && (b[c.direction + (1 != c.n ? c.n: "") + "axis"] = c)
			});
			return b
		};
		ka.getXAxes = function() {
			return ja
		};
		ka.getYAxes = function() {
			return za
		};
		ka.c2p = B;
		ka.p2c = function(a) {
			var b = {},
			c, d, g;
			for (c = 0; c < ja.length; ++c) if ((d = ja[c]) && d.used) if (g = "x" + d.n, null == a[g] && 1 == d.n && (g = "x"), null != a[g]) {
				b.left = d.p2c(a[g]);
				break
			}
			for (c = 0; c < za.length; ++c) if ((d = za[c]) && d.used) if (g = "y" + d.n, null == a[g] && 1 == d.n && (g = "y"), null != a[g]) {
				b.top = d.p2c(a[g]);
				break
			}
			return b
		};
		ka.getOptions = function() {
			return w
		};
		ka.highlight = va;
		ka.unhighlight = N;
		ka.triggerRedrawOverlay = ua;
		ka.pointOffset = function(a) {
			return {
				left: parseInt(ja[t(a, "x") - 1].p2c( + a.x) + J.left),
				top: parseInt(za[t(a, "y") - 1].p2c( + a.y) + J.top)
			}
		};
		ka.shutdown = function() {
			aa && clearTimeout(aa);
			L.unbind("mousemove", ia);
			L.unbind("mouseleave", ga);
			L.unbind("click", Y);
			j(Ca.shutdown, [L])
		};
		ka.resize = function() {
			z();
			q(R);
			q(la)
		};
		ka.hooks = Ca;
		for (var Pa = 0; Pa < e.length; ++Pa) {
			var Qa = e[Pa];
			Qa.init(ka);
			Qa.options && a.extend(!0, w, Qa.options)
		}
		a.extend(!0, w, d);
		null == w.xaxis.color && (w.xaxis.color = w.grid.color);
		null == w.yaxis.color && (w.yaxis.color = w.grid.color);
		null == w.xaxis.tickColor && (w.xaxis.tickColor = w.grid.tickColor);
		null == w.yaxis.tickColor && (w.yaxis.tickColor = w.grid.tickColor);
		null == w.grid.borderColor && (w.grid.borderColor = w.grid.color);
		null == w.grid.tickColor && (w.grid.tickColor = a.color.parse(w.grid.color).scale("a", 0.22).toString());
		for (d = 0; d < Math.max(1, w.xaxes.length); ++d) w.xaxes[d] = a.extend(!0, {},
		w.xaxis, w.xaxes[d]);
		for (d = 0; d < Math.max(1, w.yaxes.length); ++d) w.yaxes[d] = a.extend(!0, {},
		w.yaxis, w.yaxes[d]);
		w.xaxis.noTicks && null == w.xaxis.ticks && (w.xaxis.ticks = w.xaxis.noTicks);
		w.yaxis.noTicks && null == w.yaxis.ticks && (w.yaxis.ticks = w.yaxis.noTicks);
		w.x2axis && (w.xaxes[1] = a.extend(!0, {},
		w.xaxis, w.x2axis), w.xaxes[1].position = "top");
		w.y2axis && (w.yaxes[1] = a.extend(!0, {},
		w.yaxis, w.y2axis), w.yaxes[1].position = "right");
		w.grid.coloredAreas && (w.grid.markings = w.grid.coloredAreas);
		w.grid.coloredAreasColor && (w.grid.markingsColor = w.grid.coloredAreasColor);
		w.lines && a.extend(!0, w.series.lines, w.lines);
		w.points && a.extend(!0, w.series.points, w.points);
		w.bars && a.extend(!0, w.series.bars, w.bars);
		null != w.shadowSize && (w.series.shadowSize = w.shadowSize);
		for (d = 0; d < w.xaxes.length; ++d) r(ja, d + 1).options = w.xaxes[d];
		for (d = 0; d < w.yaxes.length; ++d) r(za, d + 1).options = w.yaxes[d];
		for (var Q in Ca) w.hooks[Q] && w.hooks[Q].length && (Ca[Q] = Ca[Q].concat(w.hooks[Q]));
		j(Ca.processOptions, [w]);
		Q = c.children("canvas.base");
		d = c.children("canvas.overlay");
		0 == Q.length || 0 == d ? (c.html(""), c.css({
			padding: 0
		}), "static" == c.css("position") && c.css("position", "relative"), z(), R = p(!0, "base"), la = p(!1, "overlay"), Q = !1) : (R = Q.get(0), la = d.get(0), Q = !0);
		C = R.getContext("2d");
		M = la.getContext("2d");
		L = a([la, R]);
		Q && (c.data("plot").shutdown(), ka.resize(), M.clearRect(0, 0, ya, Aa), L.unbind(), c.children().not([R, la]).remove());
		c.data("plot", ka);
		k(g);
		D();
		E();
		w.grid.hoverable && (L.mousemove(ia), L.mouseleave(ga));
		w.grid.clickable && L.click(Y);
		j(Ca.bindEvents, [L]);
		var Da = [],
		aa = null
	}
	function b(a, b) {
		return b * Math.floor(a / b)
	}
	a.plot = function(b, g, d) {
		return new e(a(b), g, d, a.plot.plugins)
	};
	a.plot.version = "0.7";
	a.plot.plugins = [];
	a.plot.formatDate = function(a, b, d) {
		var e = function(a) {
			a = "" + a;
			return 1 == a.length ? "0" + a: a
		},
		j = [],
		k = !1,
		t = !1,
		A = a.getUTCHours(),
		B = 12 > A;
		null == d && (d = "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" ")); - 1 != b.search(/%p|%P/) && (12 < A ? A -= 12 : 0 == A && (A = 12));
		for (var r = 0; r < b.length; ++r) {
			var p = b.charAt(r);
			if (k) {
				switch (p) {
				case "h":
					p = "" + A;
					break;
				case "H":
					p = e(A);
					break;
				case "M":
					p = e(a.getUTCMinutes());
					break;
				case "S":
					p = e(a.getUTCSeconds());
					break;
				case "d":
					p = "" + a.getUTCDate();
					break;
				case "m":
					p = "" + (a.getUTCMonth() + 1);
					break;
				case "y":
					p = "" + a.getUTCFullYear();
					break;
				case "b":
					p = "" + d[a.getUTCMonth()];
					break;
				case "p":
					p = B ? "am": "pm";
					break;
				case "P":
					p = B ? "AM": "PM";
					break;
				case "0":
					p = "",
					t = !0
				}
				p && t && (p = e(p), t = !1);
				j.push(p);
				t || (k = !1)
			} else "%" == p ? k = !0 : j.push(p)
		}
		return j.join("")
	}
})(jQuery); (function(a) {
	var e = {
		series: {
			pie: {
				show: !1,
				radius: "auto",
				innerRadius: 0,
				startAngle: 1.5,
				tilt: 1,
				offset: {
					top: 0,
					left: "auto"
				},
				stroke: {
					color: "#FFF",
					width: 1
				},
				label: {
					show: "auto",
					formatter: function(a, c) {
						return '<div style="font-size:x-small;text-align:center;padding:2px;color:' + c.color + ';">' + a + "<br/>" + Math.round(c.percent) + "%</div>"
					},
					radius: 1,
					background: {
						color: null,
						opacity: 0
					},
					threshold: 0
				},
				combine: {
					threshold: -1,
					color: null,
					label: "Other"
				},
				highlight: {
					opacity: 0.5
				}
			}
		}
	};
	a.plot.plugins.push({
		init: function(b) {
			function c(b) {
				sa || (sa = !0, r = b.getCanvas(), p = a(r).parent(), e = b.getOptions(), b.setData(g(b.getData())))
			}
			function g(a) {
				for (var b = 0; b < a.length; ++b) if ("number" == typeof a[b].data) a[b].data = [[1, a[b].data]];
				else if ("undefined" == typeof a[b].data || "undefined" == typeof a[b].data[0])"undefined" != typeof a[b].data && "undefined" != typeof a[b].data.label && (a[b].label = a[b].data.label),
				a[b].data = [[1, 0]];
				for (var b = a,
				c = 0; c < b.length; ++c) {
					var d = parseFloat(b[c].data[0][1]);
					d && (D += d)
				}
				for (var c = b = 0,
				d = e.series.pie.combine.color,
				g = [], j = 0; j < a.length; ++j) a[j].data[0][1] = parseFloat(a[j].data[0][1]),
				a[j].data[0][1] || (a[j].data[0][1] = 0),
				a[j].data[0][1] / D <= e.series.pie.combine.threshold ? (b += a[j].data[0][1], c++, d || (d = a[j].color)) : g.push({
					data: [[1, a[j].data[0][1]]],
					color: a[j].color,
					label: a[j].label,
					angle: a[j].data[0][1] * 2 * Math.PI / D,
					percent: 100 * (a[j].data[0][1] / D)
				});
				0 < c && g.push({
					data: [[1, b]],
					color: d,
					label: e.series.pie.combine.label,
					angle: b * 2 * Math.PI / D,
					percent: 100 * (b / D)
				});
				return g
			}
			function d(b, c) {
				function d() {
					ctx.clearRect(0, 0, r.width, r.height);
					p.children().filter(".pieLabel, .pieLabelBackground").remove()
				}
				function g() {
					function b(g, e, j) {
						0 >= g || (j ? ctx.fillStyle = e: (ctx.strokeStyle = e, ctx.lineJoin = "round"), ctx.beginPath(), 1E-9 < Math.abs(g - 2 * Math.PI) ? ctx.moveTo(0, 0) : a.browser.msie && (g -= 1E-4), ctx.arc(0, 0, c, d, d + g, !1), ctx.closePath(), d += g, j ? ctx.fill() : ctx.stroke())
					}
					startAngle = Math.PI * e.series.pie.startAngle;
					var c = 1 < e.series.pie.radius ? e.series.pie.radius: z * e.series.pie.radius;
					ctx.save();
					ctx.translate(q, F);
					ctx.scale(1, e.series.pie.tilt);
					ctx.save();
					for (var d = startAngle,
					k = 0; k < j.length; ++k) j[k].startAngle = d,
					b(j[k].angle, j[k].color, !0);
					ctx.restore();
					ctx.save();
					ctx.lineWidth = e.series.pie.stroke.width;
					d = startAngle;
					for (k = 0; k < j.length; ++k) b(j[k].angle, e.series.pie.stroke.color, !1);
					ctx.restore();
					n(ctx);
					if (e.series.pie.label.show) for (var k = startAngle,
					t = 1 < e.series.pie.label.radius ? e.series.pie.label.radius: z * e.series.pie.label.radius, G = 0; G < j.length; ++G) {
						if (j[G].percent >= 100 * e.series.pie.label.threshold) {
							var w = j[G],
							A = k,
							B = G;
							if (0 != w.data[0][1]) {
								var D = e.legend.labelFormatter,
								C = void 0,
								M = e.series.pie.label.formatter,
								C = D ? D(w.label, w) : w.label;
								M && (C = M(C, w));
								D = (A + w.angle + A) / 2;
								A = q + Math.round(Math.cos(D) * t);
								D = F + Math.round(Math.sin(D) * t) * e.series.pie.tilt;
								p.append('<span class="pieLabel" id="pieLabel' + B + '" style="position:absolute;top:' + D + "px;left:" + A + 'px;">' + C + "</span>");
								B = p.children("#pieLabel" + B);
								C = D - B.height() / 2;
								D = A - B.width() / 2;
								B.css("top", C);
								B.css("left", D);
								if (0 < 0 - C || 0 < 0 - D || 0 > r.height - (C + B.height()) || 0 > r.width - (D + B.width())) E = !0;
								0 != e.series.pie.label.background.opacity && (A = e.series.pie.label.background.color, null == A && (A = w.color), w = "top:" + C + "px;left:" + D + "px;", a('<div class="pieLabelBackground" style="position:absolute;width:' + B.width() + "px;height:" + B.height() + "px;" + w + "background-color:" + A + ';"> </div>').insertBefore(B).css("opacity", e.series.pie.label.background.opacity))
							}
						}
						k += j[G].angle
					}
					ctx.restore()
				}
				if (p) {
					ctx = c;
					W = p.children().filter(".legend").children().width();
					z = Math.min(r.width, r.height / e.series.pie.tilt) / 2;
					F = r.height / 2 + e.series.pie.offset.top;
					q = r.width / 2;
					q = "auto" == e.series.pie.offset.left ? e.legend.position.match("w") ? q + W / 2 : q - W / 2 : q + e.series.pie.offset.left;
					q < z ? q = z: q > r.width - z && (q = r.width - z);
					for (var j = b.getData(), k = 0; E && k < V;) {
						E = !1;
						0 < k && (z *= ha);
						k += 1;
						d();
						if (0.8 >= e.series.pie.tilt) {
							var t = 1 < e.series.pie.radius ? e.series.pie.radius: z * e.series.pie.radius;
							if (! (t >= r.width / 2 - 5 || t * e.series.pie.tilt >= r.height / 2 - 15 || 10 >= t)) {
								ctx.save();
								ctx.translate(5, 15);
								ctx.globalAlpha = 0.02;
								ctx.fillStyle = "#000";
								ctx.translate(q, F);
								ctx.scale(1, e.series.pie.tilt);
								for (var A = 1; 10 >= A; A++) ctx.beginPath(),
								ctx.arc(0, 0, t, 0, 2 * Math.PI, !1),
								ctx.fill(),
								t -= A;
								ctx.restore()
							}
						}
						g()
					}
					k >= V && (d(), p.prepend('<div class="error">Could not draw pie with labels contained inside canvas</div>'));
					b.setSeries && b.insertLegend && (b.setSeries(j), b.insertLegend())
				}
			}
			function n(a) {
				0 < e.series.pie.innerRadius && (a.save(), innerRadius = 1 < e.series.pie.innerRadius ? e.series.pie.innerRadius: z * e.series.pie.innerRadius, a.globalCompositeOperation = "destination-out", a.beginPath(), a.fillStyle = e.series.pie.stroke.color, a.arc(0, 0, innerRadius, 0, 2 * Math.PI, !1), a.fill(), a.closePath(), a.restore(), a.save(), a.beginPath(), a.strokeStyle = e.series.pie.stroke.color, a.arc(0, 0, innerRadius, 0, 2 * Math.PI, !1), a.stroke(), a.closePath(), a.restore())
			}
			function j(a) {
				t("plothover", a)
			}
			function k(a) {
				t("plotclick", a)
			}
			function t(a, c) {
				var d = b.offset(),
				g = parseInt(c.pageX - d.left);
				a: {
					for (var d = parseInt(c.pageY - d.top), j = b.getData(), n = b.getOptions(), n = 1 < n.series.pie.radius ? n.series.pie.radius: z * n.series.pie.radius, k = 0; k < j.length; ++k) {
						var r = j[k];
						if (r.pie.show) {
							ctx.save();
							ctx.beginPath();
							ctx.moveTo(0, 0);
							ctx.arc(0, 0, n, r.startAngle, r.startAngle + r.angle, !1);
							ctx.closePath();
							x = g - q;
							y = d - F;
							if (ctx.isPointInPath) {
								if (ctx.isPointInPath(g - q, d - F)) {
									ctx.restore();
									g = {
										datapoint: [r.percent, r.data],
										dataIndex: 0,
										series: r,
										seriesIndex: k
									};
									break a
								}
							} else {
								p1X = n * Math.cos(r.startAngle);
								p1Y = n * Math.sin(r.startAngle);
								p2X = n * Math.cos(r.startAngle + r.angle / 4);
								p2Y = n * Math.sin(r.startAngle + r.angle / 4);
								p3X = n * Math.cos(r.startAngle + r.angle / 2);
								p3Y = n * Math.sin(r.startAngle + r.angle / 2);
								p4X = n * Math.cos(r.startAngle + r.angle / 1.5);
								p4Y = n * Math.sin(r.startAngle + r.angle / 1.5);
								p5X = n * Math.cos(r.startAngle + r.angle);
								p5Y = n * Math.sin(r.startAngle + r.angle);
								arrPoly = [[0, 0], [p1X, p1Y], [p2X, p2Y], [p3X, p3Y], [p4X, p4Y], [p5X, p5Y]];
								arrPoint = [x, y];
								for (var t = !1,
								B = -1,
								D = arrPoly.length,
								E = D - 1; ++B < D; E = B)(arrPoly[B][1] <= arrPoint[1] && arrPoint[1] < arrPoly[E][1] || arrPoly[E][1] <= arrPoint[1] && arrPoint[1] < arrPoly[B][1]) && arrPoint[0] < (arrPoly[E][0] - arrPoly[B][0]) * (arrPoint[1] - arrPoly[B][1]) / (arrPoly[E][1] - arrPoly[B][1]) + arrPoly[B][0] && (t = !t);
								if (t) {
									ctx.restore();
									g = {
										datapoint: [r.percent, r.data],
										dataIndex: 0,
										series: r,
										seriesIndex: k
									};
									break a
								}
							}
							ctx.restore()
						}
					}
					g = null
				}
				if (e.grid.autoHighlight) for (d = 0; d < S.length; ++d) j = S[d],
				j.auto == a && !(g && j.series == g.series) && (j = j.series, null == j && (S = [], b.triggerRedrawOverlay()), "number" == typeof j && (j = series[j]), j = A(j), -1 != j && (S.splice(j, 1), b.triggerRedrawOverlay()));
				g && (d = g.series, "number" == typeof d && (d = series[d]), j = A(d), -1 == j ? (S.push({
					series: d,
					auto: a
				}), b.triggerRedrawOverlay()) : a || (S[j].auto = !1));
				p.trigger(a, [{
					pageX: c.pageX,
					pageY: c.pageY
				},
				g])
			}
			function A(a) {
				for (var b = 0; b < S.length; ++b) if (S[b].series == a) return b;
				return - 1
			}
			function B(a, b) {
				var c = a.getOptions(),
				d = 1 < c.series.pie.radius ? c.series.pie.radius: z * c.series.pie.radius;
				b.save();
				b.translate(q, F);
				b.scale(1, c.series.pie.tilt);
				for (i = 0; i < S.length; ++i) {
					var g = S[i].series;
					0 > g.angle || (b.fillStyle = "rgba(255, 255, 255, " + c.series.pie.highlight.opacity + ")", b.beginPath(), 1E-9 < Math.abs(g.angle - 2 * Math.PI) && b.moveTo(0, 0), b.arc(0, 0, d, g.startAngle, g.startAngle + g.angle, !1), b.closePath(), b.fill())
				}
				n(b);
				b.restore()
			}
			var r = null,
			p = null,
			z = null,
			q = null,
			F = null,
			D = 0,
			E = !0,
			V = 10,
			ha = 0.95,
			W = 0,
			sa = !1,
			S = [];
			b.hooks.processOptions.push(function(a, b) {
				b.series.pie.show && (b.grid.show = !1, "auto" == b.series.pie.label.show && (b.series.pie.label.show = b.legend.show ? !1 : !0), "auto" == b.series.pie.radius && (b.series.pie.radius = b.series.pie.label.show ? 0.75 : 1), 1 < b.series.pie.tilt && (b.series.pie.tilt = 1), 0 > b.series.pie.tilt && (b.series.pie.tilt = 0), a.hooks.processDatapoints.push(c), a.hooks.drawOverlay.push(B), a.hooks.draw.push(d))
			});
			b.hooks.bindEvents.push(function(a, b) {
				var c = a.getOptions();
				c.series.pie.show && c.grid.hoverable && b.unbind("mousemove").mousemove(j);
				c.series.pie.show && c.grid.clickable && b.unbind("click").click(k)
			})
		},
		options: e,
		name: "pie",
		version: "1.0"
	})
})(jQuery); (function(a) {
	a.plot.plugins.push({
		init: function(a) {
			a.hooks.processDatapoints.push(function(a, c, g) {
				if (null != c.stack) {
					var d;
					a = a.getData();
					for (var e = null,
					j = 0; j < a.length && c != a[j]; ++j) a[j].stack == c.stack && (e = a[j]);
					if (d = e) {
						a = g.pointsize;
						e = g.points;
						j = d.datapoints.pointsize;
						d = d.datapoints.points;
						var k = [],
						t,
						A,
						B,
						r,
						p,
						z,
						q = c.lines.show;
						r = c.bars.horizontal;
						var F = 2 < a && (r ? g.format[2].x: g.format[2].y);
						c = q && c.lines.steps;
						B = !0;
						for (var D = r ? 1 : 0, E = r ? 0 : 1, V = 0, ha = 0, W; ! (V >= e.length);) {
							W = k.length;
							if (null == e[V]) {
								for (m = 0; m < a; ++m) k.push(e[V + m]);
								V += a
							} else if (ha >= d.length) {
								if (!q) for (m = 0; m < a; ++m) k.push(e[V + m]);
								V += a
							} else if (null == d[ha]) {
								for (m = 0; m < a; ++m) k.push(null);
								B = !0;
								ha += j
							} else {
								t = e[V + D];
								A = e[V + E];
								r = d[ha + D];
								p = d[ha + E];
								z = 0;
								if (t == r) {
									for (m = 0; m < a; ++m) k.push(e[V + m]);
									k[W + E] += p;
									z = p;
									V += a;
									ha += j
								} else if (t > r) {
									if (q && 0 < V && null != e[V - a]) {
										B = A + (e[V - a + E] - A) * (r - t) / (e[V - a + D] - t);
										k.push(r);
										k.push(B + p);
										for (m = 2; m < a; ++m) k.push(e[V + m]);
										z = p
									}
									ha += j
								} else {
									if (B && q) {
										V += a;
										continue
									}
									for (m = 0; m < a; ++m) k.push(e[V + m]);
									q && (0 < ha && null != d[ha - j]) && (z = p + (d[ha - j + E] - p) * (t - r) / (d[ha - j + D] - r));
									k[W + E] += z;
									V += a
								}
								B = !1;
								W != k.length && F && (k[W + 2] += z)
							}
							if (c && W != k.length && 0 < W && null != k[W] && k[W] != k[W - a] && k[W + 1] != k[W - a + 1]) {
								for (m = 0; m < a; ++m) k[W + a + m] = k[W + m];
								k[W + 1] = k[W - a + 1]
							}
						}
						g.points = k
					}
				}
			})
		},
		options: {
			series: {
				stack: null
			}
		},
		name: "stack",
		version: "1.2"
	})
})(jQuery); (function(a) {
	a.plot.plugins.push({
		init: function(a) {
			function b(a, b) {
				var c = a.bars.order,
				d = b.bars.order;
				return c < d ? -1 : c > d ? 1 : 0
			}
			function c(a, b, c) {
				for (var d = 0; b <= c; b++) d += a[b].bars.barWidth + 2 * j;
				return d
			}
			var g, d, n, j, k = 1;
			a.hooks.processDatapoints.push(function(a, e, B) {
				var r = null;
				if (null != e.bars && e.bars.show && null != e.bars.order) {
					for (var p = a.getPlaceholder().innerWidth(), z = a.getData(), q = [], F = 0; F < z.length; F++) q[0] = z[F].data[0][0],
					q[1] = z[F].data[z[F].data.length - 1][0];
					k = (q[1] - q[0]) / p;
					a = a.getData();
					p = [];
					for (z = 0; z < a.length; z++) null != a[z].bars.order && a[z].bars.show && p.push(a[z]);
					g = p.sort(b);
					d = g.length;
					n = e.bars.lineWidth ? e.bars.lineWidth: 2;
					j = n * k;
					if (2 <= d) {
						for (a = r = 0; a < g.length; ++a) if (e == g[a]) {
							r = a;
							break
						}
						r += 1;
						a = a = 0;
						0 != d % 2 && (a = g[Math.ceil(d / 2)].bars.barWidth / 2);
						r = a = r <= Math.ceil(d / 2) ? -1 * c(g, r - 1, Math.floor(d / 2) - 1) - a: c(g, Math.ceil(d / 2), r - 2) + a;
						a = B.pointsize;
						p = B.points;
						for (q = z = 0; q < p.length; q += a) p[q] += r,
						e.data[z][3] = p[q],
						z++;
						r = p;
						B.points = r
					}
				}
				return r
			})
		},
		options: {
			series: {
				bars: {
					order: null
				}
			}
		},
		name: "orderBars",
		version: "0.2"
	})
})(jQuery);
document.createElement("canvas").getContext ||
function() {
	function a() {
		return this.context_ || (this.context_ = new z(this))
	}
	function e(a, b, c) {
		var d = ua.call(arguments, 2);
		return function() {
			return a.apply(b, d.concat(ua.call(arguments)))
		}
	}
	function b(a) {
		return String(a).replace(/&/g, "&amp;").replace(/"/g, "&quot;")
	}
	function c(a) {
		a.namespaces.g_vml_ || a.namespaces.add("g_vml_", "urn:schemas-microsoft-com:vml", "#default#VML");
		a.namespaces.g_o_ || a.namespaces.add("g_o_", "urn:schemas-microsoft-com:office:office", "#default#VML");
		a.styleSheets.ex_canvas_ || (a = a.createStyleSheet(), a.owningElement.id = "ex_canvas_", a.cssText = "canvas{display:inline-block;overflow:hidden;text-align:left;width:300px;height:150px}")
	}
	function g(a) {
		var b = a.srcElement;
		switch (a.propertyName) {
		case "width":
			b.getContext().clearRect();
			b.style.width = b.attributes.width.nodeValue + "px";
			b.firstChild.style.width = b.clientWidth + "px";
			break;
		case "height":
			b.getContext().clearRect(),
			b.style.height = b.attributes.height.nodeValue + "px",
			b.firstChild.style.height = b.clientHeight + "px"
		}
	}
	function d(a) {
		a = a.srcElement;
		a.firstChild && (a.firstChild.style.width = a.clientWidth + "px", a.firstChild.style.height = a.clientHeight + "px")
	}
	function n() {
		return [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
	}
	function j(a, b) {
		for (var c = n(), d = 0; 3 > d; d++) for (var g = 0; 3 > g; g++) {
			for (var e = 0,
			j = 0; 3 > j; j++) e += a[d][j] * b[j][g];
			c[d][g] = e
		}
		return c
	}
	function k(a, b) {
		b.fillStyle = a.fillStyle;
		b.lineCap = a.lineCap;
		b.lineJoin = a.lineJoin;
		b.lineWidth = a.lineWidth;
		b.miterLimit = a.miterLimit;
		b.shadowBlur = a.shadowBlur;
		b.shadowColor = a.shadowColor;
		b.shadowOffsetX = a.shadowOffsetX;
		b.shadowOffsetY = a.shadowOffsetY;
		b.strokeStyle = a.strokeStyle;
		b.globalAlpha = a.globalAlpha;
		b.font = a.font;
		b.textAlign = a.textAlign;
		b.textBaseline = a.textBaseline;
		b.arcScaleX_ = a.arcScaleX_;
		b.arcScaleY_ = a.arcScaleY_;
		b.lineScale_ = a.lineScale_
	}
	function t(a) {
		var b = a.indexOf("(", 3),
		c = a.indexOf(")", b + 1),
		b = a.substring(b + 1, c).split(",");
		4 == b.length && "a" == a.substr(3, 1) ? alpha = Number(b[3]) : b[3] = 1;
		return b
	}
	function A(a, b, c) {
		return Math.min(c, Math.max(b, a))
	}
	function B(a, b, c) {
		0 > c && c++;
		1 < c && c--;
		return 1 > 6 * c ? a + 6 * (b - a) * c: 1 > 2 * c ? b: 2 > 3 * c ? a + 6 * (b - a) * (2 / 3 - c) : a
	}
	function r(a) {
		var b = 1;
		a = String(a);
		if ("#" != a.charAt(0)) if (/^rgb/.test(a)) {
			b = t(a);
			a = "#";
			for (var c, d = 0; 3 > d; d++) c = -1 != b[d].indexOf("%") ? Math.floor(255 * (parseFloat(b[d]) / 100)) : Number(b[d]),
			a += va[A(c, 0, 255)];
			b = b[3]
		} else if (/^hsl/.test(a)) {
			a = b = t(a);
			h = parseFloat(a[0]) / 360 % 360;
			0 > h && h++;
			s = A(parseFloat(a[1]) / 100, 0, 1);
			l = A(parseFloat(a[2]) / 100, 0, 1);
			if (0 == s) a = c = d = l;
			else {
				var d = 0.5 > l ? l * (1 + s) : l + s - l * s,
				g = 2 * l - d;
				a = B(g, d, h + 1 / 3);
				c = B(g, d, h);
				d = B(g, d, h - 1 / 3)
			}
			a = "#" + va[Math.floor(255 * a)] + va[Math.floor(255 * c)] + va[Math.floor(255 * d)];
			b = b[3]
		} else a = Ba[a] || a;
		return {
			color: a,
			alpha: b
		}
	}
	function p(a) {
		switch (a) {
		case "butt":
			return "flat";
		case "round":
			return "round";
		default:
			return "square"
		}
	}
	function z(a) {
		this.m_ = n();
		this.mStack_ = [];
		this.aStack_ = [];
		this.currentPath_ = [];
		this.fillStyle = this.strokeStyle = "#000";
		this.lineWidth = 1;
		this.lineJoin = "miter";
		this.lineCap = "butt";
		this.miterLimit = 1 * Y;
		this.globalAlpha = 1;
		this.font = "10px sans-serif";
		this.textAlign = "left";
		this.textBaseline = "alphabetic";
		this.canvas = a;
		var b = a.ownerDocument.createElement("div");
		b.style.width = a.clientWidth + "px";
		b.style.height = a.clientHeight + "px";
		b.style.overflow = "hidden";
		b.style.position = "absolute";
		a.appendChild(b);
		this.element_ = b;
		this.lineScale_ = this.arcScaleY_ = this.arcScaleX_ = 1
	}
	function q(a, b, c, d) {
		a.currentPath_.push({
			type: "bezierCurveTo",
			cp1x: b.x,
			cp1y: b.y,
			cp2x: c.x,
			cp2y: c.y,
			x: d.x,
			y: d.y
		});
		a.currentX_ = d.x;
		a.currentY_ = d.y
	}
	function F(a, b) {
		var c = r(a.strokeStyle),
		d = c.color,
		c = c.alpha * a.globalAlpha,
		g = a.lineScale_ * a.lineWidth;
		1 > g && (c *= g);
		b.push("<g_vml_:stroke", ' opacity="', c, '"', ' joinstyle="', a.lineJoin, '"', ' miterlimit="', a.miterLimit, '"', ' endcap="', p(a.lineCap), '"', ' weight="', g, 'px"', ' color="', d, '" />')
	}
	function D(a, b, c, d) {
		var g = a.fillStyle,
		e = a.arcScaleX_,
		j = a.arcScaleY_,
		n = d.x - c.x,
		k = d.y - c.y;
		if (g instanceof V) {
			var q = 0,
			p = d = 0,
			t = 0,
			z = 1;
			if ("gradient" == g.type_) {
				q = g.x1_ / e;
				c = g.y1_ / j;
				var A = a.getCoords_(g.x0_ / e, g.y0_ / j),
				q = a.getCoords_(q, c),
				q = 180 * Math.atan2(q.x - A.x, q.y - A.y) / Math.PI;
				0 > q && (q += 360);
				1E-6 > q && (q = 0)
			} else A = a.getCoords_(g.x0_, g.y0_),
			d = (A.x - c.x) / n,
			p = (A.y - c.y) / k,
			n /= e * Y,
			k /= j * Y,
			z = sa.max(n, k),
			t = 2 * g.r0_ / z,
			z = 2 * g.r1_ / z - t;
			e = g.colors_;
			e.sort(function(a, b) {
				return a.offset - b.offset
			});
			j = e.length;
			A = e[0].color;
			c = e[j - 1].color;
			n = e[0].alpha * a.globalAlpha;
			a = e[j - 1].alpha * a.globalAlpha;
			for (var k = [], B = 0; B < j; B++) {
				var D = e[B];
				k.push(D.offset * z + t + " " + D.color)
			}
			b.push('<g_vml_:fill type="', g.type_, '"', ' method="none" focus="100%"', ' color="', A, '"', ' color2="', c, '"', ' colors="', k.join(","), '"', ' opacity="', a, '"', ' g_o_:opacity2="', n, '"', ' angle="', q, '"', ' focusposition="', d, ",", p, '" />')
		} else g instanceof ha ? n && k && b.push("<g_vml_:fill", ' position="', -c.x / n * e * e, ",", -c.y / k * j * j, '"', ' type="tile"', ' src="', g.src_, '" />') : (g = r(a.fillStyle), b.push('<g_vml_:fill color="', g.color, '" opacity="', g.alpha * a.globalAlpha, '" />'))
	}
	function E(a, b, c) {
		isFinite(b[0][0]) && (isFinite(b[0][1]) && isFinite(b[1][0]) && isFinite(b[1][1]) && isFinite(b[2][0]) && isFinite(b[2][1])) && (a.m_ = b, c && (a.lineScale_ = ga(ia(b[0][0] * b[1][1] - b[0][1] * b[1][0]))))
	}
	function V(a) {
		this.type_ = a;
		this.r1_ = this.y1_ = this.x1_ = this.r0_ = this.y0_ = this.x0_ = 0;
		this.colors_ = []
	}
	function ha(a, b) {
		if (!a || 1 != a.nodeType || "IMG" != a.tagName) throw new W("TYPE_MISMATCH_ERR");
		if ("complete" != a.readyState) throw new W("INVALID_STATE_ERR");
		switch (b) {
		case "repeat":
		case null:
		case "":
			this.repetition_ = "repeat";
			break;
		case "repeat-x":
		case "repeat-y":
		case "no-repeat":
			this.repetition_ = b;
			break;
		default:
			throw new W("SYNTAX_ERR");
		}
		this.src_ = a.src;
		this.width_ = a.width;
		this.height_ = a.height
	}
	function W(a) {
		this.code = this[a];
		this.message = a + ": DOM Exception " + this.code
	}
	var sa = Math,
	S = sa.round,
	U = sa.sin,
	ba = sa.cos,
	ia = sa.abs,
	ga = sa.sqrt,
	Y = 10,
	xa = Y / 2,
	ua = Array.prototype.slice;
	c(document);
	var ta = {
		init: function(a) { / MSIE / .test(navigator.userAgent) && !window.opera && (a = a || document, a.createElement("canvas"), a.attachEvent("onreadystatechange", e(this.init_, this, a)))
		},
		init_: function(a) {
			a = a.getElementsByTagName("canvas");
			for (var b = 0; b < a.length; b++) this.initElement(a[b])
		},
		initElement: function(b) {
			if (!b.getContext) {
				b.getContext = a;
				c(b.ownerDocument);
				b.innerHTML = "";
				b.attachEvent("onpropertychange", g);
				b.attachEvent("onresize", d);
				var e = b.attributes;
				e.width && e.width.specified ? b.style.width = e.width.nodeValue + "px": b.width = b.clientWidth;
				e.height && e.height.specified ? b.style.height = e.height.nodeValue + "px": b.height = b.clientHeight
			}
			return b
		}
	};
	ta.init();
	for (var va = [], N = 0; 16 > N; N++) for (var na = 0; 16 > na; na++) va[16 * N + na] = N.toString(16) + na.toString(16);
	var Ba = {
		aliceblue: "#F0F8FF",
		antiquewhite: "#FAEBD7",
		aquamarine: "#7FFFD4",
		azure: "#F0FFFF",
		beige: "#F5F5DC",
		bisque: "#FFE4C4",
		black: "#000000",
		blanchedalmond: "#FFEBCD",
		blueviolet: "#8A2BE2",
		brown: "#A52A2A",
		burlywood: "#DEB887",
		cadetblue: "#5F9EA0",
		chartreuse: "#7FFF00",
		chocolate: "#D2691E",
		coral: "#FF7F50",
		cornflowerblue: "#6495ED",
		cornsilk: "#FFF8DC",
		crimson: "#DC143C",
		cyan: "#00FFFF",
		darkblue: "#00008B",
		darkcyan: "#008B8B",
		darkgoldenrod: "#B8860B",
		darkgray: "#A9A9A9",
		darkgreen: "#006400",
		darkgrey: "#A9A9A9",
		darkkhaki: "#BDB76B",
		darkmagenta: "#8B008B",
		darkolivegreen: "#556B2F",
		darkorange: "#FF8C00",
		darkorchid: "#9932CC",
		darkred: "#8B0000",
		darksalmon: "#E9967A",
		darkseagreen: "#8FBC8F",
		darkslateblue: "#483D8B",
		darkslategray: "#2F4F4F",
		darkslategrey: "#2F4F4F",
		darkturquoise: "#00CED1",
		darkviolet: "#9400D3",
		deeppink: "#FF1493",
		deepskyblue: "#00BFFF",
		dimgray: "#696969",
		dimgrey: "#696969",
		dodgerblue: "#1E90FF",
		firebrick: "#B22222",
		floralwhite: "#FFFAF0",
		forestgreen: "#228B22",
		gainsboro: "#DCDCDC",
		ghostwhite: "#F8F8FF",
		gold: "#FFD700",
		goldenrod: "#DAA520",
		grey: "#808080",
		greenyellow: "#ADFF2F",
		honeydew: "#F0FFF0",
		hotpink: "#FF69B4",
		indianred: "#CD5C5C",
		indigo: "#4B0082",
		ivory: "#FFFFF0",
		khaki: "#F0E68C",
		lavender: "#E6E6FA",
		lavenderblush: "#FFF0F5",
		lawngreen: "#7CFC00",
		lemonchiffon: "#FFFACD",
		lightblue: "#ADD8E6",
		lightcoral: "#F08080",
		lightcyan: "#E0FFFF",
		lightgoldenrodyellow: "#FAFAD2",
		lightgreen: "#90EE90",
		lightgrey: "#D3D3D3",
		lightpink: "#FFB6C1",
		lightsalmon: "#FFA07A",
		lightseagreen: "#20B2AA",
		lightskyblue: "#87CEFA",
		lightslategray: "#778899",
		lightslategrey: "#778899",
		lightsteelblue: "#B0C4DE",
		lightyellow: "#FFFFE0",
		limegreen: "#32CD32",
		linen: "#FAF0E6",
		magenta: "#FF00FF",
		mediumaquamarine: "#66CDAA",
		mediumblue: "#0000CD",
		mediumorchid: "#BA55D3",
		mediumpurple: "#9370DB",
		mediumseagreen: "#3CB371",
		mediumslateblue: "#7B68EE",
		mediumspringgreen: "#00FA9A",
		mediumturquoise: "#48D1CC",
		mediumvioletred: "#C71585",
		midnightblue: "#191970",
		mintcream: "#F5FFFA",
		mistyrose: "#FFE4E1",
		moccasin: "#FFE4B5",
		navajowhite: "#FFDEAD",
		oldlace: "#FDF5E6",
		olivedrab: "#6B8E23",
		orange: "#FFA500",
		orangered: "#FF4500",
		orchid: "#DA70D6",
		palegoldenrod: "#EEE8AA",
		palegreen: "#98FB98",
		paleturquoise: "#AFEEEE",
		palevioletred: "#DB7093",
		papayawhip: "#FFEFD5",
		peachpuff: "#FFDAB9",
		peru: "#CD853F",
		pink: "#FFC0CB",
		plum: "#DDA0DD",
		powderblue: "#B0E0E6",
		rosybrown: "#BC8F8F",
		royalblue: "#4169E1",
		saddlebrown: "#8B4513",
		salmon: "#FA8072",
		sandybrown: "#F4A460",
		seagreen: "#2E8B57",
		seashell: "#FFF5EE",
		sienna: "#A0522D",
		skyblue: "#87CEEB",
		slateblue: "#6A5ACD",
		slategray: "#708090",
		slategrey: "#708090",
		snow: "#FFFAFA",
		springgreen: "#00FF7F",
		steelblue: "#4682B4",
		tan: "#D2B48C",
		thistle: "#D8BFD8",
		tomato: "#FF6347",
		turquoise: "#40E0D0",
		violet: "#EE82EE",
		wheat: "#F5DEB3",
		whitesmoke: "#F5F5F5",
		yellowgreen: "#9ACD32"
	},
	qa = {},
	N = z.prototype;
	N.clearRect = function() {
		this.textMeasureEl_ && (this.textMeasureEl_.removeNode(!0), this.textMeasureEl_ = null);
		this.element_.innerHTML = ""
	};
	N.beginPath = function() {
		this.currentPath_ = []
	};
	N.moveTo = function(a, b) {
		var c = this.getCoords_(a, b);
		this.currentPath_.push({
			type: "moveTo",
			x: c.x,
			y: c.y
		});
		this.currentX_ = c.x;
		this.currentY_ = c.y
	};
	N.lineTo = function(a, b) {
		var c = this.getCoords_(a, b);
		this.currentPath_.push({
			type: "lineTo",
			x: c.x,
			y: c.y
		});
		this.currentX_ = c.x;
		this.currentY_ = c.y
	};
	N.bezierCurveTo = function(a, b, c, d, g, e) {
		g = this.getCoords_(g, e);
		a = this.getCoords_(a, b);
		c = this.getCoords_(c, d);
		q(this, a, c, g)
	};
	N.quadraticCurveTo = function(a, b, c, d) {
		a = this.getCoords_(a, b);
		c = this.getCoords_(c, d);
		d = {
			x: this.currentX_ + 2 / 3 * (a.x - this.currentX_),
			y: this.currentY_ + 2 / 3 * (a.y - this.currentY_)
		};
		q(this, d, {
			x: d.x + (c.x - this.currentX_) / 3,
			y: d.y + (c.y - this.currentY_) / 3
		},
		c)
	};
	N.arc = function(a, b, c, d, g, e) {
		c *= Y;
		var j = e ? "at": "wa",
		k = a + ba(d) * c - xa,
		n = b + U(d) * c - xa;
		d = a + ba(g) * c - xa;
		g = b + U(g) * c - xa;
		k == d && !e && (k += 0.125);
		a = this.getCoords_(a, b);
		k = this.getCoords_(k, n);
		d = this.getCoords_(d, g);
		this.currentPath_.push({
			type: j,
			x: a.x,
			y: a.y,
			radius: c,
			xStart: k.x,
			yStart: k.y,
			xEnd: d.x,
			yEnd: d.y
		})
	};
	N.rect = function(a, b, c, d) {
		this.moveTo(a, b);
		this.lineTo(a + c, b);
		this.lineTo(a + c, b + d);
		this.lineTo(a, b + d);
		this.closePath()
	};
	N.strokeRect = function(a, b, c, d) {
		var g = this.currentPath_;
		this.beginPath();
		this.moveTo(a, b);
		this.lineTo(a + c, b);
		this.lineTo(a + c, b + d);
		this.lineTo(a, b + d);
		this.closePath();
		this.stroke();
		this.currentPath_ = g
	};
	N.fillRect = function(a, b, c, d) {
		var g = this.currentPath_;
		this.beginPath();
		this.moveTo(a, b);
		this.lineTo(a + c, b);
		this.lineTo(a + c, b + d);
		this.lineTo(a, b + d);
		this.closePath();
		this.fill();
		this.currentPath_ = g
	};
	N.createLinearGradient = function(a, b, c, d) {
		var g = new V("gradient");
		g.x0_ = a;
		g.y0_ = b;
		g.x1_ = c;
		g.y1_ = d;
		return g
	};
	N.createRadialGradient = function(a, b, c, d, g, e) {
		var j = new V("gradientradial");
		j.x0_ = a;
		j.y0_ = b;
		j.r0_ = c;
		j.x1_ = d;
		j.y1_ = g;
		j.r1_ = e;
		return j
	};
	N.drawImage = function(a, b) {
		var c, d, g, e, j, k, n, q;
		g = a.runtimeStyle.width;
		e = a.runtimeStyle.height;
		a.runtimeStyle.width = "auto";
		a.runtimeStyle.height = "auto";
		var p = a.width,
		r = a.height;
		a.runtimeStyle.width = g;
		a.runtimeStyle.height = e;
		if (3 == arguments.length) c = arguments[1],
		d = arguments[2],
		j = k = 0,
		n = g = p,
		q = e = r;
		else if (5 == arguments.length) c = arguments[1],
		d = arguments[2],
		g = arguments[3],
		e = arguments[4],
		j = k = 0,
		n = p,
		q = r;
		else if (9 == arguments.length) j = arguments[1],
		k = arguments[2],
		n = arguments[3],
		q = arguments[4],
		c = arguments[5],
		d = arguments[6],
		g = arguments[7],
		e = arguments[8];
		else throw Error("Invalid number of arguments");
		var t = this.getCoords_(c, d),
		z = [];
		z.push(" <g_vml_:group", ' coordsize="', 10 * Y, ",", 10 * Y, '"', ' coordorigin="0,0"', ' style="width:', 10, "px;height:", 10, "px;position:absolute;");
		if (1 != this.m_[0][0] || this.m_[0][1] || 1 != this.m_[1][1] || this.m_[1][0]) {
			var A = [];
			A.push("M11=", this.m_[0][0], ",", "M12=", this.m_[1][0], ",", "M21=", this.m_[0][1], ",", "M22=", this.m_[1][1], ",", "Dx=", S(t.x / Y), ",", "Dy=", S(t.y / Y), "");
			var B = this.getCoords_(c + g, d),
			D = this.getCoords_(c, d + e);
			c = this.getCoords_(c + g, d + e);
			t.x = sa.max(t.x, B.x, D.x, c.x);
			t.y = sa.max(t.y, B.y, D.y, c.y);
			z.push("padding:0 ", S(t.x / Y), "px ", S(t.y / Y), "px 0;filter:progid:DXImageTransform.Microsoft.Matrix(", A.join(""), ", sizingmethod='clip');")
		} else z.push("top:", S(t.y / Y), "px;left:", S(t.x / Y), "px;");
		z.push(' ">', '<g_vml_:image src="', a.src, '"', ' style="width:', Y * g, "px;", " height:", Y * e, 'px"', ' cropleft="', j / p, '"', ' croptop="', k / r, '"', ' cropright="', (p - j - n) / p, '"', ' cropbottom="', (r - k - q) / r, '"', " />", "</g_vml_:group>");
		this.element_.insertAdjacentHTML("BeforeEnd", z.join(""))
	};
	N.stroke = function(a) {
		for (var b = {
			x: null,
			y: null
		},
		c = {
			x: null,
			y: null
		},
		d = 0; d < this.currentPath_.length; d += 5E3) {
			var g = [];
			g.push("<g_vml_:shape", ' filled="', !!a, '"', ' style="position:absolute;width:', 10, "px;height:", 10, 'px;"', ' coordorigin="0,0"', ' coordsize="', 10 * Y, ",", 10 * Y, '"', ' stroked="', !a, '"', ' path="');
			for (var e = d; e < Math.min(d + 5E3, this.currentPath_.length); e++) {
				0 == e % 5E3 && 0 < e && g.push(" m ", S(this.currentPath_[e - 1].x), ",", S(this.currentPath_[e - 1].y));
				var j = this.currentPath_[e];
				switch (j.type) {
				case "moveTo":
					g.push(" m ", S(j.x), ",", S(j.y));
					break;
				case "lineTo":
					g.push(" l ", S(j.x), ",", S(j.y));
					break;
				case "close":
					g.push(" x ");
					j = null;
					break;
				case "bezierCurveTo":
					g.push(" c ", S(j.cp1x), ",", S(j.cp1y), ",", S(j.cp2x), ",", S(j.cp2y), ",", S(j.x), ",", S(j.y));
					break;
				case "at":
				case "wa":
					g.push(" ", j.type, " ", S(j.x - this.arcScaleX_ * j.radius), ",", S(j.y - this.arcScaleY_ * j.radius), " ", S(j.x + this.arcScaleX_ * j.radius), ",", S(j.y + this.arcScaleY_ * j.radius), " ", S(j.xStart), ",", S(j.yStart), " ", S(j.xEnd), ",", S(j.yEnd))
				}
				if (j) {
					if (null == b.x || j.x < b.x) b.x = j.x;
					if (null == c.x || j.x > c.x) c.x = j.x;
					if (null == b.y || j.y < b.y) b.y = j.y;
					if (null == c.y || j.y > c.y) c.y = j.y
				}
			}
			g.push(' ">');
			a ? D(this, g, b, c) : F(this, g);
			g.push("</g_vml_:shape>");
			this.element_.insertAdjacentHTML("beforeEnd", g.join(""))
		}
	};
	N.fill = function() {
		this.stroke(!0)
	};
	N.closePath = function() {
		this.currentPath_.push({
			type: "close"
		})
	};
	N.getCoords_ = function(a, b) {
		var c = this.m_;
		return {
			x: Y * (a * c[0][0] + b * c[1][0] + c[2][0]) - xa,
			y: Y * (a * c[0][1] + b * c[1][1] + c[2][1]) - xa
		}
	};
	N.save = function() {
		var a = {};
		k(this, a);
		this.aStack_.push(a);
		this.mStack_.push(this.m_);
		this.m_ = j(n(), this.m_)
	};
	N.restore = function() {
		this.aStack_.length && (k(this.aStack_.pop(), this), this.m_ = this.mStack_.pop())
	};
	N.translate = function(a, b) {
		E(this, j([[1, 0, 0], [0, 1, 0], [a, b, 1]], this.m_), !1)
	};
	N.rotate = function(a) {
		var b = ba(a);
		a = U(a);
		E(this, j([[b, a, 0], [ - a, b, 0], [0, 0, 1]], this.m_), !1)
	};
	N.scale = function(a, b) {
		this.arcScaleX_ *= a;
		this.arcScaleY_ *= b;
		E(this, j([[a, 0, 0], [0, b, 0], [0, 0, 1]], this.m_), !0)
	};
	N.transform = function(a, b, c, d, g, e) {
		E(this, j([[a, b, 0], [c, d, 0], [g, e, 1]], this.m_), !0)
	};
	N.setTransform = function(a, b, c, d, g, e) {
		E(this, [[a, b, 0], [c, d, 0], [g, e, 1]], !0)
	};
	N.drawText_ = function(a, c, d, g, e) {
		var j = this.m_;
		g = 0;
		var k = 1E3,
		n = 0,
		q = [],
		p;
		p = this.font;
		if (qa[p]) p = qa[p];
		else {
			var r = document.createElement("div").style;
			try {
				r.font = p
			} catch(t) {}
			p = qa[p] = {
				style: r.fontStyle || "normal",
				variant: r.fontVariant || "normal",
				weight: r.fontWeight || "normal",
				size: r.fontSize || 10,
				family: r.fontFamily || "sans-serif"
			}
		}
		var r = p,
		z = this.element_;
		p = {};
		for (var A in r) p[A] = r[A];
		A = parseFloat(z.currentStyle.fontSize);
		z = parseFloat(r.size);
		p.size = "number" == typeof r.size ? r.size: -1 != r.size.indexOf("px") ? z: -1 != r.size.indexOf("em") ? A * z: -1 != r.size.indexOf("%") ? A / 100 * z: -1 != r.size.indexOf("pt") ? z / 0.75 : A;
		p.size *= 0.981;
		A = p.style + " " + p.variant + " " + p.weight + " " + p.size + "px " + p.family;
		z = this.element_.currentStyle;
		r = this.textAlign.toLowerCase();
		switch (r) {
		case "left":
		case "center":
		case "right":
			break;
		case "end":
			r = "ltr" == z.direction ? "right": "left";
			break;
		case "start":
			r = "rtl" == z.direction ? "right": "left";
			break;
		default:
			r = "left"
		}
		switch (this.textBaseline) {
		case "hanging":
		case "top":
			n = p.size / 1.75;
			break;
		case "middle":
			break;
		default:
		case null:
		case "alphabetic":
		case "ideographic":
		case "bottom":
			n = -p.size / 2.25
		}
		switch (r) {
		case "right":
			g = 1E3;
			k = 0.05;
			break;
		case "center":
			g = k = 500
		}
		c = this.getCoords_(c + 0, d + n);
		q.push('<g_vml_:line from="', -g, ' 0" to="', k, ' 0.05" ', ' coordsize="100 100" coordorigin="0 0"', ' filled="', !e, '" stroked="', !!e, '" style="position:absolute;width:1px;height:1px;">');
		e ? F(this, q) : D(this, q, {
			x: -g,
			y: 0
		},
		{
			x: k,
			y: p.size
		});
		e = j[0][0].toFixed(3) + "," + j[1][0].toFixed(3) + "," + j[0][1].toFixed(3) + "," + j[1][1].toFixed(3) + ",0,0";
		c = S(c.x / Y) + "," + S(c.y / Y);
		q.push('<g_vml_:skew on="t" matrix="', e, '" ', ' offset="', c, '" origin="', g, ' 0" />', '<g_vml_:path textpathok="true" />', '<g_vml_:textpath on="true" string="', b(a), '" style="v-text-align:', r, ";font:", b(A), '" /></g_vml_:line>');
		this.element_.insertAdjacentHTML("beforeEnd", q.join(""))
	};
	N.fillText = function(a, b, c, d) {
		this.drawText_(a, b, c, d, !1)
	};
	N.strokeText = function(a, b, c, d) {
		this.drawText_(a, b, c, d, !0)
	};
	N.measureText = function(a) {
		this.textMeasureEl_ || (this.element_.insertAdjacentHTML("beforeEnd", '<span style="position:absolute;top:-20000px;left:0;padding:0;margin:0;border:none;white-space:pre;"></span>'), this.textMeasureEl_ = this.element_.lastChild);
		var b = this.element_.ownerDocument;
		this.textMeasureEl_.innerHTML = "";
		this.textMeasureEl_.style.font = this.font;
		this.textMeasureEl_.appendChild(b.createTextNode(a));
		return {
			width: this.textMeasureEl_.offsetWidth
		}
	};
	N.clip = function() {};
	N.arcTo = function() {};
	N.createPattern = function(a, b) {
		return new ha(a, b)
	};
	V.prototype.addColorStop = function(a, b) {
		b = r(b);
		this.colors_.push({
			offset: a,
			color: b.color,
			alpha: b.alpha
		})
	};
	N = W.prototype = Error();
	N.INDEX_SIZE_ERR = 1;
	N.DOMSTRING_SIZE_ERR = 2;
	N.HIERARCHY_REQUEST_ERR = 3;
	N.WRONG_DOCUMENT_ERR = 4;
	N.INVALID_CHARACTER_ERR = 5;
	N.NO_DATA_ALLOWED_ERR = 6;
	N.NO_MODIFICATION_ALLOWED_ERR = 7;
	N.NOT_FOUND_ERR = 8;
	N.NOT_SUPPORTED_ERR = 9;
	N.INUSE_ATTRIBUTE_ERR = 10;
	N.INVALID_STATE_ERR = 11;
	N.SYNTAX_ERR = 12;
	N.INVALID_MODIFICATION_ERR = 13;
	N.NAMESPACE_ERR = 14;
	N.INVALID_ACCESS_ERR = 15;
	N.VALIDATION_ERR = 16;
	N.TYPE_MISMATCH_ERR = 17;
	G_vmlCanvasManager = ta;
	CanvasRenderingContext2D = z;
	CanvasGradient = V;
	CanvasPattern = ha;
	DOMException = W
} (); (function(a) {
	elFinder = function(e, b) {
		function c(a, b) {
			a && d.view.win.width(a);
			b && d.view.nav.add(d.view.cwd).height(b)
		}
		function g() {
			c(null, d.dialog.height() - d.view.tlb.parent().height() - (a.browser.msie ? 47 : 32))
		}
		var d = this,
		n;
		this.log = function(a) {
			window.console && window.console.log && window.console.log(a)
		};
		this.options = a.extend({},
		this.options, b || {});
		if (this.options.url) {
			this.id = "";
			this.id = (n = a(e).attr("id")) ? n: "el-finder-" + Math.random().toString().substring(2);
			this.version = "1.1 RC3";
			this.jquery = a.fn.jquery.split(".").join("");
			this.cwd = {};
			this.cdc = {};
			this.buffer = {};
			this.selected = [];
			this.history = [];
			this.locked = !1;
			this.zIndex = 2;
			this.dialog = null;
			this.anchor = this.options.docked ? a("<div/>").hide().insertBefore(e) : null;
			this.params = {
				dotFiles: !1,
				arc: "",
				uplMaxSize: ""
			};
			this.vCookie = "el-finder-view-" + this.id;
			this.pCookie = "el-finder-places-" + this.id;
			this.lCookie = "el-finder-last-" + this.id;
			this.view = new this.view(this, e);
			this.ui = new this.ui(this);
			this.eventsManager = new this.eventsManager(this);
			this.quickLook = new this.quickLook(this);
			this.cookie = function(b, c) {
				if ("undefined" == typeof c) {
					if (document.cookie && "" != document.cookie) {
						var d, g = document.cookie.split(";");
						b += "=";
						for (d = 0; d < g.length; d++) if (g[d] = a.trim(g[d]), g[d].substring(0, b.length) == b) return decodeURIComponent(g[d].substring(b.length))
					}
					return ""
				}
				g = a.extend({},
				this.options.cookie);
				null === c && (c = "", g.expires = -1);
				"number" == typeof g.expires && (d = new Date, d.setTime(d.getTime() + 864E5 * g.expires), g.expires = d);
				document.cookie = b + "=" + encodeURIComponent(c) + "; expires=" + g.expires.toUTCString() + (g.path ? "; path=" + g.path: "") + (g.domain ? "; domain=" + g.domain: "") + (g.secure ? "; secure": "")
			};
			this.lock = function(a) {
				this.view.spinner(this.locked = a || !1);
				this.eventsManager.lock = this.locked
			};
			this.lockShortcuts = function(a) {
				this.eventsManager.lock = a
			};
			this.setView = function(a) {
				if ("list" == a || "icons" == a) this.options.view = a,
				this.cookie(this.vCookie, a)
			};
			this.ajax = function(b, c, g) {
				var e = {
					url: this.options.url,
					async: !0,
					type: "GET",
					data: b,
					dataType: "json",
					cache: !1,
					lock: !0,
					force: !1,
					silent: !1
				};
				"object" == typeof g && (e = a.extend({},
				e, g));
				e.silent || (e.error = d.view.fatal);
				e.success = function(a) {
					e.lock && d.lock();
					a.debug && d.log(a.debug);
					if (a.error && (!e.silent && d.view.error(a.error, a.errorData), !e.force)) return;
					c(a);
					delete a
				};
				e.lock && this.lock(!0);
				a.ajax(e)
			};
			this.tmb = function() {
				this.ajax({
					cmd: "tmb",
					current: d.cwd.hash
				},
				function(b) {
					if ("icons" == d.options.view && b.images && b.current == d.cwd.hash) {
						for (var c in b.images) d.cdc[c] && (d.cdc[c].tmb = b.images[c], a('div[key="' + c + '"]>p', d.view.cwd).css("background", ' url("' + b.images[c] + '") 0 0 no-repeat'));
						b.tmb && d.tmb()
					}
				},
				{
					lock: !1,
					silent: !0
				})
			};
			this.getPlaces = function() {
				var a = [],
				b = this.cookie(this.pCookie);
				b.length && ( - 1 != b.indexOf(":") ? a = b.split(":") : a.push(b));
				return a
			};
			this.addPlace = function(b) {
				var c = this.getPlaces();
				if ( - 1 == a.inArray(b, c)) return c.push(b),
				this.savePlaces(c),
				!0
			};
			this.removePlace = function(b) {
				var c = this.getPlaces();
				if ( - 1 != a.inArray(b, c)) return this.savePlaces(a.map(c,
				function(a) {
					return a == b ? null: a
				})),
				!0
			};
			this.savePlaces = function(a) {
				this.cookie(this.pCookie, a.join(":"))
			};
			this.reload = function(a) {
				var b;
				this.cwd = a.cwd;
				this.cdc = {};
				for (b = 0; b < a.cdc.length; b++) this.cdc[a.cdc[b].hash] = a.cdc[b],
				this.cwd.size += a.cdc[b].size;
				a.tree && (this.view.renderNav(a.tree), this.eventsManager.updateNav());
				this.updateCwd();
				a.tmb && (!d.locked && "icons" == d.options.view) && d.tmb();
				if (a.select && a.select.length) for (b = a.select.length; b--;) this.cdc[a.select[b]] && this.selectById(a.select[b]);
				this.lastDir(this.cwd.hash);
				0 < this.options.autoReload && (this.iID && clearInterval(this.iID), this.iID = setInterval(function() { ! d.locked && d.ui.exec("reload")
				},
				6E4 * this.options.autoReload))
			};
			this.updateCwd = function() {
				this.lockShortcuts();
				this.selected = [];
				this.view.renderCwd();
				this.eventsManager.updateCwd();
				this.view.tree.find('a[key="' + this.cwd.hash + '"]').trigger("select")
			};
			this.drop = function(b, c, g) {
				if (c.helper.find('[key="' + g + '"]').length) return d.view.error("Unable to copy into itself");
				var e = [];
				c.helper.find('div:not(.noaccess):has(>label):not(:has(em[class="readonly"],em[class=""]))').each(function() {
					e.push(a(this).hide().attr("key"))
				});
				c.helper.find("div:has(>label):visible").length || c.helper.hide();
				e.length ? (d.setBuffer(e, b.shiftKey ? 0 : 1, g), d.buffer.files && setTimeout(function() {
					d.ui.exec("paste");
					d.buffer = {}
				},
				300)) : a(this).removeClass("el-finder-droppable")
			};
			this.getSelected = function(a) {
				var b = [];
				if (0 <= a) return this.cdc[this.selected[a]] || {};
				for (a = 0; a < this.selected.length; a++) this.cdc[this.selected[a]] && b.push(this.cdc[this.selected[a]]);
				return b
			};
			this.select = function(b, c) {
				c && a(".ui-selected", d.view.cwd).removeClass("ui-selected");
				b.addClass("ui-selected");
				d.updateSelect()
			};
			this.selectById = function(b) {
				b = a('[key="' + b + '"]', this.view.cwd);
				b.length && (this.select(b), this.checkSelectedPos())
			};
			this.unselect = function(a) {
				a.removeClass("ui-selected");
				d.updateSelect()
			};
			this.toggleSelect = function(a) {
				a.toggleClass("ui-selected");
				this.updateSelect()
			};
			this.selectAll = function() {
				a("[key]", d.view.cwd).addClass("ui-selected");
				d.updateSelect()
			};
			this.unselectAll = function() {
				a(".ui-selected", d.view.cwd).removeClass("ui-selected");
				d.updateSelect()
			};
			this.updateSelect = function() {
				d.selected = [];
				a(".ui-selected", d.view.cwd).each(function() {
					d.selected.push(a(this).attr("key"))
				});
				d.view.selectedInfo();
				d.ui.update();
				d.quickLook.update()
			};
			this.checkSelectedPos = function(a) {
				var b = d.view.cwd.find(".ui-selected:" + (a ? "last": "first")).eq(0);
				a = b.position();
				var b = b.outerHeight(),
				c = d.view.cwd.height();
				0 > a.top ? d.view.cwd.scrollTop(a.top + d.view.cwd.scrollTop() - 2) : c - a.top < b && d.view.cwd.scrollTop(a.top + b - c + d.view.cwd.scrollTop())
			};
			this.setBuffer = function(a, b, c) {
				this.buffer = {
					src: this.cwd.hash,
					dst: c,
					files: [],
					names: [],
					cut: b || 0
				};
				for (b = 0; b < a.length; b++) if (c = a[b], (c = this.cdc[c]) && c.read && "link" != c.type) this.buffer.files.push(c.hash),
				this.buffer.names.push(c.name);
				this.buffer.files.length || (this.buffer = {})
			};
			this.isValidName = function(a) {
				return ! this.cwd.dotFiles && 0 == a.indexOf(".") ? !1 : a.match(/^[^\\\/\<\>:]+$/)
			};
			this.fileExists = function(a) {
				for (var b in this.cdc) if (this.cdc[b].name == a) return b;
				return ! 1
			};
			this.uniqueName = function(a, b) {
				var c = a = d.i18n(a),
				g = 0;
				b = b || "";
				if (!this.fileExists(c + b)) return c + b;
				for (; 100 > g++;) if (!this.fileExists(c + g + b)) return c + g + b;
				return c.replace("100", "") + Math.random() + b
			};
			this.lastDir = function(a) {
				if (this.options.rememberLastDir) return a ? this.cookie(this.lCookie, a) : this.cookie(this.lCookie)
			};
			this.time = function() {
				return (new Date).getMilliseconds()
			};
			this.setView(this.cookie(this.vCookie));
			c(d.options.width, d.options.height);
			if (this.options.dialog || this.options.docked) this.options.dialog = a.extend({
				width: 570,
				dialogClass: "",
				minWidth: 480,
				minHeight: 330
			},
			this.options.dialog || {}),
			this.options.dialog.dialogClass += "el-finder-dialog",
			this.options.dialog.resize = g,
			this.options.docked ? (this.options.dialog.close = function() {
				d.dock()
			},
			this.view.win.data("size", {
				width: this.view.win.width(),
				height: this.view.nav.height()
			})) : this.dialog = a("<div/>").append(this.view.win).dialog(this.options.dialog);
			this.ajax({
				cmd: "open",
				target: this.lastDir() || "",
				init: !0,
				tree: !0
			},
			function(b) {
				b.cwd && (d.eventsManager.init(), d.reload(b), d.params = b.params, a("*", document.body).each(function() {
					var b = parseInt(a(this).css("z-index"));
					b >= d.zIndex && (d.zIndex = b + 1)
				}), d.ui.init(b.disabled))
			},
			{
				force: !0
			});
			this.open = function() {
				this.dialog ? this.dialog.dialog("open") : this.view.win.show();
				this.eventsManager.lock = !1
			};
			this.close = function() {
				this.options.docked && this.view.win.attr("undocked") ? this.dock() : this.dialog ? this.dialog.dialog("close") : this.view.win.hide();
				this.eventsManager.lock = !0
			};
			this.dock = function() {
				if (this.options.docked && this.view.win.attr("undocked")) {
					var a = this.view.win.data("size");
					this.view.win.insertAfter(this.anchor).removeAttr("undocked");
					c(a.width, a.height);
					this.dialog.dialog("destroy");
					this.dialog = null
				}
			};
			this.undock = function() {
				this.options.docked && !this.view.win.attr("undocked") && (this.dialog = a("<div/>").append(this.view.win.css("width", "100%").attr("undocked", !0).show()).dialog(this.options.dialog), g())
			}
		} else alert("Invalid configuration! You have to set URL option.")
	};
	elFinder.prototype.i18n = function(a) {
		return this.options.i18n[this.options.lang] && this.options.i18n[this.options.lang][a] ? this.options.i18n[this.options.lang][a] : a
	};
	elFinder.prototype.options = {
		url: "",
		lang: "en",
		cssClass: "",
		wrap: 14,
		places: "Places",
		placesFirst: !0,
		editorCallback: null,
		cutURL: "",
		closeOnEditorCallback: !0,
		i18n: {},
		view: "icons",
		width: "",
		height: "",
		disableShortcuts: !1,
		rememberLastDir: !0,
		cookie: {
			expires: 30,
			domain: "",
			path: "/",
			secure: !1
		},
		toolbar: [["back", "reload"], ["select", "open"], ["mkdir", "mkfile", "upload"], ["copy", "paste", "rm"], ["rename", "edit"], ["info", "quicklook"], ["icons", "list"], ["help"]],
		contextmenu: {
			cwd: "reload delim mkdir mkfile upload delim paste delim info".split(" "),
			file: "select open quicklook delim copy cut rm delim duplicate rename edit resize archive extract delim info".split(" "),
			group: "copy cut rm delim archive extract delim info".split(" ")
		},
		dialog: null,
		docked: !1,
		autoReload: 0
	};
	a.fn.elfinder = function(a) {
		return this.each(function() {
			var b = "string" == typeof a ? a: "";
			this.elfinder || (this.elfinder = new elFinder(this, "object" == typeof a ? a: {}));
			switch (b) {
			case "close":
			case "hide":
				this.elfinder.close();
				break;
			case "open":
			case "show":
				this.elfinder.open();
				break;
			case "dock":
				this.elfinder.dock();
				break;
			case "undock":
				this.elfinder.undock()
			}
		})
	}
})(jQuery); (function(a) {
	elFinder.prototype.view = function(e, b) {
		var c = this;
		this.fm = e;
		this.kinds = {
			unknown: "Unknown",
			directory: "Folder",
			symlink: "Alias",
			"symlink-broken": "Broken alias",
			"application/x-empty": "Plain text",
			"application/postscript": "Postscript document",
			"application/octet-stream": "Application",
			"application/vnd.ms-office": "Microsoft Office document",
			"application/vnd.ms-word": "Microsoft Word document",
			"application/vnd.ms-excel": "Microsoft Excel document",
			"application/vnd.ms-powerpoint": "Microsoft Powerpoint presentation",
			"application/pdf": "Portable Document Format (PDF)",
			"application/vnd.oasis.opendocument.text": "Open Office document",
			"application/x-shockwave-flash": "Flash application",
			"application/xml": "XML document",
			"application/x-bittorrent": "Bittorrent file",
			"application/x-7z-compressed": "7z archive",
			"application/x-tar": "TAR archive",
			"application/x-gzip": "GZIP archive",
			"application/x-bzip2": "BZIP archive",
			"application/zip": "ZIP archive",
			"application/x-rar": "RAR archive",
			"application/javascript": "Javascript application",
			"text/plain": "Plain text",
			"text/x-php": "PHP source",
			"text/html": "HTML document",
			"text/javascript": "Javascript source",
			"text/css": "CSS style sheet",
			"text/rtf": "Rich Text Format (RTF)",
			"text/rtfd": "RTF with attachments (RTFD)",
			"text/x-c": "C source",
			"text/x-c++": "C++ source",
			"text/x-shellscript": "Unix shell script",
			"text/x-python": "Python source",
			"text/x-java": "Java source",
			"text/x-ruby": "Ruby source",
			"text/x-perl": "Perl script",
			"text/xml": "XML document",
			"image/x-ms-bmp": "BMP image",
			"image/jpeg": "JPEG image",
			"image/gif": "GIF Image",
			"image/png": "PNG image",
			"image/x-targa": "TGA image",
			"image/tiff": "TIFF image",
			"image/vnd.adobe.photoshop": "Adobe Photoshop image",
			"audio/mpeg": "MPEG audio",
			"audio/midi": "MIDI audio",
			"audio/ogg": "Ogg Vorbis audio",
			"audio/mp4": "MP4 audio",
			"audio/wav": "WAV audio",
			"video/x-dv": "DV video",
			"video/mp4": "MP4 video",
			"video/mpeg": "MPEG video",
			"video/x-msvideo": "AVI video",
			"video/quicktime": "Quicktime video",
			"video/x-ms-wmv": "WM video",
			"video/x-flv": "Flash video",
			"video/x-matroska": "Matroska video"
		};
		this.tlb = a("<ul />");
		this.nav = a('<div class="el-finder-nav"/>').resizable({
			handles: "e",
			autoHide: !0,
			minWidth: 200,
			maxWidth: 500
		});
		this.cwd = a('<div class="el-finder-cwd"/>').attr("unselectable", "on");
		this.spn = a('<div class="el-finder-spinner"/>');
		this.err = a('<p class="el-finder-err"><strong/></p>').click(function() {
			a(this).hide()
		});
		this.nfo = a('<div class="el-finder-stat"/>');
		this.pth = a('<div class="el-finder-path"/>');
		this.sel = a('<div class="el-finder-sel"/>');
		this.stb = a('<div class="el-finder-statusbar"/>').append(this.pth).append(this.nfo).append(this.sel);
		this.wrz = a('<div class="el-finder-workzone" />').append(this.nav).append(this.cwd).append(this.spn).append(this.err).append('<div style="clear:both" />');
		this.win = a(b).empty().attr("id", this.fm.id).addClass("el-finder " + (e.options.cssClass || "")).append(a('<div class="el-finder-toolbar" />').append(this.tlb)).append(this.wrz).append(this.stb);
		this.tree = a('<ul class="el-finder-tree"></ul>').appendTo(this.nav);
		this.plc = a('<ul class="el-finder-places"><li><a href="#" class="el-finder-places-root"><div/>' + this.fm.i18n(this.fm.options.places) + "</a><ul/></li></ul>").hide();
		this.nav[this.fm.options.placesFirst ? "prepend": "append"](this.plc);
		this.spinner = function(a) {
			this.win.toggleClass("el-finder-disabled", a);
			this.spn.toggle(a)
		};
		this.fatal = function(a) {
			c.error("404" != a.status ? "Invalid backend configuration": "Unable to connect to backend")
		};
		this.error = function(a, b) {
			this.fm.lock();
			this.err.show().children("strong").html(this.fm.i18n(a) + "!" + this.formatErrorData(b));
			setTimeout(function() {
				c.err.fadeOut("slow")
			},
			4E3)
		};
		this.renderNav = function(a) {
			function b(a) {
				var c, g, e = '<ul style="display:none">';
				for (c = 0; c < a.length; c++) g = "",
				!a[c].read && !a[c].write ? g = "noaccess": a[c].read ? a[c].write || (g = "readonly") : g = "dropbox",
				e += '<li><a href="#" class="' + g + '" key="' + a[c].hash + '"><div' + (a[c].dirs.length ? ' class="collapsed"': "") + "/>" + a[c].name + "</a>",
				a[c].dirs.length && (e += b(a[c].dirs)),
				e += "</li>";
				return e + "</ul>"
			}
			var c = a.dirs.length ? b(a.dirs) : "";
			this.tree.html('<li><a href="#" class="el-finder-tree-root" key="' + a.hash + '"><div' + (c ? ' class="collapsed expanded"': "") + "/>" + a.name + "</a>" + c + "</li>");
			this.fm.options.places && this.renderPlaces()
		};
		this.renderPlaces = function() {
			var b, d, e = this.fm.getPlaces(),
			j = this.plc.show().find("ul").empty().hide();
			a("div:first", this.plc).removeClass("collapsed expanded");
			if (e.length) {
				e.sort(function(a, b) {
					var d = c.tree.find('a[key="' + a + '"]').text() || "",
					g = c.tree.find('a[key="' + b + '"]').text() || "";
					return d.localeCompare(g)
				});
				for (b = 0; b < e.length; b++)(d = this.tree.find('a[key="' + e[b] + '"]:not(.dropbox)').parent()) && d.length ? j.append(d.clone().children("ul").remove().end().find("div").removeClass("collapsed expanded").end()) : this.fm.removePlace(e[b]);
				j.children().length && a("div:first", this.plc).addClass("collapsed")
			}
		};
		this.renderCwd = function() {
			this.cwd.empty();
			var a = 0,
			b = 0,
			c = "",
			j;
			for (j in this.fm.cdc) a++,
			b += this.fm.cdc[j].size,
			c += "icons" == this.fm.options.view ? this.renderIcon(this.fm.cdc[j]) : this.renderRow(this.fm.cdc[j], a % 2);
			"icons" == this.fm.options.view ? this.cwd.append(c) : this.cwd.append('<table><tr><th colspan="2">' + this.fm.i18n("Name") + "</th><th>" + this.fm.i18n("Permissions") + "</th><th>" + this.fm.i18n("Modified") + '</th><th class="size">' + this.fm.i18n("Size") + "</th><th>" + this.fm.i18n("Kind") + "</th></tr>" + c + "</table>");
			this.pth.text(e.cwd.rel);
			this.nfo.text(e.i18n("items") + ": " + a + ", " + this.formatSize(b));
			this.sel.empty()
		};
		this.renderIcon = function(a) {
			var b = "<p" + (a.tmb ? " style=\"background:url('" + a.tmb + "') 0 0 no-repeat\"": "") + "/><label>" + this.formatName(a.name) + "</label>";
			if (a.link || "symlink-broken" == a.mime) b += "<em/>"; ! a.read && !a.write ? b += '<em class="noaccess"/>': a.read && !a.write ? b += '<em class="readonly"/>': !a.read && a.write && (b += '<em class="' + ("directory" == a.mime ? "dropbox": "noread") + '" />');
			return '<div class="' + this.mime2class(a.mime) + '" key="' + a.hash + '">' + b + "</div>"
		};
		this.renderRow = function(a, b) {
			var e = a.link || "symlink-broken" == a.mime ? "<em/>": ""; ! a.read && !a.write ? e += '<em class="noaccess"/>': a.read && !a.write ? e += '<em class="readonly"/>': !a.read && a.write && (e += '<em class="' + ("directory" == a.mime ? "dropbox": "noread") + '" />');
			return '<tr key="' + a.hash + '" class="' + c.mime2class(a.mime) + (b ? " el-finder-row-odd": "") + '"><td class="icon"><p>' + e + "</p></td><td>" + a.name + "</td><td>" + c.formatPermissions(a.read, a.write, a.rm) + "</td><td>" + c.formatDate(a.date) + '</td><td class="size">' + c.formatSize(a.size) + "</td><td>" + c.mime2kind(a.link ? "symlink": a.mime) + "</td></tr>"
		};
		this.updateFile = function(a) {
			var b = this.cwd.find('[key="' + a.hash + '"]');
			b.replaceWith("DIV" == b[0].nodeName ? this.renderIcon(a) : this.renderRow(a))
		};
		this.selectedInfo = function() {
			var a, b = 0,
			e;
			if (c.fm.selected.length) {
				e = this.fm.getSelected();
				for (a = 0; a < e.length; a++) b += e[a].size
			}
			this.sel.text(0 < a ? this.fm.i18n("selected items") + ": " + e.length + ", " + this.formatSize(b) : "")
		};
		this.formatName = function(a) {
			var b = c.fm.options.wrap;
			if (0 < b) {
				if (a.length > 2 * b) return a.substr(0, b) + "&shy;" + a.substr(b, b - 5) + "&hellip;" + a.substr(a.length - 3);
				if (a.length > b) return a.substr(0, b) + "&shy;" + a.substr(b)
			}
			return a
		};
		this.formatErrorData = function(a) {
			var b, e = "";
			if ("object" == typeof a) for (b in e = "<br />", a) e += b + " " + c.fm.i18n(a[b]) + "<br />";
			return e
		};
		this.mime2class = function(a) {
			return a.replace("/", " ").replace(/\./g, "-")
		};
		this.formatDate = function(a) {
			return a.replace(/([a-z]+)\s/i,
			function(a, b) {
				return c.fm.i18n(b) + " "
			})
		};
		this.formatSize = function(a) {
			var b = 1,
			c = "";
			1073741824 < a ? (b = 1073741824, c = "Gb") : 1048576 < a ? (b = 1048576, c = "Mb") : 1024 < a && (b = 1024, c = "Kb");
			return Math.round(a / b) + " " + c
		};
		this.formatPermissions = function(a, b, e) {
			var j = [];
			a && j.push(c.fm.i18n("read"));
			b && j.push(c.fm.i18n("write"));
			e && j.push(c.fm.i18n("remove"));
			return j.join("/")
		};
		this.mime2kind = function(a) {
			return this.fm.i18n(this.kinds[a] || "unknown")
		}
	}
})(jQuery); (function(a) {
	elFinder.prototype.ui = function(e) {
		var b = this;
		this.fm = e;
		this.cmd = {};
		this.buttons = {};
		this.menu = a('<div class="el-finder-contextmenu" />').appendTo(document.body).hide();
		this.dockButton = a('<div class="el-finder-dock-button" title="' + b.fm.i18n("Dock/undock filemanger window") + '" />');
		this.exec = function(b, g) {
			if (this.cmd[b]) {
				if ("open" != b && !this.cmd[b].isAllowed()) return this.fm.view.error("Command not allowed");
				this.fm.locked || (this.fm.quickLook.hide(), a(".el-finder-info").remove(), this.cmd[b].exec(g), this.update())
			}
		};
		this.cmdName = function(a) {
			return this.cmd[a] && this.cmd[a].name ? "archive" == a && 1 == this.fm.params.archives.length ? this.fm.i18n("Create") + " " + this.fm.view.mime2kind(this.fm.params.archives[0]).toLowerCase() : this.fm.i18n(this.cmd[a].name) : a
		};
		this.isCmdAllowed = function(a) {
			return b.cmd[a] && b.cmd[a].isAllowed()
		};
		this.execIfAllowed = function(a) {
			this.isCmdAllowed(a) && this.exec(a)
		};
		this.includeInCm = function(a, b) {
			return this.isCmdAllowed(a) && this.cmd[a].cm(b)
		};
		this.showMenu = function(c) {
			var g, d, e, j, k;
			this.hideMenu();
			g = b.fm.selected.length ? 1 == b.fm.selected.length ? "file": "group": "cwd";
			var t = b.fm.options.contextmenu[g] || [];
			for (d = 0; d < t.length; d++) if ("delim" == t[d]) b.menu.children().length && !b.menu.children(":last").hasClass("delim") && b.menu.append('<div class="delim" />');
			else if (b.fm.ui.includeInCm(t[d], g)) {
				j = b.cmd[t[d]].argc();
				k = "";
				if (j.length) {
					k = '<span/><div class="el-finder-contextmenu-sub" style="z-index:' + (parseInt(b.menu.css("z-index")) + 1) + '">';
					for (e = 0; e < j.length; e++) k += '<div name="' + t[d] + '" argc="' + j[e].argc + '" class="' + j[e]["class"] + '">' + j[e].text + "</div>";
					k += "</div>"
				}
				b.menu.append('<div class="' + t[d] + '" name="' + t[d] + '">' + k + b.cmdName(t[d]) + "</div>")
			}
			e = a(window);
			g = e.height();
			d = e.width();
			e = e.scrollTop();
			j = this.menu.width();
			k = this.menu.height();
			this.menu.css({
				left: c.clientX + j > d ? c.clientX - j: c.clientX,
				top: c.clientY + k > g && c.clientY > k ? c.clientY + e - k: c.clientY + e
			}).show().find("div[name]").hover(function() {
				var b = a(this),
				c = b.children("div"),
				d;
				b.addClass("hover");
				c.length && (c.attr("pos") || (d = b.outerWidth(), c.css(a(window).width() - d - b.offset().left > c.width() ? "left": "right", d - 5).attr("pos", !0)), c.show())
			},
			function() {
				a(this).removeClass("hover").children("div").hide()
			}).click(function(c) {
				c.stopPropagation();
				c = a(this);
				c.children("div").length || (b.hideMenu(), b.exec(c.attr("name"), c.attr("argc")))
			})
		};
		this.hideMenu = function() {
			this.menu.hide().empty()
		};
		this.update = function() {
			for (var a in this.buttons) this.buttons[a].toggleClass("disabled", !this.cmd[a].isAllowed())
		};
		this.init = function(c) {
			var g, d, e = !1,
			j = this.fm.options.toolbar;
			this.fm.options.editorCallback || c.push("select"); ! b.fm.params.archives.length && -1 == a.inArray("archive", c) && c.push("archive");
			for (g in this.commands) - 1 == a.inArray(g, c) && (this.commands[g].prototype = this.command.prototype, this.cmd[g] = new this.commands[g](this.fm));
			for (g = 0; g < j.length; g++) {
				e && this.fm.view.tlb.append('<li class="delim" />');
				e = !1;
				for (c = 0; c < j[g].length; c++) d = j[g][c],
				this.cmd[d] && (e = !0, this.buttons[d] = a('<li class="' + d + '" title="' + this.cmdName(d) + '" name="' + d + '" />').appendTo(this.fm.view.tlb).click(function(a) {
					a.stopPropagation()
				}).bind("click",
				function(b) {
					return function() { ! a(this).hasClass("disabled") && b.exec(a(this).attr("name"))
					}
				} (this)).hover(function() { ! a(this).hasClass("disabled") && a(this).addClass("el-finder-tb-hover")
				},
				function() {
					a(this).removeClass("el-finder-tb-hover")
				}))
			}
			this.update();
			this.menu.css("z-index", this.fm.zIndex);
			this.fm.options.docked && this.dockButton.hover(function() {
				a(this).addClass("el-finder-dock-button-hover")
			},
			function() {
				a(this).removeClass("el-finder-dock-button-hover")
			}).click(function() {
				b.fm.view.win.attr("undocked") ? b.fm.dock() : b.fm.undock();
				a(this).trigger("mouseout")
			}).prependTo(this.fm.view.tlb)
		}
	};
	elFinder.prototype.ui.prototype.command = function() {};
	elFinder.prototype.ui.prototype.command.prototype.isAllowed = function() {
		return ! 0
	};
	elFinder.prototype.ui.prototype.command.prototype.cm = function() {
		return ! 1
	};
	elFinder.prototype.ui.prototype.command.prototype.argc = function() {
		return []
	};
	elFinder.prototype.ui.prototype.commands = {
		back: function(a) {
			var b = this;
			this.name = "Back";
			this.fm = a;
			this.exec = function() {
				this.fm.history.length && this.fm.ajax({
					cmd: "open",
					target: this.fm.history.pop()
				},
				function(a) {
					b.fm.reload(a)
				})
			};
			this.isAllowed = function() {
				return this.fm.history.length
			}
		},
		reload: function(a) {
			var b = this;
			this.name = "Reload";
			this.fm = a;
			this.exec = function() {
				this.fm.ajax({
					cmd: "open",
					target: this.fm.cwd.hash,
					tree: !0
				},
				function(a) {
					b.fm.reload(a)
				})
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		open: function(e) {
			var b = this;
			this.name = "Open";
			this.fm = e;
			this.exec = function(c) {
				var g = null,
				g = c ? {
					hash: a(c).attr("key"),
					mime: "directory",
					read: !a(c).hasClass("noaccess") && !a(c).hasClass("dropbox")
				}: this.fm.getSelected(0);
				if (g.hash) {
					if (!g.read) return this.fm.view.error("Access denied");
					if ("link" == g.type && !g.link) return this.fm.view.error("Unable to open broken link");
					"directory" == g.mime ? (c = g.link || g.hash, b.fm.history.push(b.fm.cwd.hash), b.fm.ajax({
						cmd: "open",
						target: c
					},
					function(a) {
						b.fm.reload(a)
					})) : (c = g, g = "", c.dim && (g = c.dim.split("x"), g = "width=" + (parseInt(g[0]) + 20) + ",height=" + (parseInt(g[1]) + 20) + ","), window.open(c.url || b.fm.options.url + "?cmd=open&current=" + (c.parent || b.fm.cwd.hash) + "&target=" + (c.link || c.hash), !1, "top=50,left=50," + g + "scrollbars=yes,resizable=yes"))
				}
			};
			this.isAllowed = function() {
				return 1 == this.fm.selected.length && this.fm.getSelected(0).read
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		select: function(a) {
			this.name = "Select file";
			this.fm = a;
			this.exec = function() {
				var a = this.fm.getSelected(0);
				if (!a.url) return this.fm.view.error("File URL disabled by connector config");
				this.fm.options.editorCallback("root" == this.fm.options.cutURL ? a.url.substr(this.fm.params.url.length) : a.url.replace(RegExp("^(" + this.fm.options.cutURL + ")"), ""));
				this.fm.options.closeOnEditorCallback && (this.fm.dock(), this.fm.close())
			};
			this.isAllowed = function() {
				return 1 == this.fm.selected.length && !/(symlink\-broken|directory)/.test(this.fm.getSelected(0).mime)
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		quicklook: function(a) {
			var b = this;
			this.name = "Preview with Quick Look";
			this.fm = a;
			this.exec = function() {
				b.fm.quickLook.toggle()
			};
			this.isAllowed = function() {
				return 1 == this.fm.selected.length
			};
			this.cm = function() {
				return ! 0
			}
		},
		info: function(e) {
			var b = this;
			this.name = "Get info";
			this.fm = e;
			this.exec = function() {
				function c(c) {
					var k = ["50%", "50%"],
					t,
					A = '<table cellspacing="0"><tr><td>' + b.fm.i18n("Name") + "</td><td>" + c.name + "</td></tr><tr><td>" + b.fm.i18n("Kind") + "</td><td>" + b.fm.view.mime2kind(c.link ? "symlink": c.mime) + "</td></tr><tr><td>" + b.fm.i18n("Size") + "</td><td>" + b.fm.view.formatSize(c.size) + "</td></tr><tr><td>" + b.fm.i18n("Modified") + "</td><td>" + b.fm.view.formatDate(c.date) + "</td></tr><tr><td>" + b.fm.i18n("Permissions") + "</td><td>" + b.fm.view.formatPermissions(c.read, c.write, c.rm) + "</td></tr>";
					c.link && (A += "<tr><td>" + b.fm.i18n("Link to") + "</td><td>" + c.linkTo + "</td></tr>");
					c.dim && (A += "<tr><td>" + b.fm.i18n("Dimensions") + "</td><td>" + c.dim + " px.</td></tr>");
					c.url && (A += "<tr><td>" + b.fm.i18n("URL") + '</td><td><a href="' + c.url + '" target="_blank">' + c.url + "</a></td></tr>");
					1 < g && (t = a(".el-finder-dialog-info:last"), t.length ? (k = t.offset().left + 10, t = t.offset().top + 10, k = [k < d - 350 ? k: 20, t < e - 300 ? t: 20]) : (k = Math.round((d - 350) / 2 - 10 * g), t = Math.round((e - 300) / 2 - 10 * g), k = [20 < k ? k: 20, 20 < t ? t: 20]));
					a("<div />").append(A + "</table>").dialog({
						dialogClass: "el-finder-dialog el-finder-dialog-info",
						width: 390,
						position: k,
						title: b.fm.i18n("directory" == c.mime ? "Folder info": "File info"),
						close: function() {
							0 >= --g && b.fm.lockShortcuts();
							a(this).dialog("destroy")
						},
						buttons: {
							Ok: function() {
								a(this).dialog("close")
							}
						}
					})
				}
				var g = this.fm.selected.length,
				d = a(window).width(),
				e = a(window).height();
				this.fm.lockShortcuts(!0);
				g ? a.each(this.fm.getSelected(),
				function() {
					c(this)
				}) : c(b.fm.cwd)
			};
			this.cm = function() {
				return ! 0
			}
		},
		rename: function(e) {
			var b = this;
			this.name = "Rename";
			this.fm = e;
			this.exec = function() {
				function c() {
					j.html(A);
					b.fm.lockShortcuts()
				}
				function g() {
					if (!b.fm.locked) {
						var a, d = k.val();
						if (t.name == k.val()) return c();
						b.fm.isValidName(d) ? b.fm.fileExists(d) && (a = "File or folder with the same name already exists") : a = "Invalid name";
						if (a) return b.fm.view.error(a),
						e.addClass("ui-selected"),
						b.fm.lockShortcuts(!0),
						k.select().focus();
						b.fm.ajax({
							cmd: "rename",
							current: b.fm.cwd.hash,
							target: t.hash,
							name: d
						},
						function(a) {
							a.error ? c() : ("directory" == t.mime && b.fm.removePlace(t.hash) && b.fm.addPlace(a.target), b.fm.reload(a))
						},
						{
							force: !0
						})
					}
				}
				var d = this.fm.getSelected(),
				e,
				j,
				k,
				t,
				A;
				1 == d.length && (t = d[0], e = this.fm.view.cwd.find('[key="' + t.hash + '"]'), j = "icons" == this.fm.options.view ? e.children("label") : e.find("td").eq(1), A = j.html(), k = a('<input type="text" />').val(t.name).appendTo(j.empty()).bind("change blur", g).keydown(function(b) {
					b.stopPropagation();
					27 == b.keyCode ? c() : 13 == b.keyCode && (t.name == k.val() ? c() : a(this).trigger("change"))
				}).click(function(a) {
					a.stopPropagation()
				}).select().focus(), this.fm.lockShortcuts(!0))
			};
			this.isAllowed = function() {
				return this.fm.cwd.write && this.fm.getSelected(0).write
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		copy: function(a) {
			this.name = "Copy";
			this.fm = a;
			this.exec = function() {
				this.fm.setBuffer(this.fm.selected)
			};
			this.isAllowed = function() {
				if (this.fm.selected.length) for (var a = this.fm.getSelected(), c = a.length; c--;) if (a[c].read) return ! 0;
				return ! 1
			};
			this.cm = function(a) {
				return "cwd" != a
			}
		},
		cut: function(a) {
			this.name = "Cut";
			this.fm = a;
			this.exec = function() {
				this.fm.setBuffer(this.fm.selected, 1)
			};
			this.isAllowed = function() {
				if (this.fm.selected.length) for (var a = this.fm.getSelected(), c = a.length; c--;) if (a[c].read && a[c].rm) return ! 0;
				return ! 1
			};
			this.cm = function(a) {
				return "cwd" != a
			}
		},
		paste: function(a) {
			var b = this;
			this.name = "Paste";
			this.fm = a;
			this.exec = function() {
				var a;
				this.fm.buffer.dst || (this.fm.buffer.dst = this.fm.cwd.hash);
				a = this.fm.view.tree.find('[key="' + this.fm.buffer.dst + '"]');
				if (!a.length || a.hasClass("noaccess") || a.hasClass("readonly")) return this.fm.view.error("Access denied");
				if (this.fm.buffer.src == this.fm.buffer.dst) return this.fm.view.error("Unable to copy into itself");
				a = {
					cmd: "paste",
					current: this.fm.cwd.hash,
					src: this.fm.buffer.src,
					dst: this.fm.buffer.dst,
					cut: this.fm.buffer.cut
				};
				132 < this.fm.jquery ? a.targets = this.fm.buffer.files: a["targets[]"] = this.fm.buffer.files;
				this.fm.ajax(a,
				function(a) {
					a.cdc && b.fm.reload(a)
				},
				{
					force: !0
				})
			};
			this.isAllowed = function() {
				return this.fm.buffer.files
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		rm: function(e) {
			var b = this;
			this.name = "Remove";
			this.fm = e;
			this.exec = function() {
				var c, g = [],
				d = this.fm.getSelected();
				for (c = 0; c < d.length; c++) {
					if (!d[c].rm) return this.fm.view.error(d[c].name + ": " + this.fm.i18n("Access denied"));
					g.push(d[c].hash)
				}
				g.length && (this.fm.lockShortcuts(!0), a('<div><div class="ui-state-error ui-corner-all"><span class="ui-icon ui-icon-alert"/><strong>' + this.fm.i18n("Are you shure you want to remove files?<br /> This cannot be undone!") + "</strong></div></div>").dialog({
					title: this.fm.i18n("Confirmation required"),
					dialogClass: "el-finder-dialog",
					width: 350,
					close: function() {
						b.fm.lockShortcuts()
					},
					buttons: {
						Cancel: function() {
							a(this).dialog("close")
						},
						Ok: function() {
							a(this).dialog("close");
							var c = {
								cmd: "rm",
								current: b.fm.cwd.hash
							};
							132 < b.fm.jquery ? c.targets = g: c["targets[]"] = g;
							b.fm.ajax(c,
							function(a) {
								a.tree && b.fm.reload(a)
							},
							{
								force: !0
							})
						}
					}
				}))
			};
			this.isAllowed = function() {
				if (this.fm.selected.length) for (var a = this.fm.getSelected(), b = a.length; b--;) if (a[b].rm) return ! 0;
				return ! 1
			};
			this.cm = function(a) {
				return "cwd" != a
			}
		},
		mkdir: function(e) {
			var b = this;
			this.name = "New folder";
			this.fm = e;
			this.exec = function() {
				function c() {
					if (!b.fm.locked) {
						var a, c = input.val();
						b.fm.isValidName(c) ? b.fm.fileExists(c) && (a = "File or folder with the same name already exists") : a = "Invalid name";
						if (a) return b.fm.view.error(a),
						b.fm.lockShortcuts(!0),
						el.addClass("ui-selected"),
						input.select().focus();
						b.fm.ajax({
							cmd: "mkdir",
							current: b.fm.cwd.hash,
							name: c
						},
						function(a) {
							if (a.error) return el.addClass("ui-selected"),
							input.select().focus();
							b.fm.reload(a)
						},
						{
							force: !0
						})
					}
				}
				b.fm.unselectAll();
				var g = this.fm.uniqueName("untitled folder");
				input = a('<input type="text"/>').val(g);
				prev = this.fm.view.cwd.find(".directory:last");
				f = {
					name: g,
					hash: "",
					mime: "directory",
					read: !0,
					write: !0,
					date: "",
					size: 0
				};
				el = "list" == this.fm.options.view ? a(this.fm.view.renderRow(f)).children("td").eq(1).empty().append(input).end().end() : a(this.fm.view.renderIcon(f)).children("label").empty().append(input).end();
				el.addClass("directory ui-selected");
				prev.length ? el.insertAfter(prev) : "list" == this.fm.options.view ? el.insertAfter(this.fm.view.cwd.find("tr").eq(0)) : el.prependTo(this.fm.view.cwd);
				b.fm.checkSelectedPos();
				input.select().focus().click(function(a) {
					a.stopPropagation()
				}).bind("change blur", c).keydown(function(a) {
					a.stopPropagation();
					27 == a.keyCode ? (el.remove(), b.fm.lockShortcuts()) : 13 == a.keyCode && c()
				});
				b.fm.lockShortcuts(!0)
			};
			this.isAllowed = function() {
				return this.fm.cwd.write
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		mkfile: function(e) {
			var b = this;
			this.name = "New text file";
			this.fm = e;
			this.exec = function() {
				function c() {
					if (!b.fm.locked) {
						var a, c = d.val();
						b.fm.isValidName(c) ? b.fm.fileExists(c) && (a = "File or folder with the same name already exists") : a = "Invalid name";
						if (a) return b.fm.view.error(a),
						b.fm.lockShortcuts(!0),
						e.addClass("ui-selected"),
						d.select().focus();
						b.fm.ajax({
							cmd: "mkfile",
							current: b.fm.cwd.hash,
							name: c
						},
						function(a) {
							if (a.error) return e.addClass("ui-selected"),
							d.select().focus();
							b.fm.reload(a)
						},
						{
							force: !0
						})
					}
				}
				b.fm.unselectAll();
				var g = this.fm.uniqueName("untitled file", ".txt"),
				d = a('<input type="text"/>').val(g),
				g = {
					name: g,
					hash: "",
					mime: "text/plain",
					read: !0,
					write: !0,
					date: "",
					size: 0
				},
				e = "list" == this.fm.options.view ? a(this.fm.view.renderRow(g)).children("td").eq(1).empty().append(d).end().end() : a(this.fm.view.renderIcon(g)).children("label").empty().append(d).end();
				e.addClass("text ui-selected").appendTo("list" == this.fm.options.view ? b.fm.view.cwd.children("table") : b.fm.view.cwd);
				d.select().focus().bind("change blur", c).click(function(a) {
					a.stopPropagation()
				}).keydown(function(a) {
					a.stopPropagation();
					27 == a.keyCode ? (e.remove(), b.fm.lockShortcuts()) : 13 == a.keyCode && c()
				});
				b.fm.lockShortcuts(!0)
			};
			this.isAllowed = function() {
				return this.fm.cwd.write
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		upload: function(e) {
			var b = this;
			this.name = "Upload files";
			this.fm = e;
			this.exec = function() {
				function c() {
					function c() {
						try {
							n = e.contentWindow ? e.contentWindow.document: e.contentDocument ? e.contentDocument: e.document;
							if (null == n.body || "" == n.body.innerHTML) {
								if (--j) return setTimeout(c, 100);
								b.fm.lock();
								d.remove();
								return b.fm.view.error("Unable to access iframe DOM after 50 tries")
							}
							t = a(n.body).html();
							A = 141 <= b.fm.jquery ? a.parseJSON(t) : /^[\],:{}\s]*$/.test(t.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ? window.JSON && window.JSON.parse ? window.JSON.parse(t) : (new Function("return " + t))() : {
								error: "Unable to parse server response"
							}
						} catch(g) {
							A = {
								error: "Unable to parse server response"
							}
						}
						b.fm.lock();
						d.remove();
						A.error && b.fm.view.error(A.error, A.errorData);
						A.cwd && b.fm.reload(A);
						A.tmb && b.fm.tmb()
					}
					var d = a('<iframe name="' + g + '" name="' + g + '" src="about:blank"/>'),
					e = d[0],
					j = 50,
					n,
					t,
					A;
					d.css({
						position: "absolute",
						top: "-1000px",
						left: "-1000px"
					}).appendTo("body").bind("load",
					function() {
						d.unbind("load");
						c()
					});
					b.fm.lock(!0);
					k.submit()
				}
				for (var g = "el-finder-io-" + (new Date).getTime(), d = a('<div class="ui-state-error ui-corner-all"><span class="ui-icon ui-icon-alert"/><div/></div>'), e = this.fm.params.uplMaxSize ? "<p>" + this.fm.i18n("Maximum allowed files size") + ": " + this.fm.params.uplMaxSize + "</p>": "", j = a('<p class="el-finder-add-field"><span class="ui-state-default ui-corner-all"><em class="ui-icon ui-icon-circle-plus"/></span>' + this.fm.i18n("Add field") + "</p>").click(function() {
					a(this).before('<p><input type="file" name="upload[]"/></p>')
				}), k = '<form method="post" enctype="multipart/form-data" action="' + b.fm.options.url + '" target="' + g + '"><input type="hidden" name="cmd" value="upload" /><input type="hidden" name="current" value="' + b.fm.cwd.hash + '" />', t = a("<div/>"), A = 3; A--;) k += '<p><input type="file" name="upload[]"/></p>';
				k = a(k + "</form>");
				t.append(k.append(d.hide()).prepend(e).append(j)).dialog({
					dialogClass: "el-finder-dialog",
					title: b.fm.i18n("Upload files"),
					modal: !0,
					resizable: !1,
					close: function() {
						b.fm.lockShortcuts()
					},
					buttons: {
						Cancel: function() {
							a(this).dialog("close")
						},
						Ok: function() {
							if (a(":file[value]", k).length) setTimeout(function() {
								b.fm.lock();
								a.browser.safari ? a.ajax({
									url: b.fm.options.url,
									data: {
										cmd: "ping"
									},
									error: c,
									success: c
								}) : c()
							}),
							a(this).dialog("close");
							else {
								var g = b.fm.i18n("Select at least one file to upload");
								d.show().find("div").empty().text(g)
							}
						}
					}
				});
				b.fm.lockShortcuts(!0)
			};
			this.isAllowed = function() {
				return this.fm.cwd.write
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		duplicate: function(a) {
			var b = this;
			this.name = "Duplicate";
			this.fm = a;
			this.exec = function() {
				this.fm.ajax({
					cmd: "duplicate",
					current: this.fm.cwd.hash,
					target: this.fm.selected[0]
				},
				function(a) {
					b.fm.reload(a)
				})
			};
			this.isAllowed = function() {
				return this.fm.cwd.write && 1 == this.fm.selected.length && this.fm.getSelected()[0].read
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		edit: function(e) {
			var b = this;
			this.name = "Edit text file";
			this.fm = e;
			this.exec = function() {
				var c = this.fm.getSelected(0);
				this.fm.lockShortcuts(!0);
				this.fm.ajax({
					cmd: "read",
					current: this.fm.cwd.hash,
					target: c.hash
				},
				function(g) {
					b.fm.lockShortcuts(!0);
					var d = a("<textarea/>").val(g.content || "").keydown(function(a) {
						a.stopPropagation()
					});
					a("<div/>").append(d).dialog({
						dialogClass: "el-finder-dialog",
						title: b.fm.i18n(b.name),
						modal: !0,
						width: 500,
						close: function() {
							b.fm.lockShortcuts()
						},
						buttons: {
							Cancel: function() {
								a(this).dialog("close")
							},
							Ok: function() {
								var g = d.val();
								a(this).dialog("close");
								b.fm.ajax({
									cmd: "edit",
									current: b.fm.cwd.hash,
									target: c.hash,
									content: g
								},
								function(a) {
									a.target && (b.fm.cdc[a.target.hash] = a.target, b.fm.view.updateFile(a.target), b.fm.selectById(a.target.hash))
								},
								{
									type: "POST"
								})
							}
						}
					})
				})
			};
			this.isAllowed = function() {
				if (1 == b.fm.selected.length) {
					var a = this.fm.getSelected()[0];
					return a.write && (0 == a.mime.indexOf("text") || "application/x-empty" == a.mime || "application/xml" == a.mime)
				}
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		archive: function(e) {
			var b = this;
			this.name = "Create archive";
			this.fm = e;
			this.exec = function(c) {
				c = {
					cmd: "archive",
					current: b.fm.cwd.hash,
					type: -1 != a.inArray(c, this.fm.params.archives) ? c: this.fm.params.archives[0],
					name: b.fm.i18n("Archive")
				};
				132 < this.fm.jquery ? c.targets = b.fm.selected: c["targets[]"] = b.fm.selected;
				this.fm.ajax(c,
				function(a) {
					b.fm.reload(a)
				})
			};
			this.isAllowed = function() {
				if (this.fm.cwd.write && this.fm.selected.length) for (var a = this.fm.getSelected(), b = a.length; b--;) if (a[b].read) return ! 0;
				return ! 1
			};
			this.cm = function(a) {
				return "cwd" != a
			};
			this.argc = function() {
				var a, g = [];
				for (a = 0; a < b.fm.params.archives.length; a++) g.push({
					"class": "archive",
					argc: b.fm.params.archives[a],
					text: b.fm.view.mime2kind(b.fm.params.archives[a])
				});
				return g
			}
		},
		extract: function(e) {
			var b = this;
			this.name = "Uncompress archive";
			this.fm = e;
			this.exec = function() {
				this.fm.ajax({
					cmd: "extract",
					current: this.fm.cwd.hash,
					target: this.fm.getSelected(0).hash
				},
				function(a) {
					b.fm.reload(a)
				})
			};
			this.isAllowed = function() {
				return this.fm.cwd.write && 1 == this.fm.selected.length && this.fm.getSelected(0).read && this.fm.params.extract.length && -1 != a.inArray(this.fm.getSelected(0).mime, this.fm.params.extract)
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		resize: function(e) {
			var b = this;
			this.name = "Resize image";
			this.fm = e;
			this.exec = function() {
				function c() {
					var a = parseInt(iw.val()) || 0,
					b = parseInt(ih.val()) || 0;
					0 >= a || 0 >= b ? (a = e, b = j) : this == iw.get(0) ? b = parseInt(a / k) : a = parseInt(b * k);
					iw.val(a);
					ih.val(b)
				}
				var g = this.fm.getSelected();
				if (g[0] && g[0].write && g[0].dim) {
					var d = g[0].dim.split("x"),
					e = parseInt(d[0]),
					j = parseInt(d[1]),
					k = e / j;
					iw = a('<input type="text" size="9" value="' + e + '" name="width"/>');
					ih = a('<input type="text" size="9" value="' + j + '" name="height"/>');
					f = a("<form/>").append(iw).append(" x ").append(ih).append(" px");
					iw.add(ih).bind("change", c);
					b.fm.lockShortcuts(!0);
					a("<div/>").append(a("<div/>").text(b.fm.i18n("Dimensions") + ":")).append(f).dialog({
						title: b.fm.i18n("Resize image"),
						dialogClass: "el-finder-dialog",
						width: 230,
						modal: !0,
						close: function() {
							b.fm.lockShortcuts()
						},
						buttons: {
							Cancel: function() {
								a(this).dialog("close")
							},
							Ok: function() {
								var c = parseInt(iw.val()) || 0,
								d = parseInt(ih.val()) || 0;
								0 < c && (c != e && 0 < d && d != j) && b.fm.ajax({
									cmd: "resize",
									current: b.fm.cwd.hash,
									target: g[0].hash,
									width: c,
									height: d
								},
								function(a) {
									b.fm.reload(a)
								});
								a(this).dialog("close")
							}
						}
					})
				}
			};
			this.isAllowed = function() {
				return 1 == this.fm.selected.length && this.fm.cdc[this.fm.selected[0]].write && this.fm.cdc[this.fm.selected[0]].resize
			};
			this.cm = function(a) {
				return "file" == a
			}
		},
		icons: function(e) {
			this.name = "View as icons";
			this.fm = e;
			this.exec = function() {
				this.fm.view.win.addClass("el-finder-disabled");
				this.fm.setView("icons");
				this.fm.updateCwd();
				this.fm.view.win.removeClass("el-finder-disabled");
				a("div.image", this.fm.view.cwd).length && this.fm.tmb()
			};
			this.isAllowed = function() {
				return "icons" != this.fm.options.view
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		list: function(a) {
			this.name = "View as list";
			this.fm = a;
			this.exec = function() {
				this.fm.view.win.addClass("el-finder-disabled");
				this.fm.setView("list");
				this.fm.updateCwd();
				this.fm.view.win.removeClass("el-finder-disabled")
			};
			this.isAllowed = function() {
				return "list" != this.fm.options.view
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		},
		help: function(e) {
			this.name = "Help";
			this.fm = e;
			this.exec = function() {
				var b, c = this.fm.i18n("helpText"),
				g;
				b = '<div class="el-finder-logo"/><strong>' + this.fm.i18n("elFinder: Web file manager") + "</strong><br/>" + this.fm.i18n("Version") + ": " + this.fm.version + '<br clear="all"/><p><strong><a href="http://elrte.ru/donate?prod=elfinder&lang=' + this.fm.options.lang + '" target="_blank">' + this.fm.i18n("Donate to support project development") + '</a></strong></p><p><a href="http://elrte.ru/redmine/wiki/elfinder/" target="_blank">' + this.fm.i18n("elFinder documentation") + "</a></p>";
				b = b + ("<p>" + ("helpText" != c ? c: "elFinder works similar to file manager on your computer. <br /> To make actions on files/folders use icons on top panel. If icon action it is not clear for you, hold mouse cursor over it to see the hint. <br /> Manipulations with existing files/folders can be done through the context menu (mouse right-click).<br/> To copy/delete a group of files/folders, select them using Shift/Alt(Command) + mouse left-click.") + "</p>") + ("<p><strong>" + this.fm.i18n("elFinder support following shortcuts") + ":</strong><ul><li><kbd>Ctrl+A</kbd> - " + this.fm.i18n("Select all files") + "</li><li><kbd>Ctrl+C/Ctrl+X/Ctrl+V</kbd> - " + this.fm.i18n("Copy/Cut/Paste files") + "</li><li><kbd>Enter</kbd> - " + this.fm.i18n("Open selected file/folder") + "</li><li><kbd>Space</kbd> - " + this.fm.i18n("Open/close QuickLook window") + "</li><li><kbd>Delete/Cmd+Backspace</kbd> - " + this.fm.i18n("Remove selected files") + "</li><li><kbd>Ctrl+I</kbd> - " + this.fm.i18n("Selected files or current directory info") + "</li><li><kbd>Ctrl+N</kbd> - " + this.fm.i18n("Create new directory") + "</li><li><kbd>Ctrl+U</kbd> - " + this.fm.i18n("Open upload files form") + "</li><li><kbd>Left arrow</kbd> - " + this.fm.i18n("Select previous file") + "</li><li><kbd>Right arrow </kbd> - " + this.fm.i18n("Select next file") + "</li><li><kbd>Ctrl+Right arrow</kbd> - " + this.fm.i18n("Open selected file/folder") + "</li><li><kbd>Ctrl+Left arrow</kbd> - " + this.fm.i18n("Return into previous folder") + "</li><li><kbd>Shift+arrows</kbd> - " + this.fm.i18n("Increase/decrease files selection") + "</li></ul></p><p>" + this.fm.i18n("Contacts us if you need help integrating elFinder in you products") + ": dev@std42.ru</p>");
				c = '<div class="el-finder-help-std"/><p>' + this.fm.i18n("Javascripts/PHP programming: Dmitry (dio) Levashov, dio@std42.ru") + "</p><p>" + this.fm.i18n("Python programming, techsupport: Troex Nevelin, troex@fury.scancode.ru") + "</p><p>" + this.fm.i18n("Design: Valentin Razumnih") + "</p><p>" + this.fm.i18n("Spanish localization") + ': Alex (xand) Vavilin, xand@xand.es, <a href="http://xand.es" target="_blank">http://xand.es</a></p><p>' + this.fm.i18n("Icons") + ': <a href="http://www.famfamfam.com/lab/icons/silk/" target="_blank">Famfam silk icons</a>, <a href="http://www.fatcow.com/free-icons/" target="_blank">Fatcow icons</a></p><p>' + this.fm.i18n('Copyright: <a href="http://www.std42.ru" target="_blank">Studio 42 LTD</a>') + "</p><p>" + this.fm.i18n("License: BSD License") + "</p><p>" + this.fm.i18n('Web site: <a href="http://www.elrte.ru/elfinder/" target="_blank">elrte.ru</a>') + "</p>";
				g = '<div class="el-finder-logo"/><strong><a href="http://www.eldorado-cms.ru" target="_blank">ELDORADO.CMS</a></strong><br/>' + this.fm.i18n("Simple and usefull Content Management System") + "<hr/>" + this.fm.i18n("Support project development and we will place here info about you");
				b = '<ul><li><a href="#el-finder-help-h">' + this.fm.i18n("Help") + '</a></li><li><a href="#el-finder-help-a">' + this.fm.i18n("Authors") + '</a><li><a href="#el-finder-help-sp">' + this.fm.i18n("Sponsors") + '</a></li></ul><div id="el-finder-help-h"><p>' + b + '</p></div><div id="el-finder-help-a"><p>' + c + '</p></div><div id="el-finder-help-sp"><p>' + g + "</p></div>";
				var d = a("<div/>").html(b).dialog({
					width: 617,
					title: this.fm.i18n("Help"),
					dialogClass: "el-finder-dialog",
					modal: !0,
					close: function() {
						d.tabs("destroy").dialog("destroy").remove()
					},
					buttons: {
						Ok: function() {
							a(this).dialog("close")
						}
					}
				}).tabs()
			};
			this.cm = function(a) {
				return "cwd" == a
			}
		}
	}
})(jQuery); (function(a) {
	elFinder.prototype.quickLook = function(e) {
		function b() {
			g.media.hide().empty();
			g.win.attr("class", "el-finder-ql").css("z-index", g.fm.zIndex);
			g.title.empty();
			g.ico.removeAttr("style").show();
			g.add.hide().empty();
			g._hash = ""
		}
		function c() {
			var a = g.fm.getSelected(0);
			b();
			g._hash = a.hash;
			g.title.text(a.name);
			g.win.addClass(g.fm.view.mime2class(a.mime));
			g.name.text(a.name);
			g.kind.text(g.fm.view.mime2kind(a.link ? "symlink": a.mime));
			g.size.text(g.fm.view.formatSize(a.size));
			g.date.text(g.fm.i18n("Modified") + ": " + g.fm.view.formatDate(a.date));
			a.dim && g.add.append("<span>" + a.dim + " px</span>").show();
			a.tmb && g.ico.css("background", 'url("' + a.tmb + '") 0 0 no-repeat');
			if (a.url) {
				g.url.text(a.url).attr("href", a.url).show();
				for (var c in g.plugins) if (g.plugins[c].test && g.plugins[c].test(a.mime, g.mimes, a.name)) {
					g.plugins[c].show(g, a);
					return
				}
			} else g.url.hide();
			g.win.css({
				width: "420px",
				height: "auto"
			})
		}
		var g = this;
		this.fm = e;
		this._hash = "";
		this.title = a("<strong/>");
		this.ico = a("<p/>");
		this.info = a("<label/>");
		this.media = a('<div class="el-finder-ql-media"/>').hide();
		this.name = a('<span class="el-finder-ql-name"/>');
		this.kind = a('<span class="el-finder-ql-kind"/>');
		this.size = a('<span class="el-finder-ql-size"/>');
		this.date = a('<span class="el-finder-ql-date"/>');
		this.url = a('<a href="#"/>').hide().click(function(b) {
			b.preventDefault();
			window.open(a(this).attr("href"));
			g.hide()
		});
		this.add = a("<div/>");
		this.content = a('<div class="el-finder-ql-content"/>');
		this.win = a('<div class="el-finder-ql"/>').hide().append(a('<div class="el-finder-ql-drag-handle"/>').append(a('<span class="ui-icon ui-icon-circle-close"/>').click(function() {
			g.hide()
		})).append(this.title)).append(this.ico).append(this.media).append(this.content.append(this.name).append(this.kind).append(this.size).append(this.date).append(this.url).append(this.add)).appendTo(document.body).draggable({
			handle: ".el-finder-ql-drag-handle"
		}).resizable({
			minWidth: 420,
			minHeight: 150,
			resize: function() {
				if (g.media.children().length) {
					var a = g.media.children(":first");
					switch (a[0].nodeName) {
					case "IMG":
						var b = a.width(),
						c = a.height(),
						d = g.win.width(),
						e = "auto" == g.win.css("height") ? 350 : g.win.height() - g.content.height() - g.th,
						b = b > d || c > e ? Math.min(Math.min(d, b) / b, Math.min(e, c) / c) : Math.min(Math.max(d, b) / b, Math.max(e, c) / c);
						a.css({
							width: Math.round(a.width() * b),
							height: Math.round(a.height() * b)
						});
						break;
					case "IFRAME":
					case "EMBED":
						a.css("height", g.win.height() - g.content.height() - g.th);
						break;
					case "OBJECT":
						a.children("embed").css("height", g.win.height() - g.content.height() - g.th)
					}
				}
			}
		});
		this.th = parseInt(this.win.children(":first").css("height")) || 18;
		this.mimes = {
			"image/jpeg": "jpg",
			"image/gif": "gif",
			"image/png": "png"
		};
		for (e = 0; e < navigator.mimeTypes.length; e++) {
			var d = navigator.mimeTypes[e].type;
			d && "*" != d && (this.mimes[d] = navigator.mimeTypes[e].suffixes)
		}
		if (a.browser.safari && -1 != navigator.platform.indexOf("Mac") || a.browser.msie) this.mimes["application/pdf"] = "pdf";
		else for (e = 0; e < navigator.plugins.length; e++) for (d = 0; d < navigator.plugins[e].length; d++) {
			var n = navigator.plugins[e][d].description.toLowerCase();
			if ("pdf" == n.substring(0, n.indexOf(" "))) {
				this.mimes["application/pdf"] = "pdf";
				break
			}
		}
		this.mimes["image/x-bmp"] && (this.mimes["image/x-ms-bmp"] = "bmp");
		a.browser.msie && !this.mimes["application/x-shockwave-flash"] && (this.mimes["application/x-shockwave-flash"] = "swf");
		this.show = function() {
			if (this.win.is(":hidden") && 1 == g.fm.selected.length) {
				c();
				var b = g.fm.view.cwd.find('[key="' + g.fm.selected[0] + '"]'),
				d = b.offset();
				g.fm.lockShortcuts(!0);
				this.win.css({
					width: b.width() - 20,
					height: b.height(),
					left: d.left,
					top: d.top,
					opacity: 0
				}).animate({
					width: 420,
					height: 150,
					opacity: 1,
					top: Math.round(a(window).height() / 5),
					left: a(window).width() / 2 - 210
				},
				450,
				function() {
					g.win.css({
						height: "auto"
					});
					g.fm.lockShortcuts()
				})
			}
		};
		this.hide = function() {
			if (this.win.is(":visible")) {
				var a, c = g.fm.view.cwd.find('[key="' + this._hash + '"]');
				c ? (a = c.offset(), this.media.hide(200), this.win.animate({
					width: c.width() - 20,
					height: c.height(),
					left: a.left,
					top: a.top,
					opacity: 0
				},
				350,
				function() {
					g.fm.lockShortcuts();
					b();
					g.win.hide().css("height", "auto")
				})) : (this.win.fadeOut(200), b(), g.fm.lockShortcuts())
			}
		};
		this.toggle = function() {
			this.win.is(":visible") ? this.hide() : this.show()
		};
		this.update = function() {
			1 != this.fm.selected.length ? this.hide() : this.win.is(":visible") && this.fm.selected[0] != this._hash && c()
		};
		this.mediaHeight = function() {
			return this.win.is(":animated") || "auto" == this.win.css("height") ? 315 : this.win.height() - this.content.height() - this.th
		}
	};
	elFinder.prototype.quickLook.prototype.plugins = {
		image: new
		function() {
			this.test = function(a) {
				return a.match(/^image\//)
			};
			this.show = function(e, b) {
				function c(a) {
					var b = a.width(),
					c = a.height(),
					g = e.win.is(":animated"),
					t = g ? 420 : e.win.width(),
					g = g || "auto" == e.win.css("height") ? 315 : e.win.height() - e.content.height() - e.th,
					t = b > t || c > g ? Math.min(Math.min(t, b) / b, Math.min(g, c) / c) : Math.min(Math.max(t, b) / b, Math.max(g, c) / c);
					e.fm.lockShortcuts(!0);
					e.ico.hide();
					a.css({
						width: e.ico.width(),
						height: e.ico.height()
					}).animate({
						width: Math.round(t * b),
						height: Math.round(t * c)
					},
					450,
					function() {
						e.fm.lockShortcuts()
					})
				}
				var g;
				e.mimes[b.mime] && b.hash == e._hash && a("<img/>").hide().appendTo(e.media.show()).attr("src", b.url + (a.browser.msie || a.browser.opera ? "?" + Math.random() : "")).load(function() {
					g = a(this).unbind("load");
					b.hash == e._hash && (e.win.is(":animated") ? setTimeout(function() {
						c(g)
					},
					330) : c(g))
				})
			}
		},
		text: new
		function() {
			this.test = function(a) {
				return 0 == a.indexOf("text") && -1 == a.indexOf("rtf") || a.match(/application\/(xml|javascript|json)/)
			};
			this.show = function(a, b) {
				b.hash == a._hash && (a.ico.hide(), a.media.append('<iframe src="' + b.url + '" style="height:' + a.mediaHeight() + 'px" />').show())
			}
		},
		swf: new
		function() {
			this.test = function(a, b) {
				return "application/x-shockwave-flash" == a && b[a]
			};
			this.show = function(a, b) {
				if (b.hash == a._hash) {
					a.ico.hide();
					var c = a.media.append('<embed pluginspage="http://www.macromedia.com/go/getflashplayer" quality="high" src="' + b.url + '" style="width:100%;height:' + a.mediaHeight() + 'px" type="application/x-shockwave-flash" />');
					a.win.is(":animated") ? c.slideDown(450) : c.show()
				}
			}
		},
		audio: new
		function() {
			this.test = function(a, b) {
				return 0 == a.indexOf("audio") && b[a]
			};
			this.show = function(a, b) {
				if (b.hash == a._hash) {
					a.ico.hide();
					var c = a.win.is(":animated") || "auto" == a.win.css("height") ? 100 : a.win.height() - a.content.height() - a.th;
					a.media.append('<embed src="' + b.url + '" style="width:100%;height:' + c + 'px" />').show()
				}
			}
		},
		video: new
		function() {
			this.test = function(a, b) {
				return 0 == a.indexOf("video") && b[a]
			};
			this.show = function(a, b) {
				b.hash == a._hash && (a.ico.hide(), a.media.append('<embed src="' + b.url + '" style="width:100%;height:' + a.mediaHeight() + 'px" />').show())
			}
		},
		pdf: new
		function() {
			this.test = function(a, b) {
				return "application/pdf" == a && b[a]
			};
			this.show = function(a, b) {
				b.hash == a._hash && (a.ico.hide(), a.media.append('<embed src="' + b.url + '" style="width:100%;height:' + a.mediaHeight() + 'px" />').show())
			}
		}
	}
})(jQuery); (function(a) {
	elFinder.prototype.eventsManager = function(e) {
		function b(b, d) {
			var e, j, k;
			if (a("[key]", c.cwd).length) {
				if (0 == c.fm.selected.length) e = a("[key]:" + (b ? "first": "last"), c.cwd),
				c.fm.select(e);
				else if (d) e = a(".ui-selected:" + (b ? "last": "first"), c.cwd),
				j = e[b ? "next": "prev"]("[key]"),
				j.length && (e = j),
				c.fm.select(e, !0);
				else {
					c.pointer && (k = a('[key="' + c.pointer + '"].ui-selected', c.cwd));
					if (!k || !k.length) k = a(".ui-selected:" + (b ? "last": "first"), c.cwd);
					e = k[b ? "next": "prev"]("[key]");
					if (e.length) if (e.hasClass("ui-selected")) if (k.hasClass("ui-selected")) if (j = k[b ? "prev": "next"]("[key]"), !j.length || !j.hasClass("ui-selected")) c.fm.unselect(k);
					else {
						for (; (j = b ? e.next("[key]") : e.prev("[key]")) && e.hasClass("ui-selected");) e = j;
						c.fm.select(e)
					} else c.fm.unselect(e);
					else c.fm.select(e);
					else e = k
				}
				c.pointer = e.attr("key");
				c.fm.checkSelectedPos(b)
			}
		}
		var c = this;
		this.lock = !1;
		this.fm = e;
		this.ui = e.ui;
		this.tree = e.view.tree;
		this.cwd = e.view.cwd;
		this.pointer = "";
		this.init = function() {
			this.cwd.bind("click",
			function(b) {
				var d = a(b.target);
				d.hasClass("ui-selected") ? c.fm.unselectAll() : (d.attr("key") || (d = d.parent("[key]")), b.ctrlKey || b.metaKey ? c.fm.toggleSelect(d) : c.fm.select(d, !0))
			}).bind(window.opera ? "click": "contextmenu",
			function(b) {
				if (!window.opera || b.ctrlKey) {
					var d = a(b.target);
					b.preventDefault();
					b.stopPropagation();
					d.hasClass("el-finder-cwd") ? c.fm.unselectAll() : c.fm.select(d.attr("key") ? d: d.parent("[key]"));
					c.fm.quickLook.hide();
					c.fm.ui.showMenu(b)
				}
			}).selectable({
				filter: "[key]",
				delay: 300,
				stop: function() {
					c.fm.updateSelect()
				}
			});
			a(document).bind("click",
			function() {
				c.fm.ui.hideMenu();
				a("input", c.cwd).trigger("change")
			});
			this.tree.bind("select",
			function(b) {
				c.tree.find("a").removeClass("selected");
				a(b.target).addClass("selected").parents("li:has(ul)").children("ul").show().prev().children("div").addClass("expanded")
			});
			this.fm.options.places && (this.fm.view.plc.click(function(b) {
				b.preventDefault();
				var d = a(b.target).attr("key");
				if (d) d != c.fm.cwd.hash && c.ui.exec("open", b.target);
				else if ("A" == b.target.nodeName || "DIV" == b.target.nodeName) b = c.fm.view.plc.find("ul"),
				b.children().length && (b.toggle(300), c.fm.view.plc.children("li").find("div").toggleClass("expanded"))
			}), this.fm.view.plc.droppable({
				accept: "(div,tr).directory",
				tolerance: "pointer",
				over: function() {
					a(this).addClass("el-finder-droppable")
				},
				out: function() {
					a(this).removeClass("el-finder-droppable")
				},
				drop: function(b, d) {
					a(this).removeClass("el-finder-droppable");
					var e = !1;
					d.helper.children(".directory:not(.noaccess,.dropbox)").each(function() {
						c.fm.addPlace(a(this).attr("key")) && (e = !0, a(this).hide())
					});
					e && (c.fm.view.renderPlaces(), c.updatePlaces(), c.fm.view.plc.children("li").children("div").trigger("click"));
					d.helper.children("div:visible").length || d.helper.hide()
				}
			}));
			a(document).bind(a.browser.mozilla || a.browser.opera ? "keypress": "keydown",
			function(a) {
				var d = a.ctrlKey || a.metaKey;
				if (!c.lock) switch (a.keyCode) {
				case 37:
				case 38:
					a.stopPropagation();
					a.preventDefault();
					37 == a.keyCode && d ? c.ui.execIfAllowed("back") : b(!1, !a.shiftKey);
					break;
				case 39:
				case 40:
					a.stopPropagation(),
					a.preventDefault(),
					d ? c.ui.execIfAllowed("open") : b(!0, !a.shiftKey)
				}
			});
			a(document).bind(a.browser.opera ? "keypress": "keydown",
			function(a) {
				if (!c.lock) switch (a.keyCode) {
				case 32:
					a.preventDefault();
					a.stopPropagation();
					c.fm.quickLook.toggle();
					break;
				case 27:
					c.fm.quickLook.hide()
				}
			});
			this.fm.options.disableShortcuts || a(document).bind("keydown",
			function(a) {
				var b = a.ctrlKey || a.metaKey;
				if (!c.lock) switch (a.keyCode) {
				case 8:
					b && c.ui.isCmdAllowed("rm") && (a.preventDefault(), c.ui.exec("rm"));
					break;
				case 13:
					if (c.ui.isCmdAllowed("select")) return c.ui.exec("select");
					c.ui.execIfAllowed("open");
					break;
				case 46:
					c.ui.execIfAllowed("rm");
					break;
				case 65:
					b && (a.preventDefault(), c.fm.selectAll());
					break;
				case 67:
					b && c.ui.execIfAllowed("copy");
					break;
				case 73:
					b && (a.preventDefault(), c.ui.exec("info"));
					break;
				case 78:
					b && (a.preventDefault(), c.ui.execIfAllowed("mkdir"));
					break;
				case 85:
					b && (a.preventDefault(), c.ui.execIfAllowed("upload"));
					break;
				case 86:
					b && c.ui.execIfAllowed("paste");
					break;
				case 88:
					b && c.ui.execIfAllowed("cut");
					break;
				case 113:
					c.ui.execIfAllowed("rename")
				}
			})
		};
		this.updateNav = function() {
			a("a", this.tree).click(function(b) {
				b.preventDefault();
				var d = a(this);
				"DIV" == b.target.nodeName && a(b.target).hasClass("collapsed") ? a(b.target).toggleClass("expanded").parent().next("ul").toggle(300) : d.attr("key") != c.fm.cwd.hash ? d.hasClass("noaccess") || d.hasClass("dropbox") ? c.fm.view.error("Access denied") : c.ui.exec("open", d.trigger("select")[0]) : (b = d.children(".collapsed"), b.length && (b.toggleClass("expanded"), d.next("ul").toggle(300)))
			});
			a("a:not(.noaccess,.readonly)", this.tree).droppable({
				tolerance: "pointer",
				accept: "(div,tr)[key]",
				over: function() {
					a(this).addClass("el-finder-droppable")
				},
				out: function() {
					a(this).removeClass("el-finder-droppable")
				},
				drop: function(b, d) {
					a(this).removeClass("el-finder-droppable");
					c.fm.drop(b, d, a(this).attr("key"))
				}
			});
			this.fm.options.places && this.updatePlaces()
		};
		this.updatePlaces = function() {
			this.fm.view.plc.children("li").find("li").draggable({
				scroll: !1,
				stop: function() {
					c.fm.removePlace(a(this).children("a").attr("key")) && (a(this).remove(), a("li", c.fm.view.plc.children("li")).length || c.fm.view.plc.children("li").find("div").removeClass("collapsed expanded").end().children("ul").hide())
				}
			})
		};
		this.updateCwd = function() {
			a("[key]", this.cwd).bind("dblclick",
			function() {
				c.fm.select(a(this), !0);
				c.ui.exec(c.ui.isCmdAllowed("select") ? "select": "open")
			}).draggable({
				delay: 3,
				addClasses: !1,
				appendTo: ".el-finder-cwd",
				revert: !0,
				drag: function(a, b) {
					b.helper.toggleClass("el-finder-drag-copy", a.shiftKey || a.ctrlKey)
				},
				helper: function() {
					var b = a(this),
					d = a('<div class="el-finder-drag-helper"/>'),
					e = 0; ! b.hasClass("ui-selected") && c.fm.select(b, !0);
					c.cwd.find(".ui-selected").each(function() {
						var b = "icons" == c.fm.options.view ? a(this).clone().removeClass("ui-selected") : a(c.fm.view.renderIcon(c.fm.cdc[a(this).attr("key")])); (0 == e++||0 == e % 12) && b.css("margin-left", 0);
						d.append(b)
					});
					return d.css("width", (12 >= e ? 85 + 29 * (e - 1) : 387) + "px")
				}
			}).filter(".directory").droppable({
				tolerance: "pointer",
				accept: "(div,tr)[key]",
				over: function() {
					a(this).addClass("el-finder-droppable")
				},
				out: function() {
					a(this).removeClass("el-finder-droppable")
				},
				drop: function(b, d) {
					a(this).removeClass("el-finder-droppable");
					c.fm.drop(b, d, a(this).attr("key"))
				}
			});
			a.browser.msie && a("*", this.cwd).attr("unselectable", "on").filter("[key]").bind("dragstart",
			function() {
				c.cwd.selectable("disable").removeClass("ui-state-disabled ui-selectable-disabled")
			}).bind("dragstop",
			function() {
				c.cwd.selectable("enable")
			})
		}
	}
})(jQuery); (function(a, e, b) {
	a.fn.dataTableSettings = [];
	var c = a.fn.dataTableSettings;
	a.fn.dataTableExt = {};
	var g = a.fn.dataTableExt;
	g.sVersion = "1.8.1";
	g.sErrMode = "alert";
	g.iApiIndex = 0;
	g.oApi = {};
	g.afnFiltering = [];
	g.aoFeatures = [];
	g.ofnSearch = {};
	g.afnSortData = [];
	g.oStdClasses = {
		sPagePrevEnabled: "paginate_enabled_previous",
		sPagePrevDisabled: "paginate_disabled_previous",
		sPageNextEnabled: "paginate_enabled_next",
		sPageNextDisabled: "paginate_disabled_next",
		sPageJUINext: "",
		sPageJUIPrev: "",
		sPageButton: "paginate_button",
		sPageButtonActive: "paginate_active",
		sPageButtonStaticDisabled: "paginate_button paginate_button_disabled",
		sPageFirst: "first",
		sPagePrevious: "previous",
		sPageNext: "next",
		sPageLast: "last",
		sStripOdd: "odd",
		sStripEven: "even",
		sRowEmpty: "dataTables_empty",
		sWrapper: "dataTables_wrapper",
		sFilter: "dataTables_filter",
		sInfo: "dataTables_info",
		sPaging: "dataTables_paginate paging_",
		sLength: "dataTables_length",
		sProcessing: "dataTables_processing",
		sSortAsc: "sorting_asc",
		sSortDesc: "sorting_desc",
		sSortable: "sorting",
		sSortableAsc: "sorting_asc_disabled",
		sSortableDesc: "sorting_desc_disabled",
		sSortableNone: "sorting_disabled",
		sSortColumn: "sorting_",
		sSortJUIAsc: "",
		sSortJUIDesc: "",
		sSortJUI: "",
		sSortJUIAscAllowed: "",
		sSortJUIDescAllowed: "",
		sSortJUIWrapper: "",
		sSortIcon: "",
		sScrollWrapper: "dataTables_scroll",
		sScrollHead: "dataTables_scrollHead",
		sScrollHeadInner: "dataTables_scrollHeadInner",
		sScrollBody: "dataTables_scrollBody",
		sScrollFoot: "dataTables_scrollFoot",
		sScrollFootInner: "dataTables_scrollFootInner",
		sFooterTH: ""
	};
	g.oJUIClasses = {
		sPagePrevEnabled: "fg-button ui-button ui-state-default ui-corner-left",
		sPagePrevDisabled: "fg-button ui-button ui-state-default ui-corner-left ui-state-disabled",
		sPageNextEnabled: "fg-button ui-button ui-state-default ui-corner-right",
		sPageNextDisabled: "fg-button ui-button ui-state-default ui-corner-right ui-state-disabled",
		sPageJUINext: "ui-icon ui-icon-circle-arrow-e",
		sPageJUIPrev: "ui-icon ui-icon-circle-arrow-w",
		sPageButton: "fg-button ui-button ui-state-default",
		sPageButtonActive: "fg-button ui-button ui-state-default ui-state-disabled",
		sPageButtonStaticDisabled: "fg-button ui-button ui-state-default ui-state-disabled",
		sPageFirst: "first ui-corner-tl ui-corner-bl",
		sPagePrevious: "previous",
		sPageNext: "next",
		sPageLast: "last ui-corner-tr ui-corner-br",
		sStripOdd: "odd",
		sStripEven: "even",
		sRowEmpty: "dataTables_empty",
		sWrapper: "dataTables_wrapper",
		sFilter: "dataTables_filter",
		sInfo: "dataTables_info",
		sPaging: "dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_",
		sLength: "dataTables_length",
		sProcessing: "dataTables_processing",
		sSortAsc: "ui-state-default",
		sSortDesc: "ui-state-default",
		sSortable: "ui-state-default",
		sSortableAsc: "ui-state-default",
		sSortableDesc: "ui-state-default",
		sSortableNone: "ui-state-default",
		sSortColumn: "sorting_",
		sSortJUIAsc: "css_right ui-icon ui-icon-triangle-1-n",
		sSortJUIDesc: "css_right ui-icon ui-icon-triangle-1-s",
		sSortJUI: "css_right ui-icon ui-icon-carat-2-n-s",
		sSortJUIAscAllowed: "css_right ui-icon ui-icon-carat-1-n",
		sSortJUIDescAllowed: "css_right ui-icon ui-icon-carat-1-s",
		sSortJUIWrapper: "DataTables_sort_wrapper",
		sSortIcon: "DataTables_sort_icon",
		sScrollWrapper: "dataTables_scroll",
		sScrollHead: "dataTables_scrollHead ui-state-default",
		sScrollHeadInner: "dataTables_scrollHeadInner",
		sScrollBody: "dataTables_scrollBody",
		sScrollFoot: "dataTables_scrollFoot ui-state-default",
		sScrollFootInner: "dataTables_scrollFootInner",
		sFooterTH: "ui-state-default"
	};
	g.oPagination = {
		two_button: {
			fnInit: function(c, g, e) {
				var k, t, A;
				c.bJUI ? (k = b.createElement("a"), t = b.createElement("a"), A = b.createElement("span"), A.className = c.oClasses.sPageJUINext, t.appendChild(A), A = b.createElement("span"), A.className = c.oClasses.sPageJUIPrev, k.appendChild(A)) : (k = b.createElement("div"), t = b.createElement("div"));
				k.className = c.oClasses.sPagePrevDisabled;
				t.className = c.oClasses.sPageNextDisabled;
				k.title = c.oLanguage.oPaginate.sPrevious;
				t.title = c.oLanguage.oPaginate.sNext;
				g.appendChild(k);
				g.appendChild(t);
				a(k).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "previous") && e(c)
				});
				a(t).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "next") && e(c)
				});
				a(k).bind("selectstart.DT",
				function() {
					return ! 1
				});
				a(t).bind("selectstart.DT",
				function() {
					return ! 1
				});
				"" !== c.sTableId && "undefined" == typeof c.aanFeatures.p && (g.setAttribute("id", c.sTableId + "_paginate"), k.setAttribute("id", c.sTableId + "_previous"), t.setAttribute("id", c.sTableId + "_next"))
			},
			fnUpdate: function(a) {
				if (a.aanFeatures.p) for (var b = a.aanFeatures.p,
				c = 0,
				g = b.length; c < g; c++) 0 !== b[c].childNodes.length && (b[c].childNodes[0].className = 0 === a._iDisplayStart ? a.oClasses.sPagePrevDisabled: a.oClasses.sPagePrevEnabled, b[c].childNodes[1].className = a.fnDisplayEnd() == a.fnRecordsDisplay() ? a.oClasses.sPageNextDisabled: a.oClasses.sPageNextEnabled)
			}
		},
		iFullNumbersShowPages: 5,
		full_numbers: {
			fnInit: function(c, g, e) {
				var k = b.createElement("span"),
				t = b.createElement("span"),
				A = b.createElement("span"),
				B = b.createElement("span"),
				r = b.createElement("span");
				k.innerHTML = c.oLanguage.oPaginate.sFirst;
				t.innerHTML = c.oLanguage.oPaginate.sPrevious;
				B.innerHTML = c.oLanguage.oPaginate.sNext;
				r.innerHTML = c.oLanguage.oPaginate.sLast;
				var p = c.oClasses;
				k.className = p.sPageButton + " " + p.sPageFirst;
				t.className = p.sPageButton + " " + p.sPagePrevious;
				B.className = p.sPageButton + " " + p.sPageNext;
				r.className = p.sPageButton + " " + p.sPageLast;
				g.appendChild(k);
				g.appendChild(t);
				g.appendChild(A);
				g.appendChild(B);
				g.appendChild(r);
				a(k).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "first") && e(c)
				});
				a(t).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "previous") && e(c)
				});
				a(B).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "next") && e(c)
				});
				a(r).bind("click.DT",
				function() {
					c.oApi._fnPageChange(c, "last") && e(c)
				});
				a("span", g).bind("mousedown.DT",
				function() {
					return ! 1
				}).bind("selectstart.DT",
				function() {
					return ! 1
				});
				"" !== c.sTableId && "undefined" == typeof c.aanFeatures.p && (g.setAttribute("id", c.sTableId + "_paginate"), k.setAttribute("id", c.sTableId + "_first"), t.setAttribute("id", c.sTableId + "_previous"), B.setAttribute("id", c.sTableId + "_next"), r.setAttribute("id", c.sTableId + "_last"))
			},
			fnUpdate: function(b, c) {
				if (b.aanFeatures.p) {
					var e = g.oPagination.iFullNumbersShowPages,
					k = Math.floor(e / 2),
					t = Math.ceil(b.fnRecordsDisplay() / b._iDisplayLength),
					A = Math.ceil(b._iDisplayStart / b._iDisplayLength) + 1,
					B = "",
					r,
					p = b.oClasses;
					t < e ? (k = 1, r = t) : A <= k ? (k = 1, r = e) : A >= t - k ? (k = t - e + 1, r = t) : (k = A - Math.ceil(e / 2) + 1, r = k + e - 1);
					for (e = k; e <= r; e++) B = A != e ? B + ('<span class="' + p.sPageButton + '">' + e + "</span>") : B + ('<span class="' + p.sPageButtonActive + '">' + e + "</span>");
					r = b.aanFeatures.p;
					for (var z, q = function(a) {
						b._iDisplayStart = (1 * this.innerHTML - 1) * b._iDisplayLength;
						c(b);
						a.preventDefault()
					},
					F = function() {
						return ! 1
					},
					e = 0, k = r.length; e < k; e++) 0 !== r[e].childNodes.length && (z = a("span:eq(2)", r[e]), z.html(B), a("span", z).bind("click.DT", q).bind("mousedown.DT", F).bind("selectstart.DT", F), z = r[e].getElementsByTagName("span"), z = [z[0], z[1], z[z.length - 2], z[z.length - 1]], a(z).removeClass(p.sPageButton + " " + p.sPageButtonActive + " " + p.sPageButtonStaticDisabled), 1 == A ? (z[0].className += " " + p.sPageButtonStaticDisabled, z[1].className += " " + p.sPageButtonStaticDisabled) : (z[0].className += " " + p.sPageButton, z[1].className += " " + p.sPageButton), 0 === t || A == t || -1 == b._iDisplayLength ? (z[2].className += " " + p.sPageButtonStaticDisabled, z[3].className += " " + p.sPageButtonStaticDisabled) : (z[2].className += " " + p.sPageButton, z[3].className += " " + p.sPageButton))
				}
			}
		}
	};
	g.oSort = {
		"string-asc": function(a, b) {
			"string" != typeof a && (a = "");
			"string" != typeof b && (b = "");
			var c = a.toLowerCase(),
			g = b.toLowerCase();
			return c < g ? -1 : c > g ? 1 : 0
		},
		"string-desc": function(a, b) {
			"string" != typeof a && (a = "");
			"string" != typeof b && (b = "");
			var c = a.toLowerCase(),
			g = b.toLowerCase();
			return c < g ? 1 : c > g ? -1 : 0
		},
		"html-asc": function(a, b) {
			var c = a.replace(/<.*?>/g, "").toLowerCase(),
			g = b.replace(/<.*?>/g, "").toLowerCase();
			return c < g ? -1 : c > g ? 1 : 0
		},
		"html-desc": function(a, b) {
			var c = a.replace(/<.*?>/g, "").toLowerCase(),
			g = b.replace(/<.*?>/g, "").toLowerCase();
			return c < g ? 1 : c > g ? -1 : 0
		},
		"date-asc": function(a, b) {
			var c = Date.parse(a),
			g = Date.parse(b);
			if (isNaN(c) || "" === c) c = Date.parse("01/01/1970 00:00:00");
			if (isNaN(g) || "" === g) g = Date.parse("01/01/1970 00:00:00");
			return c - g
		},
		"date-desc": function(a, b) {
			var c = Date.parse(a),
			g = Date.parse(b);
			if (isNaN(c) || "" === c) c = Date.parse("01/01/1970 00:00:00");
			if (isNaN(g) || "" === g) g = Date.parse("01/01/1970 00:00:00");
			return g - c
		},
		"numeric-asc": function(a, b) {
			return ("-" == a || "" === a ? 0 : 1 * a) - ("-" == b || "" === b ? 0 : 1 * b)
		},
		"numeric-desc": function(a, b) {
			return ("-" == b || "" === b ? 0 : 1 * b) - ("-" == a || "" === a ? 0 : 1 * a)
		}
	};
	g.aTypes = [function(a) {
		if ("number" == typeof a) return "numeric";
		if ("string" != typeof a) return null;
		var b, c = !1;
		b = a.charAt(0);
		if ( - 1 == "0123456789-".indexOf(b)) return null;
		for (var g = 1; g < a.length; g++) {
			b = a.charAt(g);
			if ( - 1 == "0123456789.".indexOf(b)) return null;
			if ("." == b) {
				if (c) return null;
				c = !0
			}
		}
		return "numeric"
	},
	function(a) {
		var b = Date.parse(a);
		return null !== b && !isNaN(b) || "string" == typeof a && 0 === a.length ? "date": null
	},
	function(a) {
		return "string" == typeof a && -1 != a.indexOf("<") && -1 != a.indexOf(">") ? "html": null
	}];
	g.fnVersionCheck = function(a) {
		var b = function(a, b) {
			for (; a.length < b;) a += "0";
			return a
		},
		c = g.sVersion.split(".");
		a = a.split(".");
		for (var e = "",
		t = "",
		A = 0,
		B = a.length; A < B; A++) e += b(c[A], 3),
		t += b(a[A], 3);
		return parseInt(e, 10) >= parseInt(t, 10)
	};
	g._oExternConfig = {
		iNextUnique: 0
	};
	a.fn.dataTable = function(d) {
		function n() {
			this.fnRecordsTotal = function() {
				return this.oFeatures.bServerSide ? parseInt(this._iRecordsTotal, 10) : this.aiDisplayMaster.length
			};
			this.fnRecordsDisplay = function() {
				return this.oFeatures.bServerSide ? parseInt(this._iRecordsDisplay, 10) : this.aiDisplay.length
			};
			this.fnDisplayEnd = function() {
				return this.oFeatures.bServerSide ? !1 === this.oFeatures.bPaginate || -1 == this._iDisplayLength ? this._iDisplayStart + this.aiDisplay.length: Math.min(this._iDisplayStart + this._iDisplayLength, this._iRecordsDisplay) : this._iDisplayEnd
			};
			this.sInstance = this.oInstance = null;
			this.oFeatures = {
				bPaginate: !0,
				bLengthChange: !0,
				bFilter: !0,
				bSort: !0,
				bInfo: !0,
				bAutoWidth: !0,
				bProcessing: !1,
				bSortClasses: !0,
				bStateSave: !1,
				bServerSide: !1,
				bDeferRender: !1
			};
			this.oScroll = {
				sX: "",
				sXInner: "",
				sY: "",
				bCollapse: !1,
				bInfinite: !1,
				iLoadGap: 100,
				iBarWidth: 0,
				bAutoCss: !0
			};
			this.aanFeatures = [];
			this.oLanguage = {
				sProcessing: "Processing...",
				sLengthMenu: "Show _MENU_ entries",
				sZeroRecords: "No matching records found",
				sEmptyTable: "No data available in table",
				sLoadingRecords: "Loading...",
				sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
				sInfoEmpty: "Showing 0 to 0 of 0 entries",
				sInfoFiltered: "(filtered from _MAX_ total entries)",
				sInfoPostFix: "",
				sSearch: "Search:",
				sUrl: "",
				oPaginate: {
					sFirst: "First",
					sPrevious: "Previous",
					sNext: "Next",
					sLast: "Last"
				},
				fnInfoCallback: null
			};
			this.aoData = [];
			this.aiDisplay = [];
			this.aiDisplayMaster = [];
			this.aoColumns = [];
			this.aoHeader = [];
			this.aoFooter = [];
			this.iNextId = 0;
			this.asDataSearch = [];
			this.oPreviousSearch = {
				sSearch: "",
				bRegex: !1,
				bSmart: !0
			};
			this.aoPreSearchCols = [];
			this.aaSorting = [[0, "asc", 0]];
			this.aaSortingFixed = null;
			this.asStripClasses = [];
			this.asDestoryStrips = [];
			this.sDestroyWidth = 0;
			this.fnFooterCallback = this.fnHeaderCallback = this.fnRowCallback = null;
			this.aoDrawCallback = [];
			this.fnInitComplete = this.fnPreDrawCallback = null;
			this.sTableId = "";
			this.nTableWrapper = this.nTBody = this.nTFoot = this.nTHead = this.nTable = null;
			this.bInitialised = this.bDeferLoading = !1;
			this.aoOpenRows = [];
			this.sDom = "lfrtip";
			this.sPaginationType = "two_button";
			this.iCookieDuration = 7200;
			this.sCookiePrefix = "SpryMedia_DataTables_";
			this.fnCookieCallback = null;
			this.aoStateSave = [];
			this.aoStateLoad = [];
			this.sAjaxSource = this.oLoadedState = null;
			this.sAjaxDataProp = "aaData";
			this.bAjaxDataGet = !0;
			this.jqXHR = null;
			this.fnServerData = function(b, c, d, g) {
				g.jqXHR = a.ajax({
					url: b,
					data: c,
					success: d,
					dataType: "json",
					cache: !1,
					error: function(a, b) {
						"parsererror" == b && alert("DataTables warning: JSON data from server could not be parsed. This is caused by a JSON formatting error.")
					}
				})
			};
			this.fnFormatNumber = function(a) {
				if (1E3 > a) return a;
				var b = a + "";
				a = b.split("");
				for (var c = "",
				b = b.length,
				d = 0; d < b; d++) 0 === d % 3 && 0 !== d && (c = "," + c),
				c = a[b - d - 1] + c;
				return c
			};
			this.aLengthMenu = [10, 25, 50, 100];
			this.bDrawing = this.iDraw = 0;
			this.iDrawError = -1;
			this._iDisplayLength = 10;
			this._iDisplayStart = 0;
			this._iDisplayEnd = 10;
			this._iRecordsDisplay = this._iRecordsTotal = 0;
			this.bJUI = !1;
			this.oClasses = g.oStdClasses;
			this.bSortCellsTop = this.bSorted = this.bFiltered = !1;
			this.oInit = null
		}
		function j(a) {
			return function() {
				var b = [aa(this[g.iApiIndex])].concat(Array.prototype.slice.call(arguments));
				return g.oApi[a].apply(this, b)
			}
		}
		function k(a) {
			var b, c, d = a.iInitDisplayStart;
			if (!1 === a.bInitialised) setTimeout(function() {
				k(a)
			},
			200);
			else {
				sa(a);
				F(a);
				D(a, a.aoHeader);
				a.nTFoot && D(a, a.aoFooter);
				ja(a, !0);
				a.oFeatures.bAutoWidth && Ca(a);
				b = 0;
				for (c = a.aoColumns.length; b < c; b++) null !== a.aoColumns[b].sWidth && (a.aoColumns[b].nTh.style.width = Q(a.aoColumns[b].sWidth));
				a.oFeatures.bSort ? Ba(a) : a.oFeatures.bFilter ? ga(a, a.oPreviousSearch) : (a.aiDisplay = a.aiDisplayMaster.slice(), ma(a), E(a));
				null !== a.sAjaxSource && !a.oFeatures.bServerSide ? a.fnServerData.call(a.oInstance, a.sAjaxSource, [],
				function(c) {
					var g = c;
					"" !== a.sAjaxDataProp && (g = bb(a.sAjaxDataProp)(c));
					for (b = 0; b < g.length; b++) p(a, g[b]);
					a.iInitDisplayStart = d;
					a.oFeatures.bSort ? Ba(a) : (a.aiDisplay = a.aiDisplayMaster.slice(), ma(a), E(a));
					ja(a, !1);
					t(a, c)
				},
				a) : a.oFeatures.bServerSide || (ja(a, !1), t(a))
			}
		}
		function t(a, b) {
			a._bInitComplete = !0;
			"function" == typeof a.fnInitComplete && ("undefined" != typeof b ? a.fnInitComplete.call(a.oInstance, a, b) : a.fnInitComplete.call(a.oInstance, a))
		}
		function A(a, b, c) {
			O(a.oLanguage, b, "sProcessing");
			O(a.oLanguage, b, "sLengthMenu");
			O(a.oLanguage, b, "sEmptyTable");
			O(a.oLanguage, b, "sLoadingRecords");
			O(a.oLanguage, b, "sZeroRecords");
			O(a.oLanguage, b, "sInfo");
			O(a.oLanguage, b, "sInfoEmpty");
			O(a.oLanguage, b, "sInfoFiltered");
			O(a.oLanguage, b, "sInfoPostFix");
			O(a.oLanguage, b, "sSearch");
			"undefined" != typeof b.oPaginate && (O(a.oLanguage.oPaginate, b.oPaginate, "sFirst"), O(a.oLanguage.oPaginate, b.oPaginate, "sPrevious"), O(a.oLanguage.oPaginate, b.oPaginate, "sNext"), O(a.oLanguage.oPaginate, b.oPaginate, "sLast"));
			"undefined" == typeof b.sEmptyTable && "undefined" != typeof b.sZeroRecords && O(a.oLanguage, b, "sZeroRecords", "sEmptyTable");
			"undefined" == typeof b.sLoadingRecords && "undefined" != typeof b.sZeroRecords && O(a.oLanguage, b, "sZeroRecords", "sLoadingRecords");
			c && k(a)
		}
		function B(a, c) {
			var d = a.aoColumns.length,
			g = {
				sType: null,
				_bAutoType: !0,
				bVisible: !0,
				bSearchable: !0,
				bSortable: !0,
				asSorting: ["asc", "desc"],
				sSortingClass: a.oClasses.sSortable,
				sSortingClassJUI: a.oClasses.sSortJUI,
				sTitle: c ? c.innerHTML: "",
				sName: "",
				sWidth: null,
				sWidthOrig: null,
				sClass: null,
				fnRender: null,
				bUseRendered: !0,
				iDataSort: d,
				mDataProp: d,
				fnGetData: null,
				fnSetData: null,
				sSortDataType: "std",
				sDefaultContent: null,
				sContentPadding: "",
				nTh: c ? c: b.createElement("th"),
				nTf: null
			};
			a.aoColumns.push(g);
			"undefined" == typeof a.aoPreSearchCols[d] || null === a.aoPreSearchCols[d] ? a.aoPreSearchCols[d] = {
				sSearch: "",
				bRegex: !1,
				bSmart: !0
			}: ("undefined" == typeof a.aoPreSearchCols[d].bRegex && (a.aoPreSearchCols[d].bRegex = !0), "undefined" == typeof a.aoPreSearchCols[d].bSmart && (a.aoPreSearchCols[d].bSmart = !0));
			r(a, d, null)
		}
		function r(b, c, d) {
			c = b.aoColumns[c];
			"undefined" != typeof d && null !== d && ("undefined" != typeof d.sType && (c.sType = d.sType, c._bAutoType = !1), O(c, d, "bVisible"), O(c, d, "bSearchable"), O(c, d, "bSortable"), O(c, d, "sTitle"), O(c, d, "sName"), O(c, d, "sWidth"), O(c, d, "sWidth", "sWidthOrig"), O(c, d, "sClass"), O(c, d, "fnRender"), O(c, d, "bUseRendered"), O(c, d, "iDataSort"), O(c, d, "mDataProp"), O(c, d, "asSorting"), O(c, d, "sSortDataType"), O(c, d, "sDefaultContent"), O(c, d, "sContentPadding"));
			c.fnGetData = bb(c.mDataProp);
			c.fnSetData = Oa(c.mDataProp);
			b.oFeatures.bSort || (c.bSortable = !1); ! c.bSortable || -1 == a.inArray("asc", c.asSorting) && -1 == a.inArray("desc", c.asSorting) ? (c.sSortingClass = b.oClasses.sSortableNone, c.sSortingClassJUI = "") : c.bSortable || -1 == a.inArray("asc", c.asSorting) && -1 == a.inArray("desc", c.asSorting) ? (c.sSortingClass = b.oClasses.sSortable, c.sSortingClassJUI = b.oClasses.sSortJUI) : -1 != a.inArray("asc", c.asSorting) && -1 == a.inArray("desc", c.asSorting) ? (c.sSortingClass = b.oClasses.sSortableAsc, c.sSortingClassJUI = b.oClasses.sSortJUIAscAllowed) : -1 == a.inArray("asc", c.asSorting) && -1 != a.inArray("desc", c.asSorting) && (c.sSortingClass = b.oClasses.sSortableDesc, c.sSortingClassJUI = b.oClasses.sSortJUIDescAllowed)
		}
		function p(b, c) {
			var d;
			d = "number" == typeof c.length ? c.slice() : a.extend(!0, {},
			c);
			var g = b.aoData.length,
			e = {
				nTr: null,
				_iId: b.iNextId++,
				_aData: d,
				_anHidden: [],
				_sRowStripe: ""
			};
			b.aoData.push(e);
			for (var j, k = 0,
			n = b.aoColumns.length; k < n; k++) d = b.aoColumns[k],
			"function" == typeof d.fnRender && (d.bUseRendered && null !== d.mDataProp) && Wa(b, g, k, d.fnRender({
				iDataRow: g,
				iDataColumn: k,
				aData: e._aData,
				oSettings: b
			})),
			d._bAutoType && "string" != d.sType && (j = Ia(b, g, k, "type"), null !== j && "" !== j && (j = Da(j), null === d.sType ? d.sType = j: d.sType != j && (d.sType = "string")));
			b.aiDisplayMaster.push(g);
			b.oFeatures.bDeferRender || z(b, g);
			return g
		}
		function z(c, d) {
			var g = c.aoData[d],
			e;
			if (null === g.nTr) {
				g.nTr = b.createElement("tr");
				"undefined" != typeof g._aData.DT_RowId && g.nTr.setAttribute("id", g._aData.DT_RowId);
				"undefined" != typeof g._aData.DT_RowClass && a(g.nTr).addClass(g._aData.DT_RowClass);
				for (var j = 0,
				k = c.aoColumns.length; j < k; j++) {
					var n = c.aoColumns[j];
					e = b.createElement("td");
					e.innerHTML = "function" == typeof n.fnRender && (!n.bUseRendered || null === n.mDataProp) ? n.fnRender({
						iDataRow: d,
						iDataColumn: j,
						aData: g._aData,
						oSettings: c
					}) : Ia(c, d, j, "display");
					null !== n.sClass && (e.className = n.sClass);
					n.bVisible ? (g.nTr.appendChild(e), g._anHidden[j] = null) : g._anHidden[j] = e
				}
			}
		}
		function q(b) {
			var c, d, g, e, j, k, n, q, p;
			if (b.bDeferLoading || null === b.sAjaxSource) {
				n = b.nTBody.childNodes;
				c = 0;
				for (d = n.length; c < d; c++) if ("TR" == n[c].nodeName.toUpperCase()) {
					q = b.aoData.length;
					b.aoData.push({
						nTr: n[c],
						_iId: b.iNextId++,
						_aData: [],
						_anHidden: [],
						_sRowStripe: ""
					});
					b.aiDisplayMaster.push(q);
					k = n[c].childNodes;
					g = j = 0;
					for (e = k.length; g < e; g++) if (p = k[g].nodeName.toUpperCase(), "TD" == p || "TH" == p) Wa(b, q, j, a.trim(k[g].innerHTML)),
					j++
				}
			}
			n = ra(b);
			k = [];
			c = 0;
			for (d = n.length; c < d; c++) {
				g = 0;
				for (e = n[c].childNodes.length; g < e; g++) j = n[c].childNodes[g],
				p = j.nodeName.toUpperCase(),
				("TD" == p || "TH" == p) && k.push(j)
			}
			k.length != n.length * b.aoColumns.length && Ea(b, 1, "Unexpected number of TD elements. Expected " + n.length * b.aoColumns.length + " and got " + k.length + ". DataTables does not support rowspan / colspan in the table body, and there must be one cell for each row/column combination.");
			g = 0;
			for (e = b.aoColumns.length; g < e; g++) {
				null === b.aoColumns[g].sTitle && (b.aoColumns[g].sTitle = b.aoColumns[g].nTh.innerHTML);
				n = b.aoColumns[g]._bAutoType;
				p = "function" == typeof b.aoColumns[g].fnRender;
				j = null !== b.aoColumns[g].sClass;
				q = b.aoColumns[g].bVisible;
				var r, t;
				if (n || p || j || !q) {
					c = 0;
					for (d = b.aoData.length; c < d; c++) r = k[c * e + g],
					n && "string" != b.aoColumns[g].sType && (t = Ia(b, c, g, "type"), "" !== t && (t = Da(t), null === b.aoColumns[g].sType ? b.aoColumns[g].sType = t: b.aoColumns[g].sType != t && (b.aoColumns[g].sType = "string"))),
					p && (t = b.aoColumns[g].fnRender({
						iDataRow: c,
						iDataColumn: g,
						aData: b.aoData[c]._aData,
						oSettings: b
					}), r.innerHTML = t, b.aoColumns[g].bUseRendered && Wa(b, c, g, t)),
					j && (r.className += " " + b.aoColumns[g].sClass),
					q ? b.aoData[c]._anHidden[g] = null: (b.aoData[c]._anHidden[g] = r, r.parentNode.removeChild(r))
				}
			}
		}
		function F(c) {
			var d, g, e;
			c.nTHead.getElementsByTagName("tr");
			if (0 !== c.nTHead.getElementsByTagName("th").length) {
				d = 0;
				for (e = c.aoColumns.length; d < e; d++) g = c.aoColumns[d].nTh,
				null !== c.aoColumns[d].sClass && a(g).addClass(c.aoColumns[d].sClass),
				c.aoColumns[d].sTitle != g.innerHTML && (g.innerHTML = c.aoColumns[d].sTitle)
			} else {
				var j = b.createElement("tr");
				d = 0;
				for (e = c.aoColumns.length; d < e; d++) g = c.aoColumns[d].nTh,
				g.innerHTML = c.aoColumns[d].sTitle,
				null !== c.aoColumns[d].sClass && a(g).addClass(c.aoColumns[d].sClass),
				j.appendChild(g);
				a(c.nTHead).html("")[0].appendChild(j);
				Z(c.aoHeader, c.nTHead)
			}
			if (c.bJUI) {
				d = 0;
				for (e = c.aoColumns.length; d < e; d++) {
					g = c.aoColumns[d].nTh;
					j = b.createElement("div");
					j.className = c.oClasses.sSortJUIWrapper;
					a(g).contents().appendTo(j);
					var k = b.createElement("span");
					k.className = c.oClasses.sSortIcon;
					j.appendChild(k);
					g.appendChild(j)
				}
			}
			e = function() {
				this.onselectstart = function() {
					return ! 1
				};
				return ! 1
			};
			if (c.oFeatures.bSort) for (d = 0; d < c.aoColumns.length; d++) ! 1 !== c.aoColumns[d].bSortable ? (qa(c, c.aoColumns[d].nTh, d), a(c.aoColumns[d].nTh).bind("mousedown.DT", e)) : a(c.aoColumns[d].nTh).addClass(c.oClasses.sSortableNone);
			"" !== c.oClasses.sFooterTH && a(">tr>th", c.nTFoot).addClass(c.oClasses.sFooterTH);
			if (null !== c.nTFoot) {
				g = Na(c, null, c.aoFooter);
				d = 0;
				for (e = c.aoColumns.length; d < e; d++)"undefined" != typeof g[d] && (c.aoColumns[d].nTf = g[d])
			}
		}
		function D(a, b, c) {
			var d, g, e, j = [],
			k = [],
			n = a.aoColumns.length;
			"undefined" == typeof c && (c = !1);
			d = 0;
			for (g = b.length; d < g; d++) {
				j[d] = b[d].slice();
				j[d].nTr = b[d].nTr;
				for (e = n - 1; 0 <= e; e--) ! a.aoColumns[e].bVisible && !c && j[d].splice(e, 1);
				k.push([])
			}
			d = 0;
			for (g = j.length; d < g; d++) {
				if (j[d].nTr) {
					a = 0;
					for (e = j[d].nTr.childNodes.length; a < e; a++) j[d].nTr.removeChild(j[d].nTr.childNodes[0])
				}
				e = 0;
				for (b = j[d].length; e < b; e++) if (n = c = 1, "undefined" == typeof k[d][e]) {
					j[d].nTr.appendChild(j[d][e].cell);
					for (k[d][e] = 1;
					"undefined" != typeof j[d + c] && j[d][e].cell == j[d + c][e].cell;) k[d + c][e] = 1,
					c++;
					for (;
					"undefined" != typeof j[d][e + n] && j[d][e].cell == j[d][e + n].cell;) {
						for (a = 0; a < c; a++) k[d + a][e + n] = 1;
						n++
					}
					j[d][e].cell.setAttribute("rowspan", c);
					j[d][e].cell.setAttribute("colspan", n)
				}
			}
		}
		function E(c) {
			var d, g, e = [],
			j = 0,
			k = !1;
			d = c.asStripClasses.length;
			g = c.aoOpenRows.length;
			if (! (null !== c.fnPreDrawCallback && !1 === c.fnPreDrawCallback.call(c.oInstance, c))) {
				c.bDrawing = !0;
				"undefined" != typeof c.iInitDisplayStart && -1 != c.iInitDisplayStart && (c._iDisplayStart = c.oFeatures.bServerSide ? c.iInitDisplayStart: c.iInitDisplayStart >= c.fnRecordsDisplay() ? 0 : c.iInitDisplayStart, c.iInitDisplayStart = -1, ma(c));
				if (c.bDeferLoading) c.bDeferLoading = !1,
				c.iDraw++;
				else if (c.oFeatures.bServerSide) {
					if (!c.bDestroying && !ha(c)) return
				} else c.iDraw++;
				if (0 !== c.aiDisplay.length) {
					var n = c._iDisplayStart,
					q = c._iDisplayEnd;
					c.oFeatures.bServerSide && (n = 0, q = c.aoData.length);
					for (; n < q; n++) {
						var p = c.aoData[c.aiDisplay[n]];
						null === p.nTr && z(c, c.aiDisplay[n]);
						var r = p.nTr;
						if (0 !== d) {
							var A = c.asStripClasses[j % d];
							p._sRowStripe != A && (a(r).removeClass(p._sRowStripe).addClass(A), p._sRowStripe = A)
						}
						"function" == typeof c.fnRowCallback && (r = c.fnRowCallback.call(c.oInstance, r, c.aoData[c.aiDisplay[n]]._aData, j, n), !r && !k && (Ea(c, 0, "A node was not returned by fnRowCallback"), k = !0));
						e.push(r);
						j++;
						if (0 !== g) for (p = 0; p < g; p++) r == c.aoOpenRows[p].nParent && e.push(c.aoOpenRows[p].nTr)
					}
				} else e[0] = b.createElement("tr"),
				"undefined" != typeof c.asStripClasses[0] && (e[0].className = c.asStripClasses[0]),
				k = c.oLanguage.sZeroRecords.replace("_MAX_", c.fnFormatNumber(c.fnRecordsTotal())),
				1 == c.iDraw && null !== c.sAjaxSource && !c.oFeatures.bServerSide ? k = c.oLanguage.sLoadingRecords: "undefined" != typeof c.oLanguage.sEmptyTable && 0 === c.fnRecordsTotal() && (k = c.oLanguage.sEmptyTable),
				d = b.createElement("td"),
				d.setAttribute("valign", "top"),
				d.colSpan = Aa(c),
				d.className = c.oClasses.sRowEmpty,
				d.innerHTML = k,
				e[j].appendChild(d);
				"function" == typeof c.fnHeaderCallback && c.fnHeaderCallback.call(c.oInstance, a(">tr", c.nTHead)[0], T(c), c._iDisplayStart, c.fnDisplayEnd(), c.aiDisplay);
				"function" == typeof c.fnFooterCallback && c.fnFooterCallback.call(c.oInstance, a(">tr", c.nTFoot)[0], T(c), c._iDisplayStart, c.fnDisplayEnd(), c.aiDisplay);
				j = b.createDocumentFragment();
				d = b.createDocumentFragment();
				if (c.nTBody) {
					k = c.nTBody.parentNode;
					d.appendChild(c.nTBody);
					if (!c.oScroll.bInfinite || !c._bInitComplete || c.bSorted || c.bFiltered) {
						g = c.nTBody.childNodes;
						for (d = g.length - 1; 0 <= d; d--) g[d].parentNode.removeChild(g[d])
					}
					d = 0;
					for (g = e.length; d < g; d++) j.appendChild(e[d]);
					c.nTBody.appendChild(j);
					null !== k && k.appendChild(c.nTBody)
				}
				for (d = c.aoDrawCallback.length - 1; 0 <= d; d--) c.aoDrawCallback[d].fn.call(c.oInstance, c);
				c.bSorted = !1;
				c.bFiltered = !1;
				c.bDrawing = !1;
				c.oFeatures.bServerSide && (ja(c, !1), "undefined" == typeof c._bInitComplete && t(c))
			}
		}
		function V(a) {
			a.oFeatures.bSort ? Ba(a, a.oPreviousSearch) : a.oFeatures.bFilter ? ga(a, a.oPreviousSearch) : (ma(a), E(a))
		}
		function ha(a) {
			if (a.bAjaxDataGet) {
				ja(a, !0);
				var b = a.aoColumns.length,
				c = [],
				d,
				g;
				a.iDraw++;
				c.push({
					name: "sEcho",
					value: a.iDraw
				});
				c.push({
					name: "iColumns",
					value: b
				});
				c.push({
					name: "sColumns",
					value: Ua(a)
				});
				c.push({
					name: "iDisplayStart",
					value: a._iDisplayStart
				});
				c.push({
					name: "iDisplayLength",
					value: !1 !== a.oFeatures.bPaginate ? a._iDisplayLength: -1
				});
				for (g = 0; g < b; g++) d = a.aoColumns[g].mDataProp,
				c.push({
					name: "mDataProp_" + g,
					value: "function" == typeof d ? "function": d
				});
				if (!1 !== a.oFeatures.bFilter) {
					c.push({
						name: "sSearch",
						value: a.oPreviousSearch.sSearch
					});
					c.push({
						name: "bRegex",
						value: a.oPreviousSearch.bRegex
					});
					for (g = 0; g < b; g++) c.push({
						name: "sSearch_" + g,
						value: a.aoPreSearchCols[g].sSearch
					}),
					c.push({
						name: "bRegex_" + g,
						value: a.aoPreSearchCols[g].bRegex
					}),
					c.push({
						name: "bSearchable_" + g,
						value: a.aoColumns[g].bSearchable
					})
				}
				if (!1 !== a.oFeatures.bSort) {
					d = null !== a.aaSortingFixed ? a.aaSortingFixed.length: 0;
					var e = a.aaSorting.length;
					c.push({
						name: "iSortingCols",
						value: d + e
					});
					for (g = 0; g < d; g++) c.push({
						name: "iSortCol_" + g,
						value: a.aaSortingFixed[g][0]
					}),
					c.push({
						name: "sSortDir_" + g,
						value: a.aaSortingFixed[g][1]
					});
					for (g = 0; g < e; g++) c.push({
						name: "iSortCol_" + (g + d),
						value: a.aaSorting[g][0]
					}),
					c.push({
						name: "sSortDir_" + (g + d),
						value: a.aaSorting[g][1]
					});
					for (g = 0; g < b; g++) c.push({
						name: "bSortable_" + g,
						value: a.aoColumns[g].bSortable
					})
				}
				a.fnServerData.call(a.oInstance, a.sAjaxSource, c,
				function(b) {
					W(a, b)
				},
				a);
				return ! 1
			}
			return ! 0
		}
		function W(a, b) {
			if ("undefined" != typeof b.sEcho) {
				if (1 * b.sEcho < a.iDraw) return;
				a.iDraw = 1 * b.sEcho
			} (!a.oScroll.bInfinite || a.oScroll.bInfinite && (a.bSorted || a.bFiltered)) && Ha(a);
			a._iRecordsTotal = b.iTotalRecords;
			a._iRecordsDisplay = b.iTotalDisplayRecords;
			var c = Ua(a);
			if (c = "undefined" != typeof b.sColumns && "" !== c && b.sColumns != c) var d = oa(a, b.sColumns);
			for (var g = bb(a.sAjaxDataProp)(b), e = 0, j = g.length; e < j; e++) if (c) {
				for (var k = [], n = 0, q = a.aoColumns.length; n < q; n++) k.push(g[e][d[n]]);
				p(a, k)
			} else p(a, g[e]);
			a.aiDisplay = a.aiDisplayMaster.slice();
			a.bAjaxDataGet = !1;
			E(a);
			a.bAjaxDataGet = !0;
			ja(a, !1)
		}
		function sa(a) {
			var c = b.createElement("div");
			a.nTable.parentNode.insertBefore(c, a.nTable);
			a.nTableWrapper = b.createElement("div");
			a.nTableWrapper.className = a.oClasses.sWrapper;
			"" !== a.sTableId && a.nTableWrapper.setAttribute("id", a.sTableId + "_wrapper");
			a.nTableReinsertBefore = a.nTable.nextSibling;
			for (var d = a.nTableWrapper,
			e = a.sDom.split(""), j, k, n, p, q, r, t, z = 0; z < e.length; z++) {
				k = 0;
				n = e[z];
				if ("<" == n) {
					p = b.createElement("div");
					q = e[z + 1];
					if ("'" == q || '"' == q) {
						r = "";
						for (t = 2; e[z + t] != q;) r += e[z + t],
						t++;
						"H" == r ? r = "fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix": "F" == r && (r = "fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix"); - 1 != r.indexOf(".") ? (q = r.split("."), p.setAttribute("id", q[0].substr(1, q[0].length - 1)), p.className = q[1]) : "#" == r.charAt(0) ? p.setAttribute("id", r.substr(1, r.length - 1)) : p.className = r;
						z += t
					}
					d.appendChild(p);
					d = p
				} else if (">" == n) d = d.parentNode;
				else if ("l" == n && a.oFeatures.bPaginate && a.oFeatures.bLengthChange) j = C(a),
				k = 1;
				else if ("f" == n && a.oFeatures.bFilter) j = ia(a),
				k = 1;
				else if ("r" == n && a.oFeatures.bProcessing) j = M(a),
				k = 1;
				else if ("t" == n) j = S(a),
				k = 1;
				else if ("i" == n && a.oFeatures.bInfo) j = la(a),
				k = 1;
				else if ("p" == n && a.oFeatures.bPaginate) j = w(a),
				k = 1;
				else if (0 !== g.aoFeatures.length) {
					p = g.aoFeatures;
					t = 0;
					for (q = p.length; t < q; t++) if (n == p[t].cFeature) { (j = p[t].fnInit(a)) && (k = 1);
						break
					}
				}
				1 == k && null !== j && ("object" != typeof a.aanFeatures[n] && (a.aanFeatures[n] = []), a.aanFeatures[n].push(j), d.appendChild(j))
			}
			c.parentNode.replaceChild(a.nTableWrapper, c)
		}
		function S(c) {
			if ("" === c.oScroll.sX && "" === c.oScroll.sY) return c.nTable;
			var e = b.createElement("div"),
			j = b.createElement("div"),
			k = b.createElement("div"),
			n = b.createElement("div"),
			p = b.createElement("div"),
			q = b.createElement("div"),
			r = c.nTable.cloneNode(!1),
			t = c.nTable.cloneNode(!1),
			z = c.nTable.getElementsByTagName("thead")[0],
			A = 0 === c.nTable.getElementsByTagName("tfoot").length ? null: c.nTable.getElementsByTagName("tfoot")[0],
			w = "undefined" != typeof d.bJQueryUI && d.bJQueryUI ? g.oJUIClasses: g.oStdClasses;
			j.appendChild(k);
			p.appendChild(q);
			n.appendChild(c.nTable);
			e.appendChild(j);
			e.appendChild(n);
			k.appendChild(r);
			r.appendChild(z);
			null !== A && (e.appendChild(p), q.appendChild(t), t.appendChild(A));
			e.className = w.sScrollWrapper;
			j.className = w.sScrollHead;
			k.className = w.sScrollHeadInner;
			n.className = w.sScrollBody;
			p.className = w.sScrollFoot;
			q.className = w.sScrollFootInner;
			c.oScroll.bAutoCss && (j.style.overflow = "hidden", j.style.position = "relative", p.style.overflow = "hidden", n.style.overflow = "auto");
			j.style.border = "0";
			j.style.width = "100%";
			p.style.border = "0";
			k.style.width = "150%";
			r.removeAttribute("id");
			r.style.marginLeft = "0";
			c.nTable.style.marginLeft = "0";
			null !== A && (t.removeAttribute("id"), t.style.marginLeft = "0");
			k = a(">caption", c.nTable);
			q = 0;
			for (t = k.length; q < t; q++) r.appendChild(k[q]);
			"" !== c.oScroll.sX && (j.style.width = Q(c.oScroll.sX), n.style.width = Q(c.oScroll.sX), null !== A && (p.style.width = Q(c.oScroll.sX)), a(n).scroll(function() {
				j.scrollLeft = this.scrollLeft;
				null !== A && (p.scrollLeft = this.scrollLeft)
			}));
			"" !== c.oScroll.sY && (n.style.height = Q(c.oScroll.sY));
			c.aoDrawCallback.push({
				fn: U,
				sName: "scrolling"
			});
			c.oScroll.bInfinite && a(n).scroll(function() { ! c.bDrawing && (a(this).scrollTop() + a(this).height() > a(c.nTable).height() - c.oScroll.iLoadGap && c.fnDisplayEnd() < c.fnRecordsDisplay()) && (R(c, "next"), ma(c), E(c))
			});
			c.nScrollHead = j;
			c.nScrollFoot = p;
			return e
		}
		function U(b) {
			var c = b.nScrollHead.getElementsByTagName("div")[0],
			d = c.getElementsByTagName("table")[0],
			g = b.nTable.parentNode,
			e,
			j,
			k,
			n,
			p,
			q,
			r,
			t,
			z = [];
			k = b.nTable.getElementsByTagName("thead");
			0 < k.length && b.nTable.removeChild(k[0]);
			null !== b.nTFoot && (p = b.nTable.getElementsByTagName("tfoot"), 0 < p.length && b.nTable.removeChild(p[0]));
			k = b.nTHead.cloneNode(!0);
			b.nTable.insertBefore(k, b.nTable.childNodes[0]);
			null !== b.nTFoot && (p = b.nTFoot.cloneNode(!0), b.nTable.insertBefore(p, b.nTable.childNodes[1]));
			"" === b.oScroll.sX && (g.style.width = "100%", c.parentNode.style.width = "100%");
			var A = Na(b, k);
			e = 0;
			for (j = A.length; e < j; e++) r = za(b, e),
			A[e].style.width = b.aoColumns[r].sWidth;
			null !== b.nTFoot && Va(function(a) {
				a.style.width = ""
			},
			p.getElementsByTagName("tr"));
			e = a(b.nTable).outerWidth();
			"" === b.oScroll.sX ? (b.nTable.style.width = "100%", a.browser.msie && 7 >= a.browser.version && (b.nTable.style.width = Q(a(b.nTable).outerWidth() - b.oScroll.iBarWidth))) : "" !== b.oScroll.sXInner ? b.nTable.style.width = Q(b.oScroll.sXInner) : e == a(g).width() && a(g).height() < a(b.nTable).height() ? (b.nTable.style.width = Q(e - b.oScroll.iBarWidth), a(b.nTable).outerWidth() > e - b.oScroll.iBarWidth && (b.nTable.style.width = Q(e))) : b.nTable.style.width = Q(e);
			e = a(b.nTable).outerWidth();
			"" === b.oScroll.sX && (g.style.width = Q(e + b.oScroll.iBarWidth), c.parentNode.style.width = Q(e + b.oScroll.iBarWidth));
			j = b.nTHead.getElementsByTagName("tr");
			k = k.getElementsByTagName("tr");
			Va(function(b, c) {
				q = b.style;
				q.paddingTop = "0";
				q.paddingBottom = "0";
				q.borderTopWidth = "0";
				q.borderBottomWidth = "0";
				q.height = 0;
				t = a(b).width();
				c.style.width = Q(t);
				z.push(t)
			},
			k, j);
			a(k).height(0);
			null !== b.nTFoot && (n = p.getElementsByTagName("tr"), p = b.nTFoot.getElementsByTagName("tr"), Va(function(b, c) {
				q = b.style;
				q.paddingTop = "0";
				q.paddingBottom = "0";
				q.borderTopWidth = "0";
				q.borderBottomWidth = "0";
				q.height = 0;
				t = a(b).width();
				c.style.width = Q(t);
				z.push(t)
			},
			n, p), a(n).height(0));
			Va(function(a) {
				a.innerHTML = "";
				a.style.width = Q(z.shift())
			},
			k);
			null !== b.nTFoot && Va(function(a) {
				a.innerHTML = "";
				a.style.width = Q(z.shift())
			},
			n);
			a(b.nTable).outerWidth() < e && ("" === b.oScroll.sX ? Ea(b, 1, "The table cannot fit into the current element which will cause column misalignment. It is suggested that you enable x-scrolling or increase the width the table has in which to be drawn") : "" !== b.oScroll.sXInner && Ea(b, 1, "The table cannot fit into the current element which will cause column misalignment. It is suggested that you increase the sScrollXInner property to allow it to draw in a larger area, or simply remove that parameter to allow automatic calculation"));
			"" === b.oScroll.sY && (a.browser.msie && 7 >= a.browser.version) && (g.style.height = Q(b.nTable.offsetHeight + b.oScroll.iBarWidth));
			"" !== b.oScroll.sY && b.oScroll.bCollapse && (g.style.height = Q(b.oScroll.sY), n = "" !== b.oScroll.sX && b.nTable.offsetWidth > g.offsetWidth ? b.oScroll.iBarWidth: 0, b.nTable.offsetHeight < g.offsetHeight && (g.style.height = Q(a(b.nTable).height() + n)));
			n = a(b.nTable).outerWidth();
			d.style.width = Q(n);
			c.style.width = Q(n + b.oScroll.iBarWidth);
			null !== b.nTFoot && (c = b.nScrollFoot.getElementsByTagName("div")[0], d = c.getElementsByTagName("table")[0], c.style.width = Q(b.nTable.offsetWidth + b.oScroll.iBarWidth), d.style.width = Q(b.nTable.offsetWidth));
			if (b.bSorted || b.bFiltered) g.scrollTop = 0
		}
		function ba(a) {
			if (!1 === a.oFeatures.bAutoWidth) return ! 1;
			Ca(a);
			for (var b = 0,
			c = a.aoColumns.length; b < c; b++) a.aoColumns[b].nTh.style.width = a.aoColumns[b].sWidth
		}
		function ia(c) {
			var d = c.oLanguage.sSearch,
			d = -1 !== d.indexOf("_INPUT_") ? d.replace("_INPUT_", '<input type="text" />') : "" === d ? '<input type="text" />': d + ' <input type="text" />',
			g = b.createElement("div");
			g.className = c.oClasses.sFilter;
			g.innerHTML = "<label>" + d + "</label>";
			"" !== c.sTableId && "undefined" == typeof c.aanFeatures.f && g.setAttribute("id", c.sTableId + "_filter");
			d = a("input", g);
			d.val(c.oPreviousSearch.sSearch.replace('"', "&quot;"));
			d.bind("keyup.DT",
			function() {
				for (var b = c.aanFeatures.f,
				d = 0,
				g = b.length; d < g; d++) b[d] != this.parentNode && a("input", b[d]).val(this.value);
				this.value != c.oPreviousSearch.sSearch && ga(c, {
					sSearch: this.value,
					bRegex: c.oPreviousSearch.bRegex,
					bSmart: c.oPreviousSearch.bSmart
				})
			});
			d.bind("keypress.DT",
			function(a) {
				if (13 == a.keyCode) return ! 1
			});
			return g
		}
		function ga(a, b, c) {
			ua(a, b.sSearch, c, b.bRegex, b.bSmart);
			for (b = 0; b < a.aoPreSearchCols.length; b++) xa(a, a.aoPreSearchCols[b].sSearch, b, a.aoPreSearchCols[b].bRegex, a.aoPreSearchCols[b].bSmart);
			0 !== g.afnFiltering.length && Y(a);
			a.bFiltered = !0;
			a._iDisplayStart = 0;
			ma(a);
			E(a);
			ta(a, 0)
		}
		function Y(a) {
			for (var b = g.afnFiltering,
			c = 0,
			d = b.length; c < d; c++) for (var e = 0,
			j = 0,
			k = a.aiDisplay.length; j < k; j++) {
				var n = a.aiDisplay[j - e];
				b[c](a, ab(a, n, "filter"), n) || (a.aiDisplay.splice(j - e, 1), e++)
			}
		}
		function xa(a, b, c, d, g) {
			if ("" !== b) {
				var e = 0;
				b = N(b, d, g);
				for (d = a.aiDisplay.length - 1; 0 <= d; d--) g = na(Ia(a, a.aiDisplay[d], c, "filter"), a.aoColumns[c].sType),
				b.test(g) || (a.aiDisplay.splice(d, 1), e++)
			}
		}
		function ua(a, b, c, d, e) {
			var j = N(b, d, e);
			if ("undefined" == typeof c || null === c) c = 0;
			0 !== g.afnFiltering.length && (c = 1);
			if (0 >= b.length) a.aiDisplay.splice(0, a.aiDisplay.length),
			a.aiDisplay = a.aiDisplayMaster.slice();
			else if (a.aiDisplay.length == a.aiDisplayMaster.length || a.oPreviousSearch.sSearch.length > b.length || 1 == c || 0 !== b.indexOf(a.oPreviousSearch.sSearch)) {
				a.aiDisplay.splice(0, a.aiDisplay.length);
				ta(a, 1);
				for (c = 0; c < a.aiDisplayMaster.length; c++) j.test(a.asDataSearch[c]) && a.aiDisplay.push(a.aiDisplayMaster[c])
			} else {
				var k = 0;
				for (c = 0; c < a.asDataSearch.length; c++) j.test(a.asDataSearch[c]) || (a.aiDisplay.splice(c - k, 1), k++)
			}
			a.oPreviousSearch.sSearch = b;
			a.oPreviousSearch.bRegex = d;
			a.oPreviousSearch.bSmart = e
		}
		function ta(a, b) {
			a.asDataSearch.splice(0, a.asDataSearch.length);
			for (var c = "undefined" != typeof b && 1 == b ? a.aiDisplayMaster: a.aiDisplay, d = 0, g = c.length; d < g; d++) a.asDataSearch[d] = va(a, ab(a, c[d], "filter"))
		}
		function va(a, c) {
			var d = "";
			"undefined" == typeof a.__nTmpFilter && (a.__nTmpFilter = b.createElement("div"));
			for (var g = a.__nTmpFilter,
			e = 0,
			j = a.aoColumns.length; e < j; e++) a.aoColumns[e].bSearchable && (d += na(c[e], a.aoColumns[e].sType) + "  "); - 1 !== d.indexOf("&") && (g.innerHTML = d, d = g.textContent ? g.textContent: g.innerText, d = d.replace(/\n/g, " ").replace(/\r/g, ""));
			return d
		}
		function N(a, b, c) {
			if (c) return a = b ? a.split(" ") : pa(a).split(" "),
			a = "^(?=.*?" + a.join(")(?=.*?") + ").*$",
			RegExp(a, "i");
			a = b ? a: pa(a);
			return RegExp(a, "i")
		}
		function na(a, b) {
			return "function" == typeof g.ofnSearch[b] ? g.ofnSearch[b](a) : "html" == b ? a.replace(/\n/g, " ").replace(/<.*?>/g, "") : "string" == typeof a ? a.replace(/\n/g, " ") : null === a ? "": a
		}
		function Ba(a, b) {
			var c, d, e, j, k = [],
			n = [],
			p = g.oSort;
			d = a.aoData;
			var q = a.aoColumns;
			if (!a.oFeatures.bServerSide && (0 !== a.aaSorting.length || null !== a.aaSortingFixed)) {
				k = null !== a.aaSortingFixed ? a.aaSortingFixed.concat(a.aaSorting) : a.aaSorting.slice();
				for (c = 0; c < k.length; c++) {
					var r = k[c][0];
					e = J(a, r);
					j = a.aoColumns[r].sSortDataType;
					if ("undefined" != typeof g.afnSortData[j]) {
						var t = g.afnSortData[j](a, r, e);
						e = 0;
						for (j = d.length; e < j; e++) Wa(a, e, r, t[e])
					}
				}
				c = 0;
				for (d = a.aiDisplayMaster.length; c < d; c++) n[a.aiDisplayMaster[c]] = c;
				var z = k.length;
				a.aiDisplayMaster.sort(function(b, d) {
					var g, e;
					for (c = 0; c < z; c++) if (g = q[k[c][0]].iDataSort, e = q[g].sType, g = p[(e ? e: "string") + "-" + k[c][1]](Ia(a, b, g, "sort"), Ia(a, d, g, "sort")), 0 !== g) return g;
					return p["numeric-asc"](n[b], n[d])
				})
			} ("undefined" == typeof b || b) && !a.oFeatures.bDeferRender && G(a);
			a.bSorted = !0;
			a.oFeatures.bFilter ? ga(a, a.oPreviousSearch, 1) : (a.aiDisplay = a.aiDisplayMaster.slice(), a._iDisplayStart = 0, ma(a), E(a))
		}
		function qa(b, c, d, g) {
			a(c).bind("click.DT",
			function(a) {
				if (!1 !== b.aoColumns[d].bSortable) {
					var c = function() {
						var c, g;
						if (a.shiftKey) {
							for (var e = !1,
							j = 0; j < b.aaSorting.length; j++) if (b.aaSorting[j][0] == d) {
								e = !0;
								c = b.aaSorting[j][0];
								g = b.aaSorting[j][2] + 1;
								"undefined" == typeof b.aoColumns[c].asSorting[g] ? b.aaSorting.splice(j, 1) : (b.aaSorting[j][1] = b.aoColumns[c].asSorting[g], b.aaSorting[j][2] = g);
								break
							} ! 1 === e && b.aaSorting.push([d, b.aoColumns[d].asSorting[0], 0])
						} else 1 == b.aaSorting.length && b.aaSorting[0][0] == d ? (c = b.aaSorting[0][0], g = b.aaSorting[0][2] + 1, "undefined" == typeof b.aoColumns[c].asSorting[g] && (g = 0), b.aaSorting[0][1] = b.aoColumns[c].asSorting[g], b.aaSorting[0][2] = g) : (b.aaSorting.splice(0, b.aaSorting.length), b.aaSorting.push([d, b.aoColumns[d].asSorting[0], 0]));
						Ba(b)
					};
					b.oFeatures.bProcessing ? (ja(b, !0), setTimeout(function() {
						c();
						b.oFeatures.bServerSide || ja(b, !1)
					},
					0)) : c();
					"function" == typeof g && g(b)
				}
			})
		}
		function G(b) {
			var c, d, g, e, j, k = b.aoColumns.length,
			n = b.oClasses;
			for (c = 0; c < k; c++) b.aoColumns[c].bSortable && a(b.aoColumns[c].nTh).removeClass(n.sSortAsc + " " + n.sSortDesc + " " + b.aoColumns[c].sSortingClass);
			e = null !== b.aaSortingFixed ? b.aaSortingFixed.concat(b.aaSorting) : b.aaSorting.slice();
			for (c = 0; c < b.aoColumns.length; c++) if (b.aoColumns[c].bSortable) {
				j = b.aoColumns[c].sSortingClass;
				g = -1;
				for (d = 0; d < e.length; d++) if (e[d][0] == c) {
					j = "asc" == e[d][1] ? n.sSortAsc: n.sSortDesc;
					g = d;
					break
				}
				a(b.aoColumns[c].nTh).addClass(j);
				b.bJUI && (d = a("span", b.aoColumns[c].nTh), d.removeClass(n.sSortJUIAsc + " " + n.sSortJUIDesc + " " + n.sSortJUI + " " + n.sSortJUIAscAllowed + " " + n.sSortJUIDescAllowed), d.addClass( - 1 == g ? b.aoColumns[c].sSortingClassJUI: "asc" == e[g][1] ? n.sSortJUIAsc: n.sSortJUIDesc))
			} else a(b.aoColumns[c].nTh).addClass(b.aoColumns[c].sSortingClass);
			j = n.sSortColumn;
			if (b.oFeatures.bSort && b.oFeatures.bSortClasses) {
				g = da(b);
				if (b.oFeatures.bDeferRender) a(g).removeClass(j + "1 " + j + "2 " + j + "3");
				else if (g.length >= k) for (c = 0; c < k; c++) if ( - 1 != g[c].className.indexOf(j + "1")) {
					d = 0;
					for (b = g.length / k; d < b; d++) g[k * d + c].className = a.trim(g[k * d + c].className.replace(j + "1", ""))
				} else if ( - 1 != g[c].className.indexOf(j + "2")) {
					d = 0;
					for (b = g.length / k; d < b; d++) g[k * d + c].className = a.trim(g[k * d + c].className.replace(j + "2", ""))
				} else if ( - 1 != g[c].className.indexOf(j + "3")) {
					d = 0;
					for (b = g.length / k; d < b; d++) g[k * d + c].className = a.trim(g[k * d + c].className.replace(" " + j + "3", ""))
				}
				var n = 1,
				p;
				for (c = 0; c < e.length; c++) {
					p = parseInt(e[c][0], 10);
					d = 0;
					for (b = g.length / k; d < b; d++) g[k * d + p].className += " " + j + n;
					3 > n && n++
				}
			}
		}
		function w(a) {
			if (a.oScroll.bInfinite) return null;
			var c = b.createElement("div");
			c.className = a.oClasses.sPaging + a.sPaginationType;
			g.oPagination[a.sPaginationType].fnInit(a, c,
			function(a) {
				ma(a);
				E(a)
			});
			"undefined" == typeof a.aanFeatures.p && a.aoDrawCallback.push({
				fn: function(a) {
					g.oPagination[a.sPaginationType].fnUpdate(a,
					function(a) {
						ma(a);
						E(a)
					})
				},
				sName: "pagination"
			});
			return c
		}
		function R(a, b) {
			var c = a._iDisplayStart;
			if ("first" == b) a._iDisplayStart = 0;
			else if ("previous" == b) a._iDisplayStart = 0 <= a._iDisplayLength ? a._iDisplayStart - a._iDisplayLength: 0,
			0 > a._iDisplayStart && (a._iDisplayStart = 0);
			else if ("next" == b) 0 <= a._iDisplayLength ? a._iDisplayStart + a._iDisplayLength < a.fnRecordsDisplay() && (a._iDisplayStart += a._iDisplayLength) : a._iDisplayStart = 0;
			else if ("last" == b) if (0 <= a._iDisplayLength) {
				var d = parseInt((a.fnRecordsDisplay() - 1) / a._iDisplayLength, 10) + 1;
				a._iDisplayStart = (d - 1) * a._iDisplayLength
			} else a._iDisplayStart = 0;
			else Ea(a, 0, "Unknown paging action: " + b);
			return c != a._iDisplayStart
		}
		function la(a) {
			var c = b.createElement("div");
			c.className = a.oClasses.sInfo;
			"undefined" == typeof a.aanFeatures.i && (a.aoDrawCallback.push({
				fn: L,
				sName: "information"
			}), "" !== a.sTableId && c.setAttribute("id", a.sTableId + "_info"));
			return c
		}
		function L(b) {
			if (b.oFeatures.bInfo && 0 !== b.aanFeatures.i.length) {
				var c = b._iDisplayStart + 1,
				d = b.fnDisplayEnd(),
				g = b.fnRecordsTotal(),
				e = b.fnRecordsDisplay(),
				j = b.fnFormatNumber(c),
				k = b.fnFormatNumber(d),
				n = b.fnFormatNumber(g),
				p = b.fnFormatNumber(e);
				b.oScroll.bInfinite && (j = b.fnFormatNumber(1));
				j = 0 === b.fnRecordsDisplay() && b.fnRecordsDisplay() == b.fnRecordsTotal() ? b.oLanguage.sInfoEmpty + b.oLanguage.sInfoPostFix: 0 === b.fnRecordsDisplay() ? b.oLanguage.sInfoEmpty + " " + b.oLanguage.sInfoFiltered.replace("_MAX_", n) + b.oLanguage.sInfoPostFix: b.fnRecordsDisplay() == b.fnRecordsTotal() ? b.oLanguage.sInfo.replace("_START_", j).replace("_END_", k).replace("_TOTAL_", p) + b.oLanguage.sInfoPostFix: b.oLanguage.sInfo.replace("_START_", j).replace("_END_", k).replace("_TOTAL_", p) + " " + b.oLanguage.sInfoFiltered.replace("_MAX_", b.fnFormatNumber(b.fnRecordsTotal())) + b.oLanguage.sInfoPostFix;
				null !== b.oLanguage.fnInfoCallback && (j = b.oLanguage.fnInfoCallback(b, c, d, g, e, j));
				b = b.aanFeatures.i;
				c = 0;
				for (d = b.length; c < d; c++) a(b[c]).html(j)
			}
		}
		function C(c) {
			if (c.oScroll.bInfinite) return null;
			var d = '<select size="1" ' + ("" === c.sTableId ? "": 'name="' + c.sTableId + '_length"') + ">",
			g,
			e;
			if (2 == c.aLengthMenu.length && "object" == typeof c.aLengthMenu[0] && "object" == typeof c.aLengthMenu[1]) {
				g = 0;
				for (e = c.aLengthMenu[0].length; g < e; g++) d += '<option value="' + c.aLengthMenu[0][g] + '">' + c.aLengthMenu[1][g] + "</option>"
			} else {
				g = 0;
				for (e = c.aLengthMenu.length; g < e; g++) d += '<option value="' + c.aLengthMenu[g] + '">' + c.aLengthMenu[g] + "</option>"
			}
			var d = d + "</select>",
			j = b.createElement("div");
			"" !== c.sTableId && "undefined" == typeof c.aanFeatures.l && j.setAttribute("id", c.sTableId + "_length");
			j.className = c.oClasses.sLength;
			j.innerHTML = "<label>" + c.oLanguage.sLengthMenu.replace("_MENU_", d) + "</label>";
			a('select option[value="' + c._iDisplayLength + '"]', j).attr("selected", !0);
			a("select", j).bind("change.DT",
			function() {
				var b = a(this).val(),
				d = c.aanFeatures.l;
				g = 0;
				for (e = d.length; g < e; g++) d[g] != this.parentNode && a("select", d[g]).val(b);
				c._iDisplayLength = parseInt(b, 10);
				ma(c);
				c.fnDisplayEnd() == c.fnRecordsDisplay() && (c._iDisplayStart = c.fnDisplayEnd() - c._iDisplayLength, 0 > c._iDisplayStart && (c._iDisplayStart = 0)); - 1 == c._iDisplayLength && (c._iDisplayStart = 0);
				E(c)
			});
			return j
		}
		function M(a) {
			var c = b.createElement("div");
			"" !== a.sTableId && "undefined" == typeof a.aanFeatures.r && c.setAttribute("id", a.sTableId + "_processing");
			c.innerHTML = a.oLanguage.sProcessing;
			c.className = a.oClasses.sProcessing;
			a.nTable.parentNode.insertBefore(c, a.nTable);
			return c
		}
		function ja(a, b) {
			if (a.oFeatures.bProcessing) for (var c = a.aanFeatures.r,
			d = 0,
			g = c.length; d < g; d++) c[d].style.visibility = b ? "visible": "hidden"
		}
		function za(a, b) {
			for (var c = -1,
			d = 0; d < a.aoColumns.length; d++) if (!0 === a.aoColumns[d].bVisible && c++, c == b) return d;
			return null
		}
		function J(a, b) {
			for (var c = -1,
			d = 0; d < a.aoColumns.length; d++) if (!0 === a.aoColumns[d].bVisible && c++, d == b) return ! 0 === a.aoColumns[d].bVisible ? c: null;
			return null
		}
		function ya(a, b) {
			var c, d;
			c = a._iDisplayStart;
			for (d = a._iDisplayEnd; c < d; c++) if (a.aoData[a.aiDisplay[c]].nTr == b) return a.aiDisplay[c];
			c = 0;
			for (d = a.aoData.length; c < d; c++) if (a.aoData[c].nTr == b) return c;
			return null
		}
		function Aa(a) {
			for (var b = 0,
			c = 0; c < a.aoColumns.length; c++) ! 0 === a.aoColumns[c].bVisible && b++;
			return b
		}
		function ma(a) {
			a._iDisplayEnd = !1 === a.oFeatures.bPaginate ? a.aiDisplay.length: a._iDisplayStart + a._iDisplayLength > a.aiDisplay.length || -1 == a._iDisplayLength ? a.aiDisplay.length: a._iDisplayStart + a._iDisplayLength
		}
		function Ga(a, c) {
			if (!a || null === a || "" === a) return 0;
			"undefined" == typeof c && (c = b.getElementsByTagName("body")[0]);
			var d, g = b.createElement("div");
			g.style.width = Q(a);
			c.appendChild(g);
			d = g.offsetWidth;
			c.removeChild(g);
			return d
		}
		function Ca(c) {
			var d = 0,
			g, e = 0,
			j = c.aoColumns.length,
			k, n = a("th", c.nTHead);
			for (k = 0; k < j; k++) c.aoColumns[k].bVisible && (e++, null !== c.aoColumns[k].sWidth && (g = Ga(c.aoColumns[k].sWidthOrig, c.nTable.parentNode), null !== g && (c.aoColumns[k].sWidth = Q(g)), d++));
			if (j == n.length && 0 === d && e == j && "" === c.oScroll.sX && "" === c.oScroll.sY) for (k = 0; k < c.aoColumns.length; k++) g = a(n[k]).width(),
			null !== g && (c.aoColumns[k].sWidth = Q(g));
			else {
				d = c.nTable.cloneNode(!1);
				k = c.nTHead.cloneNode(!0);
				e = b.createElement("tbody");
				g = b.createElement("tr");
				d.removeAttribute("id");
				d.appendChild(k);
				null !== c.nTFoot && (d.appendChild(c.nTFoot.cloneNode(!0)), Va(function(a) {
					a.style.width = ""
				},
				d.getElementsByTagName("tr")));
				d.appendChild(e);
				e.appendChild(g);
				e = a("thead th", d);
				0 === e.length && (e = a("tbody tr:eq(0)>td", d));
				n = Na(c, k);
				for (k = e = 0; k < j; k++) {
					var p = c.aoColumns[k];
					p.bVisible && null !== p.sWidthOrig && "" !== p.sWidthOrig ? n[k - e].style.width = Q(p.sWidthOrig) : p.bVisible ? n[k - e].style.width = "": e++
				}
				for (k = 0; k < j; k++) c.aoColumns[k].bVisible && (e = Pa(c, k), null !== e && (e = e.cloneNode(!0), "" !== c.aoColumns[k].sContentPadding && (e.innerHTML += c.aoColumns[k].sContentPadding), g.appendChild(e)));
				j = c.nTable.parentNode;
				j.appendChild(d);
				"" !== c.oScroll.sX && "" !== c.oScroll.sXInner ? d.style.width = Q(c.oScroll.sXInner) : "" !== c.oScroll.sX ? (d.style.width = "", a(d).width() < j.offsetWidth && (d.style.width = Q(j.offsetWidth))) : "" !== c.oScroll.sY && (d.style.width = Q(j.offsetWidth));
				d.style.visibility = "hidden";
				ka(c, d);
				j = a("tbody tr:eq(0)", d).children();
				0 === j.length && (j = Na(c, a("thead", d)[0]));
				if ("" !== c.oScroll.sX) {
					for (k = e = g = 0; k < c.aoColumns.length; k++) c.aoColumns[k].bVisible && (g = null === c.aoColumns[k].sWidthOrig ? g + a(j[e]).outerWidth() : g + (parseInt(c.aoColumns[k].sWidth.replace("px", ""), 10) + (a(j[e]).outerWidth() - a(j[e]).width())), e++);
					d.style.width = Q(g);
					c.nTable.style.width = Q(g)
				}
				for (k = e = 0; k < c.aoColumns.length; k++) c.aoColumns[k].bVisible && (g = a(j[e]).width(), null !== g && 0 < g && (c.aoColumns[k].sWidth = Q(g)), e++);
				c.nTable.style.width = Q(a(d).outerWidth());
				d.parentNode.removeChild(d)
			}
		}
		function ka(b, c) {
			"" === b.oScroll.sX && "" !== b.oScroll.sY ? (a(c).width(), c.style.width = Q(a(c).outerWidth() - b.oScroll.iBarWidth)) : "" !== b.oScroll.sX && (c.style.width = Q(a(c).outerWidth()))
		}
		function Pa(a, c) {
			var d = Qa(a, c);
			if (0 > d) return null;
			if (null === a.aoData[d].nTr) {
				var g = b.createElement("td");
				g.innerHTML = Ia(a, d, c, "");
				return g
			}
			return da(a, d)[c]
		}
		function Qa(a, b) {
			for (var c = -1,
			d = -1,
			g = 0; g < a.aoData.length; g++) {
				var e = Ia(a, g, b, "display") + "",
				e = e.replace(/<.*?>/g, "");
				e.length > c && (c = e.length, d = g)
			}
			return d
		}
		function Q(a) {
			if (null === a) return "0px";
			if ("number" == typeof a) return 0 > a ? "0px": a + "px";
			var b = a.charCodeAt(a.length - 1);
			return 48 > b || 57 < b ? a: a + "px"
		}
		function Da(a) {
			for (var b = g.aTypes,
			c = b.length,
			d = 0; d < c; d++) {
				var e = b[d](a);
				if (null !== e) return e
			}
			return "string"
		}
		function aa(a) {
			for (var b = 0; b < c.length; b++) if (c[b].nTable == a) return c[b];
			return null
		}
		function T(a) {
			for (var b = [], c = a.aoData.length, d = 0; d < c; d++) b.push(a.aoData[d]._aData);
			return b
		}
		function ra(a) {
			for (var b = [], c = 0, d = a.aoData.length; c < d; c++) null !== a.aoData[c].nTr && b.push(a.aoData[c].nTr);
			return b
		}
		function da(a, b) {
			var c = [],
			d,
			g,
			e,
			j,
			k,
			n;
			g = 0;
			var p = a.aoData.length;
			"undefined" != typeof b && (g = b, p = b + 1);
			for (e = g; e < p; e++) if (n = a.aoData[e], null !== n.nTr) {
				g = [];
				j = 0;
				for (k = n.nTr.childNodes.length; j < k; j++) d = n.nTr.childNodes[j].nodeName.toLowerCase(),
				("td" == d || "th" == d) && g.push(n.nTr.childNodes[j]);
				j = d = 0;
				for (k = a.aoColumns.length; j < k; j++) a.aoColumns[j].bVisible ? c.push(g[j - d]) : (c.push(n._anHidden[j]), d++)
			}
			return c
		}
		function pa(a) {
			return a.replace(RegExp("(\\/|\\.|\\*|\\+|\\?|\\||\\(|\\)|\\[|\\]|\\{|\\}|\\\\|\\$|\\^)", "g"), "\\$1")
		}
		function fa(a, b) {
			for (var c = -1,
			d = 0,
			g = a.length; d < g; d++) a[d] == b ? c = d: a[d] > b && a[d]--; - 1 != c && a.splice(c, 1)
		}
		function oa(a, b) {
			for (var c = b.split(","), d = [], g = 0, e = a.aoColumns.length; g < e; g++) for (var j = 0; j < e; j++) if (a.aoColumns[g].sName == c[j]) {
				d.push(j);
				break
			}
			return d
		}
		function Ua(a) {
			for (var b = "",
			c = 0,
			d = a.aoColumns.length; c < d; c++) b += a.aoColumns[c].sName + ",";
			return b.length == d ? "": b.slice(0, -1)
		}
		function Ea(a, b, c) {
			a = "" === a.sTableId ? "DataTables warning: " + c: "DataTables warning (table id = '" + a.sTableId + "'): " + c;
			if (0 === b) if ("alert" == g.sErrMode) alert(a);
			else throw a;
			else "undefined" != typeof console && "undefined" != typeof console.log && console.log(a)
		}
		function Ha(a) {
			a.aoData.splice(0, a.aoData.length);
			a.aiDisplayMaster.splice(0, a.aiDisplayMaster.length);
			a.aiDisplay.splice(0, a.aiDisplay.length);
			ma(a)
		}
		function ca(a) {
			if (a.oFeatures.bStateSave && "undefined" == typeof a.bDestroying) {
				var b, c, d, g;
				g = "{" + ('"iCreate":' + (new Date).getTime() + ",");
				g += '"iStart":' + (a.oScroll.bInfinite ? 0 : a._iDisplayStart) + ",";
				g += '"iEnd":' + (a.oScroll.bInfinite ? a._iDisplayLength: a._iDisplayEnd) + ",";
				g += '"iLength":' + a._iDisplayLength + ",";
				g += '"sFilter":"' + encodeURIComponent(a.oPreviousSearch.sSearch) + '",';
				g += '"sFilterEsc":' + !a.oPreviousSearch.bRegex + ",";
				g += '"aaSorting":[ ';
				for (b = 0; b < a.aaSorting.length; b++) g += "[" + a.aaSorting[b][0] + ',"' + a.aaSorting[b][1] + '"],';
				g = g.substring(0, g.length - 1);
				g += '],"aaSearchCols":[ ';
				for (b = 0; b < a.aoPreSearchCols.length; b++) g += '["' + encodeURIComponent(a.aoPreSearchCols[b].sSearch) + '",' + !a.aoPreSearchCols[b].bRegex + "],";
				g = g.substring(0, g.length - 1);
				g += "],";
				g += '"abVisCols":[ ';
				for (b = 0; b < a.aoColumns.length; b++) g += a.aoColumns[b].bVisible + ",";
				g = g.substring(0, g.length - 1);
				g += "]";
				b = 0;
				for (c = a.aoStateSave.length; b < c; b++) d = a.aoStateSave[b].fn(a, g),
				"" !== d && (g = d);
				g += "}";
				X(a.sCookiePrefix + a.sInstance, g, a.iCookieDuration, a.sCookiePrefix, a.fnCookieCallback)
			}
		}
		function wa(b, c) {
			if (b.oFeatures.bStateSave) {
				var d, g, e;
				g = Ma(b.sCookiePrefix + b.sInstance);
				if (null !== g && "" !== g) {
					try {
						d = "function" == typeof a.parseJSON ? a.parseJSON(g.replace(/'/g, '"')) : eval("(" + g + ")")
					} catch(j) {
						return
					}
					g = 0;
					for (e = b.aoStateLoad.length; g < e; g++) if (!b.aoStateLoad[g].fn(b, d)) return;
					b.oLoadedState = a.extend(!0, {},
					d);
					b._iDisplayStart = d.iStart;
					b.iInitDisplayStart = d.iStart;
					b._iDisplayEnd = d.iEnd;
					b._iDisplayLength = d.iLength;
					b.oPreviousSearch.sSearch = decodeURIComponent(d.sFilter);
					b.aaSorting = d.aaSorting.slice();
					b.saved_aaSorting = d.aaSorting.slice();
					"undefined" != typeof d.sFilterEsc && (b.oPreviousSearch.bRegex = !d.sFilterEsc);
					if ("undefined" != typeof d.aaSearchCols) for (g = 0; g < d.aaSearchCols.length; g++) b.aoPreSearchCols[g] = {
						sSearch: decodeURIComponent(d.aaSearchCols[g][0]),
						bRegex: !d.aaSearchCols[g][1]
					};
					if ("undefined" != typeof d.abVisCols) {
						c.saved_aoColumns = [];
						for (g = 0; g < d.abVisCols.length; g++) c.saved_aoColumns[g] = {},
						c.saved_aoColumns[g].bVisible = d.abVisCols[g]
					}
				}
			}
		}
		function X(c, d, g, j, k) {
			var n = new Date;
			n.setTime(n.getTime() + 1E3 * g);
			g = e.location.pathname.split("/");
			c = c + "_" + g.pop().replace(/[\/:]/g, "").toLowerCase();
			var p;
			null !== k ? (p = "function" == typeof a.parseJSON ? a.parseJSON(d) : eval("(" + d + ")"), d = k(c, p, n.toGMTString(), g.join("/") + "/")) : d = c + "=" + encodeURIComponent(d) + "; expires=" + n.toGMTString() + "; path=" + g.join("/") + "/";
			k = "";
			n = 9999999999999;
			if (4096 < (null !== Ma(c) ? b.cookie.length: d.length + b.cookie.length) + 10) {
				c = b.cookie.split(";");
				for (var q = 0,
				r = c.length; q < r; q++) if ( - 1 != c[q].indexOf(j)) {
					var t = c[q].split("=");
					try {
						p = eval("(" + decodeURIComponent(t[1]) + ")")
					} catch(z) {
						continue
					}
					"undefined" != typeof p.iCreate && p.iCreate < n && (k = t[0], n = p.iCreate)
				}
				"" !== k && (b.cookie = k + "=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=" + g.join("/") + "/")
			}
			b.cookie = d
		}
		function Ma(a) {
			var c = e.location.pathname.split("/");
			a = a + "_" + c[c.length - 1].replace(/[\/:]/g, "").toLowerCase() + "=";
			for (var c = b.cookie.split(";"), d = 0; d < c.length; d++) {
				for (var g = c[d];
				" " == g.charAt(0);) g = g.substring(1, g.length);
				if (0 === g.indexOf(a)) return decodeURIComponent(g.substring(a.length, g.length))
			}
			return null
		}
		function Z(a, b) {
			var c = b.getElementsByTagName("tr"),
			d,
			g,
			e,
			j,
			k,
			n,
			p,
			q;
			a.splice(0, a.length);
			g = 0;
			for (n = c.length; g < n; g++) a.push([]);
			g = 0;
			for (n = c.length; g < n; g++) {
				e = 0;
				for (p = c[g].childNodes.length; e < p; e++) if (d = c[g].childNodes[e], "TD" == d.nodeName.toUpperCase() || "TH" == d.nodeName.toUpperCase()) {
					var r = 1 * d.getAttribute("colspan"),
					t = 1 * d.getAttribute("rowspan"),
					r = !r || 0 === r || 1 === r ? 1 : r,
					t = !t || 0 === t || 1 === t ? 1 : t;
					for (j = 0;
					"undefined" != typeof a[g][j];) j++;
					q = j;
					for (k = 0; k < r; k++) for (j = 0; j < t; j++) a[g + j][q + k] = {
						cell: d,
						unique: 1 == r ? !0 : !1
					},
					a[g + j].nTr = c[g]
				}
			}
		}
		function Na(a, b, c) {
			var d = [];
			"undefined" == typeof c && (c = a.aoHeader, "undefined" != typeof b && (c = [], Z(c, b)));
			b = 0;
			for (var g = c.length; b < g; b++) for (var e = 0,
			j = c[b].length; e < j; e++) if (c[b][e].unique && ("undefined" == typeof d[e] || !a.bSortCellsTop)) d[e] = c[b][e].cell;
			return d
		}
		function fb() {
			var a = b.createElement("p"),
			c = a.style;
			c.width = "100%";
			c.height = "200px";
			var d = b.createElement("div"),
			c = d.style;
			c.position = "absolute";
			c.top = "0px";
			c.left = "0px";
			c.visibility = "hidden";
			c.width = "200px";
			c.height = "150px";
			c.overflow = "hidden";
			d.appendChild(a);
			b.body.appendChild(d);
			c = a.offsetWidth;
			d.style.overflow = "scroll";
			a = a.offsetWidth;
			c == a && (a = d.clientWidth);
			b.body.removeChild(d);
			return c - a
		}
		function Va(a, b, c) {
			for (var d = 0,
			g = b.length; d < g; d++) for (var e = 0,
			j = b[d].childNodes.length; e < j; e++) 1 == b[d].childNodes[e].nodeType && ("undefined" != typeof c ? a(b[d].childNodes[e], c[d].childNodes[e]) : a(b[d].childNodes[e]))
		}
		function O(a, b, c, d) {
			"undefined" == typeof d && (d = c);
			"undefined" != typeof b[c] && (a[d] = b[c])
		}
		function ab(a, b, c) {
			for (var d = [], g = 0, e = a.aoColumns.length; g < e; g++) d.push(Ia(a, b, g, c));
			return d
		}
		function Ia(a, b, c, d) {
			var g = a.aoColumns[c];
			if (void 0 === (c = g.fnGetData(a.aoData[b]._aData))) return a.iDrawError != a.iDraw && null === g.sDefaultContent && (Ea(a, 0, "Requested unknown parameter '" + g.mDataProp + "' from the data source for row " + b), a.iDrawError = a.iDraw),
			g.sDefaultContent;
			null === c && null !== g.sDefaultContent && (c = g.sDefaultContent);
			return "display" == d && null === c ? "": c
		}
		function Wa(a, b, c, d) {
			a.aoColumns[c].fnSetData(a.aoData[b]._aData, d)
		}
		function bb(a) {
			if (null === a) return function() {
				return null
			};
			if ("function" == typeof a) return function(b) {
				return a(b)
			};
			if ("string" == typeof a && -1 != a.indexOf(".")) {
				var b = a.split(".");
				return 2 == b.length ?
				function(a) {
					return a[b[0]][b[1]]
				}: 3 == b.length ?
				function(a) {
					return a[b[0]][b[1]][b[2]]
				}: function(a) {
					for (var c = 0,
					d = b.length; c < d; c++) a = a[b[c]];
					return a
				}
			}
			return function(b) {
				return b[a]
			}
		}
		function Oa(a) {
			if (null === a) return function() {};
			if ("function" == typeof a) return function(b, c) {
				return a(b, c)
			};
			if ("string" == typeof a && -1 != a.indexOf(".")) {
				var b = a.split(".");
				return 2 == b.length ?
				function(a, c) {
					a[b[0]][b[1]] = c
				}: 3 == b.length ?
				function(a, c) {
					a[b[0]][b[1]][b[2]] = c
				}: function(a, c) {
					for (var d = 0,
					g = b.length - 1; d < g; d++) a = a[b[d]];
					a[b[b.length - 1]] = c
				}
			}
			return function(b, c) {
				b[a] = c
			}
		}
		this.oApi = {};
		this.fnDraw = function(a) {
			var b = aa(this[g.iApiIndex]);
			"undefined" != typeof a && !1 === a ? (ma(b), E(b)) : V(b)
		};
		this.fnFilter = function(b, c, d, e, j) {
			var k = aa(this[g.iApiIndex]);
			if (k.oFeatures.bFilter) if ("undefined" == typeof d && (d = !1), "undefined" == typeof e && (e = !0), "undefined" == typeof j && (j = !0), "undefined" == typeof c || null === c) {
				if (ga(k, {
					sSearch: b,
					bRegex: d,
					bSmart: e
				},
				1), j && "undefined" != typeof k.aanFeatures.f) {
					c = k.aanFeatures.f;
					d = 0;
					for (e = c.length; d < e; d++) a("input", c[d]).val(b)
				}
			} else k.aoPreSearchCols[c].sSearch = b,
			k.aoPreSearchCols[c].bRegex = d,
			k.aoPreSearchCols[c].bSmart = e,
			ga(k, k.oPreviousSearch, 1)
		};
		this.fnSettings = function() {
			return aa(this[g.iApiIndex])
		};
		this.fnVersionCheck = g.fnVersionCheck;
		this.fnSort = function(a) {
			var b = aa(this[g.iApiIndex]);
			b.aaSorting = a;
			Ba(b)
		};
		this.fnSortListener = function(a, b, c) {
			qa(aa(this[g.iApiIndex]), a, b, c)
		};
		this.fnAddData = function(a, b) {
			if (0 === a.length) return [];
			var c = [],
			d,
			e = aa(this[g.iApiIndex]);
			if ("object" == typeof a[0]) for (var j = 0; j < a.length; j++) {
				d = p(e, a[j]);
				if ( - 1 == d) return c;
				c.push(d)
			} else {
				d = p(e, a);
				if ( - 1 == d) return c;
				c.push(d)
			}
			e.aiDisplay = e.aiDisplayMaster.slice(); ("undefined" == typeof b || b) && V(e);
			return c
		};
		this.fnDeleteRow = function(b, c, d) {
			var e = aa(this[g.iApiIndex]);
			b = "object" == typeof b ? ya(e, b) : b;
			var j = e.aoData.splice(b, 1),
			k = a.inArray(b, e.aiDisplay);
			e.asDataSearch.splice(k, 1);
			fa(e.aiDisplayMaster, b);
			fa(e.aiDisplay, b);
			"function" == typeof c && c.call(this, e, j);
			e._iDisplayStart >= e.aiDisplay.length && (e._iDisplayStart -= e._iDisplayLength, 0 > e._iDisplayStart && (e._iDisplayStart = 0));
			if ("undefined" == typeof d || d) ma(e),
			E(e);
			return j
		};
		this.fnClearTable = function(a) {
			var b = aa(this[g.iApiIndex]);
			Ha(b); ("undefined" == typeof a || a) && E(b)
		};
		this.fnOpen = function(c, d, e) {
			var j = aa(this[g.iApiIndex]);
			this.fnClose(c);
			var k = b.createElement("tr"),
			n = b.createElement("td");
			k.appendChild(n);
			n.className = e;
			n.colSpan = Aa(j);
			"undefined" != typeof d.jquery || "object" == typeof d ? n.appendChild(d) : n.innerHTML = d;
			d = a("tr", j.nTBody); - 1 != a.inArray(c, d) && a(k).insertAfter(c);
			j.aoOpenRows.push({
				nTr: k,
				nParent: c
			});
			return k
		};
		this.fnClose = function(a) {
			for (var b = aa(this[g.iApiIndex]), c = 0; c < b.aoOpenRows.length; c++) if (b.aoOpenRows[c].nParent == a) return (a = b.aoOpenRows[c].nTr.parentNode) && a.removeChild(b.aoOpenRows[c].nTr),
			b.aoOpenRows.splice(c, 1),
			0;
			return 1
		};
		this.fnGetData = function(a, b) {
			var c = aa(this[g.iApiIndex]);
			if ("undefined" != typeof a) {
				var d = "object" == typeof a ? ya(c, a) : a;
				return "undefined" != typeof b ? Ia(c, d, b, "") : "undefined" != typeof c.aoData[d] ? c.aoData[d]._aData: null
			}
			return T(c)
		};
		this.fnGetNodes = function(a) {
			var b = aa(this[g.iApiIndex]);
			return "undefined" != typeof a ? "undefined" != typeof b.aoData[a] ? b.aoData[a].nTr: null: ra(b)
		};
		this.fnGetPosition = function(a) {
			var b = aa(this[g.iApiIndex]),
			c = a.nodeName.toUpperCase();
			if ("TR" == c) return ya(b, a);
			if ("TD" == c || "TH" == c) for (var c = ya(b, a.parentNode), d = da(b, c), e = 0; e < b.aoColumns.length; e++) if (d[e] == a) return [c, J(b, e), e];
			return null
		};
		this.fnUpdate = function(b, c, d, e, j) {
			var k = aa(this[g.iApiIndex]);
			c = "object" == typeof c ? ya(k, c) : c;
			if (a.isArray(b) && "object" == typeof b) {
				k.aoData[c]._aData = b.slice();
				for (d = 0; d < k.aoColumns.length; d++) this.fnUpdate(Ia(k, c, d), c, d, !1, !1)
			} else if ("object" == typeof b) {
				k.aoData[c]._aData = a.extend(!0, {},
				b);
				for (d = 0; d < k.aoColumns.length; d++) this.fnUpdate(Ia(k, c, d), c, d, !1, !1)
			} else Wa(k, c, d, b),
			null !== k.aoColumns[d].fnRender && (b = k.aoColumns[d].fnRender({
				iDataRow: c,
				iDataColumn: d,
				aData: k.aoData[c]._aData,
				oSettings: k
			}), k.aoColumns[d].bUseRendered && Wa(k, c, d, b)),
			null !== k.aoData[c].nTr && (da(k, c)[d].innerHTML = b);
			d = a.inArray(c, k.aiDisplay);
			k.asDataSearch[d] = va(k, ab(k, c, "filter")); ("undefined" == typeof j || j) && ba(k); ("undefined" == typeof e || e) && V(k);
			return 0
		};
		this.fnSetColumnVis = function(a, b, c) {
			var d = aa(this[g.iApiIndex]),
			e,
			j;
			j = d.aoColumns.length;
			var k, n;
			if (d.aoColumns[a].bVisible != b) {
				if (b) {
					for (e = n = 0; e < a; e++) d.aoColumns[e].bVisible && n++;
					n = n >= Aa(d);
					if (!n) for (e = a; e < j; e++) if (d.aoColumns[e].bVisible) {
						k = e;
						break
					}
					e = 0;
					for (j = d.aoData.length; e < j; e++) null !== d.aoData[e].nTr && (n ? d.aoData[e].nTr.appendChild(d.aoData[e]._anHidden[a]) : d.aoData[e].nTr.insertBefore(d.aoData[e]._anHidden[a], da(d, e)[k]))
				} else {
					e = 0;
					for (j = d.aoData.length; e < j; e++) null !== d.aoData[e].nTr && (k = da(d, e)[a], d.aoData[e]._anHidden[a] = k, k.parentNode.removeChild(k))
				}
				d.aoColumns[a].bVisible = b;
				D(d, d.aoHeader);
				d.nTFoot && D(d, d.aoFooter);
				e = 0;
				for (j = d.aoOpenRows.length; e < j; e++) d.aoOpenRows[e].nTr.colSpan = Aa(d);
				if ("undefined" == typeof c || c) ba(d),
				E(d);
				ca(d)
			}
		};
		this.fnPageChange = function(a, b) {
			var c = aa(this[g.iApiIndex]);
			R(c, a);
			ma(c); ("undefined" == typeof b || b) && E(c)
		};
		this.fnDestroy = function() {
			var b = aa(this[g.iApiIndex]),
			d = b.nTableWrapper.parentNode,
			e = b.nTBody,
			j,
			k;
			b.bDestroying = !0;
			j = 0;
			for (k = b.aoColumns.length; j < k; j++) ! 1 === b.aoColumns[j].bVisible && this.fnSetColumnVis(j, !0);
			a(b.nTableWrapper).find("*").andSelf().unbind(".DT");
			a("tbody>tr>td." + b.oClasses.sRowEmpty, b.nTable).parent().remove();
			b.nTable != b.nTHead.parentNode && (a(">thead", b.nTable).remove(), b.nTable.appendChild(b.nTHead));
			b.nTFoot && b.nTable != b.nTFoot.parentNode && (a(">tfoot", b.nTable).remove(), b.nTable.appendChild(b.nTFoot));
			b.nTable.parentNode.removeChild(b.nTable);
			a(b.nTableWrapper).remove();
			b.aaSorting = [];
			b.aaSortingFixed = [];
			G(b);
			a(ra(b)).removeClass(b.asStripClasses.join(" "));
			b.bJUI ? (a("th", b.nTHead).removeClass([g.oStdClasses.sSortable, g.oJUIClasses.sSortableAsc, g.oJUIClasses.sSortableDesc, g.oJUIClasses.sSortableNone].join(" ")), a("th span." + g.oJUIClasses.sSortIcon, b.nTHead).remove(), a("th", b.nTHead).each(function() {
				var b = a("div." + g.oJUIClasses.sSortJUIWrapper, this),
				c = b.contents();
				a(this).append(c);
				b.remove()
			})) : a("th", b.nTHead).removeClass([g.oStdClasses.sSortable, g.oStdClasses.sSortableAsc, g.oStdClasses.sSortableDesc, g.oStdClasses.sSortableNone].join(" "));
			b.nTableReinsertBefore ? d.insertBefore(b.nTable, b.nTableReinsertBefore) : d.appendChild(b.nTable);
			j = 0;
			for (k = b.aoData.length; j < k; j++) null !== b.aoData[j].nTr && e.appendChild(b.aoData[j].nTr); ! 0 === b.oFeatures.bAutoWidth && (b.nTable.style.width = Q(b.sDestroyWidth));
			a(">tr:even", e).addClass(b.asDestoryStrips[0]);
			a(">tr:odd", e).addClass(b.asDestoryStrips[1]);
			j = 0;
			for (k = c.length; j < k; j++) c[j] == b && c.splice(j, 1);
			b = null
		};
		this.fnAdjustColumnSizing = function(a) {
			var b = aa(this[g.iApiIndex]);
			ba(b);
			"undefined" == typeof a || a ? this.fnDraw(!1) : ("" !== b.oScroll.sX || "" !== b.oScroll.sY) && this.oApi._fnScrollDraw(b)
		};
		for (var Ja in g.oApi) Ja && (this[Ja] = j(Ja));
		this.oApi._fnExternApiFunc = j;
		this.oApi._fnInitalise = k;
		this.oApi._fnInitComplete = t;
		this.oApi._fnLanguageProcess = A;
		this.oApi._fnAddColumn = B;
		this.oApi._fnColumnOptions = r;
		this.oApi._fnAddData = p;
		this.oApi._fnCreateTr = z;
		this.oApi._fnGatherData = q;
		this.oApi._fnBuildHead = F;
		this.oApi._fnDrawHead = D;
		this.oApi._fnDraw = E;
		this.oApi._fnReDraw = V;
		this.oApi._fnAjaxUpdate = ha;
		this.oApi._fnAjaxUpdateDraw = W;
		this.oApi._fnAddOptionsHtml = sa;
		this.oApi._fnFeatureHtmlTable = S;
		this.oApi._fnScrollDraw = U;
		this.oApi._fnAjustColumnSizing = ba;
		this.oApi._fnFeatureHtmlFilter = ia;
		this.oApi._fnFilterComplete = ga;
		this.oApi._fnFilterCustom = Y;
		this.oApi._fnFilterColumn = xa;
		this.oApi._fnFilter = ua;
		this.oApi._fnBuildSearchArray = ta;
		this.oApi._fnBuildSearchRow = va;
		this.oApi._fnFilterCreateSearch = N;
		this.oApi._fnDataToSearch = na;
		this.oApi._fnSort = Ba;
		this.oApi._fnSortAttachListener = qa;
		this.oApi._fnSortingClasses = G;
		this.oApi._fnFeatureHtmlPaginate = w;
		this.oApi._fnPageChange = R;
		this.oApi._fnFeatureHtmlInfo = la;
		this.oApi._fnUpdateInfo = L;
		this.oApi._fnFeatureHtmlLength = C;
		this.oApi._fnFeatureHtmlProcessing = M;
		this.oApi._fnProcessingDisplay = ja;
		this.oApi._fnVisibleToColumnIndex = za;
		this.oApi._fnColumnIndexToVisible = J;
		this.oApi._fnNodeToDataIndex = ya;
		this.oApi._fnVisbleColumns = Aa;
		this.oApi._fnCalculateEnd = ma;
		this.oApi._fnConvertToWidth = Ga;
		this.oApi._fnCalculateColumnWidths = Ca;
		this.oApi._fnScrollingWidthAdjust = ka;
		this.oApi._fnGetWidestNode = Pa;
		this.oApi._fnGetMaxLenString = Qa;
		this.oApi._fnStringToCss = Q;
		this.oApi._fnArrayCmp = function(a, b) {
			if (a.length != b.length) return 1;
			for (var c = 0; c < a.length; c++) if (a[c] != b[c]) return 2;
			return 0
		};
		this.oApi._fnDetectType = Da;
		this.oApi._fnSettingsFromNode = aa;
		this.oApi._fnGetDataMaster = T;
		this.oApi._fnGetTrNodes = ra;
		this.oApi._fnGetTdNodes = da;
		this.oApi._fnEscapeRegex = pa;
		this.oApi._fnDeleteIndex = fa;
		this.oApi._fnReOrderIndex = oa;
		this.oApi._fnColumnOrdering = Ua;
		this.oApi._fnLog = Ea;
		this.oApi._fnClearTable = Ha;
		this.oApi._fnSaveState = ca;
		this.oApi._fnLoadState = wa;
		this.oApi._fnCreateCookie = X;
		this.oApi._fnReadCookie = Ma;
		this.oApi._fnDetectHeader = Z;
		this.oApi._fnGetUniqueThs = Na;
		this.oApi._fnScrollBarWidth = fb;
		this.oApi._fnApplyToChildren = Va;
		this.oApi._fnMap = O;
		this.oApi._fnGetRowData = ab;
		this.oApi._fnGetCellData = Ia;
		this.oApi._fnSetCellData = Wa;
		this.oApi._fnGetObjectDataFn = bb;
		this.oApi._fnSetObjectDataFn = Oa;
		var eb = this;
		return this.each(function() {
			var e = 0,
			j, t, z, w, e = 0;
			for (j = c.length; e < j; e++) {
				if (c[e].nTable == this) {
					if ("undefined" == typeof d || "undefined" != typeof d.bRetrieve && !0 === d.bRetrieve) return c[e].oInstance;
					if ("undefined" != typeof d.bDestroy && !0 === d.bDestroy) {
						c[e].oInstance.fnDestroy();
						break
					} else {
						Ea(c[e], 0, "Cannot reinitialise DataTable.\n\nTo retrieve the DataTables object for this table, please pass either no arguments to the dataTable() function, or set bRetrieve to true. Alternatively, to destory the old table and create a new one, set bDestroy to true (note that a lot of changes to the configuration can be made through the API which is usually much faster).");
						return
					}
				}
				if ("" !== c[e].sTableId && c[e].sTableId == this.getAttribute("id")) {
					c.splice(e, 1);
					break
				}
			}
			var v = new n;
			c.push(v);
			var D = !1,
			C = !1,
			e = this.getAttribute("id");
			null !== e ? (v.sTableId = e, v.sInstance = e) : v.sInstance = g._oExternConfig.iNextUnique++;
			if ("table" != this.nodeName.toLowerCase()) Ea(v, 0, "Attempted to initialise DataTables on a node which is not a table: " + this.nodeName);
			else {
				v.nTable = this;
				v.oInstance = 1 == eb.length ? eb: a(this).dataTable();
				v.oApi = eb.oApi;
				v.sDestroyWidth = a(this).width();
				if ("undefined" != typeof d && null !== d) {
					v.oInit = d;
					O(v.oFeatures, d, "bPaginate");
					O(v.oFeatures, d, "bLengthChange");
					O(v.oFeatures, d, "bFilter");
					O(v.oFeatures, d, "bSort");
					O(v.oFeatures, d, "bInfo");
					O(v.oFeatures, d, "bProcessing");
					O(v.oFeatures, d, "bAutoWidth");
					O(v.oFeatures, d, "bSortClasses");
					O(v.oFeatures, d, "bServerSide");
					O(v.oFeatures, d, "bDeferRender");
					O(v.oScroll, d, "sScrollX", "sX");
					O(v.oScroll, d, "sScrollXInner", "sXInner");
					O(v.oScroll, d, "sScrollY", "sY");
					O(v.oScroll, d, "bScrollCollapse", "bCollapse");
					O(v.oScroll, d, "bScrollInfinite", "bInfinite");
					O(v.oScroll, d, "iScrollLoadGap", "iLoadGap");
					O(v.oScroll, d, "bScrollAutoCss", "bAutoCss");
					O(v, d, "asStripClasses");
					O(v, d, "fnPreDrawCallback");
					O(v, d, "fnRowCallback");
					O(v, d, "fnHeaderCallback");
					O(v, d, "fnFooterCallback");
					O(v, d, "fnCookieCallback");
					O(v, d, "fnInitComplete");
					O(v, d, "fnServerData");
					O(v, d, "fnFormatNumber");
					O(v, d, "aaSorting");
					O(v, d, "aaSortingFixed");
					O(v, d, "aLengthMenu");
					O(v, d, "sPaginationType");
					O(v, d, "sAjaxSource");
					O(v, d, "sAjaxDataProp");
					O(v, d, "iCookieDuration");
					O(v, d, "sCookiePrefix");
					O(v, d, "sDom");
					O(v, d, "bSortCellsTop");
					O(v, d, "oSearch", "oPreviousSearch");
					O(v, d, "aoSearchCols", "aoPreSearchCols");
					O(v, d, "iDisplayLength", "_iDisplayLength");
					O(v, d, "bJQueryUI", "bJUI");
					O(v.oLanguage, d, "fnInfoCallback");
					"function" == typeof d.fnDrawCallback && v.aoDrawCallback.push({
						fn: d.fnDrawCallback,
						sName: "user"
					});
					"function" == typeof d.fnStateSaveCallback && v.aoStateSave.push({
						fn: d.fnStateSaveCallback,
						sName: "user"
					});
					"function" == typeof d.fnStateLoadCallback && v.aoStateLoad.push({
						fn: d.fnStateLoadCallback,
						sName: "user"
					});
					v.oFeatures.bServerSide && v.oFeatures.bSort && v.oFeatures.bSortClasses ? v.aoDrawCallback.push({
						fn: G,
						sName: "server_side_sort_classes"
					}) : v.oFeatures.bDeferRender && v.aoDrawCallback.push({
						fn: G,
						sName: "defer_sort_classes"
					});
					"undefined" != typeof d.bJQueryUI && d.bJQueryUI && (v.oClasses = g.oJUIClasses, "undefined" == typeof d.sDom && (v.sDom = '<"H"lfr>t<"F"ip>'));
					if ("" !== v.oScroll.sX || "" !== v.oScroll.sY) v.oScroll.iBarWidth = fb();
					"undefined" != typeof d.iDisplayStart && "undefined" == typeof v.iInitDisplayStart && (v.iInitDisplayStart = d.iDisplayStart, v._iDisplayStart = d.iDisplayStart);
					"undefined" != typeof d.bStateSave && (v.oFeatures.bStateSave = d.bStateSave, wa(v, d), v.aoDrawCallback.push({
						fn: ca,
						sName: "state_save"
					}));
					"undefined" != typeof d.iDeferLoading && (v.bDeferLoading = !0, v._iRecordsTotal = d.iDeferLoading, v._iRecordsDisplay = d.iDeferLoading);
					"undefined" != typeof d.aaData && (C = !0);
					"undefined" != typeof d && "undefined" != typeof d.aoData && (d.aoColumns = d.aoData);
					"undefined" != typeof d.oLanguage && ("undefined" != typeof d.oLanguage.sUrl && "" !== d.oLanguage.sUrl ? (v.oLanguage.sUrl = d.oLanguage.sUrl, a.getJSON(v.oLanguage.sUrl, null,
					function(a) {
						A(v, a, !0)
					}), D = !0) : A(v, d.oLanguage, !1))
				} else d = {};
				"undefined" == typeof d.asStripClasses && (v.asStripClasses.push(v.oClasses.sStripOdd), v.asStripClasses.push(v.oClasses.sStripEven));
				t = !1;
				z = a(">tbody>tr", this);
				e = 0;
				for (j = v.asStripClasses.length; e < j; e++) if (z.filter(":lt(2)").hasClass(v.asStripClasses[e])) {
					t = !0;
					break
				}
				t && (v.asDestoryStrips = ["", ""], a(z[0]).hasClass(v.oClasses.sStripOdd) && (v.asDestoryStrips[0] += v.oClasses.sStripOdd + " "), a(z[0]).hasClass(v.oClasses.sStripEven) && (v.asDestoryStrips[0] += v.oClasses.sStripEven), a(z[1]).hasClass(v.oClasses.sStripOdd) && (v.asDestoryStrips[1] += v.oClasses.sStripOdd + " "), a(z[1]).hasClass(v.oClasses.sStripEven) && (v.asDestoryStrips[1] += v.oClasses.sStripEven), z.removeClass(v.asStripClasses.join(" ")));
				t = [];
				var F, e = this.getElementsByTagName("thead");
				0 !== e.length && (Z(v.aoHeader, e[0]), t = Na(v));
				if ("undefined" == typeof d.aoColumns) {
					F = [];
					e = 0;
					for (j = t.length; e < j; e++) F.push(null)
				} else F = d.aoColumns;
				e = 0;
				for (j = F.length; e < j; e++)"undefined" != typeof d.saved_aoColumns && d.saved_aoColumns.length == j && (null === F[e] && (F[e] = {}), F[e].bVisible = d.saved_aoColumns[e].bVisible),
				B(v, t ? t[e] : null);
				if ("undefined" != typeof d.aoColumnDefs) for (e = d.aoColumnDefs.length - 1; 0 <= e; e--) {
					var E = d.aoColumnDefs[e].aTargets;
					a.isArray(E) || Ea(v, 1, "aTargets must be an array of targets, not a " + typeof E);
					t = 0;
					for (z = E.length; t < z; t++) if ("number" == typeof E[t] && 0 <= E[t]) {
						for (; v.aoColumns.length <= E[t];) B(v);
						r(v, E[t], d.aoColumnDefs[e])
					} else if ("number" == typeof E[t] && 0 > E[t]) r(v, v.aoColumns.length + E[t], d.aoColumnDefs[e]);
					else if ("string" == typeof E[t]) {
						j = 0;
						for (w = v.aoColumns.length; j < w; j++)("_all" == E[t] || a(v.aoColumns[j].nTh).hasClass(E[t])) && r(v, j, d.aoColumnDefs[e])
					}
				}
				if ("undefined" != typeof F) {
					e = 0;
					for (j = F.length; e < j; e++) r(v, e, F[e])
				}
				e = 0;
				for (j = v.aaSorting.length; e < j; e++) {
					v.aaSorting[e][0] >= v.aoColumns.length && (v.aaSorting[e][0] = 0);
					F = v.aoColumns[v.aaSorting[e][0]];
					"undefined" == typeof v.aaSorting[e][2] && (v.aaSorting[e][2] = 0);
					"undefined" == typeof d.aaSorting && "undefined" == typeof v.saved_aaSorting && (v.aaSorting[e][1] = F.asSorting[0]);
					t = 0;
					for (z = F.asSorting.length; t < z; t++) if (v.aaSorting[e][1] == F.asSorting[t]) {
						v.aaSorting[e][2] = t;
						break
					}
				}
				G(v);
				e = a(">thead", this);
				0 === e.length && (e = [b.createElement("thead")], this.appendChild(e[0]));
				v.nTHead = e[0];
				e = a(">tbody", this);
				0 === e.length && (e = [b.createElement("tbody")], this.appendChild(e[0]));
				v.nTBody = e[0];
				e = a(">tfoot", this);
				0 < e.length && (v.nTFoot = e[0], Z(v.aoFooter, v.nTFoot));
				if (C) for (e = 0; e < d.aaData.length; e++) p(v, d.aaData[e]);
				else q(v);
				v.aiDisplay = v.aiDisplayMaster.slice();
				v.bInitialised = !0; ! 1 === D && k(v)
			}
		})
	}
})(jQuery, window, document);
$.fn.wl_Alert = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Alert.methods[a]) return $.fn.wl_Alert.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Alert") ? $.extend({},
		b.data("wl_Alert"), a) : $.extend({},
		$.fn.wl_Alert.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		b.data("wl_Alert") || (b.data("wl_Alert", {}), b.bind("click.wl_Alert",
		function(a) {
			a.preventDefault();
			b.data("wl_Alert").sticky || $.fn.wl_Alert.methods.close.call(b[0])
		}).find("a").bind("click.wl_Alert",
		function(a) {
			a.stopPropagation()
		}));
		b.is(":hidden") && b.slideDown(c.speed / 2);
		c && $.extend(b.data("wl_Alert"), c)
	})
};
$.fn.wl_Alert.defaults = {
	speed: 500,
	sticky: !1,
	onBeforeClose: function() {},
	onClose: function() {}
};
$.fn.wl_Alert.version = "1.1";
$.fn.wl_Alert.methods = {
	close: function() {
		var a = $(this),
		e = a.data("wl_Alert");
		if (!1 === e.onBeforeClose.call(this, a)) return ! 1;
		a.fadeOutSlide(e.speed,
		function() {
			e.onClose.call(a[0], a)
		})
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Alert.defaults[a] || null == $.fn.wl_Alert.defaults[a] ? b.data("wl_Alert")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.wl_Alert = function(a, e, b, c, g) {
	$("div.alert").each(function() {
		var b = $(this);
		b.text() == a && b.slideUp($.fn.wl_Alert.defaults.speed)
	});
	e = $('<div class="alert ' + e + '">' + a + "</div>").hide();
	c ? e.appendTo(b).wl_Alert(g) : e.prependTo(b).wl_Alert(g);
	return e
};
$.fn.wl_Autocomplete = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Autocomplete.methods[a]) return $.fn.wl_Autocomplete.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Autocomplete") ? $.extend({},
		b.data("wl_Autocomplete"), a) : $.extend({},
		$.fn.wl_Autocomplete.defaults, a, b.data());
		else try {
			return b.autocomplete(a, e[1], e[2])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		b.data("wl_Autocomplete") || (b.data("wl_Autocomplete", {}), $.isFunction(window[c.source]) && (c.source = window[c.source].call(this)), "string" === typeof c.source && (c.source = $.parseData(c.source, !0)), b.autocomplete(c));
		c && $.extend(b.data("wl_Autocomplete"), c)
	})
};
$.fn.wl_Autocomplete.defaults = {};
$.fn.wl_Autocomplete.version = "1.0";
$.fn.wl_Autocomplete.methods = {
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Autocomplete.defaults[a] || null == $.fn.wl_Autocomplete.defaults[a] ? b.data("wl_Autocomplete")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Breadcrumb = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this),
		c = b.find("li");
		$a = b.find("a");
		if ($.fn.wl_Breadcrumb.methods[a]) return $.fn.wl_Breadcrumb.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var g = b.data("wl_Breadcrumb") ? $.extend({},
		b.data("wl_Breadcrumb"), a) : $.extend({},
		$.fn.wl_Breadcrumb.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		var d = b.find("a.active").eq(0);
		d.length || (d = $a.eq(g.start));
		if (!b.data("wl_Breadcrumb")) {
			b.data("wl_Breadcrumb", {});
			$a.each(function(a) {
				var b = $(this);
				b.data("id", a);
				g.numbers && b.text(a + 1 + ". " + b.text())
			});
			c.each(function(a) {
				var b = $(this);
				b.attr("class") && ($a.eq(a).wrapInner('<span class="' + b.attr("class") + '"/>'), b.removeAttr("class"))
			});
			$.browser.msie && c.filter(":last").addClass("last");
			b.delegate("a", "click.wl_Breadcrumb",
			function() {
				var a = b.data("wl_Breadcrumb") || a;
				if (a.disabled) return ! 1;
				var c = $(this);
				if (!a.allownextonly || 1 >= c.data("id") - b.find("a.active").data("id")) $.fn.wl_Breadcrumb.methods.activate.call(b[0], c),
				a.onChange.call(b[0], c, c.data("id"));
				return ! 1
			});
			if (g.connect) {
				var c = $("#" + g.connect),
				n = c.children();
				c.find(".next").bind("click.wl_Breadcrumb",
				function() {
					b.wl_Breadcrumb("next");
					return ! 1
				});
				c.find(".prev").bind("click.wl_Breadcrumb",
				function() {
					b.wl_Breadcrumb("prev");
					return ! 1
				});
				n.hide().eq(g.start).show()
			}
			g.disabled && b.wl_Breadcrumb("disable")
		}
		g && $.extend(b.data("wl_Breadcrumb"), g);
		$.fn.wl_Breadcrumb.methods.activate.call(this, d)
	})
};
$.fn.wl_Breadcrumb.defaults = {
	start: 0,
	numbers: !1,
	allownextonly: !1,
	disabled: !1,
	connect: null,
	onChange: function() {}
};
$.fn.wl_Breadcrumb.version = "1.0";
$.fn.wl_Breadcrumb.methods = {
	activate: function(a) {
		var e = $(this);
		if ("number" === typeof a) return a = e.find("li").eq(a).find("a"),
		a.trigger("click.wl_Breadcrumb"),
		!1;
		var b = e.data("wl_Breadcrumb");
		e.find("a").removeClass("active previous");
		a.parent().prevAll().find("a").addClass("previous");
		a.addClass("active");
		b.connect && $("#" + b.connect).children().hide().eq(a.data("id")).show()
	},
	disable: function() {
		var a = $(this);
		a.wl_Breadcrumb("set", "disabled", !0);
		a.addClass("disabled")
	},
	enable: function() {
		var a = $(this);
		a.wl_Breadcrumb("set", "disabled", !1);
		a.removeClass("disabled")
	},
	next: function() {
		$(this).find("a.active").parent().next().find("a").trigger("click.wl_Breadcrumb")
	},
	prev: function() {
		$(this).find("a.active").parent().prev().find("a").trigger("click.wl_Breadcrumb")
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Breadcrumb.defaults[a] || null == $.fn.wl_Breadcrumb.defaults[a] ? b.data("wl_Breadcrumb")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Calendar = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Calendar.methods[a]) return $.fn.wl_Calendar.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Calendar") ? $.extend({},
		b.data("wl_Calendar"), a) : $.extend({},
		$.fn.wl_Calendar.defaults, a, b.data());
		else try {
			return b.fullCalendar(a, e[1], e[2], e[3], e[4])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		if (!b.data("wl_Calendar")) {
			b.data("wl_Calendar", {});
			c.theme = !0;
			switch (c.header) {
			case "small":
				c.header = {
					left: "title",
					right: "prev,next"
				};
				break;
			case "small-today":
				c.header = {
					left: "title",
					right: "prev,today,next"
				}
			}
			b.fullCalendar(c)
		}
		c && $.extend(b.data("wl_Calendar"), c)
	})
};
$.fn.wl_Calendar.defaults = {};
$.fn.wl_Calendar.version = "1.0";
$.fn.wl_Calendar.methods = {
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Calendar.defaults[a] || null == $.fn.wl_Calendar.defaults[a] ? b.data("wl_Calendar")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Chart = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Chart.methods[a]) return $.fn.wl_Chart.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Chart") ? $.extend({},
		b.data("wl_Chart"), a) : $.extend({},
		$.fn.wl_Chart.defaults, a, b.data());
		else try {
			return b.data("wl_Chart").plot[a](e[1], e[2])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		/^(lines|bars|pie)$/.test(c.type) || $.error('Type of "' + c.type + '" is not allowed');
		b.data("wl_Chart") ? c.holder.unbind("plothover").unbind("mouseout") : (b.data("wl_Chart", {}), $(window).bind("resize.wl_Chart",
		function() {
			b.data("wl_Chart").holder.width("99%");
			$.fn.wl_Chart.methods.draw.call(b[0])
		}), c.holder = $("<div/>", {
			"class": "chart"
		}).css({
			width: 99 * ((c.width || b.width() || NaN) / 100),
			height: c.height || 300
		})[c.tableBefore ? "insertAfter": "insertBefore"](b));
		c.width = c.holder.width();
		c.hideTable ? b.hide() : b.show();
		c.holder.bind("plotclick",
		function(a, d, g) {
			if (g) {
				a = {};
				switch (c.type) {
				case "bars":
					a.value = g.datapoint[1];
					a.label = g.series.xaxis.ticks[g.dataIndex].label;
					a.id = g.seriesIndex;
					break;
				case "pie":
					a.value = g.datapoint[1][0][1];
					a.label = g.series.xaxis.options.ticks[0][1];
					a.id = g.seriesIndex;
					break;
				default:
					a.value = g.datapoint[1],
					a.label = g.series.xaxis.ticks[g.datapoint[0]].label,
					a.id = g.seriesIndex
				}
				a.legend = g.series.label;
				c.onClick.call(b[0], a.value, a.legend, a.label, a.id, g)
			}
		});
		if (c.tooltip) {
			c.holder.tipsy($.extend({},
			config.tooltip, {
				fallback: "",
				followMouse: !0,
				gravity: c.tooltipGravity || "n"
			}));
			var d = null,
			n;
			c.holder.bind("plothover",
			function(a, g, e) {
				if (e) {
					if (e.datapoint.toString() != d) {
						a = {};
						d = e.datapoint.toString();
						switch (c.type) {
						case "bars":
							a.value = c.stack ? e.datapoint[1] - e.datapoint[2] : e.datapoint[1];
							a.label = e.series.xaxis.ticks[e.dataIndex].label;
							a.id = e.seriesIndex;
							break;
						case "pie":
							a.value = e.datapoint[1][0][1];
							a.label = e.series.xaxis.options.ticks[0][1];
							a.id = e.seriesIndex;
							break;
						default:
							a.value = e.datapoint[1],
							a.label = e.series.xaxis.ticks[e.datapoint[0]].label,
							a.id = e.seriesIndex
						}
						a.legend = e.series.label;
						n = $.isFunction(c.tooltipPattern) ? c.tooltipPattern.call(b[0], a.value, a.legend, a.label, a.id, e) : c.tooltipPattern.replace(/%1/g, a.value).replace(/%2/g, a.legend).replace(/%3/g, a.label).replace(/%4/g, a.id);
						c.holder.tipsy("setTitel", n);
						c.holder.tipsy("show")
					}
				} else c.holder.tipsy("hide"),
				d = null
			}).bind("mouseout",
			function() {
				c.holder.tipsy("hide");
				d = null
			})
		}
		$.isArray(c.colors) || (c.colors = $.parseData(c.colors, !0));
		var j = [];
		if ($.isEmptyObject(c.data)) if ($.isEmptyObject(c.data) && b.is("table")) switch (c.xlabels = c.xlabels || [], c.data = [], c.orientation) {
		case "horizontal":
			var k = b.find("thead th"),
			t = b.find("tbody th"),
			A = b.find("tbody tr"),
			B = t.length;
			B && (k = k.slice(1));
			A.each(function(a, b) {
				var d = [];
				$(b).find("td").each(function(a, b) {
					var g = parseFloat(b.innerHTML);
					isNaN(g) || d.push([a, g || 0]);
					c.xlabels.push([a, k.eq(a).text()])
				});
				c.data.push({
					label: t.eq(a).text(),
					data: "pie" != c.type ? d: d[0][1]
				});
				j[a] = A.eq(a).data("color") || c.colors[a] || j[a % c.colors.length]
			});
			break;
		case "vertical":
			k = b.find("tbody th");
			t = b.find("thead th");
			A = b.find("tbody tr");
			if (B = t.length) t = t.slice(1),
			B--;
			var r = [];
			A.each(function(a, b) {
				$(b).find("td").each(function(b, c) {
					var d = parseFloat(c.innerHTML);
					r[b] = r[b] || [];
					isNaN(d) || r[b].push([a, d || 0])
				});
				c.xlabels.push([a, k.eq(a).text()])
			});
			for (var p = 0; p < B; p++) c.data.push({
				label: t.eq(p).text(),
				data: r[p]
			}),
			j[p] = c.colors[p] || j[p % c.colors.length];
			break;
		default:
			$.error('Orientation "' + c.orientation + '" is not allowed')
		} else $.error("No data or data table!");
		else {
			if (c.xlabels) c.xlabels = $.map(c.xlabels,
			function(a, b) {
				return [[b, a]]
			});
			else {
				var B = c,
				z = [];
				$.each(c.data,
				function(a) {
					$.map(c.data[a].data,
					function(a, b) {
						z[a[0]] = b
					})
				});
				p = $.map(z,
				function(a, b) {
					return b
				});
				B.xlabels = p
			}
			j = $.map(c.data,
			function(a, b) {
				return c.colors[b % c.colors.length]
			})
		}
		c.colors = j;
		B = {};
		switch (c.type) {
		case "bars":
			B = {
				points: {
					show: null !== c.points ? c.points: !1
				},
				bars: {
					order: c.stack ? null: !0,
					show: !0,
					border: !1,
					fill: null !== c.fill ? c.fill: !0,
					fillColor: null !== c.fillColor ? c.fillColor: null,
					align: c.align || "center",
					horizontal: c.horizontal || !1,
					barWidth: c.barWidth || c.stack ? 0.85 : 0.85 / c.data.length,
					lineWidth: null !== c.lineWidth ? c.lineWidth: 0
				},
				lines: {
					show: !1
				},
				pie: {
					show: !1
				}
			};
			break;
		case "pie":
			B = {
				points: {
					show: null !== c.points ? c.points: !0
				},
				bars: {
					show: !1
				},
				lines: {
					show: !1
				},
				pie: {
					show: !0,
					label: !0,
					tilt: c.tilt || 1,
					innerRadius: c.innerRadius ? c.innerRadius: 0,
					radius: c.tilt && !c.radius ? 0.8 : c.radius || 1,
					shadowSize: 2
				}
			};
			break;
		default:
			B = {
				points: {
					show: null !== c.points ? c.points: !0
				},
				bars: {
					show: !1
				},
				lines: {
					show: !0,
					lineWidth: null !== c.lineWidth ? c.lineWidth: 4,
					fill: null !== c.fill ? c.fill: !1,
					fillColor: null !== c.fillColor ? c.fillColor: null
				},
				pie: {
					show: !1
				}
			}
		}
		B = $.extend(!0, {},
		{
			series: $.extend(!0, {},
			{
				stack: c.stack ? !0 : null,
				points: {
					show: c.points
				}
			},
			B),
			shadowSize: c.shadowSize || 0,
			grid: {
				hoverable: c.tooltip,
				clickable: !0,
				color: "#666",
				borderWidth: null
			},
			legend: {
				show: c.legend,
				position: /^(ne|nw|se|sw)$/.test(c.legendPosition) ? c.legendPosition: "ne"
			},
			colors: c.colors,
			xaxis: {
				ticks: c.xlabels
			}
		},
		c.flot);
		c.flotobj = $.extend({},
		c.flotobj, B);
		c && $.extend(b.data("wl_Chart"), c);
		$.fn.wl_Chart.methods.draw.call(this)
	})
};
$.fn.wl_Chart.defaults = {
	width: null,
	height: 300,
	hideTable: !0,
	tableBefore: !1,
	data: {},
	stack: !1,
	type: "lines",
	points: null,
	shadowSize: 2,
	fill: null,
	fillColor: null,
	lineWidth: null,
	legend: !0,
	legendPosition: "ne",
	tooltip: !0,
	tooltipGravity: "n",
	tooltipPattern: function(a, e, b, c) {
		return "value is " + a + " from " + e + " at " + b + " (" + c + ")"
	},
	orientation: "horizontal",
	colors: "#b2e7b2 #f0b7b7 #b5f0f0 #e8e8b3 #efb7ef #bbb6f0".split(" "),
	flot: {},
	onClick: function() {}
};
$.fn.wl_Chart.version = "1.3";
$.fn.wl_Chart.methods = {
	draw: function() {
		var a = $(this),
		e = a.data("wl_Chart");
		a.data("wl_Chart").plot = $.plot(e.holder, e.data, e.flotobj)
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Chart.defaults[a] || null == $.fn.wl_Chart.defaults[a] ? b.data("wl_Chart")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Color = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Color.methods[a]) return $.fn.wl_Color.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Color") ? $.extend({},
		b.data("wl_Color"), a) : $.extend({},
		$.fn.wl_Color.defaults, a, b.data());
		else try {
			return b.miniColors(a, e[1])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		if (!b.data("wl_Color")) {
			b.data("wl_Color", {});
			b.bind("mousewheel.wl_Color",
			function(a, d) {
				if (c.mousewheel) {
					a.preventDefault();
					d = 0 > d ? -1 : 1;
					var g = b.data("hsb");
					a.shiftKey ? (g.s -= 2 * d, g.s = Math.round(Math.max(0, Math.min(100, g.s)))) : a.altKey && a.shiftKey ? (g.h += 5 * d, g.h = Math.round(g.h)) : (g.b += 2 * d, g.b = Math.round(Math.max(0, Math.min(100, g.b))));
					b.miniColors("value", g);
					b.trigger("change.wl_Color")
				}
			});
			b.miniColors($.extend({},
			{
				change: function() {
					$(this).trigger("change.wl_Color")
				}
			}), c).trigger("keyup.miniColors");
			b.bind("change.wl_Color",
			function() {
				var a = $(this).val();
				a && !/^#/.test(a) && (a = "#" + a);
				$(this).val(a);
				c.onChange.call(b[0], $(this).data("hsb"), a)
			}).attr("maxlength", 7);
			var d = b.val();
			d && !/^#/.test(b.val()) && b.val("#" + d.substr(0, 6))
		}
		c && $.extend(b.data("wl_Color"), c)
	})
};
$.fn.wl_Color.defaults = {
	mousewheel: !0,
	onChange: function() {}
};
$.fn.wl_Color.version = "1.0";
$.fn.wl_Color.methods = {
	destroy: function() {
		var a = $(this);
		a.removeData("wl_Color");
		a.miniColors("destroy")
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Color.defaults[a] ? b.data("wl_Color")[a] = c: "value" == a ? b.val(c).trigger("keyup.miniColors") : $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Date = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Date.methods[a]) return $.fn.wl_Date.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Date") ? $.extend({},
		b.data("wl_Date"), a) : $.extend({},
		$.fn.wl_Date.defaults, a, b.data());
		else try {
			return b.datepicker(a, e[1], e[2])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		if (!b.data("wl_Date")) {
			b.data("wl_Date", {});
			b.datepicker(c);
			b.bind("mousewheel.wl_Date",
			function(a, d) {
				if (c.mousewheel) {
					a.preventDefault();
					d = 0 > d ? -1 : 1;
					if (a.shiftKey) {
						var g = b.datepicker("getDate");
						d *= (new Date(g.getFullYear(), g.getMonth() + 1, 0)).getDate()
					}
					$.fn.wl_Date.methods.changeDay.call(b[0], d)
				}
			});
			if (c.value) {
				var d = (new Date).getTime();
				switch (c.value) {
				case "now":
				case "today":
					b.datepicker("setDate", new Date);
					break;
				case "next":
				case "tomorrow":
					b.datepicker("setDate", new Date(d + 864E5));
					break;
				case "prev":
				case "yesterday":
					b.datepicker("setDate", new Date(d + -864E5));
					break;
				default:
					isNaN(c.value) || b.datepicker("setDate", new Date(d + 864E5 * c.value))
				}
			}
			c.disabled && $.fn.wl_Date.methods.disable.call(this)
		}
		c && $.extend(b.data("wl_Date"), c)
	})
};
$.fn.wl_Date.defaults = {
	value: null,
	mousewheel: !0
};
$.fn.wl_Date.version = "1.0";
$.fn.wl_Date.methods = {
	disable: function() {
		var a = $(this);
		a.datepicker("disable");
		a.data("wl_Date").disabled = !0
	},
	enable: function() {
		var a = $(this);
		a.datepicker("enable");
		a.data("wl_Date").disabled = !1
	},
	next: function() {
		$.fn.wl_Date.methods.changeDay.call(this, 1)
	},
	prev: function() {
		$.fn.wl_Date.methods.changeDay.call(this, -1)
	},
	changeDay: function(a) {
		var e = $(this),
		b = e.datepicker("getDate") || new Date;
		b.setDate(b.getDate() + a);
		e.datepicker("setDate", b)
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Date.defaults[a] || null == $.fn.wl_Date.defaults[a] ? b.data("wl_Date")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Editor = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Editor.methods[a]) return $.fn.wl_Editor.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Editor") ? $.extend({},
		b.data("wl_Editor"), a) : $.extend({},
		$.fn.wl_Editor.defaults, a, b.data());
		else try {
			return b.wysiwyg(a, e[1], e[2], e[3])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		if (!b.data("wl_Editor")) {
			b.data("wl_Editor", {});
			c.buttons = c.buttons.split("|") || c.buttons;
			c.eOpts = {
				initialContent: c.initialContent,
				css: c.css
			};
			var d = {};
			$.each(c.buttons,
			function(a, b) {
				d[b] = {
					visible: !0
				}
			});
			$.extend(!0, c.eOpts, {
				controls: d
			},
			c.eOpts);
			b.wysiwyg(c.eOpts)
		}
		c && $.extend(b.data("wl_Editor"), c)
	})
};
$.fn.wl_Editor.defaults = {
	css: "css/light/editor.css",
	buttons: "bold|italic|underline|strikeThrough|justifyLeft|justifyCenter|justifyRight|justifyFull|highlight|colorpicker|indent|outdent|subscript|superscript|undo|redo|insertOrderedList|insertUnorderedList|insertHorizontalRule|createLink|insertImage|h1|h2|h3|h4|h5|h6|paragraph|rtl|ltr|cut|copy|paste|increaseFontSize|decreaseFontSize|html|code|removeFormat|insertTable",
	initialContent: ""
};
$.fn.wl_Editor.version = "1.1";
$.fn.wl_Editor.methods = {
	destroy: function() {
		var a = $(this);
		a.wysiwyg("destroy");
		a.removeData("wl_Editor")
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Editor.defaults[a] || null == $.fn.wl_Editor.defaults[a] ? b.data("wl_Editor")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_File = function(a) {
	var e = arguments;
	return this.each(function() {
		function b(a) {
			$.each(a.files,
			function(b, c) {
				j[c.name] && (j[c.name].fileupload = a.submit())
			})
		}
		function c(a) {
			var b = document.createElement("div");
			a = "on" + a;
			var c = a in b;
			c || (b.setAttribute || (b = document.createElement("div")), b.setAttribute && b.removeAttribute && (b.setAttribute(a, ""), c = "function" == typeof b[a], void 0 != typeof b[a] && (b[a] = void 0), b.removeAttribute(a)));
			return c
		}
		var g = $(this);
		if ($.fn.wl_File.methods[a]) return $.fn.wl_File.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var d = g.data("wl_File") ? $.extend({},
		g.data("wl_File"), a) : $.extend({},
		$.fn.wl_File.defaults, a, g.data());
		else try {
			return g.fileupload(a, e[1], e[2], e[3], e[4])
		} catch(n) {
			$.error('Method "' + a + '" does not exist')
		}
		if (!g.data("wl_File")) {
			g.data("wl_File", {});
			var j = {},
			k = [],
			t = 0,
			A,
			B = c("dragstart") && c("drop") && !!window.FileReader;
			d.multiple = g.is("[multiple]") || "string" === typeof g.prop("multiple") || d.multiple;
			d.queue = {};
			d.files = [];
			"string" === typeof d.allowedExtensions && (d.allowedExtensions = $.parseData(d.allowedExtensions));
			d.ui = $("<div>", {
				"class": "fileuploadui"
			}).insertAfter(g);
			d.autoUpload || (d.uiStart = $("<a>", {
				"class": "btn small fileupload_start",
				title: d.text.start
			}).html(d.text.start).bind("click",
			function() {
				$.each(j,
				function(a) {
					b(j[a].data)
				})
			}).appendTo(d.ui));
			d.uiCancel = $("<a>", {
				"class": "btn small fileupload_cancel",
				title: d.text.cancel_all
			}).html(d.text.cancel_all).appendTo(d.ui).bind("click",
			function() {
				var a = $(this),
				b = d.filepool.find("li");
				b.addClass("error");
				g.data("wl_File") || g.data("wl_File", A);
				k = g.data("wl_File").files = [];
				t = 0;
				$.each(j,
				function(a) {
					j[a] && (j[a].fileupload.abort(), delete j[a])
				});
				b.delay(700).fadeOut(function() {
					d.filepool.trigger("change");
					a.text(d.text.cancel_all).attr("title", d.text.cancel_all);
					$(this).remove()
				});
				d.onDelete.call(g[0], $.map(b,
				function(a) {
					return $(a).data("fileName")
				}))
			});
			d.filepool = $("<ul>", {
				"class": "fileuploadpool"
			}).insertAfter(g).delegate("a.cancel", "click",
			function() {
				var a = $(this).parent(),
				b = a.data("fileName");
				g.data("wl_File") || g.data("wl_File", A);
				g.data("wl_File").files = k = $.map(k,
				function(a) {
					if (a != b) return a
				});
				j[b].fileupload.abort();
				delete j[b];
				t--;
				a.addClass("error").delay(700).fadeOut();
				d.onCancel.call(g[0], b);
				d.filepool.trigger("change")
			}).delegate("a.remove", "click",
			function() {
				var a = $(this).parent(),
				b = a.data("fileName");
				g.data("wl_File") || g.data("wl_File", A);
				g.data("wl_File").files = k = $.map(k,
				function(a) {
					if (a != b) return a
				});
				a.fadeOut();
				d.onDelete.call(g[0], [b]);
				d.filepool.trigger("change")
			}).addClass(!d.multiple ? "single": "multiple").addClass(B ? "drop": "nodrop");
			g.fileupload({
				url: d.url,
				dropZone: d.dragAndDrop ? d.filepool: null,
				fileInput: g,
				singleFileUploads: !0,
				sequentialUploads: d.sequentialUploads,
				paramName: d.paramName + "[]",
				formData: d.formData,
				add: function(a, c) {
					d.multiple || (d.uiCancel.click(), d.filepool.find("li").remove());
					$.each(c.files,
					function(a, b) {
						b.ext = b.name.substring(b.name.lastIndexOf(".") + 1).toLowerCase();
						t++;
						var e = d.maxNumberOfFiles && (k.length >= d.maxNumberOfFiles || t > d.maxNumberOfFiles) ? {
							msg: "maxNumberOfFiles",
							code: 1
						}: d.allowedExtensions && -1 == $.inArray(b.ext, d.allowedExtensions) ? {
							msg: "allowedExtensions",
							code: 2
						}: "number" === typeof b.size && d.maxFileSize && b.size > d.maxFileSize ? {
							msg: "maxFileSize",
							code: 3
						}: "number" === typeof b.size && d.minFileSize && b.size < d.minFileSize ? {
							msg: "minFileSize",
							code: 4
						}: null;
						if (e) t--,
						d.onFileError.call(g[0], e, b);
						else {
							var e = b.name,
							n = $('<li><span class="name">' + e + '</span><span class="progress"></span><span class="status">' + d.text.ready + '</span><a class="cancel" title="' + d.text.cancel + '">' + d.text.cancel + "</a></li>").data("fileName", e).appendTo(d.filepool);
							j[e] = {
								element: n,
								data: c,
								progress: n.find(".progress"),
								status: n.find(".status"),
								cancel: n.find(".cancel")
							}
						}
					});
					g.data("wl_File") ? (g.data("wl_File").queue = j, A = g.data("wl_File")) : A && (A.queue = j);
					d.filepool.trigger("change");
					d.onAdd.call(g[0], a, c);
					d.autoUpload && b(c)
				},
				send: function(a, b) {
					$.each(b.files,
					function(a, c) {
						j[c.name].element.addClass(b.textStatus);
						j[c.name].progress.width("100%");
						j[c.name].status.text(d.text.uploading)
					});
					d.uiCancel.text(d.text.cancel_all).attr("title", d.text.cancel_all);
					return d.onSend.call(g[0], a, b)
				},
				done: function(a, b) {
					g.data("wl_File", A);
					$.each(b.files,
					function(a, c) {
						j[c.name] && (j[c.name].element.addClass(b.textStatus), j[c.name].progress.width("100%"), j[c.name].status.text(d.text.done), j[c.name].cancel.removeAttr("class").addClass("remove").attr("title", d.text.remove), -1 == $.inArray(c.name, k) && (k.push(c.name), g.data("wl_File").files = k), t--, delete j[c.name])
					});
					d.onDone.call(g[0], a, b);
					$.isEmptyObject(j) && (d.filepool.trigger("change"), d.uiCancel.text(d.text.remove_all).attr("title", d.text.remove_all), d.onFinish.call(g[0], a, b))
				},
				fail: function(a, b) {
					d.onFail.call(g[0], a, b)
				},
				always: function(a, b) {
					d.onAlways.call(g[0], a, b)
				},
				progress: function(a, b) {
					$.each(b.files,
					function(a, c) {
						if (j[c.name]) {
							var g = Math.round(parseInt(100 * (b.loaded / b.total), 10));
							j[c.name].progress.width(g + "%");
							j[c.name].status.text(d.text.uploading + g + "%")
						}
					});
					d.onProgress.call(g[0], a, b)
				},
				progressall: function(a, b) {
					d.onProgressAll.call(g[0], a, b)
				},
				start: function(a) {
					d.onStart.call(g[0], a)
				},
				stop: function(a) {
					d.onStop.call(g[0], a)
				},
				change: function(a, b) {
					d.onChange.call(g[0], a, b)
				},
				drop: function(a, b) {
					d.onDrop.call(g[0], a, b)
				},
				dragover: function(a) {
					d.onDragOver.call(g[0], a)
				}
			})
		}
		d && $.extend(g.data("wl_File"), d)
	})
};
$.fn.wl_File.defaults = {
	url: "upload.php",
	autoUpload: !0,
	paramName: "files",
	multiple: !1,
	allowedExtensions: !1,
	maxNumberOfFiles: 0,
	maxFileSize: 0,
	minFileSize: 0,
	sequentialUploads: !1,
	dragAndDrop: !0,
	formData: {},
	text: {
		ready: "ready",
		cancel: "cancel",
		remove: "remove",
		uploading: "uploading...",
		done: "done",
		start: "start upload",
		add_files: "add files",
		cancel_all: "cancel upload",
		remove_all: "remove all"
	},
	onAdd: function() {},
	onDelete: function() {},
	onCancel: function() {},
	onSend: function() {},
	onDone: function() {},
	onFinish: function() {},
	onFail: function() {},
	onAlways: function() {},
	onProgress: function() {},
	onProgressAll: function() {},
	onStart: function() {},
	onStop: function() {},
	onChange: function() {},
	onDrop: function() {},
	onDragOver: function() {},
	onFileError: function() {}
};
$.fn.wl_File.version = "1.2";
$.fn.wl_File.methods = {
	reset: function() {
		var a = $(this),
		e = a.data("wl_File");
		$.uniform && a.next().html($.uniform.options.fileDefaultText);
		e.filepool.empty();
		e.uiCancel.attr("title", e.text.cancel_all).text(e.text.cancel_all);
		a.data("wl_File").files = []
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_File.defaults[a] || null == $.fn.wl_File.defaults[a] ? b.data("wl_File")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.confirm = function(a, e, b) {
	var c = $.extend(!0, {},
	$.alert.defaults, $.confirm.defaults);
	if (c.nativ)(result = confirm(unescape(a))) ? $.isFunction(e) && e.call(this) : $.isFunction(b) && b.call(this);
	else return c = $.extend({},
	{
		buttons: [{
			text: c.text.ok,
			click: function() {
				$.isFunction(e) && e.call(this);
				$(this).dialog("close");
				$("#wl_dialog").remove()
			}
		},
		{
			text: c.text.cancel,
			click: function() {
				$.isFunction(b) && b.call(this);
				$(this).dialog("close");
				$("#wl_dialog").remove()
			}
		}]
	},
	c),
	$.alert(unescape(a), c)
};
$.confirm.defaults = {
	text: {
		header: "Please confirm",
		ok: "Yes",
		cancel: "No"
	}
};
$.prompt = function(a, e, b, c) {
	var g = $.extend(!0, {},
	$.alert.defaults, $.prompt.defaults);
	if (g.nativ) a = prompt(unescape($.trim(a)), unescape(e)),
	$.isFunction(b) && null !== a ? b.call(this, a) : $.isFunction(c) && c.call(this);
	else {
		var d = function(a) {
			$.isFunction(b) && b.call(this, a);
			$(this).dialog("close");
			$("#wl_dialog").remove()
		},
		g = $.extend({},
		{
			buttons: [{
				text: g.text.ok,
				click: function() {
					d.call(this, $("#wl_promptinputfield").val())
				}
			},
			{
				text: g.text.cancel,
				click: function() {
					$.isFunction(c) && c.call(this);
					$(this).dialog("close");
					$("#wl_dialog").remove()
				}
			}],
			open: function() {
				$("#wl_promptinputfield").focus().select();
				$("#wl_promptinputfield").uniform();
				$("#wl_promptinputform").bind("submit",
				function(a) {
					a.preventDefault();
					d.call(this, $("#wl_promptinputfield").val());
					$(this).parent().dialog("close");
					$("#wl_dialog").remove()
				})
			}
		},
		g);
		return $.alert("<p>" + unescape(a) + '</p><form id="wl_promptinputform"><input id="wl_promptinputfield" name="wl_promptinputfield" value="' + unescape(e) + '"></form>', g)
	}
};
$.prompt.defaults = {
	text: {
		header: "Please prompt",
		ok: "OK",
		cancel: "Cancel"
	}
};
$.alert = function(a, e) {
	e || (e = $.extend(!0, {},
	{
		buttons: [{
			text: $.alert.defaults.text.ok,
			click: function() {
				$(this).dialog("close");
				$("#wl_dialog").remove()
			}
		}]
	},
	$.alert.defaults));
	if (e.nativ) alert(a);
	else {
		var b = $("<div/>", {
			id: "wl_dialog"
		}).appendTo("body");
		e.text.header && b.attr("title", e.text.header);
		b.html(a.replace(/\n/g, "<br>"));
		b.dialog(e);
		return {
			close: function(a) {
				b.dialog("close");
				b.remove();
				$.isFunction(a) && a.call(this)
			},
			setHeader: function(a) {
				this.set("title", a)
			},
			setBody: function(a) {
				b.html(a)
			},
			set: function(a, g) {
				b.dialog("option", a, g)
			}
		}
	}
};
$.alert.defaults = {
	nativ: !1,
	resizable: !1,
	modal: !0,
	text: {
		header: "Notification",
		ok: "OK"
	}
};
$.msg = function(a, e) {
	function b(a, d, g) {
		$.isFunction(d) ? (g = d, d = 0) : d || (d = 0);
		setTimeout(function() {
			c.data("pause") ? b(a, d) : a.fadeOutSlide(e.fadeTime,
			function() {
				var b = $(".msg-box").length;
				2 > b && $(".msg-box-close").length && $(".msg-box-close").fadeOutSlide(e.fadeTime);
				c.data("msgcount", b);
				$.isFunction(g) && g.call(a)
			})
		},
		d)
	}
	e = $.extend({},
	$.msg.defaults, e);
	var c = $("#wl_msg"),
	g;
	if (!c.length) {
		var c = $("<div/>", {
			id: "wl_msg"
		}).appendTo("body").data("msgcount", 0),
		d = parseInt(c.css("top"), 10);
		c.bind("mouseenter",
		function() {
			c.data("pause", !0)
		}).bind("mouseleave",
		function() {
			c.data("pause", !1)
		});
		c.delegate(".msg-close", "click",
		function() {
			c.data("pause", !1);
			b($(this).parent())
		});
		c.delegate(".msg-box-close", "click",
		function() {
			c.fadeOutSlide(e.fadeTime)
		});
		$(window).unbind("scroll.wl_msg").bind("scroll.wl_msg",
		function() {
			var a = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
			a > d ? void 0 === window.navigator.standalone ? c.css({
				position: "fixed",
				top: 10
			}) : c.css({
				top: a + 10
			}) : void 0 === window.navigator.standalone ? c.removeAttr("style") : c.css({
				top: d
			})
		}).trigger("scroll.wl_msg")
	}
	if (!a) return ! 1;
	var n = c.data("msgcount");
	g = $('<div class="msg-box"><h3>' + (e.header || "") + '</h3><a class="msg-close">close</a><div class="msg-content">' + a.replace("\n", "<br>") + "</div></div>");
	closeall = $(".msg-box-close");
	n ? closeall.length ? g.insertBefore(closeall) : (g.appendTo(c), $('<div class="msg-box-close">close all</div>').appendTo(c).fadeInSlide(e.fadeTime)) : g.appendTo(c);
	g.fadeInSlide(e.fadeTime);
	c.data("msgcount", ++n);
	e.sticky || b(g, e.live);
	return {
		close: function(a) {
			b(g, a)
		},
		setHeader: function(a) {
			g.find("h3").eq(0).text(a)
		},
		setBody: function(a) {
			g.find(".msg-content").eq(0).html(a)
		},
		closeAll: function(a) {
			c.fadeOutSlide(e.fadeTime,
			function() {
				$.isFunction(a) && a.call(this)
			})
		}
	}
};
$.msg.defaults = {
	header: null,
	live: 5E3,
	topoffset: 90,
	fadeTime: 500,
	sticky: !1
};
$(document).ready(function() {
	$.msg(!1)
});
$.fn.wl_Fileexplorer = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Fileexplorer.methods[a]) return $.fn.wl_Fileexplorer.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Fileexplorer") ? $.extend({},
		b.data("wl_Fileexplorer"), a) : $.extend({},
		$.fn.wl_Fileexplorer.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		b.data("wl_Fileexplorer") || (b.data("wl_Fileexplorer", {}), b.elfinder(c));
		c && $.extend(b.data("wl_Fileexplorer"), c)
	})
};
$.fn.wl_Fileexplorer.defaults = {
	url: "elfinder/php/connector.php",
	toolbar: ["back reload open select quicklook info rename copy cut paste rm mkdir mkfile upload duplicate edit archive extract resize icons list help".split(" ")]
};
$.fn.wl_Fileexplorer.version = "1.0";
$.fn.wl_Fileexplorer.methods = {
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Fileexplorer.defaults[a] || null == $.fn.wl_Fileexplorer.defaults[a] ? b.data("wl_Fileexplorer")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Form = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Form.methods[a]) return $.fn.wl_Form.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Form") ? $.extend({},
		b.data("wl_Form"), a) : $.extend({},
		$.fn.wl_Form.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		var g = b.find("input,textarea,select,div.date,div.slider"),
		d = c.submitButton instanceof jQuery ? c.submitButton: b.find(c.submitButton),
		n = c.resetButton instanceof jQuery ? c.resetButton: b.find(c.resetButton);
		b.data("wl_Form") || (b.data("wl_Form", {}), c.action = b.attr("action") || c.action, c.method = b.attr("method") || c.method, b.bind({
			"submit.wl_Form": function(a) {
				a.preventDefault();
				return ! 1
			},
			"reset.wl_Form": function(a) {
				a.preventDefault();
				return ! 1
			}
		}), d.bind("click.wl_Form",
		function() {
			$.fn.wl_Form.methods.submit.call(b[0]);
			return ! 1
		}), n.bind("click.wl_Form",
		function() {
			$.fn.wl_Form.methods.reset.call(b[0]);
			return ! 1
		}), g.each(function() {
			var a = $(this),
			b = a.closest("section"),
			c = b.find("label").eq(0).html() || this.name || this.id || "";
			if (a.is("[required]") || "string" == typeof a.prop("required")) a.data("required", !0),
			b.find("span.required").length || b.find("label").eq(0).append('&nbsp;<span class="required">&nbsp;</span>');
			a.data("wl_label", c.replace(/<span>([^<]+)<\/span>|<([^>]+)\/?>/g, ""));
			a.data("wl_initdata",
			function() {
				var b = a.attr("type");
				if ("checkbox" == b || "radio" == b) return a.prop("checked");
				if (a.data("wl_Date")) return a.datepicker("getDate");
				if (a.data("wl_Slider")) {
					if (!a.data("wl_Slider").connect) return a.data("wl_Slider").range ? a.slider("option", "values") : a.slider("option", "value")
				} else return a.val()
			} ())
		}), c.status && !d.closest("div").is("span.wl_formstatus") && d.closest("div").append('<span class="wl_formstatus"></span>'), c.parseQuery && b.wl_Form.methods.unserialize.call(this));
		c && $.extend(b.data("wl_Form"), c)
	})
};
$.fn.wl_Form.defaults = {
	submitButton: "button.submit",
	resetButton: "button.reset",
	method: "post",
	action: null,
	ajax: !0,
	serialize: !1,
	parseQuery: !0,
	dataType: "text",
	status: !0,
	sent: !1,
	confirmSend: !0,
	text: {
		required: "This field is required",
		valid: "This field is invalid",
		password: "This password is to short",
		passwordmatch: "This password doesn't match",
		fileinqueue: "There is at least one file in the queue",
		incomplete: "Please fill out the form correctly!",
		send: "send form...",
		sendagain: "send again?",
		success: "form sent!",
		error: "error while sending!",
		parseerror: "Can't unserialize query string:\n %e"
	},
	tooltip: {
		gravity: "nw"
	},
	onRequireError: function() {},
	onValidError: function() {},
	onPasswordError: function() {},
	onFileError: function() {},
	onBeforePrepare: function() {},
	onBeforeSubmit: function() {},
	onReset: function() {},
	onComplete: function() {},
	onError: function() {},
	onSuccess: function() {}
};
$.fn.wl_Form.version = "1.3.5";
$.fn.wl_Form.methods = {
	disable: function() {
		var a = $(this);
		a.find(a.data("wl_Form").submitButton + ",input,textarea,select,div.date,div.slider").each(function() {
			var a = $(this);
			a.is("div") ? a.is("div.slider") && a.data("wl_Slider") ? a.wl_Slider("disable") : a.is("div.date") && a.data("wl_Date") && a.wl_Date("disable") : a.prop("disabled", !0)
		});
		a.data("wl_Form").disabled = !0
	},
	enable: function() {
		var a = $(this);
		a.find(a.data("wl_Form").submitButton + ",input,textarea,select,div.date,div.slider").each(function() {
			var a = $(this);
			a.is("div") ? a.is("div.slider") && a.data("wl_Slider") ? a.wl_Slider("enable") : a.is("div.date") && a.data("wl_Date") && a.wl_Date("enable") : a.prop("disabled", !1)
		});
		a.data("wl_Form").disabled = !1
	},
	reset: function() {
		var a = $(this),
		e = a.find("input,textarea,select,div.date,div.slider");
		if (!1 === a.data("wl_Form").onReset.call(a[0])) return ! 1;
		a.find("section.error").removeClass("error");
		e.each(function() {
			var a = $(this),
			c = a.attr("type");
			"checkbox" == c ? a.prop("checked", a.data("wl_initdata")).trigger("change") : "radio" == c ? a.prop("checked", a.data("wl_initdata")).trigger("change") : a.data("wl_Date") ? a.datepicker("setDate", a.data("wl_initdata")) : a.data("wl_Time") ? a.val(a.data("wl_initdata")) : a.data("wl_Multiselect") ? (a.wl_Multiselect("clear"), a.wl_Multiselect("select", a.data("wl_initdata"))) : a.data("wl_Slider") ? a.data("wl_Slider").connect || (a.data("wl_Slider").range ? a.slider("option", "values", $.parseData(a.data("wl_initdata"))) : a.slider("option", "value", a.data("wl_initdata")), a.wl_Slider("change"), a.wl_Slider("slide")) : a.data("wl_File") ? a.wl_File("reset") : a.data("wl_Editor") ? a.val(a.data("wl_initdata")).wysiwyg("setContent", a.data("wl_initdata")) : a.data("wl_Color") ? a.wl_Color("set", "value", a.data("wl_initdata")) : (a.val(a.data("wl_initdata")).trigger("change"), a.is("[placeholder]") && ("" == a.data("wl_initdata") || a.data("wl_initdata") == a.attr("placeholder") ? a.addClass("placeholder").val(a.attr("placeholder")).data("uservalue", !1) : a.removeClass("placeholder").data("uservalue", !0)))
		})
	},
	submit: function() {
		var a = $(this),
		e = {},
		b = a.data("wl_Form"),
		c = a.find("input,textarea,select,div.date,div.slider"),
		g = a.find(".wl_formstatus"),
		d = [],
		n = [],
		j = [],
		k = [],
		t = !0,
		A = b.submitButton instanceof jQuery ? b.submitButton: a.find(b.submitButton),
		B = function(a, b, c, d) {
			$("#" + b).length || $("<input>", {
				type: "hidden",
				id: b,
				name: c,
				value: d
			}).insertAfter(a)
		};
		g.text("");
		c.each(function() {
			var a = $(this),
			b = a.closest("section");
			b.removeClass("error");
			a.prop("placeholder") && (a.val() == a.prop("placeholder") && !a.data("uservalue")) && a.val("");
			a.data("required") && ((!a.val() || a.is(":checkbox") && !a.is(":checked")) && !a.data("wl_File") ? (d.push(a), t = !1) : a.is(":radio") ? $("input[name=" + a.attr("name") + "]:checked").length || (d.push(a), t = !1) : a.data("wl_File") && !a.data("wl_File").files.length && (d.push(b.find(".fileuploadpool").eq(0)), t = !1));
			a.data("wl_Valid") && !a.data("wl_Valid").valid && (n.push(a), t = !1);
			a.data("wl_File") && !$.isEmptyObject(a.data("wl_File").queue) && (k.push(b.find(".fileuploadpool").eq(0)), t = !1);
			if (a.data("wl_Password") && (b = a.val(), a.data("wl_Password").confirm && a.data("wl_Password").connect && b != $("#" + a.data("wl_Password").connect).val() || b && b.length < a.data("wl_Password").minLength)) j.push(a),
			t = !1
		});
		if (!t) return $.each(d,
		function(a, c) {
			c.closest("section").addClass("error");
			b.onRequireError.call(c[0], c);
			var d = c.attr("original-title");
			d && c.removeData("tipsy").removeAttr("original-title");
			c.tipsy($.extend({},
			config.tooltip, b.tooltip, {
				trigger: "manual",
				fallback: c.data("errortext") || b.text.required
			}));
			c.tipsy("show");
			c.is(":radio") ? $("input[name=" + c.attr("name") + "]").bind("focus.tooltip, click.tooltip, change.tooltip",
			function() {
				c.unbind("focus.tooltip, click.tooltip, change.tooltip").tipsy("hide");
				d && c.attr("title", d).tipsy(config.tooltip)
			}) : c.bind("focus.tooltip, click.tooltip, change.tooltip",
			function() {
				$(this).unbind("focus.tooltip, click.tooltip, change.tooltip").tipsy("hide");
				d && c.attr("original-title", d).tipsy(config.tooltip)
			})
		}),
		$.each(n,
		function(a, c) {
			c.closest("section").addClass("error");
			b.onValidError.call(c[0], c);
			var d = c.attr("original-title");
			d && c.removeData("tipsy").removeAttr("original-title");
			c.tipsy($.extend({},
			config.tooltip, b.tooltip, {
				trigger: "manual",
				fallback: c.data("errortext") || c.data("wl_Valid").errortext || b.text.valid
			}));
			c.tipsy("show");
			c.bind("focus.tooltip, click.tooltip",
			function() {
				$(this).unbind("focus.tooltip, click.tooltip").tipsy("hide");
				d && c.attr("original-title", d).tipsy(config.tooltip)
			})
		}),
		$.each(j,
		function(a, c) {
			var d = c.val();
			c.closest("section").addClass("error");
			var g = c.attr("original-title");
			g && c.removeData("tipsy").removeAttr("original-title");
			if (c.data("wl_Password").confirm) {
				var e = $("#" + c.data("wl_Password").connect);
				d != e.val() && (e.tipsy($.extend({},
				config.tooltip, b.tooltip, {
					trigger: "manual",
					fallback: e.data("errortext") || b.text.passwordmatch
				})), e.tipsy("show"), e.bind("focus.tooltip, click.tooltip",
				function() {
					$(this).unbind("focus.tooltip, click.tooltip").tipsy("hide");
					g && c.attr("original-title", g).tipsy(config.tooltip)
				}))
			}
			d.length < c.data("wl_Password").minLength && (b.onPasswordError.call(c[0], c), c.tipsy($.extend({},
			config.tooltip, b.tooltip, {
				trigger: "manual",
				fallback: c.data("errortext") || b.text.password
			})), c.tipsy("show"), c.bind("focus.tooltip, click.tooltip",
			function() {
				$(this).unbind("focus.tooltip, click.tooltip").tipsy("hide");
				g && c.attr("original-title", g).tipsy(config.tooltip)
			}))
		}),
		$.each(k,
		function(a, c) {
			c.closest("section").addClass("error");
			b.onFileError.call(c[0], c);
			c.tipsy($.extend({},
			config.tooltip, b.tooltip, {
				trigger: "manual",
				fallback: c.data("errortext") || b.text.fileinqueue
			}));
			c.tipsy("show");
			c.bind("focus.tooltip, click.tooltip, change.tooltip",
			function() {
				$(this).unbind("focus.tooltip, click.tooltip, change.tooltip").tipsy("hide")
			})
		}),
		g.text(b.text.incomplete),
		!1;
		if (b.confirmSend && !0 === b.sent) return $.confirm(b.text.sendagain,
		function() {
			b.sent = !1;
			$.fn.wl_Form.methods.submit.call(a[0])
		}),
		!1;
		if (!1 === b.onBeforePrepare.call(a[0])) return ! 1;
		c.each(function(c, d) {
			var g = $(d),
			j = g.attr("name") || d.id,
			k = null;
			if (g.data("wl_Date")) {
				var n = a.find("input[data-connect=" + d.id + "]").eq(0),
				t = new Date(g.datepicker("getDate")),
				A = t.getFullYear() + "-" + $.leadingZero(t.getMonth() + 1) + "-" + $.leadingZero(t.getDate());
				t.getTime() && (n.length ? (k = A + " " + (n.data("wl_Time").time || "00:00"), b.ajax || B(g, j + "_wlHidden", j, k)) : (k = A, b.ajax || g.val(k))); ! b.ajax && g.is("div") && B(g, j + "_wlHidden", j, k);
				e[j] = k
			} else if (g.data("wl_Slider")) if (g.data("wl_Slider").connect) b.ajax || (!0 !== g.data("wl_Slider").range ? (n = $("#" + g.data("wl_Slider").connect), n.attr("name") || n.attr("name", g.data("wl_Slider").connect)) : (n = $.parseData(g.data("wl_Slider").connect, !0), g = $("#" + n[0]), j = $("#" + n[1]), g.attr("name") || g.attr("name", n[0]), j.attr("name") || j.attr("name", n[1])));
			else {
				if (!0 !== g.data("wl_Slider").range) k = g.slider("option", "value"),
				b.ajax || B(g, j + "_wlHidden", j, k);
				else if (k = g.slider("option", "values"), !b.ajax) for (c = k.length - 1; 0 <= c; c--) B(g, j + "_" + c + "_wlHidden", j + "[]", k[c]);
				e[j] = k
			} else if (g.data("wl_Editor")) g.wysiwyg("save"),
			e[j] = g.val();
			else if (g.data("wl_File")) if (e[j] = g.data("wl_File").files, $.isEmptyObject(e[j])) e[j] = null,
			b.ajax || B(g, j + "_wlHidden", j, "null");
			else {
				if (!b.ajax) for (c = e[j].length - 1; 0 <= c; c--) B(g, j + "_" + c + "_wlHidden", j + "[]", e[j][c])
			} else g.data("wl_Time") ? g.data("wl_Time").connect || (e[j] = g.data("wl_Time").time, b.ajax || B(g, j + "_wlHidden", j, g.data("wl_Time").time)) : g.data("wl_Password") ? (g.data("wl_Password").confirmfield || (e[j] = g.val()), !b.ajax && g.data("wl_Password").confirmfield && g.prop("disabled", !0)) : g.is(":radio") ? g.is(":checked") && (e[j] = "on" != g.val() ? g.val() : d.id) : g.is(":checkbox") ? (/\[\]$/.test(j) ? (e[j] = e[j] || [], g.is(":checked") && (n = g.val(), e[j].push("on" != n ? n: g.attr("id") || n))) : g.is(":checked") ? (n = g.val(), e[j] = "on" != n ? n: g.is(":checked") || g.attr("id") || n) : e[j] = 0, !0 === e[j] ? e[j] = 1 : !1 === e[j] && (e[j] = 0), b.ajax || B(g, j + "_wlHidden", j, e[j])) : g.data("wl_Number") ? (k = g.val(), isNaN(k) && (k = null), e[j] = k) : (n = g.val(), /\[\]$/.test(j) && !$.isArray(n) ? (e[j] = e[j] || [], e[j].push(n)) : e[j] = n)
		});
		if (c = A.attr("name")) e[c] = A.attr("value") || !0,
		b.ajax || B(A, c + "_wlHidden", c, e[c]);
		A = b.onBeforeSubmit.call(a[0], e);
		if (!1 === A) return ! 1;
		"object" == typeof A && (e = A);
		b.serialize && (e = $.param(e));
		g.text(b.text.send);
		if (!b.ajax) return a.unbind("submit.wl_Form"),
		a.submit(),
		!1;
		$.fn.wl_Form.methods.disable.call(this);
		$.ajax({
			url: b.action,
			type: b.method,
			data: e,
			dataType: b.dataType,
			success: function(c, d, e) {
				g.textFadeOut(b.text.success);
				b.onSuccess.call(a[0], c, d, e)
			},
			complete: function(c, d) {
				$.fn.wl_Form.methods.enable.call(a[0]);
				b.sent = !0;
				b.onComplete.call(a[0], d, c)
			},
			error: function(c, d, e) {
				g.text(b.text.error);
				b.onError.call(a[0], d, e, c)
			}
		})
	},
	unserialize: function(a) {
		var e = $(this);
		if (a = a || location.search.substr(1)) try {
			values = decodeURIComponent(a).split("&");
			var b = [];
			$.each(values,
			function() {
				var a = this.split("="),
				c = a.shift(),
				a = a.join("=");
				"undefined" !== typeof c && "undefined" !== typeof a && (c = c.replace(/\+/g, " "), /\[\]$/.test(c) ? (c = c.replace("[]", ""), b[c] = b[c] || [], b[c].push(a.replace(/\+/g, " "))) : b[c] = a.replace(/\+/g, " "))
			});
			values = b;
			e.find("input,textarea,select,div.date,div.slider").each(function() {
				var a = $(this),
				b = a.attr("type"),
				c = this.name || this.id;
				/\[\]$/.test(c) && (c = c.replace("[]", ""));
				if (null != values[c]) if ("checkbox" == b) a.data("wl_initdata", "true" == values[c]).prop("checked", "true" == values[c]);
				else if ("radio" == b) $('input[id="' + values[c] + '"]').data("wl_initdata", !0).attr("checked", !0);
				else if ("password" != b) if (a.data("wl_Date") && a.is("input")) if (/(\d\d:\d\d)$/.test(values[c])) {
					var b = values[c].substr(11),
					e = values[c].substr(0, 10);
					a.data("wl_initdata", new Date(e)).datepicker("setDate", new Date(e));
					$('input[data-connect="' + c + '"]').data("wl_initdata", b).val(b).data("wl_Time").time = b
				} else a.data("wl_initdata", new Date(values[c])).datepicker("setDate", new Date(values[c]));
				else a.data("wl_Date") && a.is("div") ? a.data("wl_initdata", new Date(values[c])).datepicker("setDate", new Date(values[c])) : a.data("wl_Color") ? a.data("wl_initdata", values[c]).wl_Color("set", "value", values[c]) : a.data("wl_Slider") ? a.data("wl_Slider").connect || (a.data("wl_Slider").range ? a.slider("option", "values", $.parseData(values[c])) : a.slider("option", "value", values[c]), a.data("wl_initdata", values[c]), a.wl_Slider("change"), a.wl_Slider("slide")) : a.data("wl_Multiselect") ? a.data("wl_initdata", values[c]).wl_Multiselect("select", values[c]) : a.data("wl_Editor") ? a.data("wl_initdata", values[c]).val(values[c]).wysiwyg("setContent", values[c]) : a.data("wl_initdata", values[c]).val(values[c]).trigger("change")
			})
		} catch(c) {
			$.msg(e.data("wl_Form").text.parseerror.replace("%e", c))
		}
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Form.defaults[a] && null !== $.fn.wl_Form.defaults[a] ? b.data("wl_Form")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Gallery = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Gallery.methods[a]) return $.fn.wl_Gallery.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Gallery") ? $.extend({},
		b.data("wl_Gallery"), a) : $.extend({},
		$.fn.wl_Gallery.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		var g = b.find("a");
		b.data("wl_Gallery") || (b.data("wl_Gallery", {}), b.sortable({
			containment: b,
			opacity: 0.8,
			distance: 5,
			handle: "img",
			forceHelperSize: !0,
			placeholder: "sortable_placeholder",
			forcePlaceholderSize: !0,
			start: function(a, c) {
				b.dragging = !0;
				c.item.trigger("mouseleave")
			},
			stop: function() {
				b.dragging = !1
			},
			update: function(a, g) {
				var e = g.item.find("a").eq(0);
				c.onMove.call(b[0], g.item, e.attr("href"), e.attr("title"), b.find("li"))
			}
		}), c.images = [], g.attr("rel", c.group).fancybox(c.fancybox), g.each(function() {
			var a = $(this),
			b = a.find("img"),
			g = $("<span>");
			c.editBtn && g.append('<a class="edit">Edit</a>');
			c.deleteBtn && g.append('<a class="delete">Delete</a>'); (c.deleteBtn || c.editBtn) && a.append(g);
			c.images.push({
				image: b.attr("rel") || b.attr("src"),
				thumb: b.attr("src"),
				title: b.attr("title"),
				description: b.attr("alt")
			})
		}), (c.deleteBtn || c.editBtn) && b.delegate("li", "mouseenter",
		function() {
			if (!b.dragging) {
				var a = $(this),
				c = a.find("img"),
				a = a.find("span");
				c.animate({
					top: -20
				},
				200);
				a.animate({
					top: 80
				},
				200)
			}
		}).delegate("li", "mouseleave",
		function() {
			var a = $(this),
			b = a.find("img"),
			a = a.find("span");
			b.animate({
				top: 0
			},
			200);
			a.animate({
				top: 140
			},
			200)
		}), c.editBtn && b.find("a.edit").bind("click.wl_Gallery touchstart.wl_Gallery",
		function(a) {
			a.stopPropagation();
			a.preventDefault();
			var c = b.data("wl_Gallery") || c;
			a = $(this).parent().parent().parent();
			var g = a.find("a")[0].href,
			e = a.find("a")[0].title;
			c.onEdit.call(b[0], a, g, e);
			return ! 1
		}), c.deleteBtn && b.find("a.delete").bind("click.wl_Gallery touchstart.wl_Gallery",
		function(a) {
			a.stopPropagation();
			a.preventDefault();
			var c = b.data("wl_Gallery") || c;
			a = $(this).parent().parent().parent();
			var g = a.find("a")[0].href,
			e = a.find("a")[0].title;
			c.onDelete.call(b[0], a, g, e);
			return ! 1
		}));
		c && $.extend(b.data("wl_Gallery"), c)
	})
};
$.fn.wl_Gallery.defaults = {
	group: "wl_gallery",
	editBtn: !0,
	deleteBtn: !0,
	fancybox: {},
	onEdit: function() {},
	onDelete: function() {},
	onMove: function() {}
};
$.fn.wl_Gallery.version = "1.3";
$.fn.wl_Gallery.methods = {
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Gallery.defaults[a] || null == $.fn.wl_Gallery.defaults[a] ? b.data("wl_Gallery")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Multiselect = function(a) {
	var e = arguments;
	return this.each(function() {
		function b() {
			var a = d.pool.find("li").not(".used"),
			b = [];
			$.each(a,
			function(a) {
				$(this).data("pos", a)
			});
			a = d.selection.find("li");
			$.each(a,
			function(a) {
				var c = $(this);
				c.data("pos", a);
				d.items[c.data("value")].data("native").appendTo(g);
				b.push(c.data("value"))
			});
			g.data("wl_Multiselect").selected = b
		}
		function c(a) {
			var b = $(this),
			c = a.data.list.find("li.selected");
			if (b.hasClass("used")) return ! 1; ! a.shiftKey && !a.ctrlKey && c.removeClass("selected");
			if (a.shiftKey) {
				var d, c = a.data.list.find("li").not(".used");
				b.data("pos") > a.data.list.data("last") ? (d = a.data.list.data("last"), b = b.data("pos")) : (d = b.data("pos"), b = a.data.list.data("last"));
				for (; d <= b; d++) c.eq(d).addClass("selected");
				a.data.list.data("last", b)
			} else a.data.list.data("last", b.data("pos")),
			b.toggleClass("selected");
			return ! 1
		}
		var g = $(this);
		if ($.fn.wl_Multiselect.methods[a]) return $.fn.wl_Multiselect.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var d = g.data("wl_Multiselect") ? $.extend({},
		g.data("wl_Multiselect"), a) : $.extend({},
		$.fn.wl_Multiselect.defaults, a, g.data());
		else $.error('Method "' + a + '" does not exist');
		g.data("wl_Multiselect") || (g.data("wl_Multiselect", {}), g.hide(), $('<div class="comboselectbox"><div class="combowrap"><ul class="comboselect"></ul></div><div class="comboselectbuttons"><a class="add btn"></a><a class="addall btn"></a><a class="removeall btn"></a><a class="remove btn"></a></div><div class="combowrap"><ul class="comboselect"></ul></div></div>').insertAfter(g));
		var n = g.next(".comboselectbox"),
		j = n.find("a.add"),
		k = n.find("a.remove"),
		t = n.find("a.addall"),
		A = n.find("a.removeall"),
		B = n.find(".comboselect"),
		r = $([]),
		p = g.attr("name"),
		z = 0;
		d.pool = B.eq(0);
		d.selection = B.eq(1);
		d.searchfield && ($searchfield = $("<input>").bind("keyup.wl_Multiselect",
		function() {
			var a = RegExp($(this).val(), "i");
			$.each(d.pool.find("li").not(".used"),
			function(b, c) {
				var d = $(c),
				g = d.data("value") + d.data("name");
				a.test(g) ? d.removeClass("hidden") : d.addClass("hidden")
			})
		}).prependTo(n.addClass("searchable").find(".combowrap").eq(0)));
		/\[\]$/.test(p) || g.attr("name", p + "[]");
		n.height(d.height);
		d.items.length && (d.selected.length && !$.isArray(d.selected) && (d.selected = [d.selected]), $.each(d.items,
		function(a, b) {
			var c, g, e = "";
			"object" == typeof b ? (c = b.name, g = b.value) : c = g = b;
			d.selected.length && -1 != $.inArray(g, d.selected) && (e = " selected");
			r = r.add($('<option value="' + g + '"' + e + ">" + c + "</option>"))
		}), r.appendTo(g));
		g.bind("change.wl_Multiselect",
		function() {
			b()
		});
		d.items = [];
		d.selected = [];
		$.each(g.find("option"),
		function(a) {
			var b = $(this),
			c = b.text(),
			g = b.val();
			a = $("<li><a>" + c + "</a></li>").append('<a class="add"></a>').data({
				pos: a,
				value: g,
				name: c,
				"native": b
			}).appendTo(d.pool);
			d.items[g] = a;
			b.is(":selected") && (d.selected.push(g), a.clone(!0).data({
				pos: z++
			}).attr("data-value", g).append('<a class="remove"></a>').appendTo(d.selection).find(".add").remove(), a.data("native").prop("selected", !0), a.addClass("used"), d.showUsed || a.hide())
		});
		j.bind("click.wl_Multiselect",
		function() {
			var a = $.map(d.pool.find("li.selected"),
			function(a) {
				return $(a).data("value")
			});
			d.searchfield && $searchfield.focus();
			g.wl_Multiselect("select", a)
		});
		k.bind("click.wl_Multiselect",
		function() {
			var a = $.map(d.selection.find("li.selected"),
			function(a) {
				return $(a).data("value")
			});
			g.wl_Multiselect("unselect", a)
		});
		t.bind("click.wl_Multiselect",
		function() {
			var a = $.map(d.pool.find("li").not(".hidden"),
			function(a) {
				return $(a).data("value")
			});
			d.searchfield && $searchfield.val("").trigger("keyup").focus();
			g.wl_Multiselect("select", a)
		});
		A.bind("click.wl_Multiselect",
		function() {
			var a = $.map(d.selection.find("li"),
			function(a) {
				return $(a).data("value")
			});
			g.wl_Multiselect("unselect", a)
		});
		d.pool.delegate("li", "click.wl_Multiselect", {
			list: d.pool
		},
		c).delegate("li", "dblclick.wl_Multiselect",
		function() {
			g.wl_Multiselect("select", $(this).data("value"))
		}).delegate("a.add", "click.wl_Multiselect",
		function() {
			g.wl_Multiselect("select", $(this).parent().data("value"))
		}).disableSelection();
		d.selection.delegate("li", "click.wl_Multiselect", {
			list: d.selection
		},
		c).delegate("a.remove", "click.wl_Multiselect",
		function() {
			g.wl_Multiselect("unselect", $(this).parent().data("value"))
		});
		d.selection.sortable({
			containment: d.selection,
			distance: 20,
			handle: "a:first",
			forcePlaceholderSize: !0,
			forceHelperSize: !0,
			update: function() {
				b();
				d.onSort.call(g[0], g.data("wl_Multiselect").selected)
			},
			items: "li"
		});
		d && $.extend(g.data("wl_Multiselect"), d)
	})
};
$.fn.wl_Multiselect.defaults = {
	height: 200,
	items: [],
	selected: [],
	showUsed: !1,
	searchfield: !0,
	onAdd: function() {},
	onRemove: function() {},
	onSelect: function() {},
	onUnselect: function() {},
	onSort: function() {}
};
$.fn.wl_Multiselect.version = "1.3.1";
$.fn.wl_Multiselect.methods = {
	add: function(a, e) {
		var b = $(this),
		c = b.data("wl_Multiselect"),
		g = c.itemsum || 0,
		d = {};
		if ("object" != typeof a) d[a] = a;
		else if ($.isArray(a)) for (g = 0; g < a.length; g++) d[a[g]] = a[g];
		else d = a;
		$.each(d,
		function(a, d) {
			var k = $('<option value="' + a + '">' + d + "</option>").appendTo(b),
			k = $("<li><a>" + d + "</a></li>").append('<a class="add"></a>').data({
				pos: g++,
				"native": k,
				name: d,
				value: a
			}).appendTo(c.pool);
			b.data("wl_Multiselect").items[a] = k;
			e && b.wl_Multiselect("select", a)
		});
		c.onAdd.call(b[0], $.map(d,
		function(a) {
			return a
		}))
	},
	remove: function(a) {
		var e = $(this),
		b = e.data("wl_Multiselect");
		a && !$.isArray(a) && (a = [a]);
		e.wl_Multiselect("unselect", a);
		a && $.each(a,
		function(a, g) {
			var d = b.items[g];
			d.data("native").remove();
			d.remove();
			delete b.items[g];
			e.data("wl_Multiselect").items = b.items
		});
		e.trigger("change.wl_Multiselect");
		b.onRemove.call(e[0], a)
	},
	select: function(a) {
		var e = $(this),
		b = e.data("wl_Multiselect");
		a && !$.isArray(a) && (a = [a]);
		a && $.each(a,
		function(a, g) {
			var d = b.items[g];
			d.hasClass("used") || (d.removeClass("selected").clone(!0).attr("data-value", g).append('<a class="remove"></a>').appendTo(b.selection).find(".add").remove(), d.data("native").prop("selected", !0), d.addClass("used"), b.showUsed || d.addClass("hidden"))
		});
		e.trigger("change.wl_Multiselect");
		b.onSelect.call(e[0], a)
	},
	unselect: function(a) {
		var e = $(this),
		b = e.data("wl_Multiselect");
		a && !$.isArray(a) && (a = [a]);
		var c = b.selection.find("li");
		a && $.each(a,
		function(a, d) {
			var e = b.items[d];
			e.hasClass("used") && (e.data("native").prop("selected", !1), c.filter('[data-value="' + d + '"]').remove(), e.removeClass("used"), b.showUsed || e.removeClass("hidden"))
		});
		e.trigger("change.wl_Multiselect");
		b.onUnselect.call(e[0], a)
	},
	clear: function() {
		var a = $(this),
		e = a.data("wl_Multiselect");
		a.wl_Multiselect("unselect", e.selected);
		a.trigger("change.wl_Multiselect")
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Multiselect.defaults[a] ? b.data("wl_Multiselect")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Number = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Number.methods[a]) return $.fn.wl_Number.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Number") ? $.extend({},
		b.data("wl_Number"), a) : $.extend({},
		$.fn.wl_Number.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		b.data("wl_Number") || (b.data("wl_Number", {}), c.min = b.attr("min") || c.min, c.max = b.attr("max") || c.max, c.step = b.attr("step") || c.step, b.bind({
			"mousewheel.wl_Number": function(a, c) {
				var e = b.data("wl_Number") || e;
				e.mousewheel && (a.preventDefault(), c = 0 > c ? -1 : 1, a.shiftKey && (c *= 10), $.fn.wl_Number.methods.change.call(b[0], c))
			},
			"change.wl_Number": function() {
				var a = $(this);
				$.fn.wl_Number.methods.correct.call(b[0]);
				a.data("wl_Number").onChange.call(b[0], a.val())
			}
		}));
		c && $.extend(b.data("wl_Number"), c)
	})
};
$.fn.wl_Number.defaults = {
	step: 1,
	decimals: 0,
	start: 0,
	min: null,
	max: null,
	mousewheel: !0,
	onChange: function() {},
	onError: function() {}
};
$.fn.wl_Number.version = "1.0";
$.fn.wl_Number.methods = {
	correct: function() {
		var a = $(this).val().replace(/,/g, ".");
		a && $.fn.wl_Number.methods.printValue.call(this, parseFloat(a))
	},
	change: function(a) {
		var e = $(this),
		b = e.val() || e.data("wl_Number").start;
		a = parseFloat(b, 10) + a * e.data("wl_Number").step;
		$.fn.wl_Number.methods.printValue.call(this, a);
		e.trigger("change.wl_Number")
	},
	printValue: function(a) {
		var e = $(this),
		b = e.data("wl_Number") || b;
		isNaN(a) && "" != a ? (b.onError.call(this, e.val()), e.val(0).focus().select(), e.trigger("change.wl_Number")) : (null != b.min && (a = Math.max(b.min, a)), null != b.max && (a = Math.min(b.max, a)), b.decimals ? a = parseFloat(a, b.decimals).toFixed(b.decimals) : a = Math.round(a), e.val(a))
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Number.defaults[a] || null == $.fn.wl_Number.defaults[a] ? b.data("wl_Number")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Password = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if (!b.data("wl_Password") || !b.data("wl_Password").confirmfield) {
			if ($.fn.wl_Password.methods[a]) return $.fn.wl_Password.methods[a].apply(this, Array.prototype.slice.call(e, 1));
			if ("object" === typeof a || !a) var c = b.data("wl_Password") ? $.extend({},
			b.data("wl_Password"), a) : $.extend({},
			$.fn.wl_Password.defaults, a, b.data());
			else $.error('Method "' + a + '" does not exist');
			if (!b.data("wl_Password")) {
				b.data("wl_Password", {});
				var g = $("<div/>", {
					"class": "passwordstrength"
				}).appendTo(b.parent()).hide();
				if (c.confirm) {
					c.connect = this.id + "_confirm";
					var d = $(b.addClass("password").clone()).attr({
						id: c.connect,
						name: c.connect
					}).appendTo(b.parent()).removeAttr("required").hide();
					d.data("wl_Password", {
						confirmfield: !0
					})
				}
				b.bind({
					"focus.wl_Password": function() {
						b.trigger("keyup.wl_Password")
					},
					"blur.wl_Password": function() {
						var a = b.data("wl_Password") || a;
						b.val() ? a.confirm && !d.val() && g.text(a.text.confirm) : (g.hide(), a.confirm && d.hide())
					},
					"keyup.wl_Password": function() {
						var a = b.data("wl_Password") || a;
						if (b.val()) {
							a.confirm && d.show();
							var c;
							c = b.val();
							var e = 0;
							c.length < a.minLength ? c = e: (e = Math.min(15, e + 2 * c.length), c.match(/[a-z]/) && (e += 1), c.match(/[A-Z]/) && (e += 5), c.match(/\d+/) && (e += 5), c.match(/(.*[0-9].*[0-9].*[0-9])/) && (e += 7), c.match(/.[!,@,#,$,%,^,&,*,?,_,~]/) && (e += 5), c.match(/(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~])/) && (e += 7), c.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && (e += 2), c.match(/([a-zA-Z])/) && c.match(/([0-9])/) && (e += 3), c.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/) && (e += 3), c = Math.min(5, Math.ceil(e / 10)));
							a.showStrength && (g.show(), g.attr("class", "passwordstrength").addClass("s_" + c).text(a.words[c]));
							b.data("wl_Password").strength = c
						} else a.showStrength && g.hide(),
						a.confirm && d.val("").hide()
					}
				});
				c.confirm && c.showStrength && d.bind("keyup.wl_Password",
				function() {
					var a = b.data("wl_Password") || a;
					d.val() ? d.val() != b.val() ? g.text(a.text.nomatch) : g.text(a.words[b.data("wl_Password").strength]) : g.text(a.text.confirm)
				})
			}
			c && $.extend(b.data("wl_Password"), c)
		}
	})
};
$.fn.wl_Password.defaults = {
	confirm: !0,
	showStrength: !0,
	words: "too short;bad;medium;good;very good;excellent".split(";"),
	minLength: 3,
	text: {
		confirm: "please confirm",
		nomatch: "password doesn't match"
	}
};
$.fn.wl_Password.version = "1.0.1";
$.fn.wl_Password.methods = {
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Password.defaults[a] || null == $.fn.wl_Password.defaults[a] ? b.data("wl_Password")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Slider = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Slider.methods[a]) return $.fn.wl_Slider.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Slider") ? $.extend({},
		b.data("wl_Slider"), a) : $.extend({},
		$.fn.wl_Slider.defaults, a, b.data());
		else try {
			return b.slider(a, e[1], e[2])
		} catch(g) {
			$.error('Method "' + a + '" does not exist')
		}
		b.data("wl_Slider") ? b.unbind("slide slidechange").slider("destroy") : (b.data("wl_Slider", {}), b.bind("mousewheel.wl_Slider",
		function(a, d) {
			if (c.mousewheel) {
				d = 0 > d ? -1 : 1;
				a.preventDefault();
				if (!0 !== b.data("range")) {
					var g = b.slider("value");
					$.fn.wl_Slider.methods.values.call(b[0], g + d * c.step);
					if (c.tooltip) {
						var e = b.find("a");
						e.tipsy("setTitel", c.tooltipPattern.replace("%n", g + d * c.step));
						e.tipsy("update")
					}
				} else {
					var g = b.slider("values"),
					e = b.find("a"),
					j = e.eq(0).offset(),
					k = e.eq(1).offset();
					"horizontal" == c.orientation ? j.left + (k.left - j.left) / 2 > a.clientX ? $.fn.wl_Slider.methods.values.call(b[0], Math.min(g[0] + d * c.step, g[1]), g[1]) : $.fn.wl_Slider.methods.values.call(b[0], g[0], Math.max(g[1] + d * c.step, g[0])) : "vertical" == c.orientation && (k.top + (j.top - k.top) / 2 < a.pageY ? $.fn.wl_Slider.methods.values.call(b[0], Math.min(g[0] + d * c.step, g[1]), g[1]) : $.fn.wl_Slider.methods.values.call(b[0], g[0], Math.max(g[1] + d * c.step, g[0])));
					c.tooltip && (e.eq(0).tipsy("setTitel", c.tooltipPattern.replace("%n", g[0] + d * c.step)), e.eq(0).tipsy("update"), e.eq(1).tipsy("setTitel", c.tooltipPattern.replace("%n", g[0] + d * c.step)), e.eq(1).tipsy("update"))
				}
				$.fn.wl_Slider.methods.slide.call(b[0])
			}
		}));
		b.slider(c).bind("slide",
		function(a, c) {
			$.fn.wl_Slider.methods.slide.call(b[0], c.value)
		}).bind("slidechange",
		function(a, c) {
			$.fn.wl_Slider.methods.change.call(b[0], c.value)
		});
		if (c.connect) if (!0 !== b.data("range")) {
			var d = $("#" + c.connect),
			n = d.val() || b.data("value") || c.min;
			d.data("wl_Number") || d.wl_Number();
			b.unbind("slide slidechange").slider("value", n);
			b.bind("slide",
			function(a, c) {
				d.val(c.value);
				$.fn.wl_Slider.methods.slide.call(b[0], c.value)
			}).bind("slidechange",
			function(a, c) {
				d.val(c.value);
				$.fn.wl_Slider.methods.change.call(b[0], c.value)
			});
			d.val(n).wl_Number("set", $.extend({},
			c, {
				onChange: function(a) {
					b.slider("value", a);
					b.wl_Slider("slide");
					b.wl_Slider("change")
				}
			}))
		} else {
			var d = $.parseData(c.connect, !0),
			j = $("#" + d[0]),
			k = $("#" + d[1]),
			n = j.val() || b.data("values")[0] || c.min,
			t = k.val() || b.data("values")[1] || c.max;
			j.data("wl_Number") || j.wl_Number();
			k.data("wl_Number") || k.wl_Number();
			b.unbind("slide slidechange").slider("option", "values", [n, t]).bind("slide",
			function(a, c) {
				j.val(c.values[0]);
				k.val(c.values[1]);
				$.fn.wl_Slider.methods.slide.call(b[0], c.values)
			}).bind("slidechange",
			function(a, c) {
				j.val(c.values[0]);
				k.val(c.values[1]);
				$.fn.wl_Slider.methods.change.call(b[0], c.values)
			});
			j.wl_Number("set", $.extend({},
			c, {
				onChange: function(a) {
					b.slider("option", "values", [a, k.val()]);
					k.wl_Number("set", "min", parseFloat(a));
					b.wl_Slider("slide");
					b.wl_Slider("change")
				},
				min: c.min,
				max: k.val() || t
			})).val(n);
			k.wl_Number("set", $.extend({},
			c, {
				onChange: function(a) {
					b.slider("option", "values", [j.val(), a]);
					j.wl_Number("set", "max", parseFloat(a));
					b.wl_Slider("slide");
					b.wl_Slider("change")
				},
				min: j.val() || n,
				max: c.max
			})).val(t)
		}
		if (c.tooltip) {
			var n = b.find(".ui-slider-handle"),
			A,
			B = [];
			$.isArray(c.tooltipGravity) ? "horizontal" == c.orientation ? A = c.tooltipGravity[0] : "vertical" == c.orientation && (A = c.tooltipGravity[1]) : A = c.tooltipGravity;
			$.each(n,
			function(a, b) {
				B.push($(b));
				B[a].tipsy($.extend({},
				config.tooltip, {
					trigger: "manual",
					gravity: A,
					html: !0,
					fallback: c.tooltipPattern.replace("%n", (c.values ? c.values: c.value ? [c.value] : [0, 0])[a]),
					appendTo: B[a]
				}));
				B[a].tipsy("tip").find(".tipsy-inner").css("white-space", "nowrap");
				B[a].bind({
					"mouseenter.wl_Slider touchstart.wl_Slider": function() {
						B[a].tipsy("show")
					},
					"mouseleave.wl_Slider touchend.wl_Slider": function() {
						B[a].tipsy("hide")
					},
					"mouseenter touchstart": function() {
						B[a].data("mouseIsOver", !0)
					},
					"mouseleave touchend": function() {
						B[a].data("mouseIsOver", !1)
					}
				})
			});
			b.bind("slidestart",
			function(a, b) {
				$(b.handle).unbind("mouseleave.wl_Slider touchstart.wl_Slider mouseenter.wl_Slider touchend.wl_Slider")
			}).bind("slidestop",
			function(a, b) {
				var c = $(b.handle);
				c.bind({
					"mouseenter.wl_Slider touchstart.wl_Slider": function() {
						c.tipsy("show")
					},
					"mouseleave.wl_Slider touchend.wl_Slider": function() {
						c.tipsy("hide")
					}
				});
				c.data("mouseIsOver") || c.tipsy("hide")
			}).bind("slide",
			function(a, b) {
				var d = $(b.handle);
				d.tipsy("setTitel", c.tooltipPattern.replace("%n", b.value));
				d.tipsy("update")
			})
		}
		c.disabled && b.fn.wl_Slider.methods.disable.call(b[0]);
		c && $.extend(b.data("wl_Slider"), c)
	})
};
$.fn.wl_Slider.defaults = {
	min: 0,
	max: 100,
	step: 1,
	animate: !1,
	disabled: !1,
	orientation: "horizontal",
	range: !1,
	mousewheel: !0,
	connect: null,
	tooltip: !1,
	tooltipGravity: ["s", "w"],
	tooltipPattern: "%n",
	onSlide: function() {},
	onChange: function() {}
};
$.fn.wl_Slider.version = "1.1.1";
$.fn.wl_Slider.methods = {
	disable: function() {
		var a = $(this),
		e = a.data("wl_Slider");
		a.slider("disable");
		if (e.connect) if (!0 !== a.data("range")) $("#" + e.connect).prop("disabled", !0);
		else {
			var b = $.parseData(e.connect, !0),
			e = $("#" + b[0]),
			b = $("#" + b[1]);
			e.attr("disabled", !0);
			b.attr("disabled", !0)
		}
		a.data("wl_Slider").disabled = !0
	},
	enable: function() {
		var a = $(this),
		e = a.data("wl_Slider");
		a.slider("enable");
		if (a.data("wl_Slider").connect) if (!0 !== a.data("range")) $("#" + e.connect).prop("disabled", !1);
		else {
			var b = $.parseData(e.connect, !0),
			e = $("#" + b[0]),
			b = $("#" + b[1]);
			e.removeAttr("disabled");
			b.removeAttr("disabled")
		}
		a.data("wl_Slider").disabled = !1
	},
	change: function(a) {
		var e = $(this),
		b = e.data("wl_Slider"); ! 0 !== e.data("range") ? b.onChange.call(this, a || e.slider("value")) : b.onChange.call(this, a || e.slider("values"))
	},
	slide: function(a) {
		var e = $(this),
		b = e.data("wl_Slider"); ! 0 !== e.data("range") ? b.onSlide.call(this, a || e.slider("value")) : b.onSlide.call(this, a || e.slider("values"))
	},
	value: function(a) {
		var e = $(this); ! 0 !== e.data("wl_Slider").range && e.slider("value", a)
	},
	values: function(a, e) {
		var b = $(this); ! 0 === b.data("wl_Slider").range ? "object" === typeof a ? (b.slider("values", 0, a[0]), b.slider("values", 1, a[1])) : (b.slider("values", 0, a), b.slider("values", 1, e)) : $.fn.wl_Slider.methods.value.call(this, a)
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		if (b.data("wl_Slider").connect) if (!0 !== b.data("range")) var g = $("#" + b.data("wl_Slider").connect);
		else var d = $.parseData(b.data("wl_Slider").connect, !0),
		g = $("#" + d[0]),
		n = $("#" + d[1]);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Slider.defaults[a] || null == $.fn.wl_Slider.defaults[a] ? (b.slider("option", a, c).data("wl_Slider")[a] = c, g && g.data(a, c).trigger("change"), n && n.data(a, c).trigger("change")) : $.error('Key "' + a + '" is not defined')
		})
	}
};
$.wl_Store = function(a) {
	"object" != typeof $.jStorage && $.error("wl_Store requires the jStorage library");
	a = a || "wl_store";
	remove = function(e) {
		return $.jStorage.deleteKey(a + "_" + e)
	};
	flush = function() {
		return $.jStorage.flush()
	};
	index = function() {
		return $.jStorage.index()
	};
	return {
		save: function(e, b) {
			return $.jStorage.set(a + "_" + e, b)
		},
		get: function(e) {
			return $.jStorage.get(a + "_" + e)
		},
		remove: function(a) {
			return remove(a)
		},
		flush: function() {
			return flush()
		},
		index: function() {
			return index()
		}
	}
};
$.fn.wl_Time = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Time.methods[a]) return $.fn.wl_Time.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Time") ? $.extend({},
		b.data("wl_Time"), a) : $.extend({},
		$.fn.wl_Time.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		if (!b.data("wl_Time")) {
			b.data("wl_Time", {});
			b.bind({
				"mousewheel.wl_Time": function(a, c) {
					var d = b.data("wl_Time") || d;
					d.mousewheel && (a.preventDefault(), c = 0 > c ? -1 : 1, a.shiftKey && (c *= 60 / d.step), $.fn.wl_Time.methods.change.call(b[0], c))
				},
				"change.wl_Time": function() {
					var a = b.data("wl_Time") || a;
					$.fn.wl_Time.methods.correct.call(b[0]);
					$.fn.wl_Time.methods.printTime.call(b[0]);
					a.onChange.call(b[0], a.time)
				}
			}).after('<span class="timeformat"/>').attr("maxlength", 5);
			if (null !== c.connect) {
				var g = $("#" + c.connect),
				d = c.onDateChange;
				_callback = function(a) {
					var c = (new Date(g.datepicker("getDate"))).getTime();
					c && g.datepicker("setDate", new Date(c + 864E5 * a));
					d.call(b[0], a)
				};
				c.onDateChange = _callback
			}
			if (c.value) {
				var n = (new Date).getTime(),
				j;
				switch (c.value) {
				case "now":
					j = new Date(n);
					break;
				default:
					isNaN(c.value) || (j = new Date(n + 6E4 * (c.value % 60)))
				}
				c.time = $.leadingZero(j.getHours()) + ":" + $.leadingZero(j.getMinutes());
				b.val(c.time)
			}
		}
		c && $.extend(b.data("wl_Time"), c);
		b.val().match(/\d\d:\d\d/) && (b.data("wl_Time").time = b.val(), $.fn.wl_Time.methods.printTime.call(b[0]))
	})
};
$.fn.wl_Time.defaults = {
	step: 5,
	timeformat: 24,
	roundtime: !0,
	time: null,
	value: null,
	mousewheel: !0,
	onDateChange: function() {},
	onHourChange: function() {},
	onChange: function() {}
};
$.fn.wl_Time.version = "1.1";
$.fn.wl_Time.methods = {
	change: function(a) {
		var e = $(this),
		b = e.data("wl_Time"),
		c = new Date("2010/01/01 " + (e.data("wl_Time").time || "00:00")),
		g = new Date(c.getTime() + 6E4 * a * e.data("wl_Time").step);
		a = g.getHours();
		g = g.getMinutes();
		b.roundtime && (g -= g % e.data("wl_Time").step);
		e.data("wl_Time").time = $.leadingZero(a) + ":" + $.leadingZero(g);
		$.fn.wl_Time.methods.printTime.call(this);
		b.onChange.call(this, e.data("wl_Time").time);
		Math.abs(c.getMinutes() - g) == 60 - b.step && b.onHourChange.call(this, a - c.getHours());
		23 == Math.abs(c.getHours() - a) && b.onDateChange.call(this, a ? -1 : 1)
	},
	printTime: function() {
		var a = $(this),
		e = a.data("wl_Time"),
		b = e.time;
		b && (b = b.split(":"), 12 == e.timeformat ? a.val($.leadingZero(0 == b[0] % 12 ? 12 : b[0] % 12) + ":" + $.leadingZero(b[1])).next().html(1 <= b[0] / 12 ? "pm": "am") : a.val($.leadingZero(b[0]) + ":" + $.leadingZero(b[1])))
	},
	correct: function() {
		var a = $(this),
		e = a.val(),
		b;
		"" != e && (/^\d+:\d+$/.test(e) || (1 == e.length ? e = "0" + e + ":00": 2 == e.length ? e += ":00": 3 == e.length ? e = e.substr(0, 2) + ":" + e.substr(2, 3) + "0": 4 == e.length && (e = e.substr(0, 2) + ":" + e.substr(2, 4))), b = e.split(":"), !/\d\d:\d\d$/.test(e) && "" != e || 23 < b[0] || 59 < b[1] ? (a.val("00:00").focus().select(), a.data("wl_Time").time = "00:00") : a.data("wl_Time").time = e, $.fn.wl_Time.methods.printTime.call(this))
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Time.defaults[a] || null == $.fn.wl_Time.defaults[a] ? (b.data("wl_Time")[a] = c, b.trigger("change.wl_Time")) : $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Valid = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Valid.methods[a]) return $.fn.wl_Valid.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Valid") ? $.extend({},
		b.data("wl_Valid"), a) : $.extend({},
		$.fn.wl_Valid.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		"string" === typeof c.regex && (c.regex = RegExp(c.regex));
		b.data("wl_Valid") || (b.data("wl_Valid", {}), b.bind({
			"change.wl_Valid": function() {
				var a = b.data("wl_Valid") || a;
				$.fn.wl_Valid.methods.validate.call(b[0]);
				a.onChange.call(b[0], b, b.val());
				a.valid || a.onError.call(b[0], b, b.val())
			},
			"keyup.wl_Valid": function() {
				var a = b.data("wl_Valid") || a;
				a.instant && b.val().length >= b.data("wl_Valid").minLength && b.wl_Valid("validate")
			}
		}));
		c && $.extend(b.data("wl_Valid"), c);
		$.fn.wl_Valid.methods.validate.call(b)
	})
};
$.fn.wl_Valid.defaults = {
	errorClass: "error",
	instant: !0,
	regex: /.*/,
	minLength: 0,
	onChange: function() {},
	onError: function() {}
};
$.fn.wl_Valid.version = "1.0";
$.fn.wl_Valid.methods = {
	validate: function() {
		var a = $(this),
		e = a.data("wl_Valid") || e,
		b = a.val();
		e.valid = !b || e.regex.test(b);
		e.valid || b == a.attr("placeholder") ? a.removeClass(e.errorClass) : a.addClass(e.errorClass)
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			if (void 0 !== $.fn.wl_Valid.defaults[a] || null == $.fn.wl_Valid.defaults[a]) {
				switch (a) {
				case "regex":
					c = RegExp(c)
				}
				b.data("wl_Valid")[a] = c
			} else $.error('Key "' + a + '" is not defined')
		})
	}
};
$.fn.wl_Mail = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Valid.methods[a]) return $.fn.wl_Valid.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Valid") ? $.extend({},
		b.data("wl_Valid"), a) : $.extend({},
		$.fn.wl_Mail.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		b.wl_Valid(c);
		c && $.extend(b.data("wl_Valid"), c)
	})
};
$.fn.wl_Mail.defaults = {
	regex: /^([\w-]+(?:\.[\w-]+)*)\@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$|(\[?(\d{1,3}\.){3}\d{1,3}\]?)$/i,
	onChange: function(a, e) {
		a.val(e.toLowerCase())
	}
};
$.fn.wl_URL = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Valid.methods[a]) return $.fn.wl_Valid.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a) var c = b.data("wl_Valid") ? $.extend({},
		b.data("wl_Valid"), a) : $.extend({},
		$.fn.wl_URL.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		b.wl_Valid(c);
		c && $.extend(b.data("wl_Valid"), c)
	})
};
$.fn.wl_URL.defaults = {
	regex: /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w]))*\.+(([\w#!:.?+=&%@!\-\/]))?/,
	instant: !1,
	onChange: function(a, e) {
		"" != e && !/^(ftp|http|https):\/\//.test(e) && a.val("http://" + e).trigger("change.wl_Valid")
	}
};
$.fn.wl_Widget = function(a) {
	var e = arguments;
	return this.each(function() {
		var b = $(this);
		if ($.fn.wl_Widget.methods[a]) return $.fn.wl_Widget.methods[a].apply(this, Array.prototype.slice.call(e, 1));
		if ("object" === typeof a || !a)
			var c = b.data("wl_Widget") ? $.extend({}, b.data("wl_Widget"), a) : $.extend({}, $.fn.wl_Widget.defaults, a, b.data());
		else $.error('Method "' + a + '" does not exist');
		if (!b.data("wl_Widget")) {
			b.data("wl_Widget", {});
			b.find("div.widget").each(function() {
				var a = $(this),
					b = $.extend({},
					c, a.data()),
					d = a.find("h3.handle"),
					g = a.find("div").eq(0);
				a.parent();
				a.data("wl_Widget", {});
				b.icon && (d.addClass("icon"), $("<a>", {
					"class": "icon i_" + b.icon
				}).appendTo(d));
				b.sortable && a.addClass("sortable");
				b.collapsible && ($("<a>", {
					"class": "collapse",
					title: b.text.collapse
				}).appendTo(d), b.collapsed && (g.hide(), a.addClass("collapsed"), d.find("a.collapse").attr("title", b.text.expand)), d.delegate("a.collapse", "click.wl_Widget touchstart.wl_Widget",
				function() {
					var b = a.data("wl_Widget") || b,
					c = a.find("div").eq(0);
					c.is(":hidden") ? c.slideDown(100,
					function() {
						a.removeClass("collapsed").data("wl_Widget").collapsed = !1;
						d.find("a.collapse").attr("title", b.text.collapse);
						b.onExpand.call(a[0]);
						$.fn.wl_Widget.methods.save();
						$(window).resize()
					}) : g.slideUp(100,
					function() {
						a.addClass("collapsed").data("wl_Widget").collapsed = !0;
						d.find("a.collapse").attr("title", b.text.expand);
						b.onCollapse.call(a[0]);
						$.fn.wl_Widget.methods.save()
					});
					return ! 1
				}).bind("dblclick",
				function() {
					d.find("a.collapse").trigger("click");
					return ! 1
				}));
				d.delegate("a.reload", "click.wl_Widget touchstart.wl_Widget",
				function() {
					var b = a.data("wl_Widget") || b,
					c = a.find("div").eq(0);
					a.addClass("loading");
					c.height(g.height());
					b.removeContent && c.html(b.text.loading);
					c.load(b.load,
					function(g, e, j) {
						a.removeClass("loading");
						//c.height("auto");
						"error" == e && c.html(j.status + " " + j.statusText);
						b.reload && (clearTimeout(a.data("wl_Widget").timeout), a.data("wl_Widget").timeout = setTimeout(function() {
							d.find("a.reload").trigger("click.wl_Widget")
						},
						1E3 * b.reload))
					});
					return ! 1
				});
				d.delegate("a", "click.wl_Widget",
				function(a) {
					a.stopPropagation();
					return ! 1
				});
				b && $.extend(a.data("wl_Widget"), b);
				b.load && $("<a>", {
					"class": "reload",
					title: b.text.reload
				}).appendTo(d).trigger("click.wl_Widget")
			});
			var g = $("#content");
			g.data("wl_Widget") || g.data("wl_Widget", {
				containercount: $("div.widgets").length,
				currentid: 1
			});
			if (g.data("wl_Widget").currentid++>=g.data("wl_Widget").containercount) {
				var g = $("div.widgets"),
				d = new $.wl_Store("wl_" + location.pathname.toString());
				g.each(function(a) {
					a = d.get("widgets_" + a);
					var b = $(this);
					if (!a) return ! 1;
					$.each(a,
					function(a, c) {
						var d = $("#" + a);
						c.collapsed && d.data("wl_Widget").collapsible ? d.addClass("collapsed").find("div").eq(0).hide().data("wl_Widget", {
							collapsed: !0
						}) : d.removeClass("collapsed").find("div").eq(0).show().data("wl_Widget", {
							collapsed: !1
						});
						if (d.length && (d.prevAll("div").length != c.position || d.parent()[0] !== b[0])) children = b.children("div.widget"),
						children.eq(c.position).length ? d.insertBefore(children.eq(c.position)) : children.length ? d.insertAfter(children.eq(c.position - 1)) : d.appendTo(b)
					})
				});
				g.sortable({
					items: g.find("div.widget.sortable"),
					containment: "#content",
					opacity: 0.8,
					distance: 5,
					handle: "h3.handle",
					connectWith: g,
					forceHelperSize: !0,
					placeholder: "sortable_placeholder",
					forcePlaceholderSize: !0,
					zIndex: 1E4,
					start: function(a, b) {
						b.item.data("wl_Widget").onDrag.call(b.item[0])
					},
					stop: function(a, b) {
						b.item.data("wl_Widget").onDrop.call(b.item[0]);
						$.fn.wl_Widget.methods.save()
					}
				})
			}
		}
		c && $.extend(b.data("wl_Widget"), c)
	})
};
$.fn.wl_Widget.defaults = {
	collapsed: !1,
	load: null,
	reload: !1,
	removeContent: !0,
	collapsible: !0,
	sortable: !0,
	text: {
		loading: "loading...",
		reload: "reload",
		collapse: "collapse widget",
		expand: "expand widget"
	},
	onDrag: function() {},
	onDrop: function() {},
	onExpand: function() {},
	onCollapse: function() {}
};
$.fn.wl_Widget.version = "1.2";
$.fn.wl_Widget.methods = {
	save: function() {
		var a = $("div.widgets"),
		e = new $.wl_Store("wl_" + location.pathname.toString());
		a.each(function(a) {
			var c = {};
			$(this).find("div.widget").each(function(a) {
				var b = $(this);
				c[this.id] = {
					position: a,
					collapsed: b.find("div").eq(0).is(":hidden")
				}
			});
			e.save("widgets_" + a, c)
		})
	},
	set: function(a, e) {
		var b = $(this),
		c = {};
		"object" === typeof a ? c = a: a && void 0 !== e && (c[a] = e);
		$.each(c,
		function(a, c) {
			void 0 !== $.fn.wl_Widget.defaults[a] || null == $.fn.wl_Widget.defaults[a] ? b.data("wl_Widget")[a] = c: $.error('Key "' + a + '" is not defined')
		})
	}
};