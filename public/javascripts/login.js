/*
 * Copyright (c) 2013 
 * ==================================
 * 
 *
 * This is part of an item on themeforest
 * You can check out the screenshots and purchase it on themeforest:
 * http://rxa.li/whitelabel
 * 
 * 
 * ===========================================
 * original filename: login.js
 * filesize: 597 Bytes
 * last modified: Fri, 17 May 2013 15:07:57 +0200
 *
 */
$(document).ready(function(){var c=$("body"),a=$("#content").find("#loginform");$.browser.msie||c.fadeTo(0,0).delay(500).fadeTo(1E3,1);$("input").uniform();a.wl_Form({status:!1,onBeforeSubmit:function(b){a.wl_Form("set","sent",!1);b.username||b.password?location.href="dashboard.html":$.wl_Alert("Please provide something!","info","#content");return!1}})});
